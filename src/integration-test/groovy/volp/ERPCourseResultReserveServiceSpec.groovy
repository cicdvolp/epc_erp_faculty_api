package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPCourseResultReserveServiceSpec extends Specification {

    ERPCourseResultReserveService ERPCourseResultReserveService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPCourseResultReserve(...).save(flush: true, failOnError: true)
        //new ERPCourseResultReserve(...).save(flush: true, failOnError: true)
        //ERPCourseResultReserve ERPCourseResultReserve = new ERPCourseResultReserve(...).save(flush: true, failOnError: true)
        //new ERPCourseResultReserve(...).save(flush: true, failOnError: true)
        //new ERPCourseResultReserve(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPCourseResultReserve.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPCourseResultReserveService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPCourseResultReserve> ERPCourseResultReserveList = ERPCourseResultReserveService.list(max: 2, offset: 2)

        then:
        ERPCourseResultReserveList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPCourseResultReserveService.count() == 5
    }

    void "test delete"() {
        Long ERPCourseResultReserveId = setupData()

        expect:
        ERPCourseResultReserveService.count() == 5

        when:
        ERPCourseResultReserveService.delete(ERPCourseResultReserveId)
        sessionFactory.currentSession.flush()

        then:
        ERPCourseResultReserveService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPCourseResultReserve ERPCourseResultReserve = new ERPCourseResultReserve()
        ERPCourseResultReserveService.save(ERPCourseResultReserve)

        then:
        ERPCourseResultReserve.id != null
    }
}
