<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="description" content="Responsive Admin Template" />
        <meta name="author" content="RedstarHospital" />
        <title>EduPlusCampus</title>
        <!-- google font -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- icons -->
        <asset:stylesheet src="smart/fonts/font-awesome/css/font-awesome.min.css" />
        <asset:stylesheet src="smart/plugins/iconic/css/material-design-iconic-font.min.css" />
        <!-- bootstrap -->
        <asset:stylesheet src="smart/plugins/bootstrap/css/bootstrap.min.css" />
        <!-- style -->
        <asset:stylesheet src="smart/pages/extra_pages.css" />

    </head>
    <body>
        <div class="limiter">
            <div class="container-login100 page-background">
                <div class="wrap-login100">
                    <g:form url="[action:'savepasswordreset']" onsubmit="return checkpwd()">
                        <!-- <span class="login100-form-title  p-t-27">
                            Forgot Your Password?
                        </span> -->
                        <p class="text-center txt-small-heading">
                            Reset Password
                        </p>
                        <div class="wrap-input100 validate-input" data-validate="Enter username">
                            <input class="input100" id="email" value="${email}"  type="email" name="email" required autocomplete="off" placeholder="Your Email" >
                            <span class="focus-input100" data-placeholder="&#xf207;"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <input class="input100" id="pwd" type="password" name="pwd" required autocomplete="off" placeholder="New Password" >
                            <span class="focus-input100" data-placeholder="&#xf191;"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <input class="input100" id="confirmpassword" name="confirmpassword" type="password" name="confirmpassword" required autocomplete="off" placeholder="Confirm Password" >
                            <span class="focus-input100" data-placeholder="&#xf191;"></span>
                        </div>
                        <div class="container-login100-form-btn">
                            <span class="confirmmsg" id="msg"></span>
                            <input class="login100-form-btn" type="submit" value="Change Password">
                        </div>
                    </g:form>
                </div>
            </div>
        </div>
        <!-- start js include path -->
        <asset:javascript src="smart/plugins/jquery/jquery.min.js"/>
        <!-- bootstrap -->
        <asset:javascript src="smart/plugins/bootstrap/js/bootstrap.min.js"/>
        <asset:javascript src="smart/pages/extra-pages/pages.js"/>
        <!-- end js include path -->
        <script>
            function checkpwd(){
                if(document.getElementById("pwd").value==document.getElementById("confirmpassword").value){
                    return true;
                } else {
                    document.getElementById("msg").innerHTML = "Password not matched";
                    return false;
                }
            }
        </script>
    </body>
</html>