<!doctype html>
<html lang="en" class="no-js">
    <head>
        <g:render template="/layouts/smart_template_inst/meta" />
        <g:render template="/layouts/smart_template_inst/css" />
         <link rel="shortcut icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="${createLinkTo(dir:'images',file:'epn144.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="${createLinkTo(dir:'images',file:'img/logos/apple-touch-icon-72x72.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="${createLinkTo(dir:'images',file:'apple-touch-icon-114x114.png')}" />
        <title>

            <g:layoutTitle default="EduPlusCampus"/>
        </title>
        <g:layoutHead/>
    </head>
    <body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-blue indigo-sidebar-color logo-blue">
        <div class="page-wrapper">
            <g:render template="/layouts/smart_template_inst/header" />
            <g:render template="/layouts/smart_template_inst/quicklinks" />
            <div class="page-container">
                <g:render template="/layouts/smart_template_inst/navsidebar" />
                <g:layoutBody/>
            </div>
            <g:render template="/layouts/smart_template_inst/footer" />
        </div>
        <g:render template="/layouts/smart_template_inst/js" />
        <div id="smartprogressbar" style="display:none; position:fixed; top: 50%; left: 35%; padding:20px; background:#e5e5e9;">
            <span style="color:blue; font-size:14px; font-weight:700; padding-bottom:5px;">Please Wait...</span>
            <div class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
        </div>
    </body>
</html>
