<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">${session?.linkname}</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>
                &nbsp;
                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>
                &nbsp;
                |
                &nbsp;
                <a class="parent-item" onclick="goBack()">Back</a>
                &nbsp;
                <!--
                <i class="fa fa-angle-right"></i>
                -->
            </li>
            <!--
            <li>
                ${session?.roletype}
                &nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                ${session?.role}
                &nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">${session?.linkname}</li>
            -->
        </ol>
    </div>
</div>
<script>
    function goBack() {
        window.history.back();
    }
</script>