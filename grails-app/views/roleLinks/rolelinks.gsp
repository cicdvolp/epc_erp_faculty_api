<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst_scroll"/>
        <meta name="author" content="pratik"/>
        <asset:javascript src="jquery.min.js"/>
        <script>
            function addrolevalidate() {
                var role_type = document.forms["addRoleForm"]["roletype"].value;
                 if (role_type == "null"  ) {
                    alert("Please select Role Type.");
                    return false;
                }
                  var user_type = document.forms["addRoleForm"]["usertype"].value;
                if (user_type == "null"  ) {
                    alert("Please select User Type.");
                    return false;
                }
            }
            function validate() {
                var role_type = document.forms["myForm"]["roletype"].value;
                 if (role_type == "null"  ) {
                    alert("Please select Role Type.");
                    return false;
                }
                  var user_type = document.forms["myForm"]["usertype"].value;
                if (user_type == "null"  ) {
                    alert("Please select User Type.");
                    return false;
                }
                var role = document.forms["myForm"]["roleList"].value;
                if (role == "null"  ) {
                    alert("Please select Role.");
                    return false;
                }
            }
           /* function roleValidate(){
                var role = document.getElementById("roleList").value;
                if(role> 0){
                    document.getElementById("errorroleid").innerHTML = '';
                } else {
                    document.getElementById("errorroleid").innerHTML = 'Please Select Role..';
                }
            }*/
            function usertypeValidate(){
                var user_type = document.getElementById("usertype").value;
                /*alert("user_type : " + user_type);*/
                if(user_type> 0){
                    document.getElementById("errorusertypeid").innerHTML = '';
                } else {
                    document.getElementById("errorusertypeid").innerHTML = 'Please Select User Type..';
                }
            }
           /* function roletypeValidate(){
                var role_type = document.getElementById("roletype").value;
                if(role_type> 0){
                    document.getElementById("errorroletypeid").innerHTML = '';
                } else {
                    document.getElementById("errorroletypeid").innerHTML = 'Please Select Role Type..';
                }
            }*/
            function callme() {
                var user_type = document.getElementById("usertype").value;
                var role_type = document.getElementById("roletype").value;
                /*alert("role_type : " + role_type + ", user_type : " + user_type);*/
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("roledivid").innerHTML = this.responseText;
                        getRoleLinksByRole();
                    }
                };
                xmlhttp.open("GET", "${request.contextPath}/RoleLinks/getRoleByRoleTypeAndUserType?usertype=" + user_type + "&roletype=" + role_type);
                xmlhttp.send();
           }

           setTimeout(function(){
                //alert('test - ' + ${isupdaterolelink});
                if(${isupdaterolelink}) {
                } else {
                    callme();
                }
           },1500);
            function getRoleLinksByRole() {
                var role = document.getElementById("roleList").value;
                /*alert("role_type : " + role_type);*/
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("filterdivtableid").innerHTML = this.responseText;
                      /*  $('#exportTable').DataTable( {
                            dom: 'Bfrtip',
                            buttons: [
                                'copyHtml5',
                                'excelHtml5',
                                'csvHtml5',
                                'pdfHtml5'
                            ],
                            paging: false
                        } );*/
                    }
                };
                xmlhttp.open("GET", "${request.contextPath}/RoleLinks/getRoleLinksByRoleType?role="+ role);
                xmlhttp.send();
            }
        </script>
        <style>
            p.field-wrapper input {
                float: right;
            }
            p.required-field label::after {
                content: "*";
                color: red;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header">Role Link Form</header>
                                </div>
                                <div class="card-body">
                                    <g:form action='addRoleLink' , controller='RoleLinksController' params="[roleLinkId:roleLink?.id]"  name="myForm">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Link Name<span class="erp-mandatory-mark">*</span> : </label>
                                                <input class="form-control" type="text" name="linkName" value="${roleLink?.link_name}" required="true"  />
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Link Display Name<span class="erp-mandatory-mark">*</span> : </label>
                                                <input class="form-control" type="text" name="link_displayname" value="${roleLink?.link_displayname}" required="true"  />
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Controller Name<span class="erp-mandatory-mark">*</span> : </label>
                                                <input class="form-control" type="text" name="controllerName" value="${roleLink?.controller_name}" required="true"  />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Action Name<span class="erp-mandatory-mark">*</span> : </label>
                                                <input class="form-control" type="text" name="actionName" value="${roleLink?.action_name}" required="true"  />
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Vue JS Redirect Name : </label>
                                                <input class="form-control" type="text" name="vue_js_name" value="${roleLink?.vue_js_name}"  />
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Sort Order<span class="erp-mandatory-mark">*</span> : </label>
                                                <input class="form-control" type="number" name="sortOrder" value="${roleLink?.sort_order}" required="true"  />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Role Type<span class="erp-mandatory-mark">*</span> : </label>
                                                <g:select class="form-control required" name='roleType' value="${defaultroletype?.id}" from='${roleTypeList}'  onchange="callme();" optionKey="id" optionValue="type_displayname" id="roletype" required="true"></g:select>
                                                <!--<g:if test="${defaultroletype == null}">
                                                    <p style="color:red; display: inline;" id="errorroletypeid">Please Select Role Type..</p>
                                                </g:if>
                                                <g:else>
                                                    <p style="color:red;" id="errorroletypeid"></p>
                                                </g:else> -->
                                            </div>
                                            <div class="col-sm-4">
                                                <label>User Type<span class="erp-mandatory-mark">*</span> : </label>
                                                <g:select class="form-control" name='userType' id="usertype" onchange="callme();usertypeValidate();" value="${defaultuserType?.id}"  from='${userTypeList}' optionKey="id" required="true"></g:select>
                                                <!-- <g:if test="${defaultuserType == null}">
                                                    <p style="color:red;" id="errorusertypeid">Please Select User Type..</p>
                                                </g:if>
                                                <g:else>
                                                    <p style="color:red;" id="errorusertypeid"></p>
                                                </g:else> -->
                                            </div>
                                            <div class="col-sm-4" id="roledivid">
                                               <label>Role<span class="erp-mandatory-mark">*</span> : </label>
                                                 <g:link controller="ERPRoomType" action="addRole">
                                                    <i class="fa fa-plus-square" aria-hidden="true" onclick="document.getElementById('addRoleModal').style.display='block'" style="color:blue; float:right; padding-top:9px;" name="addsoftwaremodel" ></i>
                                                 </g:link>
                                                 <g:select class="form-control" name='role' id="roleList" value="${defaultrole?.id}" onchange="getRoleLinksByRole();" from='${roleList}' optionValue="role_displayname"  optionKey="id" required="true"></g:select>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <label>Is Quick Link<span class="erp-mandatory-mark">*</span> : </label>
                                            </div>
                                            <div class="col-sm-2">
                                                <g:if test="${roleLink?.isquicklink}">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <input type="radio" name="quickLink" value="true" checked="true">Yes<br>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="radio" name="quickLink" value="false">No<br>
                                                        </div>
                                                    </div>
                                                </g:if>
                                                <g:else>
                                                    <g:if test="${!roleLink?.isquicklink}">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <input type="radio" name="quickLink" value="true">Yes<br>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="radio" name="quickLink" value="false" checked="true">No<br>
                                                            </div>
                                                        </div>
                                                    </g:if>
                                                    <g:else>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <input type="radio" name="quickLink" value="true">Yes<br>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="radio" name="quickLink" value="false" checked="true">No<br>
                                                            </div>
                                                        </div>
                                                    </g:else>
                                                </g:else>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <center>
                                                    <button onclick="validate();" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary" type="submit" id="addRoleLinkBtn">SAVE</button>

                                                </center>
                                            </div>
                                        </div>
                                    </g:form>
                                   <!-- <g:link controller="roleLinks" action="rolelinkslistAll">
                                      <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" style="float: right;">View Role Links List All</button>

                                    </g:link>-->

                                   <g:submitToRemote update="filterdivtableid" url="[action: 'rolelinkslistAll', controller:'roleLinks']" value="View Role Links List All" class="btn btn-primary" before="document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';" />
                                   <!-- <g:form action='getRoleLinksByRole' , controller='RoleLinksController' params="[roleLinkId:roleLink?.id]">
                                       <center>
                                             <button onclick="validate();" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary" type="submit">Role Link list</button>
                                       </center>
                                    </g:form>-->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="filterdivtableid">
                        <div class="col-md-12">
                            <div class="card card-topline-purple" style="width:fit-content;">
                                <div class="card-head">
                                    <header class="erp-table-header">Role Links List</header>
                                </div>
                                <!--<div class="card-body">-->
                                    <div class="table-responsive" id="bar-parent">
                                        <table id="exportTable" class="display nowrap erp-full-width mdl-data-table ml-table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Role Type</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Role</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Vue JS Redirect Name</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Sort Order</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Link Name</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Link Display Name</th>
                                                    <th class="mdl-data-table__cell--non-numeric">User Type</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Active</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Actions</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is Quick Link</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is Icon Set?</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <g:each var="i" in="${roleLinksList}" status="k">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${count=count+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${i.role?.roletype?.type}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${i.role?.role}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${i?.vue_js_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${i.sort_order}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:link action="${i?.action_name}" controller="${i?.controller_name}" id="rediredrolelink" style="color:blue;">
                                                                ${i?.link_name}
                                                            </g:link>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:link action="${i?.action_name}" controller="${i?.controller_name}" id="rediredrolelink" style="color:blue;">
                                                                ${i?.link_displayname}
                                                            </g:link>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">${i.role?.usertype?.type}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:if test="${i.isrolelinkactive}">
                                                                <g:link action="activeRoleLinks" onclick="return confirm('Are you sure you want to In-Active?')" params="[roleLinkId:i.id]" >
                                                                    <center><i class="fa fa-toggle-on fa-2x" style="color:Green" aria-hidden="true"></i></center>
                                                                </g:link>
                                                            </g:if>
                                                            <g:else>
                                                                <g:link action="activeRoleLinks" onclick="return confirm('Are you sure you want to Active?')" params="[roleLinkId:i.id]">
                                                                    <center><i class="fa fa-toggle-off fa-2x" style="color:Red" aria-hidden="true"></i></center>
                                                                </g:link>
                                                            </g:else>
                                                            <% /* <center><input type="checkbox" checked data-toggle="toggle" data-on="Active" data-off="In-Active" data-onstyle="success" data-offstyle="danger"></center>*/ %>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:link action="rolelinks" params="[roleLinkId:i.id, roletype:i?.role?.roletype?.id, usertype:i?.role?.usertype?.id, role:i.role?.id, roleLinksList:i, isEdit:true]" id="editRoleLink">
                                                                <i class="fa fa-pencil-square-o fa-2x" style="color:green" name="edits" title="Edit Role Link"></i>
                                                            </g:link>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <g:link action="rolelinks" params="[roleLinkId:i.id, roletype:i?.role?.roletype?.id, usertype:i?.role?.usertype?.id, role:i.role?.id, copy:'true', roleLinksList:i]" id="copyRoleLink">
                                                                <i class="fa fa-copy fa-2x" style="color:black" name="copy" title="Copy Role Link"></i>
                                                            </g:link>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <g:link action="deleteRoleLinks" params="[roleLinkId:i.id]">
                                                                <i class="fa fa-trash fa-2x" style="color:red" name="delete" title="Delete Role Link"></i>
                                                            </g:link>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:if test="${i.isquicklink}">
                                                                <g:link action="setQuickLinks" onclick="return confirm('Are you sure you want to set as not Quick Link?')" params="[roleLinkId:i.id]" >
                                                                    <center><i class="fa fa-check fa-2x" style="color:Green" aria-hidden="true"></i></center>
                                                                </g:link>
                                                            </g:if>
                                                            <g:else>
                                                                <g:link action="setQuickLinks" onclick="return confirm('Are you sure you want to set as Quick Link?')" params="[roleLinkId:i.id]">
                                                                    <center><i class="fa fa-close fa-2x" style="color:Red" aria-hidden="true"></i></center>
                                                                </g:link>
                                                            </g:else>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:if test="${i?.linkiconimagepath}">
                                                                <g:if test="${i?.linkiconimagefilename}">
                                                                    <center><i class="fa fa-check fa-2x" style="color:Green" aria-hidden="true"></i></center>
                                                                </g:if>
                                                                <g:else>
                                                                    <center><i class="fa fa-close fa-2x" style="color:Red" aria-hidden="true"></i></center>
                                                                </g:else>
                                                            </g:if>
                                                            <g:else>
                                                                <center><i class="fa fa-close fa-2x" style="color:Red" aria-hidden="true"></i></center>
                                                            </g:else>
                                                        </td>
                                                    </tr>
                                                </g:each>
                                            </tbody>
                                        </table>
                                    </div>
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                <!-- end content here -->
            </div>
        </div>
        <!-- end page content -->
    </body>
</html>