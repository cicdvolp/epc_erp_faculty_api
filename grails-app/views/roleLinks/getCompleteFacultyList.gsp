<g:if test="${flash.message}">
	<center><h4 style="color: red;"><div class="message" role="status">${flash.message}</div></h4></center>
</g:if>
<div id="newtable">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-topline-purple">
                        <div class="card-head">
                            <header class="erp-table-header">Faculty List</header>
                        </div>
                        <!--<div class="card-body">-->
                        <g:form action="saveRolesofFaculties">
                            <g:hiddenField name="roletypefaculty" value="${roleType?.id}" />
                            <g:hiddenField name="roleidfaculty" value="${role?.id}" />
                            <!-- <g:hiddenField name="departmentType" value="${depttypeid}" /> -->
                            <g:hiddenField name="departmentList" value="${deptid}" />
                            <g:hiddenField name="empType" value="${emptypeid}" />
                            <div class="table-responsive" id="bar-parent">
                                <table id="common" class="table table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                            <th>
                                               <g:checkBox style="width: 20px;height: 20px;" name="myCheckbox" value="${true}" id="headerCheckbox1" />
                                               Select All
                                            </th>
                                            <th class="mdl-data-table__cell--non-numeric">Employee Code</th>
                                            <th class="mdl-data-table__cell--non-numeric">Name</th>
                                            <th class="mdl-data-table__cell--non-numeric">Department</th>
                                            <th class="mdl-data-table__cell--non-numeric">Employee Type</th>
                                            <th class="mdl-data-table__cell--non-numeric">Role Type</th>
                                            <th class="mdl-data-table__cell--non-numeric">Role</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <g:each in="${instructorList1}" var="inst" status="i">
                                            <tr>
                                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                <td>
                                                  <center><input  style="width: 20px;height: 20px;" type="checkbox" name="checkedValue" class="columnCheckbox1" value="${inst?.id}" checked/></center>
                                                </td>
                                                <td class="mdl-data-table__cell--non-numeric">${inst.employee_code}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${inst.person.fullname_as_per_previous_marksheet}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${inst.department}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${inst.employeetype}</td>
                                                <g:each in="${roleType}" var="rlt" status="j">
                                                    <td class="mdl-data-table__cell--non-numeric">${rlt}</td>
                                                </g:each>
                                                <g:each in="${role}" var="rl" status="k">
                                                    <td class="mdl-data-table__cell--non-numeric">${rl}</td>
                                                </g:each>
                                            </tr>
                                        </g:each>
                                    </tbody>
                                </table>
                            </div>
                            <br><br>
                            <div class="form-row col-sm-12">
                                <div class="form-row col-sm-12">
                                    <div class="col-sm-6">
                                        <div style="float:right;">
                                            <g:submitToRemote url="[action: 'saveRolesofFaculties', controller:'RoleLinks']" update="updateMessage" value="Assign Role" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary" before="if(!confirm('Are You sure!')) return true;document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </g:form>
                        <div id="updateMessage"></div>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<g:render template="/layouts/smart_template_inst/js_datatable" />

<script>
     $(document).ready(function(){
           $('#headerCheckbox1').click(function() {
                var checked = this.checked;
                $('.columnCheckbox1').prop('checked', checked);
           });
     });
</script>