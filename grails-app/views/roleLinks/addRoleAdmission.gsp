 <head>
     <script language="JavaScript">
        $(".selectall").click(function(){
        $(".individual").prop("checked",$(this).prop("checked"));
        });
     </script>
 </head>

 <g:form action="assignRoleAdmission">
    <input type="hidden" id="instructorid"  name="instructorid" value="${instructorid}">
    <g:if test="${rolelist.size()==0}">
        <div class="row">
             <div class="col-md-12">
                 <div class="row">
                     <div class="col-md-12">
                         <div class="card card-topline-purple">
                             All Roles are Allocated.<br>
                         </div>
                    </div>
                 </div>
             </div>
        </div>
    </g:if>
    <g:else>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Role : </header>
                            </div>
                            <!--<div class="card-body">-->
                                <div class="table-responsive">
                                    <table class="mdl-data-table ml-table-bordered">

                                         <g:set var="x" value="${1}"/>
                                         <thead>
                                                  <tr>
                                                    <th class="mdl-data-table__cell--non-numeric"><g:checkBox name="checkAll" id ="checkAll" class="selectall"  onClick="selectAll(this)" value="" />All</th>
                                                  </tr>
                                         </thead>
                                          <tbody>
                                          <g:each in="${rolelist}" var="rb" status="i">
                                          <tr>
                                             <td class="mdl-data-table__cell--non-numeric"><g:checkBox name="rolecheckbox" id ="rolecheckbox" class="individual"  value="${rb.id}" checked="false"/>${rb.role}</td>
                                          </tr>
                                          </g:each>
                                           </tbody>
                                    </table>
                                </div>
                                <br>
                                <center><input type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary"  value="Assign Role"/></center><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </g:else>

</g:form>


