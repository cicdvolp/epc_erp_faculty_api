package volp

class DivisionOffering {

    int capacity   //division total capacity
    String rollnumbersuffix
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    boolean isregular
    String timetable_filename
    String timetable_filepath

    //belongsto ERPProggrp, specificProgrom:program,
    static belongsTo=[classteacher:Instructor,division:Division,academicyear:AcademicYear,semester:Semester,module:Module,
                      year:Year,program:Program,shift:ERPShift,organization:Organization, erpgrant:ERPGrant]
    static constraints = {
        classteacher nullable:true
        rollnumbersuffix nullable:true
        module nullable:true
        shift nullable:true
        erpgrant nullable:true
        timetable_filename nullable:true
        timetable_filepath nullable:true
    }
    static mapping = {
        capacity defaultValue: 0
        isregular defaultValue: true
    }
    String toString(){
        division?.name+" : "+ year?.display_name + " : "+ program?.name
    }
}
