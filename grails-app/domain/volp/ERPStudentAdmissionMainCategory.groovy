package volp

class ERPStudentAdmissionMainCategory { //USED FOR VCAS AS MAIN ADMISSION CATEGORY

    boolean isoverandabove
    boolean isscholershipsectionapplicable
    boolean isecapplicable
    boolean isactive
    Double reservation_percentage
    String category

    boolean isapplicabletoadmission

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, categorytype:CategoryType]

    static constraints = {
        categorytype nullable : true
    }

    String toString() {
        category
    }

    static mapping = {
        isoverandabove defaultValue: false
        isscholershipsectionapplicable defaultValue: false
        isecapplicable defaultValue: false
        isactive defaultValue: false
        reservation_percentage defaultValue:0
        isapplicabletoadmission defaultValue: false
    }
}