package volp

class ERPStudentFacultyFeedbackPerformanceIndex {

    int number_of_students
    int expected_students

    double weighted_avg_total
    double outof
    double performance_index  //percentage
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpstudentfeedbackversion:ERPStudentFeedbackVersion,
                      academicyear:AcademicYear,semester:Semester,
                      instructor:Instructor,erpstudentguardianinstructor:Instructor,
                      erpfeedbackcategorytype:ERPFeedbackCategoryType]
    static constraints = {
        instructor nullable:true
        erpstudentguardianinstructor nullable:true
    }
}
