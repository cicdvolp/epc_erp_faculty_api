package volp

class ERPFeeStudentMaster {
    //After Payment received
    double paid_till_now  //sum of entries for ERPStudentReceipt -  sum of entries for ERPRefundReceipt 
    double from_social_welfare // sum of entries for ERPSocialWelfareReceipt
    double fees_receivable_paid

    //Before payment received
    double fees_receivable_due    //from previous year
    double feesconcession  //not included in paid_till_now

    //Fees carry forwarded at the end of year
    double fees_carryforwarded_to_nextyear    //from this year to next year


    double student_fee_due
    double sw_fee_due
    double total_fee_received_SW
    double total_fee_received_receipt

    // fees_payable = totalfees (from  ERPFeesStructureMaster table) - feesconcession - from_social_welfare + fees_receivable_due
    // fees_due : fees_payable - (paid_till_now+fees_receivable_paid)
    // fees_carryforwarded_to_nextyear = fees_payable - (paid_till_now+fees_receivable_paid)

    String concessionremark

    Date cancellation_date
    String cancellation_remark

    Date next_amount_due_date

    boolean isyeardown
    boolean isfullypaid
    boolean ispartialpaid

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[learner:Learner,erpstudentfeescategorylinking:ERPStudentFeesCategoryLinking,erpfeesstructuremaster:ERPFeesStructureMaster,
                      organization:Organization]
    static hasMany = [erpstudentreceipt:ERPStudentReceipt,erprefundreceipt:ERPRefundReceipt,erpsocialwelfarereceipt:ERPSocialWelfareReceipt]
    static constraints = {
        cancellation_date nullable: true
        cancellation_remark nullable: true
        organization nullable:true
        concessionremark nullable:true
        next_amount_due_date nullable:true
        username nullable:true
        creation_date nullable:true
        updation_date nullable:true
        creation_ip_address nullable:true
        updation_ip_address nullable:true
    }
    String toString()
    {
        paid_till_now
    }
    static mapping = {
        total_fee_received_receipt defaultValue: 0
        total_fee_received_SW defaultValue: 0
        sw_fee_due defaultValue: 0
        student_fee_due defaultValue: 0
        fees_receivable_due defaultValue: 0
        fees_carryforwarded_to_nextyear defaultValue: 0
        fees_receivable_paid defaultValue: 0
        feesconcession defaultValue: 0
        isyeardown defaultValue: false
        ispartialpaid defaultValue: false
        isfullypaid defaultValue: false
    }
}
