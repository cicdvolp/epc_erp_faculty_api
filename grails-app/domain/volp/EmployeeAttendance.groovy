package volp

class EmployeeAttendance {
    String intime
    String outtime
    Date todaysdate
    boolean hasappliedforleavefortoday
    boolean isnonworkingday // true : Non working day , false : working day
    boolean isweeklyoff // true : weekly off , false : working day
    boolean isworkfromhome

    double expectednoofhours
    String expectedintime
    String expectedouttime
    int erpcalenderintegerday
    boolean isexpectedinoutapproved
    boolean isattendancebyestablishment
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[
            instructor:Instructor,
          academicyear:AcademicYear,
          erpmonth:ERPMonth,
          erpday:ERPDay,
          erpholidaydetails:ERPHolidayDetails,
          erpcalenderyear:ERPCalendarYear,
          department:Department,
          organization:Organization,
          leavetype:LeaveType,
          leaveapprovalstatus:LeaveApprovalStatus,
          employeeleavetransaction:EmployeeLeaveTransaction
    ]
    static constraints = {
        intime  nullable:true
        outtime  nullable:true
        leavetype  nullable:true
        department nullable:true
        isweeklyoff nullable: true
        erpcalenderintegerday: 1..31
        expectedintime  nullable:true
        expectedouttime  nullable:true
        employeeleavetransaction  nullable:true
        erpholidaydetails   nullable:true
        leaveapprovalstatus   nullable:true
    }
    //static hasMany = [outofofficehours: OutOfOfficeHours]
    static mapping = {
        isexpectedinoutapproved defaultValue: false
        hasappliedforleavefortoday defaultValue: false
        isattendancebyestablishment defaultValue: false
        isworkfromhome defaultValue: false
    }
}
