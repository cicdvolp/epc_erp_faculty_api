package volp

class  Organization
{
    boolean isactive
    String trust_name
    String icard_organization_name
    String icard_organization_tag    //An Autonomous Institute to Savitribai Phule Pune University
    String website
    String address
    String uid     //This should be simmilar to login.username
    String email
    String organization_name
    String organization_code
    String registration_number
    String display_name
    String organization_detailed_name  //will be used for office order
    String establishment_email    //will be used for sending mail
    String establishment_email_credentials
    String account_email    //will be used for sending mail
    String account_email_credentials
    String official_director_email    //Director
    String official_registrar_email   //Registrar
    String official_bract_email    //BRACT
    String studentsection_email
    String studentsection_email_credentials
    int minweeklyhoursperweek
    boolean iseducationalinstitute

    String gsuit_credentials_file_path
    String gsuit_credentials_file_name
    String gsuit_org_unit
    int gsuit_port_number
    boolean isgsuitapplicable
    boolean enableadmissionemailotp
    boolean enableadmissionmobileotp

    String instructor_website
    String learner_website
    String admission_website

    String account_sign
    String account_sign_fees_estimate
    String account_stamp
    String logincode

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    String organization_number
    String ptrc_number
    String director_name

    int sort_order

    String admission_vuejs_page
    String hostel_vuejs_page
    String adhock_vuejs_page

    boolean ischallanapplicable
    boolean isreceiptapplicable
    String challan_bank_name
    String challan_bank_logo
    String challan_bank_address
    String challan_sign_place

    String org_logo
    String org_exam_code
    String org_university_code
    String org_admission_code

    String logo_file_path
    String logo_file_name

    String payroll_email
    String payroll_watermark

    boolean autoapproveleavejobon
    boolean autoapprovecertificatejobon
    boolean autoinstallmentemailjobon
    boolean autoattendancesendonemail

    boolean isschool
    String communication_access_key
    Date payment_due_date

    static belongsTo=[organization:Organization,organizationtype:OrganizationType,logo : Logo,hoisign:Logo, organizationgroup:OrganizationGroup,
                      examcoordinator : Instructor, timetablecoordinator : Instructor , feedbackcoordinator : Instructor]

    static constraints = {
        organization_name nullable: true
        organization_code nullable: true
        registration_number nullable: true
        display_name nullable: true
        organization nullable: true
        organizationtype nullable: true
        organizationgroup nullable: true
        logo nullable: true
        hoisign nullable: true
        organization_detailed_name nullable: true
        establishment_email nullable: true
        establishment_email_credentials nullable: true
        account_email nullable: true
        account_email_credentials nullable: true
        official_director_email nullable: true
        official_registrar_email nullable: true
        official_bract_email nullable: true
        trust_name nullable: true
        icard_organization_name nullable: true
        icard_organization_tag nullable: true
        website nullable: true
        address nullable: true
        organization_number nullable: true
        examcoordinator nullable: true
        timetablecoordinator nullable: true
        feedbackcoordinator nullable: true
        gsuit_credentials_file_path nullable: true
        gsuit_credentials_file_name nullable: true
        gsuit_org_unit nullable: true
        instructor_website nullable: true
        learner_website nullable: true
        admission_website nullable: true
        ptrc_number nullable: true
        director_name nullable:true
        account_sign nullable:true
        account_sign_fees_estimate nullable:true
        account_stamp nullable:true
        adhock_vuejs_page nullable:true
        admission_vuejs_page nullable:true
        hostel_vuejs_page nullable:true
        org_logo nullable:true
        org_exam_code nullable:true
        org_university_code nullable:true
        logo_file_path nullable:true
        logo_file_name nullable:true
        challan_bank_name nullable:true
        challan_bank_logo nullable:true
        challan_bank_address nullable:true
        challan_sign_place nullable:true
        payroll_email nullable:true
        payroll_watermark nullable:true
        logincode nullable:true
        studentsection_email nullable:true
        studentsection_email_credentials nullable:true
        org_admission_code nullable:true
        communication_access_key nullable:true
        payment_due_date nullable:true
    }

    static mapping = {
        isreceiptapplicable defaultValue:true
        sort_order defaultValue:0
        minweeklyhoursperweek defaultValue : 40
        ischallanapplicable defaultValue : false
        enableadmissionemailotp defaultValue : true
        iseducationalinstitute defaultValue : true
        isgsuitapplicable defaultValue : false
        isschool defaultValue : false
        gsuit_port_number defaultValue : 0
        autoapproveleavejobon defaultValue : false
        autoapprovecertificatejobon defaultValue : false
        autoinstallmentemailjobon defaultValue : false
        autoattendancesendonemail defaultValue : false
    }
}
