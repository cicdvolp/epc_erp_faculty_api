package volp
class QueryLevel
{
    int level  //1,2,3
    String name//Coordinator,Customer Support, Technical
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
        creation_username nullable:true
        updation_username nullable:true
        creation_date nullable:true
        updation_date nullable:true
        creation_ip_address nullable:true
        updation_ip_address nullable:true
        name nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        level defaultValue: 0
    }

}
