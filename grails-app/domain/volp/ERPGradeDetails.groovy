package volp

class ERPGradeDetails {
    String gradename  //ex.A+,AA
    String gradetype  //ex.Fail , FailSubmittive , Faile Formative
    double gradepoint    //10,9,8….
    double min   //minimum range
    double max   //maximum range
    String remark     //excellent,good,outstanding etc.
    int sort_order
    boolean ispassgrade

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,erpgrademaster:ERPGradeMaster]

    static constraints = {
        remark nullable:true
        gradetype nullable:true
    }

    static mapping = {
        min defaultValue: 0
        max defaultValue: 0
        sort_order defaultValue:0
        ispassgrade defaultValue: false
    }
}
