package volp

class ERPCourseOffering
{
    long unique_course_offering_id
    int threshold
    boolean isDeleted
    boolean isalloted   //for elective subject..to freez allocation //to approve allocation
    int sort_order
    double credit
    int bucket_number
    int display_order // used for SP College

    int min_student
    int max_student
    int balance_student

    boolean is_applicable_to_attendance
    boolean is_applicatble_to_online_exam
    boolean is_not_applicatble_to_exam
    boolean is_applicable_to_feedback  //true : applicable to feedback, false: Not applicable to feedback
    boolean is_applicable_to_po_attainment  //true : applicable to feedback, false: Not applicable to feedback
    boolean isforspecialization
    double reregfees

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    //belongsto year
    //erpgrademaster : relative scheme set for course
    static belongsTo=[erpassesmentscheme:ERPAssesmentScheme,erpgrademaster:ERPGradeMaster,
                      erpcoursemarksentrytype:ERPCourseMarksEntryType,courseowner:Instructor,
                      year:Year,program:Program,erpcourse:ERPCourse,
                      coursetypeacademics:CourseTypeAcademics,
                      academicyear:AcademicYear,semester:Semester,erpcoursecategory:ERPCourseCategory,
                      courserule:CourseRule,
                      coursetype:CourseType,module:Module,templateoffering:TemplateOffering,
                      templatedetails:TemplateDetails,organization:Organization,erpprogramgroup:ERPProgramGroup,
                      erpexamconducttype:ERPExamConductType, syllabuspattern:SyllabusPattern,
                      erpreregistrationcoursetype : ERPReRegistrationCourseType,
                      divisionofferingsubjectgroupoffering:DivisionOfferingSubjectGroupOffering,specialization_in:ERPCourseOffering]
                        //coursetype for exam only
    static constraints = {
       // unique_course_offering_id unique: true
        module nullable:true
        templateoffering nullable:true
        templatedetails nullable:true
        erpprogramgroup nullable:true
        program nullable:true
        courseowner nullable:true
        erpcoursemarksentrytype nullable:true
        erpassesmentscheme nullable:true
        erpgrademaster nullable:true
        courserule nullable:true
        coursetype nullable:true
        year nullable:true
        erpcoursecategory nullable:true
        erpexamconducttype nullable:true
        coursetypeacademics nullable:true
        syllabuspattern nullable:true
        erpreregistrationcoursetype nullable:true
        divisionofferingsubjectgroupoffering nullable:true
        specialization_in nullable:true
    }
    static mapping = {
        display_order defaultValue:0
        bucket_number defaultValue:0
        sort_order defaultValue:0
        reregfees defaultValue:0
        min_student defaultValue:0
        max_student defaultValue:0
        balance_student defaultValue:0
        is_applicatble_to_online_exam defaultValue:false
        is_applicable_to_po_attainment defaultValue:true
        isDeleted defaultValue:false
        unique_course_offering_id defaultValue:0
        is_not_applicatble_to_exam defaultValue:false
        is_applicable_to_feedback defaultValue:true
        is_applicable_to_attendance defaultValue:true
        isforspecialization defaultValue:false
    }
}