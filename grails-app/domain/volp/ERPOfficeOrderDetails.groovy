package volp

class ERPOfficeOrderDetails {
    Date order_date
    String outwardnumber    // reference number
    String employee_number
    String subject
    String fullname
    String address
    String department    // appointed department
    String designation  // appointed designation
    String current_organization
    String appointed_organization
    String mobileno
    String emailid
    Date appointment_form
    Date appointment_to

    double consolidated_salary
    String consolidated_salary_in_word
    double incentive
    String incentive_in_word
    double allowance
    String allowance_in_word

    String file_name
    String file_path

    double basic_pay
    String basic_pay_in_word
    String pay_band
    double grade_pay
    String grade_pay_in_word

    boolean approved_by_hod   // 3 checklist for order approval
    boolean approved_by_director
    boolean approved_by_trust

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[employee:Instructor,departmentb:Department,designationb:Designation,organization:Organization,erpordertype:ERPOrderType]
    static constraints = {
        consolidated_salary nullable:true
        basic_pay nullable:true
        subject nullable:true
        pay_band nullable:true
        grade_pay nullable:true
        approved_by_hod nullable:true
        approved_by_director nullable:true
        approved_by_trust nullable:true
        employee nullable: true
        departmentb nullable: true
        designationb nullable: true
        organization nullable: true
        mobileno nullable: true
        emailid nullable: true
        file_name nullable: true
        file_path nullable: true
        consolidated_salary_in_word nullable: true
        basic_pay_in_word nullable: true
        grade_pay_in_word nullable: true
        incentive_in_word nullable: true
        allowance_in_word nullable: true
    }

    static mapping = {
        incentive defaultValue: 0
        allowance defaultValue: 0
    }
}
