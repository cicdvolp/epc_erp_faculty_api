package volp

class WhatsAppCredentials {
    String name //for
    String accesskey
    String api_end_point_url
    boolean isactive //to disable it
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}
