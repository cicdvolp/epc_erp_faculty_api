package volp

class ApplicationRound {

    String name
    String displayname
    String dteapplicationprefix
    String url
    String merittype
    boolean isactive
    boolean bypassotp
    double feesamount
    double feesamount_as_per_previous_round

    Date from_date
    Date to_date
    String acknoledgementmsg
    String note

    boolean isopenforallotment
    boolean isrequisitionformrequired
    int srcollspeedinmilisec
    int splashvacancytimeinmin
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[paymentorganization : Organization, admissionyear:AcademicYear]

    static constraints = {
        url nullable : true
        dteapplicationprefix nullable : true
        paymentorganization nullable : true
        acknoledgementmsg nullable : true
        from_date nullable : true
        to_date nullable : true
        note nullable : true
        merittype nullable : true
    }

    static mapping = {
        feesamount defaultValue: 0
        feesamount_as_per_previous_round defaultValue: 0
        splashvacancytimeinmin defaultValue: 1
        srcollspeedinmilisec defaultValue: 50000
        isactive defaultValue: false
        bypassotp defaultValue: false
        isopenforallotment defaultValue: false
        isrequisitionformrequired defaultValue: false
    }

}
