package volp

class ProgramYear {

    boolean isactive

    static belongsTo=[program:Program,year:Year,organization:Organization]

    static constraints = {
        organization nullable:true
    }

    static mapping = {
        isactive defaultValue: true
    }
}
