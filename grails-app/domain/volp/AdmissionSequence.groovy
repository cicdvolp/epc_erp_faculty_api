package volp

class AdmissionSequence {

    int application_track_number
    int admission_track_number
    int application_fees_track_number
    int admission_fees_track_number

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,academicyear:AcademicYear]

    static constraints = {
    }
}
