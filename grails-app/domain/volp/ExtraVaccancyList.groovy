package volp

class ExtraVaccancyList {

    int extracount
    Date extravacancyupdationtime
    Date extravacancydeletiontime
    boolean isdeleted

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, vaccancylist:VaccancyList,
                      extravacancyupdatedby : Instructor, extravacancydeletedby : Instructor]

    static constraints = {
        extravacancydeletedby nullable : true
        extravacancydeletiontime nullable : true
    }

    static mapping = {
        extracount defaultValue: 0
        isdeleted defaultValue: false
    }
}
