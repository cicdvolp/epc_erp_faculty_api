package volp

class HostelReceiptTransaction {

    String tid
    Date date
    double amount
    String transaction_proof_filename
    String transaction_proof_filepath
    String bank_name
    String remark

    String erp_transaction_id
    String transaction_message
    String transaction_error_message
    String request_details
    double received_amount
    String payment_remark
    Date request_transaction_date
    String response_transaction_date
    String paymentgateway_transaction_id
    String transaction_response_entire_url
    String transaction_status_api__response_entire_url
    String bank_transaction_id
    String card_id
    String customer_id
    String utr_no
    String customer_name
    String mobile_number
    String account_number

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hostelpaymentmode:HostelPaymentMode,
                      learner:Learner, hostelstudentreceipt:HostelStudentReceipt,
                      hostelfeesstatus: HostelFeesStatus, hostelfeestudentmaster:HostelFeeStudentMaster,
                      hosteloffering:HostelOffering, academicyear:AcademicYear,
                      transactionrequesttype:TransactionRequestType, transactioncurrencycode:TransactionCurrencyCode,
                      transactionorganizationonlineaccount:TransactionOrganizationOnlineAccount,
                      transactionstatus:TransactionStatus]

    static constraints = {
        transactionrequesttype nullable : true
        transactionstatus nullable : true
        transactioncurrencycode nullable : true
        hostelstudentreceipt nullable : true
        erp_transaction_id nullable : true
        request_details nullable : true
        transaction_message nullable : true
        request_transaction_date nullable : true
        remark nullable : true
        date nullable : true
        bank_transaction_id nullable:true
        card_id nullable:true
        customer_id nullable:true
        customer_name nullable:true
        utr_no nullable:true
        mobile_number nullable:true
        account_number nullable:true
        tid nullable:true
        hostelfeesstatus nullable:true
        transaction_error_message nullable:true
        response_transaction_date nullable:true
        paymentgateway_transaction_id nullable:true
        transaction_response_entire_url nullable:true
        bank_name nullable:true
        payment_remark nullable:true
        transaction_proof_filename nullable:true
        transaction_proof_filepath nullable:true
        transactionorganizationonlineaccount nullable:true
        transaction_status_api__response_entire_url nullable:true
    }

    static mapping = {
        received_amount defaultValue: 0
        transaction_status_api__response_entire_url sqlType: "LONGTEXT"
    }
}
