package volp

class AdmissionStepMaster {

    String stepname
    String stepdescription // Application receiving  , Document Verification ,   Provisional Merit display date , Grievance date , File Merit display date
    int stepnumber

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address


    static belongsTo=[organization:Organization]

    static constraints = {
    }
}
