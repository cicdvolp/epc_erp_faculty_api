package volp

class StudentGuardianMeetingDetails {

    Date conductiondate
    String conductiontime
    String meetinglink
    String mom
    String studentremark
    boolean studentacknoledgement
    boolean iscriticalanalysis

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[guardian:Instructor,guardianmeetingschedule:GuardianMeetingSchedule,
                      organization:Organization, program:Program,year:Year,
                      academicyear:AcademicYear,semester: Semester, learner:Learner]

    static constraints = {
        guardianmeetingschedule nullable: true
        conductiondate nullable: true
        conductiontime nullable: true
        meetinglink nullable: true
        studentremark nullable: true
    }

    static mapping = {
        iscriticalanalysis defaultValue: false
        studentacknoledgement defaultValue: false
    }
}
