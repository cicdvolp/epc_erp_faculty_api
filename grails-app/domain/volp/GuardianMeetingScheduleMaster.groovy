package volp

class GuardianMeetingScheduleMaster {

    String meeetingnumber
    Date fromdate
    Date proposeddate

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, program:Program,year:Year,
                      academicyear:AcademicYear,semester: Semester]

    static constraints = {
        // unique_course_offering_id unique: true
        fromdate nullable:true
    }
}
