package volp

class ApplicationRoundOrganization {
    String password
    int delayinsecond
    int vacancylockon

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, applicationround: ApplicationRound,
                      admissionyear:AcademicYear,year:Year, programtype:ProgramType,
                      erpadmissionround:ERPAdmissionRound]

    static constraints = {
        password nullable : true
    }

    static mapping = {
        delayinsecond defaultValue: 0
        vacancylockon defaultValue: 0
    }
}
