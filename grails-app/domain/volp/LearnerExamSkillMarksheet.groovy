package volp

class LearnerExamSkillMarksheet {

    String monthofexam
    String yearofexam
    String resultdate
    String printdate
    boolean islast
    int skillsheetserialnumber

    String pdffilepath
    String pdffilename

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[academicyear:AcademicYear,semester:Semester,
                      year:Year,organization:Organization,
                      learner:Learner,program:Program,programtype:ProgramType,
    ]

    static hasMany = [learnerexamskillmarks:LearnerExamSkillMarks]

    static constraints = {
        pdffilepath nullable:true
        pdffilename nullable: true
        printdate nullable: true
    }

    static mapping = {
        islast defaultValue: true
        skillsheetserialnumber defaultValue: 0
    }
}
