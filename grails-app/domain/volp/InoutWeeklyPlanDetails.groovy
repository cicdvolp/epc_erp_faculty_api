package volp

class InoutWeeklyPlanDetails {
    String intime
    String outtime
    boolean isweeklyoff
    static belongsTo=[
        erpday:ERPDay,
        organization:Organization,
        inoutweeklyplanmaster:InoutWeeklyPlanMaster
    ]
    static constraints = {}
}
