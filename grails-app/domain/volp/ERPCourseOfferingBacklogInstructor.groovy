package volp

class ERPCourseOfferingBacklogInstructor {

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpcourseoffering:ERPCourseOffering,
                      instructor:Instructor,verifyinginstructor:Instructor,
                      academicyear:AcademicYear,
                      semester:Semester,
                      organization:Organization]

    static constraints = {
        instructor nullable :true
        verifyinginstructor nullable :true
    }
}
