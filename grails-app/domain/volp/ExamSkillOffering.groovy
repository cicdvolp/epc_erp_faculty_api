package volp

class ExamSkillOffering {

    boolean isactive
    double outofmarks
    double passingmarks

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,examskill:ExamSkill,academicyear:AcademicYear,semester:Semester,erpgrademaster:ERPGradeMaster]

    static constraints = {}

    static mapping = {
        isactive defaultValue: true
        passingmarks defaultValue: 0
        outofmarks defaultValue: 0
    }
}
