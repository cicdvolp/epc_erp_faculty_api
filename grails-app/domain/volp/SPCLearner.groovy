package volp

class SPCLearner {

   String first_name
   String middle_name
   String last_name
   String full_name

   Integer rollno

   String mobile
   Integer mobile_otp
   boolean is_mobile_otp_verified

   String email
   Integer email_otp
   boolean is_email_otp_verified

   boolean is_submitted

   String photo_file_path
   String photo_file_name

   static constraints = {
      photo_file_path nullable:true
      photo_file_name nullable:true
      first_name nullable:true
      middle_name nullable:true
      last_name nullable:true
      rollno nullable:true
      full_name nullable:true
      mobile nullable:true
      mobile_otp nullable:true
      email nullable:true
      email_otp nullable:true
   }
   static mapping = {
      is_submitted defaultValue: false
      is_mobile_otp_verified defaultValue: false
      is_email_otp_verified defaultValue: false
   }
   String toString() {
      full_name
   }
}
