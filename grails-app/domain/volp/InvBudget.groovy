package volp

class InvBudget {

    double amount
    String activity_name
    String file_name
    String file_path
    Date budget_entry_date
    boolean isactive
    boolean isapprove               // not in use

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization          : Organization,
                      academicyear          : AcademicYear,
                      invbudgettype         : InvBudgetType,
                      invbudgetlevel        : InvBudgetLevel,
                      department            : Department,
                      invapprovalstatus     : InvApprovalStatus,
                      enterby               : Instructor]

    static constraints = {
        activity_name nullable : true
        file_name nullable : true
        file_path nullable : true
        department nullable: true
    }

    static mapping = {
        isactive defaultValue: true
        isapprove defaultValue: false
        amount defaultValue: 0
    }
}
