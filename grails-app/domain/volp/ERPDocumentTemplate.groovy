package volp

class ERPDocumentTemplate {
    boolean isActive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    String display_template//admin side sample
    String edit_template//file details
    String generate_template//final code
    static belongsTo=[organization:Organization,erpdocumentmaster:ERPDocumentMaster, erpdocumenttemplatetype:ERPDocumentTemplateType]
    static constraints = {
        erpdocumentmaster nullable: true
        erpdocumenttemplatetype nullable: true
    }
}
