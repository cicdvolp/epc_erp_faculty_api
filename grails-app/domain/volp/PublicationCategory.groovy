package volp

class PublicationCategory {
    String type //conference, journal, book, book chapter, poster
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
    static belongsTo=[organization:Organization]


}
