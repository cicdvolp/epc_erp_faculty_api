package volp

class ERPStudentFacultyFeedbackCategoryLog {

    int number_of_students
    int expected_students

    double category_avg
    double outof
    double category_performance_index  //percentage

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      erpstudentfeedbackversion:ERPStudentFeedbackVersion,
                      academicyear:AcademicYear,semester:Semester,
                      erpfeedbackcategory:ERPFeedbackCategory,
                      instructor:Instructor,
                      erpstudentguardianinstructor:Instructor,erpfeedbackcategorytype:ERPFeedbackCategoryType]

    static constraints = {
        instructor nullable:true
        erpstudentguardianinstructor nullable:true
    }

    static mapping = {
        category_avg defaultValue: 0
        outof defaultValue: 0
        category_performance_index defaultValue: 0
    }
}
