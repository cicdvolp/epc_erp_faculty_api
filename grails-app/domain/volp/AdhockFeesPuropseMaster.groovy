package volp

class AdhockFeesPuropseMaster {

    String purpose

    double fees

    boolean isactive

    boolean isfeespredefined        // if true then accept same master set fees amount
                                    // else set by faculty or student

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static constraints = {
    }

    static mapping = {
        isactive defaultValue: true
        isfeespredefined defaultValue: false
        fees defaultValue: 0
    }
}
