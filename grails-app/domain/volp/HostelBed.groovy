package volp

class HostelBed {

    String name
    String description
    boolean isactive
    boolean isreserved

    String bed_image_file_name
    String bed_image_file_path

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organizationgroup:OrganizationGroup, hostelroom : HostelRoom]

    static constraints = {
        name unique: 'hostelroom'
        bed_image_file_name nullable : true
        bed_image_file_path nullable : true
    }

    static mapping = {
        isactive defaultValue: true
        isreserved defaultValue: false
    }
}
