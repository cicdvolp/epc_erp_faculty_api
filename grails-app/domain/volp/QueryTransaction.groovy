package volp
class QueryTransaction
{
    String remark
    Date actiondate
    boolean ismailsent

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[query:Query,instructor:Instructor,querystatus:QueryStatus,querylevel:QueryLevel,
                      organization:Organization]
    static constraints = {
        remark nullable:true
        actiondate nullable:true
        query nullable:true
        instructor nullable:true
        querystatus nullable:true
        organization nullable:true
    }
    static mapping = {
        ismailsent defaultValue: false
    }
}
