package volp

class ERPLearnerNotificationHistory {

    String noticationto
    String notificationtext
    boolean studentreadreceipt
    boolean successstatus
    String reasonoffailure


    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,learner:Learner,erpnotificationtext:ERPNotificationText]
    static constraints = {
        notificationtext nullable : true
        reasonoffailure nullable : true
    }

    static mapping = {
        studentreadreceipt defaultValue: false
        successstatus defaultValue: false
    }
}
