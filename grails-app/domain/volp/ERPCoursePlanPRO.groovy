package volp

class ERPCoursePlanPRO {

    String pro_code
    String pro_statement
    boolean is_deleted

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, erpcourseplan : ERPCoursePlan]

    static constraints = {
    }

    static mapping={
        is_deleted defaultValue:false
    }
}
