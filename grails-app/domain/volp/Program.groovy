package volp

class Program {

    String name
    String abbrivation
    String specialization
    boolean isdeleted
    boolean isfeesprogramwise
    String examdisplayname
    String icard_color_code
    boolean isapplicabletoadmission
    int sort_order

    String displayname       //used in passing certificate(VIT)
    String icarddisplayname  //used for icard strip displayname(ZEAL)
	static belongsTo=[department:Department,
                      organization:Organization,
                      programtype:ProgramType,
                      erpprogramgroup:ERPProgramGroup]  //Desh/Computer
    String username    
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    String toString()
    {
        name
    }
    static mapping = {
        isapplicabletoadmission defaultValue: true
        sort_order defaultValue: 0
    }
    static constraints = {
        icard_color_code nullable: true
        displayname nullable: true
        erpprogramgroup nullable: true
        organization nullable: true
        abbrivation nullable:true
        examdisplayname nullable:true
        specialization nullable:true
        icarddisplayname nullable:true
    }
}
