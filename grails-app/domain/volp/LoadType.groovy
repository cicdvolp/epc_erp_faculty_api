package volp

class LoadType
{
    int nooftimetableslotperweek

    int actual_execution_hrs   //total hours per turn in minutes
    int numberofturnspersemester
    int hrs   //total hours per week

    String loadtype_name   //Theory2,Lab2,Theory3 //display name
    String description
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,type:LoadTypeCategory]
    static constraints = {
        description nullable: true
    }

    static mapping = {
        numberofturnspersemester defaultValue : 0
    }


}
