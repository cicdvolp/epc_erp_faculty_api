package volp

class ERPFeedbackCategoryType {

    String type   //Guardian/SubjectFaculty/Institute/Exit/PO-PSO
    boolean notvisible
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpfeedbacktype:ERPFeedBackType]
    static constraints = {
        erpfeedbacktype nullable:true
    }
}
