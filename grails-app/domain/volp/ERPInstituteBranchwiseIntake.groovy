package volp

class ERPInstituteBranchwiseIntake {

    int intake
    int grant_intake
    int non_grant_intake
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,
                      program:Program,
                      programtype:ProgramType,
                      year:Year,
                      erpshift:ERPShift,
                      academicyear:AcademicYear,
                      admissiontype:AdmissionType]

    static constraints = {
        admissiontype nullable : true
    }

    static mapping = {
        grant_intake defaultValue: 0
        non_grant_intake defaultValue: 0
    }
}
