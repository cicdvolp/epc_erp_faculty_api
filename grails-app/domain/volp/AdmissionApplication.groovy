package volp

class AdmissionApplication {

    String applicationid
    Date application_date
    boolean is_application_undertaking_selected
    double application_fees
    boolean is_application_fees_paid
    boolean is_selected_for_admission
    String admissionid
    Date admission_date
    boolean is_admission_undertaking_selected

    boolean is_subject_preference_approved
    boolean is_out_of_admission
    boolean is_admission_fees_paid
    double paid_admission_fees_amount

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    String remark

    static belongsTo=[organization:Organization,
                      admissionapplicant:AdmissionApplicant,
                      academicyear:AcademicYear,
                      program:Program,
                      year:Year,admissionstatus:AdmissionStatus,
                      subjectpreferencestatus:SubjectPreferenceStatus,
                      erpdocumentstatus:ERPDocumentStatus,
                      erpseattype:ERPSeatType
    ]

    static mapping = {
        is_application_undertaking_selected defaultValue:false
        is_application_fees_paid defaultValue:false
        is_selected_for_admission defaultValue:false
        is_admission_undertaking_selected defaultValue:false
        is_subject_preference_approved defaultValue:false
        is_admission_fees_paid defaultValue:false
        is_out_of_admission defaultValue:false

        application_fees defaultValue:0
        paid_admission_fees_amount defaultValue:0
    }


    static constraints = {
        admissionid nullable : true
        admission_date nullable : true
        application_date nullable : true
        admissionstatus nullable : true
        remark nullable: true
        subjectpreferencestatus nullable: true
        erpdocumentstatus nullable: true
        erpseattype nullable: true
    }
}
