package volp

class ERPInstructorTaskMaster {

    String name
    String description

    boolean isdelete
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, erptaskmaster:ERPTaskMaster, instructor : Instructor]

    static constraints = {
        description nullable : true
        erptaskmaster nullable : true
    }

    static mapping = {
        isactive defaultValue:true
        isdelete defaultValue:false
    }
}
