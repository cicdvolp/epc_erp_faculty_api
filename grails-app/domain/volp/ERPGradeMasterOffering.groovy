package volp

class ERPGradeMasterOffering
{
    boolean is_grade_penalty_applicable_in_reexam

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    // absolustegrademasteroffering is used when student is less then 25

    static belongsTo=[organization:Organization,erpgrademaster:ERPGradeMaster,absolustegrademasteroffering:ERPGradeMasterOffering,
                      academicyear:AcademicYear,semester:Semester]


    static constraints = {
        absolustegrademasteroffering nullable : true
    }

    static mapping = {
        is_grade_penalty_applicable_in_reexam defaultValue: false
    }
}
