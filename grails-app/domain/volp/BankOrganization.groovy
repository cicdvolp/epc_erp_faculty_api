package volp

class BankOrganization {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,erpbankname:ERPBankName, banktype:BankType]

    static constraints = {
        banktype nullable : true
    }
}
