package volp

class AdmissionApplicantAcademics {

    String institute_name
    String board_university
    String passing_year

    int attempt_number
    double obtained_marks
    double out_of_marks
    double percentage

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, admissionapplication:AdmissionApplication,
                      admissionapplicant:AdmissionApplicant,qualifyingexamination:QualifyingExamination,
                      qualifyingexamgrade:QualifyingExamGrade,boarduniversity:BoardUniversity]


    static mapping = {
        attempt_number defaultValue:0
        obtained_marks defaultValue:0
        out_of_marks defaultValue:0
        percentage defaultValue:0
    }
    static constraints = {
        boarduniversity nullable: true
        board_university nullable: true
    }
}
