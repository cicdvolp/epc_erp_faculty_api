package volp

class EMandateCredential {

    String host_url
    String short_code
    String utilcode
    String client_name
    String account_no
    String merchant_category_code

    boolean isenabled

    static belongsTo=[ organization:Organization]

    static constraints = {
        account_no nullable : true
        client_name nullable : true
    }

    static mapping = {
        isenabled defaultValue: false
    }
}
