package volp

class ERPStudentFeedbackVersionProgramType {

    boolean isactive
    Date startdate
    Date enddate
    double threshould
    boolean  issecretcoderequired

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,erpstudentfeedbackversion:ERPStudentFeedbackVersion,
                      programtypeyear:ProgramTypeYear, programyear:ProgramYear]

    static constraints = {
        programyear nullable:true
        startdate nullable:true
        enddate nullable:true
    }

    static mapping = {
        isactive defaultValue: true
        issecretcoderequired defaultValue: false
        threshould defaultValue: 0
    }
}
