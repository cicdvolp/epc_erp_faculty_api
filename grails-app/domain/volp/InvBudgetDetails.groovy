package volp

class InvBudgetDetails {

    int quantity_required
    double approx_cost_per_unit
    double total_cost

    String remark
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization          : Organization,
                      invbudget             : InvBudget,
                      invmaterial           : InvMaterial]

    static constraints = {
        remark nullable : true
    }

    static mapping = {
        isactive defaultValue: true
        quantity_required defaultValue: 0
        approx_cost_per_unit defaultValue: 0
        total_cost defaultValue: 0

    }
}
