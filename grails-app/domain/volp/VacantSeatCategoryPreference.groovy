package volp

class VacantSeatCategoryPreference {
    int preference
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      vacant: ERPStudentAdmissionMainCategory,
                      transferto: ERPStudentAdmissionMainCategory]
    static constraints = {
    }
}
