package volp

class ERPResultClass {
    double min_percentage   //minimum range
    double max_percentage   //maximum range
    String resultclass     //Pass,Second class,first class etc.

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,program:Program]

    static constraints = {

    }
    static mapping = {
        max_percentage defaultValue: 0
        min_percentage defaultValue: 0
        isactive defaultValue: false
    }
}
