package volp

class ERPSelfAppraisalParameter {

    int parameter_no
    String parameter_name
    String parameter_description
    double parameter_weightage
    boolean isActive
    boolean ishierarchyfinalized

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      academicyear:AcademicYear,
                      erpselfappraisalfacultytype:ERPSelfAppraisalFacultyType]

    static constraints = {
    }
    static mapping = {
        isActive defaultValue: true
        ishierarchyfinalized defaultValue: false
    }
}
