package volp

class EmployeeLeaveTransaction
{
    Date application_date
    Date fromdate
    Date todate
    double  numberofdays
    double numberofhours
    String fromtime
    String totime
    boolean isfirsthalf

    String reason
    boolean is_load_adjustment_done
    String nature_of_work
    String place_of_work
    String contact_phone_number
    String alternate_arragement_person_name
    String alternate_arragement_person_contact_number
    String certificate_file_name
    String certificate_file_path
    int vacation_slot_number

    int leave_registation_srno

    boolean is_applied_by_establishment

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,leavetype:LeaveType,employeeleavemaster:EmployeeLeaveMaster,
                      leaveapprovalstatus:LeaveApprovalStatus,
                      academicyear:AcademicYear,organization:Organization]
    static constraints = {
        reason nullable:true
        employeeleavemaster nullable: true
        nature_of_work nullable:true
        place_of_work nullable:true
        contact_phone_number nullable:true
        alternate_arragement_person_name nullable:true
        alternate_arragement_person_contact_number nullable:true
        certificate_file_name nullable:true
        certificate_file_path nullable:true
        fromtime nullable:true
        totime nullable:true
        isfirsthalf nullable:true
        employeeleavemaster  nullable:true
        vacation_slot_number defaultValue: 0
        numberofhours defaultValue: 0
        leave_registation_srno defaultValue: 0
        is_load_adjustment_done defaultValue: false
        is_applied_by_establishment defaultValue: false
    }
    static mapping = {
        is_load_adjustment_done defaultValue: false
        is_applied_by_establishment defaultValue: false
        isfirsthalf defaultValue: false
        vacation_slot_number defaultValue: 0
        numberofhours defaultValue: 0
    }
}
