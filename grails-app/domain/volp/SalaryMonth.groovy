package volp

class SalaryMonth {
    String name    //  oct-2020
    int month_number//1..12
    int number_of_days//1..31
    Date fromdate
    Date todate
    boolean isactive
    boolean isfreezed
    boolean ispartialsalaryapplicable

    boolean financial_last_month_for_extra_tax
    boolean is_partial// true if partial salary is done for this month
    boolean is_arrears_applicable

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
static belongsTo=[erpmonth:ERPMonth,erpyear:ERPCalendarYear,organization:Organization]
    static constraints = {
        month_number range: 1..12
        number_of_days range:1..31

    }
    static mapping = {
        is_arrears_applicable defaultValue:false
        isactive defaultValue:true
        isfreezed defaultValue:false
        financial_last_month_for_extra_tax defaultValue:false
        ispartialsalaryapplicable defaultValue:false
    }
}
