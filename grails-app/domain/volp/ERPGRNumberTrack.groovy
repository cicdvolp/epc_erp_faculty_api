package volp

class ERPGRNumberTrack {
    int number
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpgrnumberinstitutecode:ERPGRNumberInstituteCode,
                      erpgrnumberprogramcode:ERPGRNumberProgramCode,
                      academicYear:AcademicYear,programtype:ProgramType,year:Year, organization:Organization]
    static constraints = {
        erpgrnumberprogramcode nullable: true
        programtype nullable: true
        academicYear nullable: true
        year nullable: true
        erpgrnumberinstitutecode nullable: true
    }
}
