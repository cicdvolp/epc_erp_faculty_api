package volp

class AttendanceBiometricLog {

    String employee_code                // as it is attendance system emp code
    Date punchdatetime
    int attendance_id                   // table id
    int ticketno                   // ticket only for pccoe
    String deviceaddress                   // ticket only for pccoe
    String time                   // ticket only for pccoe

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, instructor:Instructor]

    static constraints = {
        ticketno defaultValue:0
        deviceaddress nullable: true
        time nullable: true
        attendance_id nullable: true
    }
}
