package volp

class ERPTimeTableVersion
{
    Date version_date
    boolean isCurrent
    Date wef
    Date todate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,academicyear:AcademicYear,semester:Semester,department:Department,erptimetableversionmaster:ERPTimeTableVersionMaster]
    static constraints = {
    }
}
