package volp

class ERPStudentFacultyFeedbackRawLog  //every feedback first raw file generation..common for subject and guardian feedback
{
    int number_of_students
    int expected_students

    double question_avg
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpstudentfeedbackversion:ERPStudentFeedbackVersion,
                      academicyear:AcademicYear,semester:Semester,
                      erpcourseofferinginstructor:ERPCourseOfferingInstructor,
                      erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      erpcourseoffering:ERPCourseOffering,
                      erpfeedbackquestionare:ERPFeedbackQuestionare,
                      instructor:Instructor,
                      erpstudentguardianinstructor:Instructor,erpfeedbackcategorytype:ERPFeedbackCategoryType]
    static constraints = {
        erpcourseofferinginstructor nullable:true
        erpcourseoffering nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpstudentguardianinstructor nullable:true
        instructor nullable:true
    }
}
