package volp

class MeritList {

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,admissionapplicatnt:AdmissionApplicant,
                      admissionapplication:AdmissionApplication,admissionstep:AdmissionStepMaster,
                      categorytype:CategoryType,erpadmissionmaincategory:ERPStudentAdmissionMainCategory,
                      erpdomacile:ERPDomacile,academicyear:AcademicYear,
                      program:Program, erpseattype:ERPSeatType,
                      year:Year]
    static constraints = {
        erpdomacile nullable : true
        erpseattype nullable : true
    }
}
