package volp

class ErrorHostelOnlineTransactions {

    double amount
    String response                 // for paytm - params response store
    String status_response          // for paytm transaction status api call response store

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organizationgroup:OrganizationGroup,
                      learner:Learner,
                      academicyear:AcademicYear,
                      hostelreceipttransaction:HostelReceiptTransaction]
    static constraints = {
        response nullable:true
        status_response nullable:true
    }

    static mapping = {
        response sqlType: "LONGTEXT"
        status_response sqlType: "LONGTEXT"
    }
}
