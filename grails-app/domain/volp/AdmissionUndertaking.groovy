package volp

class AdmissionUndertaking {
    String formname
    String formpath
    String formfilename
    boolean isactive
    static belongsTo=[organization:Organization]
    static mapping = {
        isactive defaultValue: true
    }
    static constraints = {
    }
}
