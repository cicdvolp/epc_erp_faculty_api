package volp

class TaskTransaction {
    Date action_date //Date-Time
    boolean iscurrent    //true
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,task:Task,taskstatus:TaskStatus,
                      actionby:Instructor, prev_task_transaction:TaskTransaction
    ]
    static constraints = {
        prev_task_transaction nullable:true
    }
}
