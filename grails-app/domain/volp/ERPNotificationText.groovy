package volp

class ERPNotificationText
{
    String subject
    String body
    String notificationfrom
    Date sentdate

    String file_name
    String file_path

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,sentby:Instructor,
                      erpcommunicationmode:ERPCommunicationMode,erpnotificationtotype:ERPNotificationToType,
                      smstemplatemaster : SMSTemplateMaster]

    static constraints = {
        subject nullable:true
        file_name nullable: true
        file_path nullable: true
        smstemplatemaster nullable: true
    }

}
