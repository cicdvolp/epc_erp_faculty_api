package volp

class ProgramYearSlotMaster {

    boolean islocked
    Date wef
    Date todate

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, programyear:ProgramYear,
                      academicyear:AcademicYear,semester:Semester,
                      slotmaster:SlotMaster, erptimetableversion:ERPTimeTableVersion]


    static constraints = {
        wef nullable : true
        todate nullable : true
    }

    static mapping = {
        islocked defaultValue: false
    }
}
