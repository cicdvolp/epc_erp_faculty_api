package volp

class EmployeeMonthlyAttendanceDetails {

    double weeklyoffandholiday
    double biometricpresentdays
    double absent
    double cl_count
    double ml_count
    double hpl_count
    double el_count
    double vacation_count
    double od_count
    double lwp_count
    double compoff_count
    double maternityleave_count
    double wfh
    double presentdays
    double salarydays
    String remark

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,
                      academicyear:AcademicYear,
                      erpmonth:ERPMonth,
                      erpcalenderyear:ERPCalendarYear,
                      department:Department,
                      organization:Organization]

    static constraints = {
        remark nullable : true
        department nullable:true
    }

}
