package volp

class ERPAttendanceReviewMaster {

    String reviewnumber
    double threshold
    Date fromdate
    Date date

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address


    static belongsTo=[organization          : Organization,
                      program               : Program,
                      year                  : Year,
                      academicyear          : AcademicYear,
                      semester              : Semester,
                      loadtypecategory      : LoadTypeCategory, ]

    static constraints = {
        organization nullable : true
        fromdate nullable : true
        program nullable : true
        year nullable : true
        academicyear nullable : true
        semester nullable : true
    }
    static mapping = {
        threshold defaultValue: 0.0
    }
}
