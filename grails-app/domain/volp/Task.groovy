package volp

class Task {
    String task_no   //Activity-01  Ex. NBA-01
    String title
    boolean isactive
    String description
    Date start_date
    Date expected_end_date
    Date completion_date

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo = [
            organization: Organization,
            assignedby  : Instructor,
            assignedto  : Instructor,
            taskstatus  : TaskStatus,
            taskactivity: TaskActivity,
            task        : Task,
            actionby    : Instructor
    ]
    static constraints = {
        task nullable: true
        completion_date nullable: true
    }
    static mapping = {
        isactive defaultValue: true
    }
}
