package volp

class Publication {

    String title
    Date date_of_publication
    String name_of_conference_journal
    double number_of_citation
    double index_value
    String isbn
    String filename
    String filepath
    String publicationlink
    boolean isfirstauthor
    String others_publication_index
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,instructor:Instructor,
                      organization:Organization,
                      publicationtype:PublicationType,
                      publicationcategory:PublicationCategory,
                      academicyear:AcademicYear, publicationindexType: PublicationIndexType]
    static constraints = {
        others_publication_index nullable: true
        isbn nullable: true
        index_value nullable: true
        publicationlink nullable: true
        filename nullable: true
        filepath nullable: true

        learner nullable: true
        instructor nullable: true
    }

}
