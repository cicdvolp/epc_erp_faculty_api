package volp

class StudentIdCardConfig {
    boolean iscollagerequired
    boolean istruestrequired
    String collage_Name
    String trust_name_color_code
    String collage_name_color_code
    String student_details_color_code
    String collage_trust_name
    String collage_address
    String trust_logo_filepath
    String trust_logo_filename
    String collage_logo_filepath
    String collage_logo_filename
    String principal_sign_filepath
    String principal_sign_filename

    String idcard_background_filename
    String idcard_background_filepath

    boolean displayfrontbarcode
    boolean displaybackbarcode
    boolean displayfrontqrcode
    boolean displaybackqrcode

    String idcard_background_filename_back
    String idcard_background_filepath_back

    String back_idcard_background_filename1
    String back_idcard_background_filepath1

    String back_idcard_background_filename2
    String back_idcard_background_filepath2


    String frontfield1name
    String frontfield1value

    String frontfield2name
    String frontfield2value

    String frontfield3name
    String frontfield3value

    String frontfield4name
    String frontfield4value

    String frontfield5name
    String frontfield5value

    String frontfield6name
    String frontfield6value

    String frontfield7name
    String frontfield7value

    String frontfield8name
    String frontfield8value

    String frontfield9name
    String frontfield9value

    String frontfield10name
    String frontfield10value

    String backfield1name
    String backfield1value

    String backfield2name
    String backfield2value

    String backfield3name
    String backfield3value

    String backfield4name
    String backfield4value

    String backfield5name
    String backfield5value

    String backfield6name
    String backfield6value

    String backfield7name
    String backfield7value

    String backfield8name
    String backfield8value

    String backfield9name
    String backfield9value

    String backfield10name
    String backfield10value

    String instructionheader
    String instructionheadercolor
    String instruction1
    String instruction2
    String instruction3
    String instruction4
    String instruction5

    String header1
    String header2
    String footer1
    String signingauthority
    String signingauthorityname

    boolean isbackcontainstable
    int noofcolumnsintable
    String columnnamecommaseperated
    String librarian_sign_filepath
    String librarian_sign_filename
    String librariansigningauthority
    String librariansigningauthorityname
    String departmentcolorstrip

    boolean displayicardvalidity
    String icardvalidityformat
    String displayposition

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, icardtemplatemaster: ICardTemplateMaster]

    static constraints = {
        collage_Name nullable :true
        collage_name_color_code nullable :true
        student_details_color_code nullable :true
        collage_trust_name nullable :true
        collage_address nullable :true
        trust_logo_filepath nullable :true
        trust_logo_filename nullable :true
        collage_logo_filepath nullable :true
        collage_logo_filename nullable :true
        principal_sign_filepath nullable :true
        principal_sign_filename nullable :true
        idcard_background_filename nullable :true
        idcard_background_filepath nullable :true
        idcard_background_filename_back nullable :true
        idcard_background_filepath_back nullable :true

        frontfield1name nullable :true
        frontfield2name nullable :true
        frontfield3name nullable :true
        frontfield4name nullable :true
        frontfield5name nullable :true
        frontfield1value nullable :true
        frontfield2value nullable :true
        frontfield3value nullable :true
        frontfield4value nullable :true
        frontfield5value nullable :true

        frontfield6name nullable :true
        frontfield7name nullable :true
        frontfield8name nullable :true
        frontfield9name nullable :true
        frontfield10name nullable :true
        frontfield6value nullable :true
        frontfield7value nullable :true
        frontfield8value nullable :true
        frontfield9value nullable :true
        frontfield10value nullable :true

        backfield1name nullable :true
        backfield2name nullable :true
        backfield3name nullable :true
        backfield4name nullable :true
        backfield5name nullable :true
        backfield1value nullable :true
        backfield2value nullable :true
        backfield3value nullable :true
        backfield4value nullable :true
        backfield5value nullable :true

        backfield6name nullable :true
        backfield7name nullable :true
        backfield8name nullable :true
        backfield9name nullable :true
        backfield10name nullable :true
        backfield6value nullable :true
        backfield7value nullable :true
        backfield8value nullable :true
        backfield9value nullable :true
        backfield10value nullable :true

        back_idcard_background_filename1 nullable :true
        back_idcard_background_filepath1 nullable :true

        back_idcard_background_filename2 nullable :true
        back_idcard_background_filepath2 nullable :true

        instruction1 nullable :true
        instruction2 nullable :true
        instruction3 nullable :true
        instruction4 nullable :true
        instruction5 nullable :true

        header1 nullable :true
        header2 nullable :true
        footer1 nullable :true
        signingauthority nullable :true
        signingauthorityname nullable :true
        trust_name_color_code nullable :true
        columnnamecommaseperated nullable :true

        librarian_sign_filepath nullable :true
        librarian_sign_filename nullable :true
        librariansigningauthority nullable :true
        librariansigningauthorityname nullable :true
        departmentcolorstrip nullable :true
        icardvalidityformat nullable :true
        displayposition nullable :true
        instructionheader nullable :true
        instructionheadercolor nullable :true
    }

    static mapping = {
        iscollagerequired defaultValue: true
        istruestrequired defaultValue: true
        displayfrontbarcode defaultValue: false
        displaybackbarcode defaultValue: false
        displayfrontqrcode defaultValue: false
        displaybackqrcode defaultValue: false
        isbackcontainstable defaultValue: false
        noofcolumnsintable defaultValue: 0
        displayicardvalidity defaultValue: false
    }
}
