package volp

class TaskStatus {
    int sequence_no
    String name     // TO DO  / IN PROCESS / DONE
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static belongsTo=[organization:Organization,taskactivity:TaskActivity,taskstatustype:TaskStatusType]
    static mapping = {
        isactive defaultValue: true
    }
}
