package volp

class ERPTimeTable
{
    int subslotnumber
    boolean isonlinesession
    String onlinesesionurl // recurring session link
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpday:ERPDay,
                      slot:Slot,
                      erpcourseofferingbatch:ERPCourseOfferingBatch,
                      divisionoffering:DivisionOffering,
                      loadtype:LoadType,
                      loadtypecategory:LoadTypeCategory,
                      erptimetableroomoffering:ERPTimeTableRoomOffering,
                      instructor:Instructor,
                      erptimetableversion:ERPTimeTableVersion,
                      academicyear:AcademicYear,
                      semester:Semester,
                      department:Department,
                      program:Program,
                      year:Year,
                      organization:Organization]
    static constraints = {
        erpcourseofferingbatch nullable : true
        instructor nullable : true
        divisionoffering nullable : true
        program nullable : true
        erptimetableversion nullable : true
        erptimetableroomoffering nullable : true
        loadtype nullable : true
        loadtypecategory nullable : true
        onlinesesionurl nullable : true
    }

    static mapping = {
        isonlinesession defaultValue: false
    }
}
