package volp
class ERPStudentFacultyFeedbackPerformanceIndexRawLogCum
{
    int number_of_students
    int expected_students

    double weighted_avg
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      academicyear:AcademicYear,
                      instructor:Instructor,
                      erpfeedbackquestionare:ERPFeedbackQuestionare,
                      erpstudentguardianinstructor:Instructor,erpfeedbackcategorytype:ERPFeedbackCategoryType]
    static constraints = {
        instructor nullable:true
        erpstudentguardianinstructor nullable:true
    }
}
