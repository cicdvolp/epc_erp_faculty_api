package volp

class InvAdvanceRequestEscalation {

    Date action_date
    double sanctioned_amount
    String remark

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization                      : Organization,
                      invadvancerequest                 : InvAdvanceRequest,
                      invfinanceapprovingauthoritylevel : InvFinanceApprovingAuthorityLevel,
                      invapprovalstatus                 : InvApprovalStatus,
                      actionby                          : Instructor]

    static constraints = {
        remark nullable:true
    }

    static mapping = {
        isactive defaultValue: true
        sanctioned_amount defaultvalue:0
    }
}
