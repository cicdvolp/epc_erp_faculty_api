package volp

class NotificationTransaction {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    Date sentdate
    String toreceiver

    String body
    String subject

    boolean isViewed

    static belongsTo=[notificationdetailmaster:NotificationDetailMaster,learner:Learner,instructor:Instructor]
    static constraints = {
        learner  nullable:true
        instructor  nullable:true
    }
}
