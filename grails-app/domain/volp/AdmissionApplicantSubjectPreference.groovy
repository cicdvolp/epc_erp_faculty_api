package volp

class AdmissionApplicantSubjectPreference {

    boolean is_subject_approved    //Used for SP clg
    boolean is_subject_deleted    //Used for SP clg

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    int preference_number
    static belongsTo=[
            organization:Organization,
            admissionapplicant:AdmissionApplicant,
            admissionapplication:AdmissionApplication,
            erpcourseoffering:ERPCourseOffering,
            learner:Learner,
            year:Year,
            academicyear:AcademicYear
    ]

    static constraints = {
        learner nullable: true
        admissionapplicant nullable: true
        admissionapplication nullable: true
    }
    static mapping = {
        is_subject_approved defaultValue:false
        is_subject_deleted defaultValue:false
        preference_number defaultValue: 0
    }
}
