package volp

class InstructorAcademics {


    boolean ishighestqualification
    String yearofpassing
    String university  //university or Board Name
    String institute_name  //university or Board Name
    String branch
    double cpi_marks   // CPI/Marks
    String phdtopic
    String filename
    String filepath
    static belongsTo=[organization:Organization,instructor:Instructor,instdegree:InstDegree,recclass:RecClass]

    static constraints = {
        filename nullable : true
        filepath nullable : true
        phdtopic nullable : true
            }
    static mapping = {
        ishighestqualification defaultValue: false
    }
}
