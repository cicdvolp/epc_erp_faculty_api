package volp

class ERPCourseResultReserveReasonMaster {

    String type
    String display_type
    String grade_name

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
        grade_name nullable:true
    }
}
