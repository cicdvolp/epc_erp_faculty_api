package volp

class MobileAppLink {

    String displayname
    String linkname
    boolean isactive
    int sort_order

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[usertype              : UserType,
                      organization          : Organization,
                      mobileAppLinkMaster   : MobileAppLinkMaster]

    static constraints = {
        sort_order nullable: false
    }
    static mapping = {
        isactive defaultValue: false
    }
}
