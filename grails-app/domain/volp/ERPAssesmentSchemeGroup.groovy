package volp

class ERPAssesmentSchemeGroup {

    int groupno
    String groupname
    double converted_marks
    double evaluated_marks
    double passing_marks
    String display_passing_marks

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpassesmentscheme:ERPAssesmentScheme]
    static hasMany = [erpassesmentschemedetails:ERPAssesmentSchemeDetails]
    static constraints = {
        groupname nullable : true
        display_passing_marks nullable : true
    }
    static mapping = {
        groupno defaultValue: 0
        passing_marks defaultValue: 0
    }
}
