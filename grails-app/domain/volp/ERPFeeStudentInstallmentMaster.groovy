package volp

class ERPFeeStudentInstallmentMaster {

    Date due_date
    int payment_sequence
    double percentage
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address


    static belongsTo=[academicyear:AcademicYear, programtype:ProgramType , year:Year , organization:Organization]
    static constraints = {
    }
}
