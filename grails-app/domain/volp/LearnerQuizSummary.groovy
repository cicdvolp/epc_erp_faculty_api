package volp

class LearnerQuizSummary {

    String ipaddress
    boolean issubmitted
    boolean issynchedtoexam
    int examdurationinminute
    Date start_date
    Date end_date
    String  extratime
    double marks
    boolean isstarted

    Date pause_date
    Date stop_date
    int  pause_duration

    int malpracticecount

    boolean isactive

    int extra_time_added_count
    String time_taken_for_exam

    String exam_ended_by

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[learner:Learner,quizoffering: QuizOffering,
                      erpcourse:ERPCourse,erpcourseoffering:ERPCourseOffering,
                      instructor:Instructor,erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      erpcourseofferingbatch:ERPCourseOfferingBatch, erpcourseofferingbatchlearner:ERPCourseOfferingBatchLearner,
                      quizexamstatus: QuizExamStatus, organization:Organization]

    static constraints = {
        instructor nullable:true
        erpcourseoffering nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpcourseofferingbatch nullable:true
        erpcourseofferingbatchlearner nullable:true
        time_taken_for_exam nullable:true
        extratime nullable:true
        end_date nullable:true
        pause_date nullable:true
        stop_date nullable:true
        exam_ended_by nullable:true
    }

    static mapping = {
        extra_time_added_count defaultValue: 0
        examdurationinminute defaultValue: 0
        pause_duration defaultValue: 0
        malpracticecount defaultValue: 0
        isactive defaultValue: true
        isstarted defaultValue: false
    }
}
