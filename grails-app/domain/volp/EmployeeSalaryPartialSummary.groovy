package volp

class EmployeeSalaryPartialSummary {

    String name
    String description
    double percentage
    Date generation_date
    int installmentno
    boolean is_salary_release
    boolean isfreezed

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[salarymonth:SalaryMonth, organization:Organization,
                      payrollemployeecategory:PayrollEmployeeCategory, payrolltmployeetype:PayrollEmployeeType]

    static constraints = {
        payrollemployeecategory nullable:true
        payrolltmployeetype nullable: true
        name nullable: true
        description nullable: true
    }

    static mapping={
        installmentno defaultValue:0
        is_salary_release defaultValue : false
        isfreezed defaultValue:false
    }
}
