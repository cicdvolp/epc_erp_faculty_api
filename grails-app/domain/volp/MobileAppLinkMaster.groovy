package volp

class MobileAppLinkMaster {

    String icon_path_name       //Leave
    String name                 //Leave
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[usertype : UserType]

    static constraints = {
        icon_path_name nullable: true
    }
    static mapping = {
        isactive defaultValue: false
    }
}
