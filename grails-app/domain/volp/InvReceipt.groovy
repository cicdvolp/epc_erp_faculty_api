package volp

class InvReceipt {

    int receipt_no
    Date receipt_date
    double amount
    String account_number
    String bank_name
    String bank_branch
    String ifsc_code
    String transaction_number
    String remark

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization      : Organization,
                      invquotation      : InvQuotation,
                      invmaterial       : InvMaterial,
                      invoice           : Invoice,
                      invvendor         : InvVendor,
                      invpaymentmethod  : InvPaymentMethod,
                      paymentby         : Instructor,
                      invpurchaseorder  : InvPurchaseOrder
    ]

    static constraints = {
        account_number bernullable:true
        bank_name nullable:true
        bank_branch nullable:true
        ifsc_code nullable:true
        transaction_number nullable:true
        invquotation nullable: true
        invmaterial nullable: true
        remark nullable:true
    }

    static mapping = {
        isactive defaultValue: true
    }
}
