package volp

class AcademicCalendar {
    String filename
    String filepath
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[academicyear:AcademicYear,semester:Semester,organization:Organization]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}
