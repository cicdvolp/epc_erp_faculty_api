package volp

class ERPCommonReceipt {
    String receipt_no     // should be linked with financial year
    double amount   //actual total amount of the receipt(current year+fees receivable paid) also includes fnexcess
    String remark
    Date date

    boolean iscancelled

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[year:Year,learner:Learner,organization:Organization]
    static hasMany = [erpcommonreceipttrasactions:ERPCommonReceiptTransaction]
    static constraints = {
        remark nullable: true
        year nullable: true
    }
    String toString()
    {
        receipt_no
    }
    static mapping = {
        iscancelled defaultValue: false
    }
}
