package volp

class ERPMCQQuestionBank {

    int qno
    String question_statement
    String question_file_path
    String question_file_name
    double weightage
    boolean isapproved
    boolean isalloted   //If question is alloted, then make it true
    int unitno
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpcourse:ERPCourse,reccourse:RecCourse,recdeptgroup:RecDeptGroup,
                      instructor:Instructor,difficultylevel:DifficultyLevel,erpmcqexamname:ERPMCQExamName,
                      erpmcqquestionbankfacultyallocation:ERPMCQQuestionBankFacultyAllocation]
    static mapping = {
        isapproved defaultValue: false
        weightage defaultValue:0.0
        unitno defaultValue:0
        isalloted defaultValue: false
    }
    static constraints = {
        question_statement nullable: true
        question_file_path nullable: true
        question_file_name nullable: true
        recdeptgroup nullable: true
        erpcourse nullable: true
        reccourse nullable: true
        erpmcqexamname nullable: true
        erpmcqquestionbankfacultyallocation nullable: true
    }
}
