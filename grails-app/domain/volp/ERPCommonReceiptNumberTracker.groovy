package volp

class ERPCommonReceiptNumberTracker {

    String year
    String number
    boolean isactive
    static belongsTo=[organization:Organization, erpreceipttypemaster:ERPReceiptTypeMaster, academicyear:AcademicYear]
    static constraints = {
    }
}
