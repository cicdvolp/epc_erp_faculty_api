package volp

class ERPTimeSheet {

    Date date
    String name
    String description

    String starttime
    String endtime
    String coursename

    int presentstudents
    int totalstudents
    boolean is_last_task_of_day

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, erpinstructortaskmaster:ERPInstructorTaskMaster, instructor : Instructor, year:Year , program:Program]

    static constraints = {
        description nullable : true
        year nullable : true
        program nullable : true
        coursename nullable : true
        erpinstructortaskmaster nullable : true
    }

    static mapping = {
        is_last_task_of_day defaultValue:false
        isactive defaultValue:true
        isdelete defaultValue:false
        presentstudents defaultValue:0
        totalstudents defaultValue:0
    }
}
