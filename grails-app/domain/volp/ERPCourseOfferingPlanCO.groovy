package volp

class ERPCourseOfferingPlanCO {

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, erpcourseofferingplan : ERPCourseOfferingPlan, erpco:ERPCO]

    static constraints = {
    }
}
