package volp

class CourseType {
	String type
    String displayname
    String ledger_display_name
    int sort_order

    boolean isactive
    String username    
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static hasMany = [loadtype: LoadType]    
    static constraints = {
    }
    static mapping = {
        isactive defaultValue:true
        sort_order defaultValue : 0
    }
    String toString(){
        displayname
    }
}
