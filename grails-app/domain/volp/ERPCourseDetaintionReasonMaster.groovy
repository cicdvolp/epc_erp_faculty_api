package volp

class ERPCourseDetaintionReasonMaster
{
    String type
    String gradename
    boolean is_fees_aplicable_in_reeexam
    boolean is_aplicable_for_reeexam

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]

    static constraints = {
        gradename nullable:true
    }

    static mapping = {
        is_fees_aplicable_in_reeexam defaultValue: false
        is_aplicable_for_reeexam defaultValue: false
    }
}
