package volp

class AdhockFeesLearnerAmount {

    double amount

    double paid_amount
    Date paid_date

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,
                      learner:Learner,
                      academicyear:AcademicYear,
                      semester:Semester,
                      erpcommonreceipt : ERPCommonReceipt,          // not be used
                      adhockfeespuropsemaster:AdhockFeesPuropseMaster,
                      adhockfeestype:AdhockFeesType]

    static hasMany = [erpcommonreceiptmany:ERPCommonReceipt]
    static constraints = {
        paid_date nullable:true
        erpcommonreceipt nullable:true
        adhockfeestype nullable:true
    }

    static mapping = {
        paid_amount defaultValue:0.0
        amount defaultValue:0.0
    }
}
