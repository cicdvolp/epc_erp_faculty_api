package volp

class ERPTimeTableVersionMaster {

    String version_name  //Ex. v1,v2,v3....
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
}
