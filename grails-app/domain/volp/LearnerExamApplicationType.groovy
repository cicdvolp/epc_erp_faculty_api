package volp

class LearnerExamApplicationType {

    String name     //reval, reexam
    String receipt_display_name     //reval, reexam
    double marks_veriation_percentage

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static mapping = {
        marks_veriation_percentage defaultValue: 0
    }
}
