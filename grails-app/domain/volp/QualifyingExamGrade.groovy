package volp

class QualifyingExamGrade {

    String name   //Distinction, First-Class
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static mapping = {
        isactive defaultValue:false
    }

    static constraints = {
    }
}
