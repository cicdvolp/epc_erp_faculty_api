package volp

class EmployeeSalaryMaster {
    int revision_no   //auto increment
    //Date applicable_date
    Date fromdate
    Date todate
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,paymatrix:PayMatrix,organization:Organization,
                      incrementyear:IncrementYear,incrementconfiguration:IncrementConfiguration,
                      paycommision:PayCommision, payrolldesignation:PayrollDesignation, payband:PayBand]

    static constraints = {
        todate nullable:true
        incrementconfiguration nullable:true
        incrementyear nullable:true
        paymatrix nullable: true
        payband nullable: true
        paycommision nullable: true
    }
}
