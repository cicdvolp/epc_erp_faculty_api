package volp

class ErpFeesCategoryWiseProgramIntake {
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    int total_intake

    static belongsTo=[academicyear:AcademicYear,year:Year,erpstudentfeescategory:ERPStudentFeesCategory,program:Program]

    static constraints = {
        academicyear nullable : true
    }

    static mapping = {
        total_intake defaultValue: 0
    }
}
