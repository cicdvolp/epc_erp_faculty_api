package volp

class EmployeeSalaryRegisterPartial {

    Date generation_date
    double working_days
    Date fromdate
    Date todate
    double gross_salary
    double gross_deductions
    double net_salary
    double percentage
    boolean isfreezed
    boolean is_salary_release

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,employeeSalarypartialsummary:EmployeeSalaryPartialSummary,
                      salarymonth:SalaryMonth,employeesalarymaster:EmployeeSalaryMaster,
                      organization:Organization, employeesalaryregister : EmployeeSalaryRegister]

    static constraints = {
        generation_date nullable: true
        working_days nullable: true
        fromdate nullable: true
        todate nullable: true
        gross_salary nullable: true
        gross_deductions nullable: true
        employeesalaryregister nullable: true
        employeesalarymaster nullable: true
    }

    static mapping={
        gross_salary defaultValue:0.0
        gross_deductions defaultValue:0.0
        net_salary defaultValue:0.0
        percentage defaultValue:0.0
        isfreezed defaultValue:false
        is_salary_release defaultValue:false
    }
}
