package volp

class SpotAdmissionDocuments {

    String name
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[applicationround : ApplicationRound]

    static constraints = {
        applicationround nullable : true
    }

    static mapping = {
        isactive defaultValue: false
    }
}
