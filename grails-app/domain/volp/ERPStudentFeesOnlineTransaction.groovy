package volp

class ERPStudentFeesOnlineTransaction {

    String erp_transaction_id   //gettime
    String transaction_message
    String transaction_error_message
    String request_details //First_amount_commission
    double amount  //amount sent from erp
    double received_amount  //amount from techprocess
    String bank_name
    String payment_remark
    Date request_transaction_date
    String response_transaction_date
    String paymentgateway_transaction_id
    String transaction_response_entire_url                      // for paytm - params response store
    String transaction_status_api__response_entire_url          // for paytm transaction status api call response store
    String bank_transaction_id
    String card_id
    String customer_id
    String utr_no
    String customer_name
    String mobile_number
    String account_number

    String transaction_proof_filename
    String transaction_proof_filepath

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,learner:Learner,academicyear:AcademicYear,transactionrequesttype:TransactionRequestType,
                      transactioncurrencycode:TransactionCurrencyCode,transactionorganizationonlineaccount:TransactionOrganizationOnlineAccount,
                      transactionstatus:TransactionStatus,erpstudentreceipt:ERPStudentReceipt,admissionapplicant:AdmissionApplicant,
                      erppaymentmode:ERPPaymentMode,admissionapplication:AdmissionApplication]
    static constraints = {
        learner nullable: true
        admissionapplicant nullable: true
        bank_transaction_id nullable:true
        card_id nullable:true
        customer_id nullable:true
        customer_name nullable:true
        utr_no nullable:true
        mobile_number nullable:true
        account_number nullable:true
        transaction_error_message nullable:true
        response_transaction_date nullable:true
        paymentgateway_transaction_id nullable:true
        transaction_response_entire_url nullable:true
        bank_name nullable:true
        payment_remark nullable:true
        erpstudentreceipt nullable:true
        erppaymentmode nullable:true
        transaction_proof_filename nullable:true
        transaction_proof_filepath nullable:true
        transactionorganizationonlineaccount nullable:true
        transaction_status_api__response_entire_url nullable:true
        admissionapplication nullable:true
    }
    static mapping = {
        transaction_status_api__response_entire_url sqlType: "LONGTEXT"
    }
}
