package volp
class ProgramTypeYear
{  //viit
    int code
    int sequence   //1:FY,2:SY,3:TY,4:B.Tech
    boolean islast     //for MCA : TY : Last Year

    boolean isapplicabletoadmission

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[programtype:ProgramType,year:Year,organization:Organization]
    static constraints = {
    }
    static mapping = {
        islast defaultValue: false
        isapplicabletoadmission defaultValue: false
    }
}