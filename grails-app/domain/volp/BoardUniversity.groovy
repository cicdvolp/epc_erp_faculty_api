package volp

class BoardUniversity {
    String name
    Boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization, program:Program, year:Year]

    static constraints = {
        program nullable: true
        year nullable: true
    }
}
