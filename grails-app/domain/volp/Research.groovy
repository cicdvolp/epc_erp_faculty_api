package volp

class Research {

    String title
    String description
    String source   //University/Agency/Company/Conference Name
    double amount
    String filename
    String filepath
    String publicationlink
    Date fromdate
    Date todate
    boolean is_principle_person
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization, researchtype: ResearchType, instructor:Instructor, researchstatus: ResearchStatus]
    static constraints = {
        filename nullable : true
        filepath nullable : true
        source nullable : true
        fromdate nullable : true
        todate nullable : true

    }

}
