package volp

class DTEAllotmentList {

    Date date_of_birth
    String mobile_number
    String gurdian_mobile
    double family_income
    String emailId
    String applicationid
    String name_of_candidate
    String name_as_per_adhar
    String merit_no
    String mht_cet_score
    String jee_marks
    String diploma_marks
    boolean isemailverified
    boolean ismobileverified
    boolean isdisabled
    boolean issonordaugherofdefence
    boolean isorphan
    boolean isfromcapround

    String mhtcetphysicspercentile
    String mhtcetchemistrypercentile
    String mhtcetmathspercentile
    String mhtcettotalpercentile

    boolean isjeemainsappeard
    String jeephysicspercentile
    String jeechemistrypercentile
    String jeemathspercentile
    String jeetotalpercentile

    boolean ispasseddiploma
    String diplomapercentage

    String filename
    String filepath

    boolean isduplicatedte

    String mother_first_name
    String parent_guardian_name
    String category_merit_no
    String all_india_merit_no
    boolean is_entry_for_spot_round

    static belongsTo=[erpround:ERPAdmissionRound,
                      organization:Organization,
                      gender:Gender,
                      category:ERPStudentAdmissionMainCategory,
                      erpshift:ERPShift,
                      erpseattype:ERPSeatType,
                      program:Program,
                      academicyear:AcademicYear,
                      year:Year,
                      erpdomacile:ERPDomacile,
                      erpadmissionquota:ERPAdmissionQuota,
                      erpnationality:ERPCountry,
                      programtype:ProgramType,
                      spotadmissioncategory:SpotAdmissionCategory,
                      erpcast:ERPCast,
                      erpsubcast:ERPSubCast]

    static constraints = {
        erpcast nullable : true
        erpsubcast nullable : true
        year nullable : true
        mht_cet_score nullable : true
        jee_marks nullable : true
        diploma_marks nullable : true
        erpdomacile nullable : true
        erpadmissionquota nullable : true
        date_of_birth nullable : true
        category_merit_no nullable : true
        merit_no nullable : true
        all_india_merit_no nullable : true
        mobile_number nullable : true
        gurdian_mobile  nullable : true
        emailId nullable : true
        erpshift nullable : true
        erpseattype nullable : true
        erpround nullable : true
        organization nullable : true
        erpadmissionquota nullable : true
        erpnationality nullable : true
        programtype nullable : true
        program nullable : true
        category nullable : true
        name_as_per_adhar nullable : true
        parent_guardian_name nullable : true
        mother_first_name nullable: true
        applicationid nullable: true
        name_of_candidate nullable: true
        merit_no nullable: true
        gender nullable: true
        academicyear nullable: true
        spotadmissioncategory nullable: true
        filename nullable: true
        filepath nullable: true
        mhtcetphysicspercentile nullable: true
        mhtcetchemistrypercentile nullable: true
        mhtcetmathspercentile nullable: true
        mhtcettotalpercentile nullable: true
        jeephysicspercentile nullable: true
        jeechemistrypercentile nullable: true
        jeemathspercentile nullable: true
        jeetotalpercentile nullable: true
        diplomapercentage nullable: true
    }

    static mapping = {
        family_income defaultValue: 0
        ismobileverified defaultValue: false
        isemailverified defaultValue: false
        is_entry_for_spot_round defaultValue: false
        isdisabled defaultValue: false
        issonordaugherofdefence defaultValue: false
        isorphan defaultValue: false
        isfromcapround defaultValue: false
        isjeemainsappeard defaultValue: false
        ispasseddiploma defaultValue: false
        isduplicatedte defaultValue: false
    }

    String toString(){
        applicationid
    }
}
