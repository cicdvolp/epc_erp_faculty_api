package volp

class GuardianMeetingSchedule {

    Date conductiondate
    String conductiontime
    String meetinglink
    String mom
    String agenda

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[guardian:Instructor,guardianmeetingschedulemaster:GuardianMeetingScheduleMaster,
                      organization:Organization, program:Program,year:Year,
                      academicyear:AcademicYear,semester: Semester]

    static constraints = {
        mom nullable : true
        agenda nullable : true
    }
}
