package volp

class ApplicationFormFees {

    double amount

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,categorytype:CategoryType, program:Program, year:Year, academicyear:AcademicYear]

    static mapping = {
        amount defaultValue:0
    }

    static constraints = {
    }
}
