package volp

class PayrollDepartment {
    String name
    boolean isactive
    int display_order

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, payrolldepartmentgroup : PayrollDepartmentGroup]

    static constraints = {
        payrolldepartmentgroup nullable:true
    }

    static mapping={
        sort display_order: "asc"
        isactive defaultValue:true
        display_order defaultValue:0
    }
}
