package volp

class AdmissionRoundHistory {
    int vaccancy
    int grant_vacancy
    int non_grant_vacancy
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,admissionstep:AdmissionStepMaster,categorytype:CategoryType,
                      academicyear:AcademicYear, program:Program,year:Year,erpadmissionmaincategory:ERPStudentAdmissionMainCategory]
    static constraints = {
    }
    static mapping = {
        grant_vacancy defaultValue: 0
        non_grant_vacancy defaultValue: 0
    }
}
