package volp

class RoleLink
{
    String vue_js_name
    String controller_name
    String action_name
    String link_name
    String link_displayname
    String link_description
    int sort_order
    boolean isrolelinkactive
    boolean isquicklink
    String linkiconimagefilename
    String linkiconimagepath
    String youtubelink
    String pdflink

    static belongsTo=[role:Role,organization:Organization]
    static constraints = {
        organization nullable: true
        linkiconimagefilename nullable: true
        linkiconimagepath nullable: true
        link_description nullable: true
        link_displayname nullable: true
        vue_js_name nullable: true
        controller_name nullable: true
        action_name nullable: true
        youtubelink nullable: true
        pdflink nullable: true
    }
    static mapping = {
        isquicklink defaultValue: false

    }
    String toString(){
        role?.roletype.type_displayname +" - "+role.role_displayname +" - "+ link_displayname
    }
}
