package volp

class AdmissionSchedule {

    Date start_date
    Date end_date
    int sortorder

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,program:Program,year:Year,academicyear:AcademicYear,admissionstepmaster:AdmissionStepMaster]

    static constraints = {
    }
}
