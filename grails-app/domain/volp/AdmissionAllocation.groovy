package volp

class AdmissionAllocation {

    String applicationid
    Date allotment_date

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[admissionyear:AcademicYear, allottedinstitute: Organization,
                      allottedprogram:Program, applicationround: ApplicationRound,
                      erpadmissionround:ERPAdmissionRound, erpshift:ERPShift,
                      erpseattype:ERPSeatType, erpstudentadmissionmainCategory: ERPStudentAdmissionMainCategory]


    static constraints = {
        erpshift nullable : true
        erpstudentadmissionmainCategory nullable : true
    }
}
