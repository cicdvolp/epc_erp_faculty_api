package volp

class LoadTypeCategory
{
    String type   //Theory,Lab,Tutorial,Minor Project,Major Project, Internship  category
    String display_name   //Theory,Lab,Tutorial,Minor Project,Major Project, Internship  category
    int sort_order   //Theory,Lab,Tutorial,Minor Project,Major Project, Internship  category

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]

    static constraints = {
    }

    static mapping = {
        sort_order defaultValue : 0
    }
}
