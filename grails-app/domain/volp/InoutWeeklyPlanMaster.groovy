package volp

class InoutWeeklyPlanMaster {
    String name //employeecode_firstname_ay_sem_serialnumber
    int seriannumber
    boolean isActive
    boolean isapproved
    boolean isapprovedbyhod
    boolean isapprovedbyest
    boolean isdeleted

    Date fromdate
    Date todate

    String remarkbyhod
    String remarkbyest

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,instructor:Instructor ,approvedby:Instructor ,approvedbyhod:Instructor ,approvedbyest:Instructor ,
                      academicyear:AcademicYear ,
                      semester:Semester]
    static constraints = {
        instructor nullable : true
        academicyear nullable : true
        semester nullable : true
        approvedby nullable : true
        approvedbyhod nullable : true
        approvedbyest nullable : true
        fromdate nullable : true
        todate nullable : true
        remarkbyhod nullable : true
        remarkbyest nullable : true
    }
    static mapping = {
        isactive defaultValue:true
        isdeleted defaultValue:false
        isapprovedbyhod defaultValue:false
        isapprovedbyest defaultValue:false
        isapproved defaultValue:false
    }
}
