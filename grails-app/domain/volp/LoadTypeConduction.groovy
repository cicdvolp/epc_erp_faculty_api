package volp

class LoadTypeConduction {

    int nooftimetableslotperweek
    int expectednooflecture
    int lecturedurationinminute

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, academicyear:AcademicYear,
                      semester:Semester, loadtype:LoadType]

    static mapping = {
        lecturedurationinminute defaultValue : 0
        expectednooflecture defaultValue : 0
    }

    static constraints = {
    }
}
