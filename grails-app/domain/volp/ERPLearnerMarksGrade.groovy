package volp

class ERPLearnerMarksGrade
{
    double total_marks    //actual_marks+grace_marks
    double actual_marks   //actual marks obtained by student

    double reevalmarks
    boolean isappliedforreeval
    boolean is_approved_by_faculty
    boolean issubmitted

    double reexammarks
    boolean isappliedforreexam
    boolean is_reexam_approved_by_faculty  //second examiner will approve
    boolean isreexam_submitted   //true mean submitted,, false means NOT submitted   first examiner will submit

    int gracemarks    //additional marks given to student who score less than 40
    boolean isAbsent
    boolean isfinalized   //HOD Grade Moderation
    boolean issummaryapproved

    String transactional_grade

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[program:Program,
                      programtype:ProgramType,
                      academicyear:AcademicYear,
                      semester:Semester,
                      divisionoffering:DivisionOffering,
                      organization:Organization,
                      approvedby:Instructor,
                      submittedby:Instructor,
                      moderatedby:Instructor,
                      learner:Learner,
                      instructor:Instructor,
                      erpgradedetails:ERPGradeDetails,
                      erpgradedetailsreval:ERPGradeDetails,
                      erpgradedetailsreexam:ERPGradeDetails,
                      erpgradedetailsordinance:ERPGradeDetails,
                      erpgradingpolicy:ERPGradingPolicy,
                      erpgrademasteroffering:ERPGradeMasterOffering,
                      erpcourseofferinglearner:ERPCourseOfferingLearner,
                      erpcourseofferinginstructor:ERPCourseOfferingInstructor,
                      erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      erpcurseofferingbatchlearner:ERPCourseOfferingBatchLearner,
                      erpexamcapcode:ERPExamCAPCode,
                      erpcourseoffering:ERPCourseOffering,erpexamconducttype:ERPExamConductType]
    static constraints = {
        erpgradedetails nullable:true
        erpcourseofferinglearner nullable:true
        erpcourseofferinginstructor nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpcurseofferingbatchlearner nullable:true
        erpexamcapcode nullable:true
        moderatedby nullable:true
        approvedby nullable:true
        learner nullable:true
        instructor nullable:true
        erpgradingpolicy nullable:true
        erpgrademasteroffering nullable:true
        erpcourseoffering nullable:true
        submittedby nullable:true
        program nullable:true
        programtype nullable:true
        academicyear nullable:true
        semester nullable:true
        divisionoffering nullable:true
        erpexamconducttype nullable:true
        erpgradedetailsreval nullable:true
        erpgradedetailsreexam nullable:true
        erpgradedetailsordinance nullable:true
        transactional_grade nullable:true
    }

    static mapping = {
        isAbsent defaultValue: false
        isfinalized defaultValue: false
        issubmitted defaultValue: false
        is_approved_by_faculty defaultValue: false
        issummaryapproved defaultValue: false
        gracemarks defaultValue: 0
        actual_marks defaultValue: 0
        isappliedforreeval defaultValue:false
        reevalmarks defaultValue : -999

        isappliedforreexam defaultValue:false
        reexammarks defaultValue : -999
        is_reexam_approved_by_faculty defaultValue: false
        isreexam_submitted defaultValue: false
    }
}
