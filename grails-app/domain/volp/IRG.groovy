package volp

class IRG {   // Internal Review Generation

    String title
    String description
    String source   //University/Agency/Company/Institute
    String filename
    String filepath
    double amount
    double institute_share
    boolean is_principle_person
    Date sanction_date
    Date completion_date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
        filename nullable : true
        filepath nullable : true
    }
    static mapping = {
        isactive defaultValue: true
    }
    static belongsTo=[organization:Organization,
                      instructor:Instructor,
                      academicyear:AcademicYear]

}
