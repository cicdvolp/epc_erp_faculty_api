package volp

class LeaveType {
    String	type //(FD_CL,HD_CL,EL,OD,ML,HPL,LWP,CO,Vacation,Maternity Leave)   Vaccation1,vaccation2,vaccation3

    String leave_type_color   // used for mobile app

    String	display_name
    String	short_code
    boolean isactive
    double aditionalinfo1  // ML To HPL
    int sortorder
    boolean is_backdated_leave_allowed
    int backdated_leave_variable
    boolean is_count_in_present_days

    boolean can_be_credited
    double min_leave_allowed
    double max_leave_allowed

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    String toString(){
        type
    }

    static belongsTo=[organization:Organization,vacationtype:VacationType, leaveexpirytype:LeaveExpiryType]
    static constraints = {
        vacationtype nullable: true
        short_code nullable: true
        leaveexpirytype nullable: true
    }
    static mapping = {
        isactive defaultValue: true
        aditionalinfo1 defaultValue: 0
        sortorder defaultValue: 0
        min_leave_allowed defaultValue: 0
        max_leave_allowed defaultValue: 0
        is_backdated_leave_allowed defaultValue: false
        can_be_credited defaultValue: false
        is_count_in_present_days defaultValue: false
        backdated_leave_variable defaultValue: 0
        leave_type_color defaultValue: "0xFF292DC2"
    }

}
