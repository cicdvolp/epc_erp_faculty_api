package volp

class Login {

    String username   //email
    String password
    String grno_empid
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    boolean isloginblocked
    String access_token

    String token
    Date token_creation_date
    Date token_expiry_date

    static belongsTo=[socialaccounttype:SocialAccountType,organization:Organization, person:Person]
    static hasMany = [roles: Role,usertype:UserType]
    static constraints = {
    	//username unique : true, blank: false
        username nullable:true
        socialaccounttype nullable:true
        grno_empid nullable:true
        organization nullable:true
        access_token nullable:true
        person nullable:true
        token nullable:true
        token_creation_date nullable:true
        token_expiry_date nullable:true
    }
}
