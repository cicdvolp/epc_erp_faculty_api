package volp

class TaskAttachment {
    String filename
    String filepath
    boolean isdeleted
    Date upload_date
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,task:Task, attachmentby:Instructor]
    static constraints = {
    }
    static mapping = {
        isdeleted defaultValue: false
    }
}

