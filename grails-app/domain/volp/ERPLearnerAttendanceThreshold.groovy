package volp

class ERPLearnerAttendanceThreshold {

    String reviewnumber

    boolean iscurrent
    boolean isattendanceprocessed
    double threshold
    Date todate
    Date fromdate

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[classteacher:Instructor,divisionoffering:DivisionOffering,academicyear:AcademicYear,
                      semester:Semester,loadtype:LoadTypeCategory, erpattendancereviewmaster:ERPAttendanceReviewMaster]
    static constraints = {
        fromdate nullable : true
        erpattendancereviewmaster nullable : true
        classteacher nullable : true
        divisionoffering nullable : true
    }
    static mapping = {
        isattendanceprocessed defaultValue: false
    }
}
