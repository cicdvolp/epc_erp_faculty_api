package volp

class SalaryComponentDetails {
    double value
    boolean is_formula_applicable
    boolean is_rule_applicable
    String formula
    String display_formula
    boolean is_percentage
    boolean isactive
    boolean applicable_to_handicap//for handicap person
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[salarycomponent:SalaryComponent,paycommision:PayCommision,salarymonth:SalaryMonth,
                      tomonth:SalaryMonth,organization:Organization, salaryrulemasteroffering:SalaryRuleMasterOffering]

    static constraints = {
        formula nullable:true
        display_formula nullable:true
        salaryrulemasteroffering nullable: true
        tomonth nullable: true
    }
    static mapping={
        applicable_to_handicap defaultValue:false
        is_rule_applicable defaultValue:false
    }

}
