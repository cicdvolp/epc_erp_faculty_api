package volp

class TaskComment {
    String comment
    Date comment_date
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,task:Task, commentby:Instructor]

    static constraints = {
    }
}
