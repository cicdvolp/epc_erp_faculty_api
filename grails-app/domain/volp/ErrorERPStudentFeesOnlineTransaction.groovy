package volp

class ErrorERPStudentFeesOnlineTransaction
{
    double amount

    String response                 // for paytm - params response store
    String status_response          // for paytm transaction status api call response store

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization, learner:Learner,admissionapplicant:AdmissionApplicant,
                      academicyear:AcademicYear, erpstudentfeesonlinetransaction:ERPStudentFeesOnlineTransaction]
    static constraints = {
        learner nullable: true
        admissionapplicant nullable:true
        response nullable:true
        status_response nullable:true
    }
    static mapping = {
        response sqlType: "LONGTEXT"
        status_response sqlType: "LONGTEXT"
    }
}