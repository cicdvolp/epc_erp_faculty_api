package volp

class SalaryComponent {
    String name// basic ,TA,DA,HRA
    String displayname // basic ,TA,DA,HRA
    String short_name // basic ,TA,DA,HRA
    int display_order//
    int display_order_for_report//
    boolean is_considered_in_employee_master
    boolean isactive

    boolean is_formula_applicable
    boolean is_rule_applicable
    boolean is_percentage
    boolean is_scaling_applicable


    boolean is_not_for_calculation

    int sequence_of_calculation

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[salarycomponenttype:SalaryComponentType,organization:Organization]

    static constraints = {
        short_name nullable : true
    }
    static mapping={
        is_considered_in_employee_master defaultValue:true
        isactive defaultValue:true
        is_scaling_applicable defaultValue:false
        is_rule_applicable defaultValue:false
        is_formula_applicable defaultValue:false
        is_percentage defaultValue:false
        is_not_for_calculation defaultValue:false
        sequence_of_calculation defaultValue:0
    }
}
