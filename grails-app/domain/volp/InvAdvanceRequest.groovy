package volp

class InvAdvanceRequest {

    double amount
    String purpose
    Date request_date
    String account_number
    String bank_name
    String bank_branch
    String ifsc_code
    boolean isapproved              // not in use
    double sanctioned_amount

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization      : Organization,
                      requestby         : Instructor,
                      invapprovalstatus : InvApprovalStatus,
                      invpaymentmethod  : InvPaymentMethod]

    static constraints = {
        account_number nullable:true
        bank_name nullable:true
        bank_branch nullable:true
        ifsc_code nullable:true
    }

    static mapping = {
        isactive defaultValue: true
        isapproved defaultValue: false
    }
}
