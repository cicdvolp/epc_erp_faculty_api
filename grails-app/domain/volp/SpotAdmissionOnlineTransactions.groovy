package volp

class SpotAdmissionOnlineTransactions {

    String receipt_no
    double receipt_amount
    String remark
    Date receipt_date
    boolean iscancelledreceipt
    String receipt_tid

    double amount
    double received_amount

    String erp_transaction_id
    String transaction_message
    String request_details
    String payment_remark
    Date request_transaction_date
    String response_transaction_date
    String paymentgateway_transaction_id
    String transaction_response_entire_url
    String bank_transaction_id
    String card_id
    String customer_id
    String utr_no
    String customer_name
    String mobile_number
    String account_number

    String transaction_proof_filename
    String transaction_proof_filepath

    double error_amount
    boolean iserrortransaction
    String transaction_error_message
    String bank_name

    String current_round_metrit_no

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[
            dteallotmentlist:DTEAllotmentList,
            academicyear:AcademicYear,
            transactionrequesttype:TransactionRequestType,
            transactioncurrencycode:TransactionCurrencyCode,
            transactionorganizationonlineaccount:TransactionOrganizationOnlineAccount,
            transactionstatus:TransactionStatus,
            erppaymentmode:ERPPaymentMode,
            applicationround:ApplicationRound]

    static constraints = {
        bank_transaction_id nullable:true
        card_id nullable:true
        customer_id nullable:true
        customer_name nullable:true
        utr_no nullable:true
        mobile_number nullable:true
        account_number nullable:true
        transaction_error_message nullable:true
        response_transaction_date nullable:true
        paymentgateway_transaction_id nullable:true
        transaction_response_entire_url nullable:true
        payment_remark nullable:true
        erppaymentmode nullable:true
        transaction_proof_filename nullable:true
        transaction_proof_filepath nullable:true
        transactionorganizationonlineaccount nullable:true
        bank_name nullable:true
        transaction_error_message nullable:true
        receipt_no nullable:true
        remark nullable:true
        receipt_date nullable:true
        receipt_tid nullable:true
        current_round_metrit_no nullable:true
    }

    static mapping = {
        error_amount defaultValue: 0
        receipt_amount defaultValue: 0
        received_amount defaultValue: 0
        iserrortransaction defaultValue: false
        iscancelledreceipt defaultValue: false
    }
}
