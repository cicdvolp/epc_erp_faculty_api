package volp

class ERPCourseInstructorAttendanceSlot {

    double period_number  //theory/practical/tutorial number
    Date execution_date
    boolean start_attendance

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      erpcourseofferingbatch:ERPCourseOfferingBatch,
                      erpcourseoffering:ERPCourseOffering,
                      slot :Slot,
                      divisionoffering:DivisionOffering,
                      instructor:Instructor,
                      academicyear:AcademicYear,
                      loadtypecategory:LoadTypeCategory,
                      semester:Semester,organization:Organization, erpcourseofferingplan:ERPCourseOfferingPlan]

    static constraints = {
        divisionoffering nullable: true
        erpcourseofferingplan nullable: true
    }

    static mapping = {
        start_attendance defaultValue: false
    }
}
