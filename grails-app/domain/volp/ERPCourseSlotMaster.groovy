package volp

class ERPCourseSlotMaster {

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, erpcourse:ERPCourse,
                      academicyear:AcademicYear,semester:Semester,
                      slotmaster:SlotMaster, department:Department]


    static constraints = {
    }
}
