package volp

class ERPHolidayDetails {

    Date holidaydate
    String remark
    boolean is_deleted
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,academicyear:AcademicYear,semester:Semester,
                      erpmonth:ERPMonth,erpday:ERPDay,erpcalendaryear:ERPCalendarYear,erpholiday:ERPHoliday]
    static constraints = {
        semester nullable:true
    }
    static mapping =
    {
        is_deleted defaultValue: false
    }
}
