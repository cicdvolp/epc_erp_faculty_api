package volp

class ERPGRNumberProgramCode
{
    String program_code    //grnumber
    String rollno_program_code   //rollnumber
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[program:Program , organization : Organization]
    static constraints = {
        rollno_program_code nullable : true
    }
    String toString(){
        program_code
    }
}
