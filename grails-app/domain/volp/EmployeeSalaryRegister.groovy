package volp

class EmployeeSalaryRegister {
    boolean is_salary_blocked
    boolean is_pay_slip_blocked
    Date generation_date
    double working_days
    Date fromdate
    Date todate
    double gross_salary
    double gross_deductions
    double net_salary
    double arrears
    boolean isarrears // if true then dont carry forward pick up from master
    String payscale
    String bankaccountnumber

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,salarymonth:SalaryMonth,employeesalarymaster:EmployeeSalaryMaster,organization:Organization,
                  payrolldesignation:PayrollDesignation]

    static constraints = {
    }
    static mapping={
        gross_salary defaultValue:0.0
        gross_deductions defaultValue:0.0
        net_salary defaultValue:0.0
        is_salary_blocked defaultValue: true
        is_pay_slip_blocked defaultValue: true
        arrears defaultValue: 0.0
        isarrears defaultValue:false
    }
}
