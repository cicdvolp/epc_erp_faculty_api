package volp

class GrayQuestCredential {

    String username
    String password
    String apikey
    String host_url

    boolean isenabled

    static belongsTo=[ organization:Organization]

    static constraints = {
    }

    static mapping = {
        isenabled defaultValue: false
    }
}
