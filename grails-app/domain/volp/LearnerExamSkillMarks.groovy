package volp

class LearnerExamSkillMarks {

    boolean isactive
    double marks
    double outofmarks

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, examskilloffering:ExamSkillOffering, learner:Learner, program:Program,programtype:ProgramType,
                      academicyear:AcademicYear,semester:Semester,divisionoffering:DivisionOffering,year:Year,erpgradedetails:ERPGradeDetails,
                      erpexamconducttype:ERPExamConductType]

    static constraints = {
    }

    static mapping = {
        isactive defaultValue: true
        marks defaultValue: 0
        outofmarks defaultValue: 0
    }
}
