package volp

class SpecialReservationType {

    String name   //Ward of Serviceman/Ward of Ex-Serviceman/Physically Challenged/Sports Played in Last Academic Year
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static constraints = {
    }
}
