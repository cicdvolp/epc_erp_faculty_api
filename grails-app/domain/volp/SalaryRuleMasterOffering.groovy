package volp

class SalaryRuleMasterOffering {

    double value1
    double value2
    double value3
    double value4
    double value5
    double value6
    double value7
    double value8
    double value9
    double value10

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, salaryrulemaster:SalaryRuleMaster]

    static constraints = {
    }

    static mapping={
        value1 defaultValue:-999999
        value2 defaultValue:-999999
        value3 defaultValue:-999999
        value4 defaultValue:-999999
        value5 defaultValue:-999999
        value6 defaultValue:-999999
        value7 defaultValue:-999999
        value8 defaultValue:-999999
        value9 defaultValue:-999999
        value10 defaultValue:-999999
    }
}
