package volp

class SMSTemplateMaster {

    String text
    String template_id

    String variable1
    String variable2
    String variable3
    String variable4
    String variable5
    String variable6
    String variable7
    String variable8
    String variable9
    String variable10

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo = [organization:Organization]

    static constraints = {
        template_id nullable : true
        variable1 nullable : true
        variable2 nullable : true
        variable3 nullable : true
        variable4 nullable : true
        variable5 nullable : true
        variable6 nullable : true
        variable7 nullable : true
        variable8 nullable : true
        variable9 nullable : true
        variable10 nullable : true
    }
    static mapping={
        isactive defaultValue:false
    }
}
