package volp

class ERPStudentFacultyFeedbackPerformanceIndexRawLog  //per feedback weighted average for subject and guardian feedback
{
    int number_of_students
    int expected_students

    double weighted_avg
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    //instructor->course
    //erpstudentguardianinstructor -> guardian
    static belongsTo=[organization:Organization,erpstudentfeedbackversion:ERPStudentFeedbackVersion,
                      academicyear:AcademicYear,semester:Semester,
                      instructor:Instructor,erpfeedbackquestionare:ERPFeedbackQuestionare,
                      erpstudentguardianinstructor:Instructor,erpfeedbackcategorytype:ERPFeedbackCategoryType]
    static constraints = {
        instructor nullable:true
        erpstudentguardianinstructor nullable:true
    }
}
