package volp

class EntranceDegree {

    String name
    boolean isactive
    boolean iscompulsory

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization : Organization]

    static constraints = {
        organization nullable:true
    }

    static mapping = {
        isactive defaultValue: true
        iscompulsory defaultValue: true
    }
}
