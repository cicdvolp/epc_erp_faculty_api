package volp

class  AdmissionApplicant {
    String applicationid
    String name_of_candidate
    String merit_no
    String mht_cet_score
    String jee_marks
    String diploma_marks

    String fullname_as_per_previous_marksheet
    String firstName
    String middleName
    String nameasperaadhar

    String lastName
    Date date_of_birth
    String father_first_name
    String mother_first_name
    double family_income

    String other_cast
    String other_subcast

    String passport_no
    Date passport_issue_date
    Date passport_valid_upto

    String visa_no
    String nature_of_visa
    Date visa_valid_upto
    Date visa_issue_date

    String resedential_permit_no
    Date resedential_permit_issue_date
    Date resedential_permit_valid_upto_date

    String fsisnumber

    boolean isapplicationsubmitted
    boolean isdocumentsubmitted
    boolean isphysicaldocumentsubmitted

    double applicable_fees
    boolean isebccandidate
    boolean isfeespaid
    boolean isofflinepayment

    Date application_submission_date
    String email
    String grno

    String mobile_number

    boolean is_signup_instruction_accepted
    boolean is_personal_details_completed
    boolean is_personal_details_freezed

    boolean is_belong_to_non_creamy_layer
    boolean is_domicile_of_institute_state
    boolean is_belong_to_special_reservation
    boolean is_last_examination_from_this_institute

    String sports_name
    double percentage_of_disability
    boolean is_passport_applicable

    String cast_validity_number
    String aadhar_card_number
    String voter_id
    String birth_place
    String birth_taluka
    String parent_guardian_full_name
    String parent_guardian_email
    String parent_guardian_mobile
    String place_of_passport_issue


    String number_of_entries_in_visa
    String place_of_visa_issue
    String university_letter_no
    Date university_letter_date
    String university_id_no
    String rp_no
    Date rp_validfrom
    Date rp_validtill
    String fsfs_id
    String photo_file_name
    String photo_file_path
    String sign_file_name
    String sign_file_path

    String creation_user_name
    String updation_user_name
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address


    static belongsTo=[organization      :Organization, erpround:ERPAdmissionRound, gender:Gender,
                      category          :ERPStudentAdmissionMainCategory, erpshift:ERPShift,
                      erpseattype       :ERPSeatType, program:Program, academicyear:AcademicYear,
                      admissionlogin    :AdmissionLogin, year:Year, admissionyear:AcademicYear,
                      erpdomacile       :ERPDomacile, erpadmissionquota:ERPAdmissionQuota, erpnationality:ERPCountry,
                      erpscholarshiptype:ERPScholarshipType, erpcast:ERPCast,
                      erpsubcast        :ERPSubCast, admissionstatus:AdmissionStatus,
                      erpstudentfeescategory:ERPStudentFeesCategory,
                      instructionmedium:InstructionMedium,
                      specialreservationtype:SpecialReservationType,
                      sportsplayinglevel:SportsPlayingLevel,
                      erpdistrict:ERPDistrict,
                      birthstate: ERPState,
                      birthcountry: ERPCountry,
                      mothertongue: ERPLanguage,
                      maritalstatus:MaritalStatus,occupation: ERPOccupation,
                      bloodgroup: ERPBloodGroup,
                      passporttype:PassportType,
                      passportcountry: ERPCountry
    ]

    static mapping = {
        isapplicationsubmitted defaultValue:false
        isdocumentsubmitted defaultValue:false
        isfeespaid defaultValue:false
        isofflinepayment defaultValue:false
        isphysicaldocumentsubmitted defaultValue:false
        is_signup_instruction_accepted defaultValue:false
        is_personal_details_completed defaultValue:false
        is_personal_details_freezed defaultValue:false
        is_belong_to_non_creamy_layer defaultValue:false
        is_domicile_of_institute_state defaultValue:false
        is_belong_to_special_reservation defaultValue:false
        is_passport_applicable defaultValue:false
        percentage_of_disability defaultValue:0
        is_last_examination_from_this_institute defaultValue:false
    }

    static constraints = {
        mobile_number nullable: true
        sports_name nullable: true
        cast_validity_number nullable: true
        aadhar_card_number nullable: true
        voter_id nullable: true
        birth_place nullable: true
        birth_taluka nullable: true
        parent_guardian_full_name nullable: true
        parent_guardian_email nullable: true
        parent_guardian_mobile nullable: true
        place_of_passport_issue nullable: true
        nature_of_visa nullable: true
        instructionmedium nullable: true
        specialreservationtype nullable: true
        sportsplayinglevel nullable: true
        erpdistrict nullable: true
        birthstate nullable: true
        birthcountry nullable: true
        mothertongue nullable: true
        maritalstatus nullable: true
        occupation nullable: true
        bloodgroup nullable: true
        passporttype nullable: true
        passportcountry nullable: true

        number_of_entries_in_visa nullable: true
        place_of_visa_issue nullable: true
        university_letter_no nullable: true
        university_letter_date nullable: true
        university_id_no nullable: true
        rp_no nullable: true
        rp_validfrom nullable: true
        rp_validtill nullable: true
        fsfs_id nullable: true
        photo_file_name nullable: true
        photo_file_path nullable: true
        sign_file_name nullable: true
        sign_file_path nullable: true

        erpdomacile nullable: true
        erpsubcast nullable: true
        email nullable:true
        grno nullable:true
        erpdomacile nullable: true
        passport_no nullable: true
        passport_valid_upto nullable: true
        passport_issue_date nullable: true
        visa_no nullable: true
        visa_valid_upto nullable: true
        visa_issue_date nullable: true
        fsisnumber nullable: true
        resedential_permit_no nullable: true
        resedential_permit_issue_date nullable: true
        resedential_permit_valid_upto_date nullable: true
        other_cast nullable: true
        other_subcast nullable: true
        erpcast nullable: true
        erpstudentfeescategory nullable: true
        application_submission_date nullable: true
        jee_marks nullable : true
        diploma_marks nullable : true
        erpscholarshiptype nullable: true
        mht_cet_score nullable: true
        erpshift nullable: true
        year nullable: true
        erpadmissionquota nullable: true
        admissionstatus nullable: true
        erpseattype nullable: true
        merit_no nullable: true
        category nullable: true
        academicyear nullable: true
        father_first_name nullable: true
        name_of_candidate nullable: true
        nameasperaadhar nullable: true
        erpround nullable: true
        gender nullable: true
        fullname_as_per_previous_marksheet nullable: true
        program nullable: true
        admissionyear nullable: true
        date_of_birth nullable: true
        mother_first_name nullable: true
        erpnationality nullable: true
        middleName nullable: true
    }

}
