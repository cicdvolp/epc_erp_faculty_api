package volp

class ERPSeatType {

    String type

    boolean isapplicabletoadmission
    int sort_order
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization, erpstudentadmissionmaincategory: ERPStudentAdmissionMainCategory]
    static constraints = {
        erpstudentadmissionmaincategory nullable : true
    }
    String toString(){
        type
    }

    static mapping = {
        isapplicabletoadmission defaultValue: false
        sort_order defaultValue:0
    }
}
