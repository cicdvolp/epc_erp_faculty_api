package volp

class SocialWelfareReceiptNumberTracking {
    String year
    String number
    boolean isactive
    static belongsTo=[organization:Organization, academicyear:AcademicYear]

    static constraints = {
        organization nullable: true
        academicyear nullable: true
    }

    static mapping = {
        isactive defaultValue: false
    }
}
