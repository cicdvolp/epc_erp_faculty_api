package volp

class JobsData {

    String title
    String company
    String location
    String description
    boolean isactive
    Date expirydate
    Date recruitmentdate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[recruitmenttype:RecruitmentType,tpoplacementtype:TPOPlacementType,instructor:Instructor,tpocompany:TPOCompany,tpo:TPO,jobapi:JobAPI]
    static constraints = {
    }
    //Alumni
    //PlacementAgency
}
