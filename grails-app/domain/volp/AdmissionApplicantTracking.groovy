package volp

class AdmissionApplicantTracking {

    Date action_date
    String remark

    Date application_received_date
    Date application_verifed_date

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization      :Organization, admissionapplicant:AdmissionApplicant,
                      admissionauthority:AdmissionAuthority, admissionstatus:AdmissionStatus,
                      instructor:Instructor]

    static constraints = {
        application_received_date nullable: true
        application_verifed_date nullable:true
        instructor nullable:true
    }
}
