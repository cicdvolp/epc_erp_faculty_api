package volp

class ERPDirectIcard {

    String grno
    String fullname
    String program    //Course
    String permanentaddress
    String mobileno
    Date dob
    String bloodgroup
    String admissionyear
    String validupto
    String phone
    String emergency_contact_person_name
    String emergency_contact_person_number
    String photopath
    String photofilename
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
        phone nullable :true
        photopath nullable :true
        photofilename nullable :true
        dob nullable :true
    }
}
