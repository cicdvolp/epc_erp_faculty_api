package volp

class ReceiptTransactions {
    String tid //chegueno//dd
    Date date
    String bank_name

    double amount

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[  learner : Learner ,  erppaymentmode:ERPPaymentMode,erpstudentreceipt:ERPStudentReceipt]

    static constraints = {
        tid nullable : true
        erpstudentreceipt nullable : true
        bank_name nullable : true
        learner nullable : true
    }
}
