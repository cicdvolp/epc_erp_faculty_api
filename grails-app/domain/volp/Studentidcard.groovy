package volp

class Studentidcard {

    String icardbarcodeFile
    Date validupto
    String icardPath
    String icardPhotoFile
    String icardSignatureFile
    String icardSignaturePath
    String parentSignatureFile
    String parentSignaturePath
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner: Learner,organization:Organization]
    static constraints = {
        icardPath nullable:true
        icardPhotoFile nullable:true
        icardSignatureFile nullable:true
        icardSignaturePath nullable:true
        icardbarcodeFile nullable:true
        parentSignatureFile nullable:true
        parentSignaturePath nullable:true
    }
}
