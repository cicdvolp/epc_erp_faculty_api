package volp

class AdmissionProcess {

    String link
    String filename
    String filepath
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,program:Program,year:Year,academicyear:AcademicYear]
    static constraints = {
        link nullable: true
        filename nullable: true
        filepath nullable: true
    }

}


