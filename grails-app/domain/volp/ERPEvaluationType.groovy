package volp

class ERPEvaluationType {
    String type  //(VIIT:FA/SA) VIT:null
    String displayname
    String ledgerdisplayname

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    String toString(){
        displayname
    }
}
