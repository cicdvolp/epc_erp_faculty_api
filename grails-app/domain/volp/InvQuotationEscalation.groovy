package volp

class InvQuotationEscalation {

    Date action_date
    String remark

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization                      : Organization,
                      invquotation                      : InvQuotation,
                      invpurchaserequisition            : InvPurchaseRequisition,
                      invfinanceapprovingauthoritylevel : InvFinanceApprovingAuthorityLevel,
                      invapprovalstatus                 : InvApprovalStatus,
                      actionby                          : Instructor]

    static constraints = {
        remark nullable : true
        actionby nullable : true
    }

    static mapping = {
        isactive defaultValue: true
    }
}
