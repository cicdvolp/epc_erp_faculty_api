package volp

class InoutMonthlyPlanDetails {

    String intime
    String outtime
    Date todaysdate
    int erpcalenderintegerday
    boolean isweeklyoff

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpday:ERPDay,
                      erpmonth:ERPMonth,
                      erpcalenderyear:ERPCalendarYear,
                      organization:Organization,
                      inoutweeklyplanmaster:InoutWeeklyPlanMaster]

    static constraints = {
        erpcalenderintegerday: 1..31
    }

    static mapping = {
        isweeklyoff defaultValue: false
    }
}
