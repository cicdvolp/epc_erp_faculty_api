package volp

class EmployeeSalaryMasterDetails {
    double value

    double formula_value
    boolean is_formula_applicable
    boolean is_rule_applicable
    String formula
    String display_formula
    boolean is_percentage

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,employeesalarymaster:EmployeeSalaryMaster,
                      salarycomponent:SalaryComponent,organization:Organization,
                      salaryrulemasteroffering:SalaryRuleMasterOffering]


    static constraints = {
        formula nullable:true
        display_formula nullable:true
        salaryrulemasteroffering nullable : true
    }

    static mapping={
        formula_value defaultValue:0
        is_formula_applicable defaultValue:false
        is_rule_applicable defaultValue:false
        is_percentage defaultValue:false
    }

}
