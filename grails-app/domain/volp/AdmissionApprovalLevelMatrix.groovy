package volp

class AdmissionApprovalLevelMatrix {
    boolean islast
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization, program:Program, year:Year,admissionlevel:AdmissionLevel]

    static constraints = {
    }
}
