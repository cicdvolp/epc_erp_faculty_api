package volp

class OutOfOfficeHours {
    String fromtime
    String totime
    Date date
    String reason
    static belongsTo=[
        instructor:Instructor,
        organization:Organization
    ]
    static hasMany = [employeeattendance: EmployeeAttendance]
    static constraints = {}
}
