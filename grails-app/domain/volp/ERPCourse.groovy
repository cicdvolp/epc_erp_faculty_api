package volp

class ERPCourse {
    String course_code
    String course_name
    String exam_course_code

    String courseAbbr
    boolean isDeleted
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,department:Department,courseowner:Instructor,year:Year,offeredbyprogram:Program]
    static hasMany = [bloomstaxonomy:BloomsTaxonomy,prerequisite:Prerequisite,equivalence:ERPCourse,program:Program]
    static constraints = {
        course_code unique:'organization'

        courseAbbr nullable : true
        courseowner nullable : true
        offeredbyprogram nullable : true
        exam_course_code nullable : true
    }
    static mapping = {
        isDeleted defaultValue: false
    }
    String toString(){
        courseAbbr
    }
}
