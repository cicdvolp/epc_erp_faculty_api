package volp

class InvoiceDetails {

    int quantity
    double cost_per_unit
    double total_cost
    int available_qty
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization  : Organization,
                      invoice       : Invoice,
                      invmaterial   : InvMaterial]

    static constraints = {}

    static mapping = {
        isactive defaultValue: true
        quantity  defaultvalue:0
        cost_per_unit defaultvalue:0
        total_cost defaultvalue:0
        avaialble_qty defaultvalue: 0
    }
}
