package volp

class ERPEMandateResponceDetails {

    String ref_id
    String msgId
    String status
    String error_code
    String error_message
    String customer_account_number
    String customer_ifsc_code
    String customer_name
    String customer_mobile_no
    String customer_email
    String customer_start_date
    String customer_expiry_date
    String customer_max_amount
    String channel
    String accounttype
    String filler1
    String filler2
    String filler3
    String filler4
    String filler5
    String filler6
    String filler7
    String filler8
    String filler9
    String filler10

    boolean isdefault
    boolean isclauseaccepted

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, learner : Learner]

    static constraints = {
        ref_id nullable : true
        msgId nullable : true
        status nullable : true
        error_code nullable : true
        error_message nullable : true
        channel nullable : true
        accounttype nullable : true
        filler1 nullable : true
        filler2 nullable : true
        filler3 nullable : true
        filler4 nullable : true
        filler5 nullable : true
        filler6 nullable : true
        filler7 nullable : true
        filler8 nullable : true
        filler9 nullable : true
        filler10 nullable : true
        customer_account_number nullable : true
        customer_ifsc_code nullable : true
        customer_name nullable : true
        customer_mobile_no nullable : true
        customer_email nullable : true
        customer_start_date nullable : true
        customer_expiry_date nullable : true
        customer_max_amount nullable : true
    }

    static mapping = {
        isclauseaccepted defaultValue: false
        isdefault defaultValue: false
    }
}
