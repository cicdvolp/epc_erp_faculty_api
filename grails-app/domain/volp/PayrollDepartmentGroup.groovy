package volp

class PayrollDepartmentGroup {

    String name
    boolean isactive
    int display_order

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static constraints = {
    }

    static mapping={
        isactive defaultValue:true
        display_order defaultValue:0
    }
}
