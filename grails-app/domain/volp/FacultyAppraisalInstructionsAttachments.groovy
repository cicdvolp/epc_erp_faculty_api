package volp

class FacultyAppraisalInstructionsAttachments {

    String file_path
    String file_name

    boolean isactive

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, academicyear:AcademicYear]

    static mapping = {
        isactive defaultValue: true
    }

    static constraints = { }
}
