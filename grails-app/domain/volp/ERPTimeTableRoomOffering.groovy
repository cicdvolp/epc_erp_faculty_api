package volp

class ERPTimeTableRoomOffering {
    boolean isavailableforall
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    String toString()
    {
        erproom.roomnumber + " : " + erproomtype.type
    }
    static belongsTo=[organization:Organization,erproom:ERPRoom,
                      erproomtype:ERPRoomType,
                      department:Department,
                      academicyear:AcademicYear,semester:Semester,
                      incharge:Instructor]

    static constraints = {
        incharge nullable : true
    }

    static mapping = {
        isavailableforall defaultValue: false
    }

}
