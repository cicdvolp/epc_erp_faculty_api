package volp

class ExamSkillMapping {

    boolean isactive
    double converted_marks

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,examskilloffering:ExamSkillOffering,
                      program:Program,year:Year,erpcourseoffering:ERPCourseOffering,
                      erpassesmentschemedetails: ERPAssesmentSchemeDetails, examskillmappingtype:ExamSkillMappingType,
                      academicyear:AcademicYear,semester:Semester]

    static constraints = {
        erpcourseoffering nullable: true
        erpassesmentschemedetails nullable: true
    }

    static mapping = {
        isactive defaultValue: true
        converted_marks defaultValue: 0
    }

}
