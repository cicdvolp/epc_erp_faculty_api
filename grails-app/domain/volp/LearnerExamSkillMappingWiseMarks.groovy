package volp

class LearnerExamSkillMappingWiseMarks {

    boolean isactive
    double marks

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,examskillmapping: ExamSkillMapping,learner:Learner, program:Program,programtype:ProgramType,
                      academicyear:AcademicYear,semester:Semester,divisionoffering:DivisionOffering,year:Year, erpexamconducttype:ERPExamConductType]

    static constraints = {
    }

    static mapping = {
        isactive defaultValue: true
        marks defaultValue: 0
    }
}
