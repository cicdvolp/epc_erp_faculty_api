package volp

class ERPMarksEntryCOEApproval {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    boolean isfinalized
    static belongsTo=[organization:Organization,erpcourseoffering:ERPCourseOffering,approvedby:Instructor]
    static constraints = {
        approvedby nullable:true
    }
}
