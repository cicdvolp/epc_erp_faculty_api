package volp

class NEFTAccountDetailsHostel {

    String bank_name
    String account_no
    String ifsc_code
    String branch
    String imps_code
    String account_name
    String branch_code
    String account_type
    String online_payment_account_no
    String hostel_challan_bank_name
    String hostel_challan_bank_logo
    String hostel_challan_bank_address

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organizationgroup : OrganizationGroup]

    static constraints = {
        online_payment_account_no nullable : true
        hostel_challan_bank_name nullable:true
        hostel_challan_bank_logo nullable:true
        hostel_challan_bank_address nullable:true
    }
}
