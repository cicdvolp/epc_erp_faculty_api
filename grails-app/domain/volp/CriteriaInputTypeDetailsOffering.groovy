package volp

class CriteriaInputTypeDetailsOffering {

    String field_name  // Table
    String field_value    //Comma separated for drop down
    int minwordcount
    int maxwordcount
    int columsequence
    boolean isoptional
    boolean isnotduplicateallow

    boolean is_range_applicable
    int minnumber //default 0
    int maxnumber
    Date mindate
    Date maxdate

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ academicyear:AcademicYear, organization:Organization,
                       criterionoffering:CriterionOffering, inputtype:InputType,
                       criterionfieldtype:CriterionFieldType, criteriainputtypeoffering:CriteriaInputTypeOffering]

    static constraints = {
        mindate nullable:true
        maxdate nullable:true
    }

    static mapping = {
        isoptional defaultValue: false
        isnotduplicateallow defaultValue: false
        is_range_applicable defaultValue: false
        maxnumber defaultValue:0
        minnumber defaultValue:0
    }
}
