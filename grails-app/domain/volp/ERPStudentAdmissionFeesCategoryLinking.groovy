package volp

class ERPStudentAdmissionFeesCategoryLinking {

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, year:Year, programtype:ProgramType,
                      erpstudentadmissionmaincategory:ERPStudentAdmissionMainCategory,
                      erpstudentfeescategory:ERPStudentFeesCategory,erpseattype:ERPSeatType,
                      erpscholarshiptype : ERPScholarshipType]

    static constraints = {
        programtype(unique: ['year', 'erpstudentadmissionmaincategory', 'erpstudentfeescategory', 'erpscholarshiptype'])
        erpseattype nullable : true
        erpscholarshiptype nullable : true
    }
}
