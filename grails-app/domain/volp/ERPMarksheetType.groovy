package volp

class ERPMarksheetType {

    String type
    String display_name
    String spi_action_name
    String result_action_name
    int sequence

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization]

    static constraints = {
        spi_action_name nullable:true
        result_action_name nullable:true
    }
}
