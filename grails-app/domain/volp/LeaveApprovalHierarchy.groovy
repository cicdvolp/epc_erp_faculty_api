package volp

class LeaveApprovalHierarchy {
    int level //(1,2,3)
    int approval_period_days
    boolean is_last_approval
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[leavetype:LeaveType,leaveapporvalauthority:LeaveApporvalAuthority,organization:Organization,
                      leaveapprovalhierarchymaster:LeaveApprovalHierarchyMaster]
    static mapping = {
        is_last_approval defaultValue: false
    }
    static constraints = {
        leaveapprovalhierarchymaster nullable: true
        leavetype nullable: true
    }
}
