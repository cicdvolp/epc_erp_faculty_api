package volp

class Slot {

    int slot_number
    String start_time
    String end_time

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[academicyear:AcademicYear,semester:Semester,slottype:SlotType,
                      organization:Organization,department:Department, slotmaster:SlotMaster]

    String toString()
    {
        start_time + " : " + end_time
    }
    static constraints = {
        department nullable : true
        slotmaster nullable : true
    }
}
