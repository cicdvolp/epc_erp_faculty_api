package volp

class ERPDocumentMasterAttachmentLinking {

    boolean is_compulsory
    boolean is_active

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, erpdocumentattachment: ERPDocumentAttachment,
                      erpdocumentmaster:ERPDocumentMaster]

    static mapping = {
        is_compulsory defaultValue: 0
        is_active defaultValue: 0
    }

    static constraints = {
    }
}
