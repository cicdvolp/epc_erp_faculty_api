package volp

class AdmissionApplicantDocuments {

    String documentpath
    String documentname
    String otherremark

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,admissionapplicant:AdmissionApplicant,admissionapplication:AdmissionApplication,
                      admissiondocuments:AdmissionDocuments,remark:Remark]


    static constraints = {
        remark nullable: true
        otherremark nullable: true
    }

}
