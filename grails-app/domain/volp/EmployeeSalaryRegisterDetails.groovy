package volp

class EmployeeSalaryRegisterDetails {
    double value
    double actual_value
    double arrears

    boolean is_rule_applicable

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,organization:Organization,
                      employeesalaryregister:EmployeeSalaryRegister,
                      employeesalarymaster:EmployeeSalaryMaster,salarycomponent:SalaryComponent,
                      salaryrulemasteroffering:SalaryRuleMasterOffering]

    static constraints = {
        salaryrulemasteroffering nullable : true
    }

    static mapping={
        is_rule_applicable defaultValue:false
        actual_value defaultValue:0
        arrears defaultValue:0
    }
}
