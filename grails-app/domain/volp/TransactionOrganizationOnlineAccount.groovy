package volp

class TransactionOrganizationOnlineAccount
{
    String merchant_code
    String merchant_key
    String merchant_iv
    String host // paytm
    boolean isactive
    String accounttype
    String online_payment_account_no

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    long orderid //ccavenue requires tid as well as orderid as unique use this as TransactionOrganizationOnlineAccount+orderid
    static belongsTo=[organization:Organization, erppaymentgatewaymaster:ERPPaymentGatewayMaster, programtype:ProgramType,
                      erpseattype:ERPSeatType, organizationgroup:OrganizationGroup]

    static constraints = {
        host nullable : true
        programtype nullable : true
        erpseattype nullable : true //for SP college Grant-NONgrant Payment gateway
        accounttype nullable : true //for SP college Grant-NONgrant Payment gateway
        online_payment_account_no nullable : true //for SP college Grant-NONgrant Payment gateway
        organizationgroup nullable : true //for SP college Grant-NONgrant Payment gateway
    }

    static mapping = {
        isactive defaultValue: false
        orderid defaultValue:1000
    }
}