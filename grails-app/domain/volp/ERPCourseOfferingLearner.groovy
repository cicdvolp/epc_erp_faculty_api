package volp

class ERPCourseOfferingLearner {
    boolean isfeespaid      //false:Not Paid    //true:Paid
    boolean iscancelled    //false:not cancelled    //true:cancelled

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[reregfees:ReRegFees,
                      erpcourseoffering:ERPCourseOffering,
                      learnerdivision:LearnerDivision,
                      erpexamconducttype:ERPExamConductType,
                      erpcourseofferinglearner:ERPCourseOfferingLearner,
                      program:Program,//learner program and not desh
                      academicyear:AcademicYear,
                      semester:Semester,
                      module:Module,
                      learner:Learner,
                      organization:Organization,
                      year:Year,
                      syllabuspattern:SyllabusPattern,
                      erpassesmentscheme:ERPAssesmentScheme]

    static constraints = {
        erpexamconducttype nullable:true
        erpcourseofferinglearner nullable:true
        erpexamconducttype nullable:true
        reregfees nullable:true
        program nullable:true
        academicyear nullable:true
        semester nullable:true
        module nullable:true
        learner nullable:true
        organization nullable:true
        year nullable:true
        syllabuspattern nullable:true
        erpassesmentscheme nullable:true
        learnerdivision nullable:true
    }

}