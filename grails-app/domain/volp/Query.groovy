package volp
class Query
{
    String ticketid   //Q-query_id
    String title
    String description
    String file_name
    String file_path
    Date post_date
    String email
    boolean ismailsent
    String url
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[usertype:UserType,querytype:QueryType,roletype:RoleType,login:Login,
                      currentquerystatus:QueryStatus,currentinstructor:Instructor,querylevel:QueryLevel,
                      organization:Organization]
    static constraints = {
        file_name nullable:true
        file_path nullable:true
        login nullable:true
        description nullable:true
        title nullable:true
        post_date nullable:true
        usertype nullable:true
        querytype nullable:true
        roletype nullable:true
        login nullable:true
        currentquerystatus nullable:true
        currentinstructor nullable:true
        organization nullable:true
        ticketid nullable:true   //
        url nullable:true
    }
    static mapping = {
        ismailsent defaultValue: false
    }
}
