package volp

class Medicalmaster {
    String name
    String displayname
    String description
    Boolean isactive
    String username
    Date creation_date
    String creation_ip_address
    Date updation_date
    String  updation_ip_address

    static belongsTo=[organization:Organization]
    static mapping = {
        isactive defaultValue: true
    }
    static constraints = {
    }
}
