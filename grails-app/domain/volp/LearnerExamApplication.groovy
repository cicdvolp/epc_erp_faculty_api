package volp

class LearnerExamApplication {

    Date application_date
    boolean isfeespaid
    double fees_paid_amount // fees for this course
    Date fees_paid_date

    boolean isrefundreceipt
    boolean iscancelled

    //String transaction_id
    //String transaction_details

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[learnerexamapplicationtype:LearnerExamApplicationType,
                      learner:Learner,
                      erpcourseoffering:ERPCourseOffering,
                      erpcourseofferinglearner:ERPCourseOfferingLearner,
                      academicyear:AcademicYear,
                      semester:Semester,
                      //erpbankname:ERPBankName,
                      //erppaymentmode:ERPPaymentMode,
                      acceptedby:Instructor,
                      organization:Organization,
                      erpcommonreceipt:ERPCommonReceipt]
    static hasMany = [erpcommonreceiptmany:ERPCommonReceipt]
    static constraints = {
        //transaction_id nullable : true
        //transaction_details nullable : true
        fees_paid_date nullable : true
        //erpbankname nullable : true
        //erppaymentmode nullable : true
        acceptedby nullable : true
        learnerexamapplicationtype nullable : true
        erpcommonreceipt nullable : true
        erpcourseoffering nullable : true
        erpcourseofferinglearner nullable : true
    }

    static mapping = {
        fees_paid_amount defaultValue: 0
        isfeespaid defaultValue: false
        isrefundreceipt defaultValue: false
        iscancelled defaultValue: false
    }
}
