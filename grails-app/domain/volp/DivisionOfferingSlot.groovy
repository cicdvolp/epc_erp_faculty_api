package volp

class DivisionOfferingSlot {

    boolean islocked

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, divisionoffering:DivisionOffering,
                      slotmaster:SlotMaster, erptimetableversion:ERPTimeTableVersion]


    static constraints = {
    }

    static mapping = {
        islocked defaultValue: false
    }
}
