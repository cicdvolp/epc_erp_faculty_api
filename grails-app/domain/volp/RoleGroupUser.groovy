package volp

class RoleGroupUser {

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[employeerolegroup: RoleGroup, instructor:Instructor, learner:Learner,
                      organization: Organization, department : Department,
                      program:Program, programyear:ProgramYear,
                      divisionoffering:DivisionOffering]

    static constraints = {
        instructor nullable:true
        learner nullable:true
        program nullable:true
        programyear nullable:true
        divisionoffering nullable:true
        department nullable:true
    }
}
