package volp

class ERPStudentFacultyFeedbackPerformanceIndexCategoryLogCum {

    int number_of_students
    int expected_stuents

    double category_avg
    double cummulative_outof
    double cummulative_performance_index  //percentage

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      academicyear:AcademicYear,
                      instructor:Instructor,
                      erpfeedbackcategory:ERPFeedbackCategory,
                      erpstudentguardianinstructor:Instructor,erpfeedbackcategorytype:ERPFeedbackCategoryType]

    static constraints = {
        instructor nullable:true
        erpstudentguardianinstructor nullable:true
    }

    static mapping = {
        category_avg defaultValue: 0
        cummulative_outof defaultValue: 0
        cummulative_performance_index defaultValue: 0
    }
}
