package volp

class ERPAdmissionQuota {

    String type
    boolean isapplicabletoadmission
    boolean isfeestypeapplicabletoquota

    String username
    Date creation_date
    Date updation_date
    boolean isForeignNational
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpadmissionquotagroup:ERPAdmissionQuotaGroup, organization:Organization, erpstudentfeescategory:ERPStudentFeesCategory]
    static constraints = {
        erpadmissionquotagroup nullable: true
        erpstudentfeescategory nullable: true
        organization nullable: true
    }
    static mapping = {
        isForeignNational defaultValue: false
        isapplicabletoadmission defaultValue: false
        isfeestypeapplicabletoquota defaultValue: false
    }
    String toString(){
        type
    }
}
