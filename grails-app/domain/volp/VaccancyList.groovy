package volp

class VaccancyList {

    int total
    int allotted
    Date lastallocation_date

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, program:Program,
                      year:Year, admissionyear:AcademicYear,
                      erpadmissionround:ERPAdmissionRound,
                      applicationround : ApplicationRound,
                      erpshift:ERPShift,
                      erpseattype:ERPSeatType,
                      erpstudentadmissionmainCategory: ERPStudentAdmissionMainCategory]

    static constraints = {
        erpshift nullable : true
        lastallocation_date nullable : true
        erpstudentadmissionmainCategory nullable : true
    }

    static mapping = {
        total defaultValue: 0
        allotted defaultValue: 0
    }

}
