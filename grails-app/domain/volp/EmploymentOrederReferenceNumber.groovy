package volp

class EmploymentOrederReferenceNumber {

    int nextavailableoutwardnumber
    String  organizationcode
    boolean isActive

    Date creation_date
    Date updation_date
    String creation_ip
    String updation_ip
    String creation_username
    String updation_username

    static belongsTo=[organization : Organization , academicyear : AcademicYear]

    static mapping = {
        isActive defaultValue:false
    }

    static constraints = {
        nextavailableoutwardnumber nullable:false
        organizationcode nullable:false
        organization nullable:true
        academicyear nullable:true
    }
}
