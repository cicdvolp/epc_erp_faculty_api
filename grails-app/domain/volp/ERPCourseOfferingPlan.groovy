package volp

class ERPCourseOfferingPlan {

    String problem_statement_actually_covered   //only for practical and tutorial
    String problem_statement
    int lecture_number_planned
    boolean isdone //true :completed false:not Completed
    Date plan_date
    Date execution_date
    String remark
    int no_lecture_per_plan

    boolean isapplicable
    boolean isextralecture

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      instructor:Instructor,erpcourseoffering:ERPCourseOffering,
                      erpcourse:ERPCourse,
                      erpcourseplan:ERPCoursePlan,academicyear:AcademicYear,semester:Semester,
                      loadtype:LoadTypeCategory,organization:Organization]

    static constraints = {
        problem_statement_actually_covered nullable:true
        plan_date nullable:true
        execution_date nullable:true
        remark nullable:true
        isdone nullable:true
        erpcourseplan nullable:true
        problem_statement nullable:true
    }

    static mapping = {
        isdone defaultValue: false
        isapplicable defaultValue: true
        isextralecture defaultValue: false
        no_lecture_per_plan defaultValue: 1
    }

    static hasMany = [erpcoursetopic:ERPCourseTopic]
}
