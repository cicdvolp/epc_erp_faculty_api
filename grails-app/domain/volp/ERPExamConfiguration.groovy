package volp

class ERPExamConfiguration {

    String name    //Ex. NBA,CODifficultyLevel
    String value   //Ex. Ex.Yes/No
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
}
