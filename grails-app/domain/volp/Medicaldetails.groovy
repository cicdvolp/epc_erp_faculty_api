package volp

class Medicaldetails {

    boolean iscompliance
    String  medicalproofilename
    String  medicalprooffilepath
    String username
    Date creation_date
    String creation_ip_address
    Date updation_date
    String updation_ip_address

    static belongsTo=[instructor:Instructor,learner:Learner,medicalmaster:Medicalmaster,organization:Organization]
    static mapping = {
        iscompliance defaultValue: false
    }
    static constraints = {
        instructor nullable:true
        learner nullable:true
        medicalproofilename nullable:true
        medicalprooffilepath nullable:true
    }
}
