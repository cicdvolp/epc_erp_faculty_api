package volp

class WhatsAppTemplate {
    String name
    String whatsapptemplate_id
    String whatsapptemplatemessage_body
    boolean isapprovedby_whatsapp
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    static mapping = {
        isapprovedby_whatsapp defaultValue: false
    }
}
