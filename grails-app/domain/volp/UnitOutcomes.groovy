package volp

class UnitOutcomes {

    String uo_code
    String uo_statement
    boolean is_deleted

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpcourse:ERPCourse, erpcourseunit: ERPCourseUnit, revisionyear:AcademicYear]

    static constraints = {
    }

    static mapping={
        is_deleted defaultValue:false
    }
}
