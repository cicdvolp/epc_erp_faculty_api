package volp

class ActivityAcademicYearSem {

    Date startdate
    Date enddate
    boolean iscurrent

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[activitymaster : ActivityMaster, academicyear:AcademicYear, semester:Semester, organization : Organization]

    static constraints = {
        activitymaster(unique: ['academicyear', 'semester'])
    }

    static mapping = {
        iscurrent defaultValue: false
    }
}
