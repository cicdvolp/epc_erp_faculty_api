package volp

class ERPFAQMaterial {

    String filename  // video url
    String filepath

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, roletype:RoleType, erpfaq:ERPFAQ, erpfaqmaterialtype:ERPFAQMaterialType]

    static constraints = {
        filepath nullable:true
    }
}
