package volp

class ERPDocumentMaster {

    String document_name
    String abbreviation
    double fees
    int total_no_of_attchment
    int min_no_of_attchment
    boolean student_section_intervention
    boolean is_active
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpdocumentrequesttype:ERPDocumentRequestType,
                      erpdocumentapprovaltype:ERPDocumentApprovalType]
    static mapping = {
        total_no_of_attchment defaultValue: 0
        min_no_of_attchment defaultValue: 0
        fees defaultValue: 0
        is_active defaultValue: 1
    }
    static constraints = {
        abbreviation nullable:true
        student_section_intervention nullable: true
    }
}
