package volp

class Department {

    String official_department_email
    String name
    String abbrivation
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address    
	static belongsTo=[hod:Instructor,organization:Organization,departmenttype:DepartmentType,stream:Stream,
                      examcoordinator : Instructor, academiccoordinator : Instructor,
                      timetablecoordinator : Instructor , feedbackcoordinator : Instructor]
    static constraints = {
        hod nullable: true
        examcoordinator nullable: true
        abbrivation nullable: true
        timetablecoordinator nullable: true
        feedbackcoordinator nullable: true
        stream nullable: true
        academiccoordinator nullable: true
        official_department_email nullable: true
    }
    String toString(){
        name
    }
}
