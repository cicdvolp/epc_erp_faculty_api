package volp

class ERPRealTimeTT {

    int subslotnumber
    boolean isonlinesession
    String onlinesesionurl
    Date date
    boolean isapproved
    String start_time
    String end_time

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erptimetable:ERPTimeTable,
                      erptimetableversion:ERPTimeTableVersion,
                      erpcourseofferingbatch:ERPCourseOfferingBatch,
                      loadtype:LoadType,
                      loadtypecategory:LoadTypeCategory,
                      erptimetableroomoffering:ERPTimeTableRoomOffering,
                      instructor:Instructor,
                      organization:Organization]

    static constraints = {
        erpcourseofferingbatch nullable : true
        instructor nullable : true
        erptimetable nullable : true
        loadtype nullable : true
        loadtypecategory nullable : true
        erptimetableversion nullable : true
        onlinesesionurl nullable : true
        erptimetableroomoffering nullable : true
    }

    static mapping = {
        isapproved defaultValue: false
        isonlinesession defaultValue: false
        subslotnumber defaultValue: 0
    }
}
