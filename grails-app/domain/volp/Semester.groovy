package volp

class Semester {

	String sem

    String display_name         // used in attempt certificate form for VIT

    int sequence
    boolean is_provision_sem

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static constraints = {
        display_name nullable: true
    }

    static mapping = {
        sequence defaultValue: 0
        is_provision_sem defaultValue: false
    }

    String toString(){
        sem
    }
}
