package volp

class ERPStudentFacultyFeedbackPerformanceIndexCummulative
{
    int number_of_students
    int expected_students

    double cummulative_weighted_avg_total
    double cummulative_outof
    double cummulative_performance_index  //percentage
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      academicyear:AcademicYear,
                      instructor:Instructor,erpstudentguardianinstructor:Instructor,
                      erpfeedbackcategorytype:ERPFeedbackCategoryType]
    static constraints = {
        instructor nullable:true
        erpstudentguardianinstructor nullable:true
    }
}
