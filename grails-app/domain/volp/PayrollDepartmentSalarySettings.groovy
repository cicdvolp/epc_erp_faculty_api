package volp

class PayrollDepartmentSalarySettings {
    boolean is_salary_blocked
    boolean is_pay_slip_blocked

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
   static belongsTo=[payrolldepartment:PayrollDepartment,salarymonth:SalaryMonth,organization:Organization]
    static constraints = {
    }
    static mapping={
        is_salary_blocked defaultValue: true
        is_pay_slip_blocked defaultValue: true
    }
}
