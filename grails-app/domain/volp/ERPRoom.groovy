package volp

class ERPRoom {
    boolean iscommonresourse
    String room_description

    String roomnumber
    String roomname
    String username
    boolean isActive
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erproomtype:ERPRoomType, incharge:Instructor]
    static constraints = {
        erproomtype nullable : true
        incharge nullable : true
        room_description nullable : true
    }

    static mapping = {
        iscommonresourse defaultValue: false
    }

    String toString(){
        roomnumber
    }
}
