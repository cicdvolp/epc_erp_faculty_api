package volp

class ERPForeignNationalFeesStudentMaster {
    //total fees payable is -> total_fees + fees_receivable_due (current + previous)
    double total_fees               //for current year
    double paid_til_now             //in current year
    double fees_receivable_due    ///from previous year
    double fees_receivable_paid
    Date cancellation_date
    String cancellation_remark

    double feesconcession  //not included in paid_till_now
    String concessionremark

    double student_fee_due
    double total_fee_received_receipt

    //Fees carry forwarded at the end of year
    double fees_carryforwarded_to_nextyear    //from this year to next year
    boolean isfullypaid
    boolean ispartialpaid

    // fees_payable = totalfees (from  ERPFeesStructureMaster table) - feesconcession + fees_receivable_due
    // fees_due : fees_payable - (paid_till_now+fees_receivable_paid)
    // fees_carryforwarded_to_nextyear = fees_payable - (paid_till_now+fees_receivable_paid)

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[feescurrencytype:FeesCurrencyType,erpstudentfeescategorylinking:ERPStudentFeesCategoryLinking,
                      learner:Learner,organization:Organization,financialyear:AcademicYear,
                      erpfeesdepositetype:ERPFeesDepositeType,erpstudentfeescategory:ERPStudentFeesCategory,
                      feesprogramtype:FeesProgramType,year:Year,erpstudenttype:ERPStudentType,erpcountry:ERPCountry]
    static hasMany = [erpstudentreceipt:ERPStudentReceipt]

    static constraints = {
        cancellation_date nullable: true
        cancellation_remark nullable: true
        concessionremark nullable: true
    }

    static mapping = {
        total_fee_received_receipt defaultValue: 0
        student_fee_due defaultValue: 0
        feesconcession defaultValue: 0
        fees_carryforwarded_to_nextyear defaultValue: 0
        ispartialpaid defaultValue: false
        isfullypaid defaultValue: false
    }
}
