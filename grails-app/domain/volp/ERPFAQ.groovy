package volp

class ERPFAQ {

    String question_no
    String question
    String text_answer
    int displayorder

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, roletype:RoleType]

    static constraints = {
        text_answer nullable:true
    }

    static mapping = {
        displayorder defaultValue: 0
    }

}
