package volp

class ERPCoursePlanProblemStatementAllocation
{
    Date allocationdate
    Date checkdate
    Date deadlinedate
    Date studentsubmisiondate
    String answer_file_path   //-> /asportal/ay/sem/grno/course/
    String answer_file_name   //  filename-> id.ext
    double maxmarks
    double obtainedmarks
    String remark
    double plagarisum_percentage
    boolean is_display_mark_to_student

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      erpcourseofferingbatchlearner:ERPCourseOfferingBatchLearner,
                      erpcourseplanproblemstatements:ERPCoursePlanProblemStatements,
                      erpcourseplan:ERPCoursePlan,erpcourse:ERPCourse,loadtype:LoadTypeCategory,
                      instructor:Instructor,erpcourseoffering:ERPCourseOffering,organization:Organization]
    static hasMany = [erplearnerlabdetailmarks:ERPLearnerLabDetailMarks]
    static constraints = {
        deadlinedate nullable:true
        studentsubmisiondate nullable:true
        answer_file_path nullable:true
        answer_file_name nullable:true
        remark nullable:true
        checkdate nullable:true
    }
    static mapping = {
        obtainedmarks defaultValue: 0
        plagarisum_percentage defaultValue: 0
        is_display_mark_to_student defaultValue: true
    }
}
