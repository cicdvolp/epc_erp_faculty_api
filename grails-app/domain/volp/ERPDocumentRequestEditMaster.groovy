package volp

class ERPDocumentRequestEditMaster {
    String residential_permit_no
    Date residential_permit_validity_date
    String unique_id_no
    String passport_no
    Date passport_issued_date
    Date passport_validity_date
    String visa_no
    String visa_type
    Date visa_issued_date
    Date visa_validity_date
    String regular_or_backlog_student
    String examination_date_month
    String result_declaration_date_month
    String behavior_of_student
    String recommended_for_extension_of_visa

    Date summer_vacation_start_date
    Date summer_vacation_end_date
    String summer_vacation_visiting_place
    Date summer_vacation_visiting_start_date
    Date summer_vacation_visiting_end_date

    int number_of_backlog
    String backlog_semester
    String backlog_academic_year
    Date backlog_exam_date
    Date backlog_result_date

    String caste_certificate_outword_no
    Date caste_certificate_issued_date
    String caste_certificate_issued_by
    String caste

    Date final_year_exam_date
    Date final_year_result_date

    String student_current_year
    String student_admission_year
    String student_admission_academic_year

    String to_academic_year

    String stud_fy_attendance
    String stud_sy_attendance
    String stud_ty_attendance
    String stud_btech_attendance
    String stud_fy_result
    String stud_sy_result
    String stud_ty_result
    String stud_btech_result
    String stud_fy_academicyear
    String stud_sy_academicyear
    String stud_ty_academicyear
    String stud_btech_academicyear

    String semester_wise_academic_year_1
    String semester_wise_academic_year_2
    String semester_wise_academic_year_3
    String semester_wise_academic_year_4
    String semester_wise_academic_year_5
    String semester_wise_academic_year_6
    String semester_wise_academic_year_7
    String semester_wise_academic_year_8

    String document_retension_list
    String bonafide_issued_for
    double cpi
    Date admissioncancelmonth
    String birthdate
    int document_outword_number

    String lcno
    String spgrno
   // String pupil_mother_first_name
   // String pupil_birth_place

    //Date  pupil_admission_date
    //String nationality
    String religion_category
    String last_institute_attended
    //String date_of_admission
    String progress
    String conduct
    String last_examinationpassed
    String reasonsfor_leaving_college
    Date dateof_leavingcollege

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,academicyear:AcademicYear,learner:Learner,instructor:Instructor,
                      erpdocumentmaster:ERPDocumentMaster,erpdocumentrequesttransaction:ERPDocumentRequestTransaction]

    static constraints = {
        learner nullable: true
        instructor nullable: true
        residential_permit_no nullable : true
        residential_permit_validity_date nullable: true
        unique_id_no nullable: true
        passport_no nullable: true
        passport_issued_date nullable: true
        birthdate nullable: true
        passport_validity_date nullable: true
        visa_no nullable: true
        visa_type nullable: true
        visa_issued_date nullable: true
        visa_validity_date nullable: true
        regular_or_backlog_student nullable: true
        examination_date_month nullable: true
        admissioncancelmonth nullable: true
        result_declaration_date_month nullable: true
        behavior_of_student nullable: true
        recommended_for_extension_of_visa nullable: true
        summer_vacation_start_date nullable: true
        summer_vacation_end_date nullable: true
        summer_vacation_visiting_place nullable:true
        summer_vacation_visiting_start_date nullable: true
        summer_vacation_visiting_end_date nullable: true
        backlog_exam_date nullable: true
        backlog_result_date nullable: true
        caste_certificate_outword_no nullable: true
        caste_certificate_issued_date nullable: true
        caste_certificate_issued_by nullable: true
        caste nullable: true
        final_year_exam_date nullable: true
        final_year_result_date nullable: true
        student_current_year nullable: true
        student_admission_year nullable: true
        student_admission_academic_year nullable: true
        number_of_backlog nullable: true
        backlog_semester nullable: true
        backlog_academic_year nullable: true
        to_academic_year nullable: true
        stud_fy_attendance nullable: true
        stud_sy_attendance nullable: true
        stud_ty_attendance nullable: true
        stud_btech_attendance nullable: true
        stud_fy_result nullable: true
        stud_sy_result nullable: true
        stud_ty_result nullable: true
        stud_btech_result nullable: true
        stud_fy_academicyear nullable: true
        stud_sy_academicyear nullable: true
        stud_ty_academicyear nullable: true
        stud_btech_academicyear nullable: true

        semester_wise_academic_year_1 nullable: true
        semester_wise_academic_year_2 nullable: true
        semester_wise_academic_year_3 nullable: true
        semester_wise_academic_year_4 nullable: true
        semester_wise_academic_year_5 nullable: true
        semester_wise_academic_year_6 nullable: true
        semester_wise_academic_year_7 nullable: true
        semester_wise_academic_year_8 nullable: true

        document_retension_list nullable: true

        bonafide_issued_for nullable: true


        lcno nullable: true
        spgrno nullable: true
        religion_category nullable: true
        last_institute_attended nullable: true
        progress nullable: true
        conduct nullable: true
        last_examinationpassed nullable: true
        reasonsfor_leaving_college nullable: true
        dateof_leavingcollege nullable: true




    }

    static mapping = {
        cpi defaultValue:0
    }
}
