package volp

class ERPLearnerMarkSheet
{
    //Provisional  After Revaluation    After Re-Exam      for-VIIT
    double current_sem_credits_registered  //new field
    double current_sem_credits_earned //new field
    double current_sem_grade_points //new field

    double cummulative_credits_registered  //new field
    double cummulative_credits_earned  //new field
    double cummulative_grade_points //new field

    String monthofexam
    String yearofexam
    String resultdate
    Date cummulativeresultasondate
    String printdate
    int resultversion
    double spi
    double cpi
    boolean islast
    int marksheetserialnumber
    boolean islastcpi
    String pdffilepath
    String pdffilename
    String erpclassofdegree

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    boolean isregularlast
    boolean isrevallast
    boolean isreexamlast
    boolean isbackloglast
    boolean isordinancelast

    boolean isresultdeclare
    Date resultdeclarationdate

    boolean  islocked

    static belongsTo=[academicyear:AcademicYear,semester:Semester, backlogsemester:Semester,
                      year:Year,organization:Organization,
                      learner:Learner,program:Program,programtype:ProgramType,
                      erplearnercummulativeresultstatus:ERPLearnerCummulativeResultStatus,erpexamconducttype:ERPExamConductType]
    static hasMany = [erplearnergraderesult:ERPLearnerGradeResult]
    static constraints = {
        pdffilepath nullable:true
        pdffilename nullable: true
        printdate nullable: true
        program nullable: true
        programtype nullable: true
        erplearnercummulativeresultstatus nullable: true
        erpexamconducttype nullable: true
        resultdeclarationdate nullable: true
        backlogsemester nullable: true
        erpclassofdegree nullable: true
    }

    static mapping = {
        isregularlast defaultValue:false
        isrevallast defaultValue:false
        isreexamlast defaultValue:false
        isbackloglast defaultValue:false
        isordinancelast defaultValue:false
        isresultdeclare defaultValue:false
        islocked defaultValue:false
    }
}
