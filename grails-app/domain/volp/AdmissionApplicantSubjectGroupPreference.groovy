package volp

class AdmissionApplicantSubjectGroupPreference {
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    int preference_number

    static belongsTo=[organization:Organization,admissionapplicant:AdmissionApplicant,admissionapplication:AdmissionApplication,subjectgroup:SubjectGroup , learner:Learner ,year:Year ,academicyear:AcademicYear]
    static constraints = {
        learner nullable: true
        admissionapplicant nullable: true
        admissionapplication nullable: true
    }
    static mapping ={
        preference_number defaultValue: 0
    }
}
