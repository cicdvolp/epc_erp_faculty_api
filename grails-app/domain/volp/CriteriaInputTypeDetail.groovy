package volp

class CriteriaInputTypeDetail {

    String field_name  // Table
    String field_value    //Comma separated for drop down
    int columsequence // Table
    int minwordcount //default 0
    int maxwordcount

    boolean is_range_applicable
    int minnumber //default 0
    int maxnumber
    Date mindate
    Date maxdate

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, criterion:Criterion, inputtype : InputType,
                       criterioninputtype:CriterionInputType, criterionfieldtype:CriterionFieldType]

    static constraints = {
        mindate nullable:true
        maxdate nullable:true
    }

    static mapping = {
        is_range_applicable defaultValue: false
        maxnumber defaultValue:0
        minnumber defaultValue:0
    }
}
