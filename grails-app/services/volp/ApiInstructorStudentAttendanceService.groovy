package volp

import grails.gorm.transactions.Transactional

@Transactional
class ApiInstructorStudentAttendanceService {

    InformationService informationService
    CommonService commonService
    ERPTimeTableService erpTimeTableService = new ERPTimeTableService()

    /*
    params.semester = semId
    params.acdYear = ayId
     */
    def getCourseAttendancetable(String username, def ayId, def semId) {
        println("getCourseAttendancetable :: " )
        Instructor instructor = Instructor.findByUid(username)
        Organization org = instructor.organization

        def sem = Semester.findById(semId)
        def academicYear = AcademicYear.findById(ayId)

        def erpcourseoffering = ERPCourseOffering.findAllByAcademicyearAndSemesterAndIsDeleted(academicYear, sem, false)

        def allclasses = []
        if(erpcourseoffering) {
            def allclasseslist = ERPCourseOfferingBatchInstructor.findAllByErpcourseofferingInList(erpcourseoffering)
            for(batchinst in allclasseslist){
                if(batchinst?.instructor?.id == instructor?.id || batchinst?.extrainstructor?.id?.contains(instructor?.id))
                    allclasses.add(batchinst)
            }
        }

        def classArray = []
        for (cls in allclasses) {
            def allsimilarcourseoffering = ERPCourseOffering.findAllByErpcourseAndAcademicyearAndSemester(cls?.erpcourseoffering?.erpcourse, cls?.erpcourseoffering?.academicyear, cls?.erpcourseoffering?.semester)
            def count = ERPCourseOfferingBatchLearner.createCriteria().list{
                'in'('erpcourseoffering', allsimilarcourseoffering)
                and{
                    'in'('erpcourseofferingbatch', cls?.erpcourseofferingbatch)
                }
            }

            TreeSet period = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatch(cls?.erpcourseofferingbatch).period_number
            int lecnos
            if (period.size() == 0)
                lecnos = 0
            else
                lecnos = period.size()

            if ((cls?.erpcourseoffering?.academicyear?.id == academicYear?.id) && (cls?.erpcourseoffering?.semester?.id == sem?.id) && (cls?.erpcourseoffering?.organization?.id == org?.id)) {
                if(cls?.erpcourseoffering?.courserule?.name == 'EL' && cls?.erpcourseoffering?.erpcoursecategory?.name != 'EL-Div'){
                    if(cls?.erpcourseofferingbatch?.divisionoffering == null)
                        classArray.add(cls : cls, count : count.size(), lecnos:lecnos, batch:cls?.erpcourseofferingbatch?.batch_number)
                }else {
                    if(cls?.erpcourseofferingbatch?.divisionoffering != null) {
                        def batch = cls?.erpcourseofferingbatch?.erpcourseoffering?.erpcourse?.course_code + '-(' + cls?.erpcourseofferingbatch?.batch_number + ')'
                        classArray.add(cls: cls, count: count.size(), lecnos: lecnos, batch: batch)
                    }
                }
            }
        }

        def graph = []
        for(c in classArray){
            if(c.count > 0) {
                graph.add(batch:c?.batch, lecnos : c?.lecnos, actualcount : c?.cls?.erpcourseofferingbatch?.expectednooflecture)
            }
        }

        ApplicationType at = ApplicationType.findByApplication_type("ERP")

        def roletype =  RoleType.findByApplicationtypeAndTypeAndOrganization(at, "Academics", instructor.organization)
        def roletypesetting = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Allow Academics Without Registration', instructor.organization)

        HashMap hm = new HashMap()
        hm.put("allclasses", classArray)
        hm.put("graph", graph)
        hm.put("roletypesetting",roletypesetting )

        hm.put("msg", "success")
        return hm
    }

    /*
    params.id = batchinstid
    params.executionDate = executeDate
    params?.topiccovered = topiccovered
     */
    def getStudentListMarkAttendance(String username, def batchinstid, def executeDate, def topiccovered){
        println("getStudentListMarkAttendance :: " )
        HashMap hm = new HashMap()
        Instructor instructor = Instructor.findByUid(username)
        Organization org = instructor.organization
        ApplicationType at = ApplicationType.findByApplication_type("ERP")
        RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "Registration", instructor.organization)

        ERPCourseOfferingBatchInstructor erpcrsbatch = ERPCourseOfferingBatchInstructor.findById(batchinstid)
        def repectiveBatchLearner = ERPCourseOfferingBatchLearner.findAllByErpcourseofferingbatch(erpcrsbatch.erpcourseofferingbatch)
        ArrayList rolllist = new ArrayList()
        if (repectiveBatchLearner) {
            for (ERPCourseOfferingBatchLearner obj : repectiveBatchLearner) {
                LearnerDivision  learnerrollno = LearnerDivision.findByLearnerAndAcademicyearAndSemesterAndIsdeleted(obj.learner, obj.erpcourseoffering?.academicyear, obj.erpcourseoffering?.semester, false)
                ArrayList rollhis = new ArrayList()
                def la_present = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchlearnerAndIs_present(obj, true)
                def la_all = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchlearner(obj)
                rollhis.add(learnerrollno)
                rollhis.add(la_present.size())
                rollhis.add(la_all.size())
                rollhis.add(obj)
                rolllist.add(rollhis)
            }
        } else {
            hm.put("msg", "No students assigned to this Course/Subject by class teacher.")
            return hm
//            render "No students assigned to this Course/Subject by class teacher."
        }

        def periodlist = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatch(erpcrsbatch.erpcourseofferingbatch)
        TreeSet period = []
        TreeSet periodwithextralecture = []
        for(item in periodlist){
            if(!item?.erpcourseofferingplan?.isextralecture)
                period.add(item?.period_number)
            periodwithextralecture.add(item?.period_number)
        }
        def lecnos
        if (period.size() == 0)
            lecnos = informationService.getFirstLecture(erpcrsbatch, 1)
        else
            lecnos = informationService.getLectureno(erpcrsbatch, period?.last())

        lecnos = Double.parseDouble(lecnos+"")

        if(periodwithextralecture?.contains(lecnos)){
            if (periodwithextralecture.size() == 0)
                lecnos = informationService.getFirstLecture(erpcrsbatch, 1)
            else
                lecnos = informationService.getLectureno(erpcrsbatch, periodwithextralecture?.last())
        }

        lecnos = Double.parseDouble(lecnos+"")


        AcademicYear academicYear = erpcrsbatch.erpcourseoffering.academicyear
        Semester semester = erpcrsbatch.erpcourseoffering.semester
        SlotType slotType = SlotType.findByTypeAndOrganization("Teaching", org)

        def slotlist = erpTimeTableService.getTimeTableSlot(academicYear, semester, erpcrsbatch?.erpcourseoffering?.program, erpcrsbatch?.erpcourseoffering?.year, slotType, org)
        if(!slotlist) {
            hm.put("msg", "Attendance Slot Not Added OR Program, Year, Slotmaster Mapping Not done, Please contact timetable coordinator !!")
            return hm
//            render "<span style=' background:red; color:#FFF; font-weight:900;'><b>Attendance Slot Not Added OR Program, Year, Slotmaster Mapping Not done, Please contact timetable coordinator !!</b></span>"
//            return
        }

        def executionDate
        if (executeDate)
            executionDate = Date.parse("yyyy-MM-dd", executeDate)

        rolllist.sort { it[0]?.rollno }

        SampleFilePath sampleFilePath = SampleFilePath.findByName("student_attendance")
        def path = ""
        if(sampleFilePath)
            path = sampleFilePath?.filepath + sampleFilePath?.filename
        AWSBucketService awsBucketService1 = new AWSBucketService()
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)

        def attendanceslotdisplay = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Display Attendance Slot Number Only In Mark Student Attendance?', org)?.value

        def attslotlist = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(erpcrsbatch, erpcrsbatch?.erpcourseofferingbatch, lecnos)

        def facultylist = []
        if(attslotlist) {
            facultylist = ERPCourseExtraInstructorAttendanceSlot.createCriteria().list() {
                projections {
                    distinct('instructor')
                }
                'in'('erpcourseinstructorattendanceslot', attslotlist)
            }
        }

        def isbulkattendanceimportavailable = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Bulk Learner Attendance Import facility Available', org)?.value

        hm.put("rolllist", rolllist)
        hm.put("lecnos", lecnos)
        hm.put("batch", erpcrsbatch.erpcourseofferingbatch)
        hm.put("insbatch", erpcrsbatch)
        hm.put("slotlist", slotlist)
        hm.put("totallecture", period.size())
        hm.put("executionDate", executionDate)
        hm.put("sample_file", url)
        hm.put("attendanceslotdisplay", attendanceslotdisplay)
        hm.put("topiccovered", topiccovered)
        hm.put("facultylist", facultylist)
        hm.put("isbulkattendanceimportavailable", isbulkattendanceimportavailable)
        hm.put("msg", "success")
        return hm
    }

    /*
    params.insbatch = instbatchid
    params.ldid = learnerdivisionid
     */
    def getLearnerRecord(String username, def learnerdivisionid, def instbatchid){
        println("getLearnerRecord :: " )
        Instructor instructor = Instructor.findByUid(username)
        ERPCourseOfferingBatchInstructor insbatch = ERPCourseOfferingBatchInstructor.findById(instbatchid)
        ERPCourseOfferingBatchLearner ld = ERPCourseOfferingBatchLearner.findById(learnerdivisionid)
        ERPCourseOfferingBatchLearner lbatch = ERPCourseOfferingBatchLearner.findByLearnerAndErpcourseofferingbatch(ld.learner, insbatch.erpcourseofferingbatch)
        def la = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchlearner(lbatch)

        LearnerDivision learnerrollno = LearnerDivision.findByLearnerAndAcademicyearAndSemesterAndIsdeleted(lbatch.learner, lbatch.erpcourseoffering?.academicyear, lbatch.erpcourseoffering?.semester, false)

        HashMap hm = new HashMap()
        hm.put("record", la)
        hm.put("ld", ld)
        hm.put("insbatch", insbatch)
        hm.put("learnerrollno", learnerrollno)
        hm.put("msg", "success")
        return hm
    }

    /*
    params.idlist = batchlearnerattendanceid
    params.checkedValue = presentbatchlearnerattendanceid
    params.insbatch = instbatchid
    params.e = learnerdivid
     */
    def updateLearnerRecord(String username, def presentbatchlearnerattendanceid, def batchlearnerattendanceid, def ipaddr) {
        println("updateLearnerRecord :: " )
        try{
            Instructor instructor = Instructor.findByUid(username)
            def idlist
            def checkedValue
            if (batchlearnerattendanceid.class.isArray())
                idlist = batchlearnerattendanceid
            else {
                idlist = []
                idlist.add(batchlearnerattendanceid)
            }
            if (presentbatchlearnerattendanceid) {
                if (presentbatchlearnerattendanceid.class.isArray())
                    checkedValue = presentbatchlearnerattendanceid
                else {
                    checkedValue = []
                    checkedValue.add(presentbatchlearnerattendanceid)
                }
            } else
                checkedValue = []


            for (id in idlist) {
                ERPCourseOfferingBatchLearnerAttendance e = ERPCourseOfferingBatchLearnerAttendance.findById(id)
                if (checkedValue.contains(id))
                    e.is_present = true
                else {
                    e.is_present = false
                }
                e.username = username
                e.updation_date = new java.util.Date()
                e.updation_ip_address = ipaddr
                e.save(flush: true, failOnError: true)

                def p = e?.erpcourseofferingbatchlearner
                LearnerDivision learnerdivision = LearnerDivision.findByLearnerAndAcademicyearAndSemesterAndIsdeleted(p?.learner, p?.erpcourseoffering?.academicyear, p?.erpcourseoffering?.semester, false)
                if (learnerdivision && !e.is_present) {
//                        commonService.sendAttendanceEmail( learnerdivision, e, username, ipaddr)
//                        commonService.sendAttendanceSMS( learnerdivision, e, username, ipaddr)
                }

                // Add Attendance Timesheet
                def insbatch = e?.erpcourseofferingbatchinstructor
                def lec = e?.period_number
                Date lecdate = e?.execution_date
                def totalstudents = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(insbatch, insbatch.erpcourseofferingbatch, lec)?.size()
                def presentstudents = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_numberAndIs_present(insbatch, insbatch.erpcourseofferingbatch, lec, true).size()
                def slotList = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(insbatch, insbatch.erpcourseofferingbatch, lec)
                def starttime = ""
                def endtime = ""
                if (slotList) {
                    starttime = slotList[0]?.slot?.start_time
                    endtime = slotList[slotList?.size() - 1]?.slot?.end_time
                }
                def coursename = insbatch?.erpcourseoffering?.erpcourse?.course_code + ":" + insbatch?.erpcourseoffering?.erpcourse?.course_name
                def taskname = ""
                if (!insbatch?.erpcourseofferingbatch?.divisionoffering)
                    taskname = "Attendance of Elective Course-" + coursename + " Batch No. " + insbatch?.erpcourseofferingbatch?.batch_number + " Lecture No. " + lec
                else {
                    if (insbatch?.erpcourseofferingbatch?.batch_number == '-1')
                        taskname = "Attendance of Theory Division-" + insbatch?.erpcourseofferingbatch?.divisionoffering + " Course-" + coursename + " Lecture No. " + lec
                    else {
                        if (insbatch?.erpcourseofferingbatch?.loadtype?.type?.type == 'Theory')
                            taskname = "Attendance of Theory Division-" + insbatch?.erpcourseofferingbatch?.divisionoffering + " Course-" + coursename + " Batch No. " + insbatch?.erpcourseofferingbatch?.batch_number + " Lecture No." + lec
                        else
                            taskname = "Attendance of " + insbatch?.erpcourseofferingbatch?.loadtype?.type?.type + " Division-" + insbatch?.erpcourseofferingbatch?.divisionoffering + " Course-" + coursename + " Batch No. " + insbatch?.erpcourseofferingbatch?.batch_number + " Lecture No. " + lec
                    }
                }

                def description = "Task Added By System On Attendance Filling"
                erpTimeTableService.addTimeSheet(insbatch?.instructor, insbatch?.erpcourseoffering?.program, insbatch?.erpcourseoffering?.year,
                        coursename, presentstudents, totalstudents, lecdate.clearTime(), starttime, endtime, taskname, description, ipaddr)
            }
            HashMap hm = new HashMap()
            hm.put('msg', "success")
            return hm
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put('msg', "failed")
            return hm
        }
    }

    /*
    params.id = batchinstid
    params?.nonedittable = nonedittable
     */
    def editStudentListMarkAttendance(String username, def batchinstid, def nonedittable){
        println("editStudentListMarkAttendance :: " )
        Instructor instructor = Instructor.findByUid(username)

        ERPCourseOfferingBatchInstructor erpcrsbatch = ERPCourseOfferingBatchInstructor.findById(batchinstid)

        def period = ERPCourseOfferingBatchLearnerAttendance.createCriteria().list(){
            projections{
                distinct('period_number')
            }
            and {
                'in'('erpcourseofferingbatchinstructor', erpcrsbatch)
                order("execution_date")
            }
        }

        HashSet monthyear = []
        def dateList = []
        def listArray = []
        for(p in period){
            def perioddetails = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, p)
            def presentStudent = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_numberAndIs_present(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, p, true).size()
            def absentStudent = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_numberAndIs_present(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, p, false).size()

            def slotList = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, p)
            def attendanceslotdisplay = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Display Attendance Slot Number Only In Mark Student Attendance?', instructor?.organization)?.value
            def presentpersent = (presentStudent / perioddetails.size()) * 100
            String presentpersent1 = String.format("%.2f", presentpersent);
            def slot
            if(slotList.size()== 1) {
                if (slotList[0].slot) {
                    if(attendanceslotdisplay == 'true')
                        slot = "Slot-"+slotList[0].slot.slot_number
                    else
                        slot = slotList[0].slot.start_time + "-" + slotList[0].slot.end_time
                }else
                    slot = "-"
            }else if(slotList.size()== 2) {
                if (slotList[0].slot && slotList[1].slot.end_time) {
                    if(attendanceslotdisplay == 'true')
                        slot = "Slot-"+slotList[0].slot.slot_number+", "+ "Slot-"+slotList[1].slot.slot_number
                    else
                        slot = slotList[0].slot.start_time + "-" + slotList[1].slot.end_time
                }else
                    slot = "-"
            }

            dateList.add(perioddetails[0]?.execution_date)
            def erpmonth = ERPMonth.findByMonthnumber(perioddetails[0]?.execution_date.getMonth() + 1)
            def year = perioddetails[0]?.execution_date.getYear() + 1900
            def erpyear = ERPCalendarYear.findByYear("" + year)

            monthyear.add(month : erpmonth, erpyear : erpyear)
            def periodarray = []
            def m =  p?.toString()
            m = m.split("[.]")
            def beforedecimal = Integer.parseInt(m[0])
            def afterdecimal = Integer.parseInt(m[1])
            def displayperiodno = beforedecimal
            if(afterdecimal)
                displayperiodno = displayperiodno + "."+afterdecimal
            periodarray.add(displayperiodno+" : "+informationService.getDate(perioddetails[0]?.execution_date))
            listArray.add(period : perioddetails[0], displayperiodno:displayperiodno, slot:slot, totalstudent : perioddetails.size(), presentstudent:presentStudent, absentStudent:absentStudent,presentpersent:presentpersent1,periodarray:periodarray)
        }
        def newArray = []
        for(m in monthyear){
            newArray.add(m)
        }
        newArray.sort{it?.month?.monthnumber}
        def graph = []
        for(m in newArray){
            int count = 0
            for(d in dateList){
                def erpmonth = ERPMonth.findByMonthnumber(d?.getMonth() + 1)
                def year = d?.getYear() + 1900
                def erpyear = ERPCalendarYear.findByYear("" + year)

                if(m?.month?.id == erpmonth?.id && m?.erpyear?.id == erpyear?.id)
                    count++
            }

            graph.add(month : m?.month?.monthname+" "+m?.erpyear?.year, count:count)
        }

        def learnerAttendancePercentage = []
        def repectiveBatchLearner = ERPCourseOfferingBatchLearner.findAllByErpcourseofferingbatch(erpcrsbatch.erpcourseofferingbatch)
        for (lbatch in repectiveBatchLearner) {
            def la = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchlearner(lbatch)
            def lapresent = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchlearnerAndIs_present(lbatch, true)

            LearnerDivision learnerrollno = LearnerDivision.findByLearnerAndAcademicyearAndSemesterAndIsdeleted(lbatch.learner, lbatch.erpcourseoffering?.academicyear, lbatch.erpcourseoffering?.semester, false)

            def percentage = 0
            if(lapresent?.size()>0 && la.size() > 0)
                percentage = ((lapresent?.size()*100)/la.size())
            if(learnerrollno?.rollnumbersuffix)
                learnerAttendancePercentage.add(rollno : learnerrollno?.rollnumbersuffix+""+learnerrollno?.rollno, percentage : percentage)
            else
                learnerAttendancePercentage.add(rollno : learnerrollno?.rollno, percentage : percentage)
        }

        HashMap hm = new HashMap()
        hm.put("listArray", listArray)
        hm.put("insbatch", erpcrsbatch)
        hm.put("graph", graph)
        hm.put("learnerAttendancePercentage", learnerAttendancePercentage)
        hm.put("nonedittable", nonedittable)
        hm.put("msg", "success")

        return hm
    }

    /*
    params.id = batchinstid
    params.period_number = period_number
    params.lec = lec
     */
    def editStudentAttendanceRecords(String username, def batchinstid, def period_number, def lec) {
        println("editStudentAttendanceRecords :: " )
        Instructor instructor = Instructor.findByUid(username)

        ERPCourseOfferingBatchInstructor erpcrsbatch = ERPCourseOfferingBatchInstructor.findById(batchinstid)
        def perioddetails = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchAndPeriod_number(erpcrsbatch.erpcourseofferingbatch, period_number)

        def batchLearner = ERPCourseOfferingBatchLearner.findAllByErpcourseofferingbatch(erpcrsbatch.erpcourseofferingbatch)

        if(!batchLearner)
        {
            batchLearner = ERPCourseOfferingBatchLearnerAttendance.createCriteria().list() {
                projections {
                    distinct('erpcourseofferingbatchlearner')
                }
                'in'('erpcourseofferingbatch', erpcrsbatch?.erpcourseofferingbatch)
            }
        }

        TreeSet period = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatch(erpcrsbatch.erpcourseofferingbatch).period_number

        def attslot = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, period_number).slot

        def studentlist = []
        for(p in batchLearner){
            LearnerDivision learnerrollno = LearnerDivision.findByLearnerAndAcademicyearAndSemesterAndIsdeleted(p.learner, p.erpcourseoffering?.academicyear, p.erpcourseoffering?.semester, false)
            def la = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchlearnerAndIs_present(p, true)
            def ispresent = false
            def present = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchlearnerAndErpcourseofferingbatchAndPeriod_number(p, erpcrsbatch.erpcourseofferingbatch, period_number)
            if (present) {
                ispresent = present?.is_present[0]
            }
            studentlist.add(student: p, totallecture: la.size(), rollnumber: learnerrollno, ispresent: ispresent)
        }
        AcademicYear academicYear = erpcrsbatch.erpcourseoffering.academicyear
        Semester semester = erpcrsbatch.erpcourseoffering.semester
        SlotType slotType = SlotType.findByTypeAndOrganization("Teaching",instructor.organization)

        def slotlist = erpTimeTableService.getTimeTableSlot(academicYear, semester, erpcrsbatch?.erpcourseoffering?.program, erpcrsbatch?.erpcourseoffering?.year, slotType, instructor.organization)
        if(!slotlist)
        {
            HashMap hm = new HashMap()
            hm.put("msg", "Attendance Slot Not Added OR Program, Year, Slotmaster Mapping Not done, Please contact timetable coordinator !!")
            return hm
//            render "<span style=' background:red; color:#FFF; font-weight:900;'><b>Attendance Slot Not Added OR Program, Year, Slotmaster Mapping Not done, Please contact timetable coordinator !!</b></span>"
//            return
        }

        def list = []
        for(s in slotlist){
            def check = false
            for(a in attslot){
                if(s?.id == a?.id)
                    check = true
            }
            list.add(slotlist : s, checkvalue:check)
        }
//        session.studentlist = []
//        for(p in batchLearner)
//            session.studentlist.add(p.id)

        studentlist.sort{it?.rollnumber?.rollno}

        def attendanceslotdisplay = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Display Attendance Slot Number Only In Mark Student Attendance?', instructor?.organization)?.value

        def roletypesettingpracticalhr
        if(erpcrsbatch.erpcourseofferingbatch.loadtype.type.type == "Lab")
            roletypesettingpracticalhr = ERPRoleTypeSettings.findByNameIlikeAndOrganization('No. of Practical Hours Per Turn', instructor.organization)?.value
        else if(erpcrsbatch.erpcourseofferingbatch.loadtype.type.type == "Tutorial")
            roletypesettingpracticalhr = ERPRoleTypeSettings.findByNameIlikeAndOrganization('No. of Tutorial Hours Per Turn', instructor.organization)?.value
        if(roletypesettingpracticalhr)
            roletypesettingpracticalhr = Integer.parseInt(roletypesettingpracticalhr)
        else
            roletypesettingpracticalhr = 1

        def attslotlist = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(erpcrsbatch, erpcrsbatch?.erpcourseofferingbatch, period_number)
        def facultylist = []
        if(attslotlist) {
            facultylist = ERPCourseExtraInstructorAttendanceSlot.createCriteria().list() {
                projections {
                    distinct('instructor')
                }
                'in'('erpcourseinstructorattendanceslot', attslotlist)
            }
        }

        HashMap hm = new HashMap()
        hm.put("insbatch", erpcrsbatch)
        hm.put("slotlist", list)
        hm.put("period", perioddetails[0])
        hm.put("studentlist", studentlist)
        hm.put("roletypesettingpracticalhr", roletypesettingpracticalhr)
        hm.put("totallecture", period.size())
        hm.put("attslot", attslot)
        hm.put("lec", lec)
        hm.put("attendanceslotdisplay", attendanceslotdisplay)
        hm.put("facultylist", facultylist)
        hm.put("msg", "success")

        return hm
    }

    /*
    params.insbatchid = insbatchid
    params.checkedInst = checkInst
    params.chkslotid = chkslotid
    params.lecdate = date
    params.batchid = batchid
    params.pldid = pldid
    params.ldid = ldid
     */
    def saveAttendance(String username, def insbatchid, def checkInst, def chkslotid, def date, def batchid, def pldid, def ldid, def ipaddr) {
        println("saveAttendance :: batchid : " + batchid )
        Instructor instructor = Instructor.findByUid(username)

        ERPCourseOfferingBatchInstructor insbatch = ERPCourseOfferingBatchInstructor.findById(insbatchid)
        def roletypesettingpracticalhr
        if(insbatch.erpcourseofferingbatch.loadtype.type.type == "Lab")
            roletypesettingpracticalhr = ERPRoleTypeSettings.findByNameIlikeAndOrganization('No. of Practical Hours Per Turn', instructor.organization)?.value
        else if(insbatch.erpcourseofferingbatch.loadtype.type.type == "Tutorial")
            roletypesettingpracticalhr = ERPRoleTypeSettings.findByNameIlikeAndOrganization('No. of Tutorial Hours Per Turn', instructor.organization)?.value
        if(roletypesettingpracticalhr)
            roletypesettingpracticalhr = Integer.parseInt(roletypesettingpracticalhr)
        else
            roletypesettingpracticalhr = 1

        def periodlist = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatch(insbatch.erpcourseofferingbatch)
        TreeSet period = []
        TreeSet periodwithextralecture = []
        for(item in periodlist){
            if(!item?.erpcourseofferingplan?.isextralecture)
                period.add(item?.period_number)
            periodwithextralecture.add(item?.period_number)
        }
        def lec
        if (period.size() == 0)
            lec = informationService.getFirstLecture(insbatch, 1)
        else {
            lec = informationService.getLectureno(insbatch, period?.last())
        }

        lec = Double.parseDouble(lec+"")

        if(periodwithextralecture?.contains(lec)){
            if (periodwithextralecture.size() == 0)
                lec = informationService.getFirstLecture(insbatch, 1)
            else
                lec = informationService.getLectureno(insbatch, periodwithextralecture?.last())
        }

        lec = Double.parseDouble(lec+"")

        if (checkInst == null) {
            HashMap hm = new HashMap()
            hm.put("msg", "Please select atleast one Faculty...")
            return hm
//            render "Please select atleast one Faculty..."
//            return
        }

        if (chkslotid == null) {
            HashMap hm = new HashMap()
            hm.put("msg", "Please select atleast one time slot...")
            return hm
//            render "Please select atleast one time slot..."
//            return
        }

        def checkedInst
        if (checkInst.class.isArray())
            checkedInst = checkInst
        else {
            checkedInst = []
            checkedInst.add(checkInst)
        }


        def slotidlist = chkslotid
        Date lecdate = date

        def slotlist
        if (slotidlist.class.isArray())
            slotlist = slotidlist
        else {
            slotlist = []
            slotlist.add(slotidlist)
        }
        def totalperiod = slotlist.size()

        if (insbatch.erpcourseofferingbatch.loadtype.type.type == "Lab" || insbatch.erpcourseofferingbatch.loadtype.type.type == "Tutorial") {
            totalperiod = slotlist.size() / roletypesettingpracticalhr
            if (slotlist.size() % roletypesettingpracticalhr != 0) {
                HashMap hm = new HashMap()
                hm.put("msg", "Please select each "+insbatch.erpcourseofferingbatch.loadtype.type.type+" of "+roletypesettingpracticalhr+" hours.")
                return hm
//                render "Please select each "+insbatch.erpcourseofferingbatch.loadtype.type.type+" of "+roletypesettingpracticalhr+" hours."
//                return
            }
        }

        if (insbatch.erpcourseofferingbatch.erpcourseoffering.courserule.name != "EL") {
            for (def i = 0; i < slotlist.size(); i++) {
                Slot slot = Slot.findById(slotlist[i])

                ERPCourseInstructorAttendanceSlot erpCourseInstructorAttendanceSlot =
                        ERPCourseInstructorAttendanceSlot.findByDivisionofferingAndSlotAndAcademicyearAndSemesterAndExecution_date(
                                insbatch.erpcourseofferingbatch.divisionoffering, slot,
                                insbatch.erpcourseofferingbatch.erpcourseoffering.academicyear,
                                insbatch.erpcourseofferingbatch.erpcourseoffering.semester, lecdate.clearTime())

                if (erpCourseInstructorAttendanceSlot != null) {
                    def isbatchcreated = false
                    if (insbatch.erpcourseofferingbatch.loadtype.type.type == "Theory") {
                        isbatchcreated = true
                    } else {
                        if (erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch?.batch_number == insbatch?.erpcourseofferingbatch?.batch_number)
                            isbatchcreated = true
                    }
                    if (isbatchcreated) {
                        HashMap hm = new HashMap()
                        hm.put("msg", " - 1327 : Attendence for " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.erpcourseoffering.program.name + ", division - " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.divisionoffering.division.name + " , slot " + slot + " is already filled by " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor?.person?.fullname_as_per_previous_marksheet + "-" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor.uid + " , for  subject -" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.erpcourseoffering.erpcourse.course_name+" for Period No : "+erpCourseInstructorAttendanceSlot?.period_number)
                        return hm
//                        render " - 1327 : Attendence for " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.erpcourseoffering.program.name + ", division - " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.divisionoffering.division.name + " , slot " + slot + " is already filled by " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor?.person?.fullname_as_per_previous_marksheet + "-" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor.uid + " , for  subject -" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.erpcourseoffering.erpcourse.course_name+" for Period No : "+erpCourseInstructorAttendanceSlot?.period_number
//                        return
                    }
                }
            }
        }

        if (insbatch.erpcourseofferingbatch.loadtype.type.type == "Theory") {
            def period_number = lec
            def current = period_number?.toString()?.split("[.]")
            def lectno = Integer.parseInt(current[0])
            def erpcourseofferingplan = ERPCourseOfferingPlan.findByErpcourseofferingbatchinstructorAndLecture_number_planned(insbatch, lectno)

            for (def i = 0; i < slotlist.size(); i++) {
                Slot slot = Slot.findById(slotlist[i])

                ERPCourseInstructorAttendanceSlot erpCourseInstructorAttendanceSlot = new ERPCourseInstructorAttendanceSlot()

                erpCourseInstructorAttendanceSlot.username = username
                erpCourseInstructorAttendanceSlot.creation_date = new java.util.Date()
                erpCourseInstructorAttendanceSlot.updation_date = new java.util.Date()
                erpCourseInstructorAttendanceSlot.creation_ip_address = ipaddr
                erpCourseInstructorAttendanceSlot.updation_ip_address = ipaddr

                erpCourseInstructorAttendanceSlot.erpcourseofferingplan = erpcourseofferingplan
                erpCourseInstructorAttendanceSlot.slot = slot
                erpCourseInstructorAttendanceSlot.erpcourseofferingbatchinstructor = insbatch
                erpCourseInstructorAttendanceSlot.erpcourseofferingbatch = insbatch.erpcourseofferingbatch
                erpCourseInstructorAttendanceSlot.period_number = period_number
                erpCourseInstructorAttendanceSlot.execution_date = lecdate.clearTime()
                erpCourseInstructorAttendanceSlot.erpcourseoffering = insbatch.erpcourseofferingbatch.erpcourseoffering
                erpCourseInstructorAttendanceSlot.divisionoffering = insbatch.erpcourseofferingbatch.divisionoffering
                erpCourseInstructorAttendanceSlot.instructor = insbatch.instructor
                erpCourseInstructorAttendanceSlot.academicyear = insbatch.erpcourseofferingbatch.erpcourseoffering.academicyear
                erpCourseInstructorAttendanceSlot.semester = insbatch.erpcourseofferingbatch.erpcourseoffering.semester
                erpCourseInstructorAttendanceSlot.organization = insbatch.erpcourseofferingbatch.erpcourseoffering.organization
                erpCourseInstructorAttendanceSlot.loadtypecategory = insbatch.erpcourseofferingbatch.loadtype.type
                erpCourseInstructorAttendanceSlot.save(flush: true, failOnError: true)
                period_number++
            }
        } else if (insbatch.erpcourseofferingbatch.loadtype.type.type == "Lab" || insbatch.erpcourseofferingbatch.loadtype.type.type == "Tutorial") {
            if (slotlist.size() % roletypesettingpracticalhr != 0) {
                HashMap hm = new HashMap()
                hm.put("msg", "Please select each "+insbatch.erpcourseofferingbatch.loadtype.type.type+" of "+roletypesettingpracticalhr+"hours.")
                return hm
//                render "Please select each "+insbatch.erpcourseofferingbatch.loadtype.type.type+" of "+roletypesettingpracticalhr+"hours."
//                return
            }
            totalperiod = slotlist.size() / 2
            def period_number = lec
            def current = period_number?.toString()?.split("[.]")
            def lectno = Integer.parseInt(current[0])
            def erpcourseofferingplan = ERPCourseOfferingPlan.findByErpcourseofferingbatchinstructorAndLecture_number_planned(insbatch, lectno)

            for (def i = 0; i < slotlist.size(); i++) {
                Slot slot = Slot.findById(slotlist[i])
                if (i % 2 == 0 && i > 0)
                    period_number++
                ERPCourseInstructorAttendanceSlot erpCourseInstructorAttendanceSlot = new ERPCourseInstructorAttendanceSlot()

                erpCourseInstructorAttendanceSlot.username = username
                erpCourseInstructorAttendanceSlot.creation_date = new java.util.Date()
                erpCourseInstructorAttendanceSlot.updation_date = new java.util.Date()
                erpCourseInstructorAttendanceSlot.creation_ip_address = ipaddr
                erpCourseInstructorAttendanceSlot.updation_ip_address = ipaddr

                erpCourseInstructorAttendanceSlot.erpcourseofferingplan = erpcourseofferingplan
                erpCourseInstructorAttendanceSlot.slot = slot
                erpCourseInstructorAttendanceSlot.erpcourseofferingbatchinstructor = insbatch
                erpCourseInstructorAttendanceSlot.erpcourseofferingbatch = insbatch.erpcourseofferingbatch
                erpCourseInstructorAttendanceSlot.period_number = period_number
                erpCourseInstructorAttendanceSlot.execution_date = lecdate.clearTime()
                erpCourseInstructorAttendanceSlot.erpcourseoffering = insbatch.erpcourseofferingbatch.erpcourseoffering
                erpCourseInstructorAttendanceSlot.divisionoffering = insbatch.erpcourseofferingbatch.divisionoffering
                erpCourseInstructorAttendanceSlot.instructor = insbatch.instructor
                erpCourseInstructorAttendanceSlot.academicyear = insbatch.erpcourseofferingbatch.erpcourseoffering.academicyear
                erpCourseInstructorAttendanceSlot.semester = insbatch.erpcourseofferingbatch.erpcourseoffering.semester
                erpCourseInstructorAttendanceSlot.organization = insbatch.erpcourseofferingbatch.erpcourseoffering.organization
                erpCourseInstructorAttendanceSlot.loadtypecategory = insbatch.erpcourseofferingbatch.loadtype.type
                erpCourseInstructorAttendanceSlot.save(flush: true, failOnError: true)
            }
        }

        ERPCourseOfferingBatch batch = ERPCourseOfferingBatch.findById(batchid)
        ArrayList allld = new ArrayList()
        println("pldid :: " + pldid)
        allld = pldid
//        if (pldid.getClass().isArray()) {
//            for (ld in ldid) {
//                println("ld :: " + ld)
//                allld.add(ld)
//            }
//        }else{
//            allld.add(ldid)
//        }
        println("pldid :: " + pldid)
        ArrayList pld = pldid
//        if (pldid.getClass().isArray()) {
//            for (ld in pldid) {
//                println("ld :: " + ld)
//                pld.add(ld)
//            }
//        } else {
//            pld.add(pldid)
//        }
        println("pld :: " + pld)
        int totalstud = 0
        lec -= 1

        for (int pp = 0; pp < totalperiod; pp++) {
            lec += 1
            for (ld in allld) {
                println("pld :: " + pld)
                println("ld :: " + ld)
                ERPCourseOfferingBatchLearner ldobj = ERPCourseOfferingBatchLearner.findById(ld)
                println("ldobj :: " + ldobj)
                totalstud++
                if (pld.contains(ld)) {
                    def current = lec?.toString()?.split("[.]")
                    def lectno = Integer.parseInt(current[0])
                    def erpcourseofferingplan = ERPCourseOfferingPlan.findByErpcourseofferingbatchinstructorAndLecture_number_planned(insbatch, lectno)

                    println("ldobj :: " + ldobj)
                    println("ldobj?.learner :: " + ldobj?.learner)
                    println("batch :: " + batch)
                    ERPCourseOfferingBatchLearner lbatch = ERPCourseOfferingBatchLearner.findByLearnerAndErpcourseofferingbatch(ldobj?.learner, batch)
                    println("lbatch :: " + lbatch)
                    ERPCourseOfferingBatchLearnerAttendance la = ERPCourseOfferingBatchLearnerAttendance.findByErpcourseofferingbatchlearnerAndErpcourseofferingbatchinstructorAndPeriod_number(lbatch, insbatch, lec)

                    if (la == null) {
                        la = new ERPCourseOfferingBatchLearnerAttendance()
                        la.username = username
                        la.creation_date = new java.util.Date()
                        la.updation_date = new java.util.Date()
                        la.creation_ip_address = ipaddr
                        la.updation_ip_address = ipaddr

                        la.period_number = lec
                        la.execution_date = lecdate.clearTime()
                        la.is_present = true
                        la.erpcourseofferingbatchlearner = lbatch
                        la.erpcourseofferingbatchinstructor = insbatch
                        la.erpcourseofferingbatch = insbatch.erpcourseofferingbatch
                        la.erpcourseoffering = insbatch.erpcourseoffering
                        la.erpcourseofferingplan = erpcourseofferingplan
                        la.learner = ldobj?.learner
                        la.instructor = insbatch.instructor
                        la.save(flush: true, failOnError: true)


                        if(erpcourseofferingplan) {
                            erpcourseofferingplan.execution_date = lecdate.clearTime()
                            erpcourseofferingplan.username = username
                            erpcourseofferingplan.updation_date = new java.util.Date()
                            erpcourseofferingplan.updation_ip_address = ipaddr
                            erpcourseofferingplan.save(flush: true, failOnError: true)
                        }
                    } else {
                    }
                } else {
                    def current = lec?.toString()?.split("[.]")
                    def lectno = Integer.parseInt(current[0])
                    def erpcourseofferingplan = ERPCourseOfferingPlan.findByErpcourseofferingbatchinstructorAndLecture_number_planned(insbatch, lectno)

//                    println("ldobj : " + ldobj)
//                    println("ldobj?.learner : " + ldobj?.learner)
//                    println("batch : " + batch)
                    ERPCourseOfferingBatchLearner lbatch = ERPCourseOfferingBatchLearner.findByLearnerAndErpcourseofferingbatch(ldobj?.learner, batch)
                    println("lbatch : " + lbatch)
                    ERPCourseOfferingBatchLearnerAttendance la = ERPCourseOfferingBatchLearnerAttendance.findByErpcourseofferingbatchlearnerAndErpcourseofferingbatchinstructorAndPeriod_number(lbatch, insbatch, lec)

                    if (la == null) {
                        la = new ERPCourseOfferingBatchLearnerAttendance()

                        la.username = username
                        la.creation_date = new java.util.Date()
                        la.updation_date = new java.util.Date()
                        la.creation_ip_address = ipaddr
                        la.updation_ip_address = ipaddr
                        la.period_number = lec
                        la.execution_date = lecdate.clearTime()
                        la.is_present = false
                        la.erpcourseofferingbatchlearner = lbatch
                        la.erpcourseofferingbatchinstructor = insbatch
                        la.erpcourseofferingbatch = insbatch.erpcourseofferingbatch
                        la.erpcourseoffering = insbatch.erpcourseoffering
                        la.erpcourseofferingplan = erpcourseofferingplan
                        la.learner = ldobj?.learner
                        la.instructor = insbatch.instructor
                        la.save(flush: true, failOnError: true)

                        if(erpcourseofferingplan) {
                            erpcourseofferingplan.execution_date = lecdate.clearTime()
                            erpcourseofferingplan.username = username
                            erpcourseofferingplan.updation_date = new java.util.Date()
                            erpcourseofferingplan.updation_ip_address = ipaddr
                            erpcourseofferingplan.save(flush: true, failOnError: true)
                        }

                        LearnerDivision learnerdivision = LearnerDivision.findByLearnerAndAcademicyearAndSemesterAndIsdeleted(ldobj?.learner, ldobj?.erpcourseoffering?.academicyear, ldobj?.erpcourseoffering?.semester, false)
                        if(learnerdivision && !la.is_present) {
//                            commonService.sendAttendanceEmail( learnerdivision, la, username, ipaddr)
//                            commonService.sendAttendanceSMS( learnerdivision, la, username, ipaddr)
                        }
                    } else {
                    }
                }
            }

            // Add Attendance Load Per Instructor
            def instlist = []
            for(inst in checkedInst) {
                instlist.add(Instructor.findById(inst))
            }
            def slotlistAttendance = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(insbatch, insbatch.erpcourseofferingbatch, lec)
            if(slotlistAttendance && instlist)
                erpTimeTableService.addInstructorAttendanceLoad(instlist, slotlistAttendance, username, ipaddr)



            // Add Attendance Timesheet
            def totalstudents = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(insbatch, insbatch.erpcourseofferingbatch, lec)?.size()
            def presentstudents = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_numberAndIs_present(insbatch, insbatch.erpcourseofferingbatch, lec, true).size()
            def slotList = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(insbatch, insbatch.erpcourseofferingbatch, lec)
            def starttime = ""
            def endtime = ""
            if(slotList) {
                starttime = slotList[0]?.slot?.start_time
                endtime = slotList[slotList?.size()-1]?.slot?.end_time
            }
            def coursename = insbatch?.erpcourseoffering?.erpcourse?.course_code +":"+ insbatch?.erpcourseoffering?.erpcourse?.course_name
            def taskname = ""
            if(!insbatch?.erpcourseofferingbatch?.divisionoffering)
                taskname = "Attendance of Elective Course-"+coursename+" Batch No. "+insbatch?.erpcourseofferingbatch?.batch_number +" Lecture No. "+lec
            else{
                if(insbatch?.erpcourseofferingbatch?.batch_number == '-1')
                    taskname = "Attendance of Theory Division-"+insbatch?.erpcourseofferingbatch?.divisionoffering +" Course-"+coursename+" Lecture No. "+lec
                else{
                    if(insbatch?.erpcourseofferingbatch?.loadtype?.type?.type == 'Theory')
                        taskname = "Attendance of Theory Division-"+insbatch?.erpcourseofferingbatch?.divisionoffering +" Course-"+coursename+ " Batch No. "+insbatch?.erpcourseofferingbatch?.batch_number + " Lecture No."+lec
                    else
                        taskname = "Attendance of "+insbatch?.erpcourseofferingbatch?.loadtype?.type?.type+" Division-"+insbatch?.erpcourseofferingbatch?.divisionoffering +" Course-"+coursename+" Batch No. "+insbatch?.erpcourseofferingbatch?.batch_number + " Lecture No. "+lec
                }
            }
            def description = "Task Added By System On Attendance Filling"
            erpTimeTableService.addTimeSheet(insbatch?.instructor, insbatch?.erpcourseoffering?.program, insbatch?.erpcourseoffering?.year,
                    coursename, presentstudents, totalstudents, lecdate.clearTime(), starttime, endtime, taskname, description, ipaddr)

        }

        def record = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatch(insbatch.erpcourseofferingbatch)

        HashMap hm =  new HashMap()
        hm.put("record", record)
        hm.put("insbatch", insbatch)
        hm.put("totallec", lec)
        hm.put("totalstud", allld.size())
        hm.put("msg", "success")

        return hm
    }

    /*
        params.id = batchinstid
        params.checkedStudent = presentStudentlist
        params.checkedSLot = slotidList
        params.checkedInst = instId
        params.lecdate = lectDate
        params.period_number = period_no
        params.lec = lect
     */
    def updateStudentAttendance(def username, def ipaddr, def batchinstid, def presentStudentlist, def slotidList, def instId, def lectDate, def period_no, def lect, def studList){
        println("updateStudentAttendance :: ")
        Instructor instructor = Instructor.findByUid(username)

        ERPCourseOfferingBatchInstructor erpcrsbatch = ERPCourseOfferingBatchInstructor.findById(batchinstid)

        def roletypesettingpracticalhr
        if(erpcrsbatch.erpcourseofferingbatch.loadtype.type.type == "Lab")
            roletypesettingpracticalhr = ERPRoleTypeSettings.findByNameIlikeAndOrganization('No. of Practical Hours Per Turn', instructor.organization)?.value
        else if(erpcrsbatch.erpcourseofferingbatch.loadtype.type.type == "Tutorial")
            roletypesettingpracticalhr = ERPRoleTypeSettings.findByNameIlikeAndOrganization('No. of Tutorial Hours Per Turn', instructor.organization)?.value
        if(roletypesettingpracticalhr)
            roletypesettingpracticalhr = Integer.parseInt(roletypesettingpracticalhr)
        else
            roletypesettingpracticalhr = 1


        def checkedStudent
        if(presentStudentlist) {
            if (presentStudentlist.class.isArray())
                checkedStudent = presentStudentlist
            else {
                checkedStudent = []
                checkedStudent.add(presentStudentlist)
            }
        }else
            checkedStudent = []
        def studlist = studList

        if(slotidList == null) {
            HashMap hm = new HashMap()
            hm.put("msg", "Please select atleast one time slot...")
            return hm
//            render "Please select atleast one time slot..."
//            return
        }

        if (instId == null) {
            HashMap hm = new HashMap()
            hm.put("msg", "Please select atleast one Faculty...")
            return hm
//            render "Please select atleast one Faculty..."
//            return
        }

        def checkedInst
        if (instId.class.isArray())
            checkedInst = instId
        else {
            checkedInst = []
            checkedInst.add(instId)
        }

        def slotlist
        if(slotidList.class.isArray())
            slotlist = slotidList
        else{
            slotlist = []
            slotlist.add(slotidList)
        }

        Date lecdate = lectDate.clearTime()


        def totalperiod = slotlist.size()

        def attslot = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, Double.parseDouble(period_no))
        if(slotlist.size() ==  attslot.size()) {
            if(erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.courserule.name != "EL") {
                for (s in slotlist) {
                    Slot slot = Slot.findById(s)
                    ERPCourseInstructorAttendanceSlot erpCourseInstructorAttendanceSlot =
                            ERPCourseInstructorAttendanceSlot.findByDivisionofferingAndSlotAndAcademicyearAndSemesterAndExecution_date(
                                    erpcrsbatch.erpcourseofferingbatch.divisionoffering, slot,
                                    erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.academicyear,
                                    erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.semester, lecdate)
                    if (erpCourseInstructorAttendanceSlot != null) {
                        def flag = false
                        for (sl in attslot) {
                            if (sl.id == erpCourseInstructorAttendanceSlot.id)
                                flag = true
                        }
                        if (!flag) {
                            def isbatchcreated = false
                            if (erpcrsbatch.erpcourseofferingbatch.loadtype.type.type == "Theory") {
                                isbatchcreated = true
                            } else {
                                if (erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch?.batch_number == erpcrsbatch?.erpcourseofferingbatch?.batch_number)
                                    isbatchcreated = true
                            }
                            if (isbatchcreated) {
                                HashMap hm = new HashMap()
                                hm.put("msg", "1101 : Attendence for " + erpCourseInstructorAttendanceSlot.erpcourseofferingbatch.erpcourseoffering.program.name + ",   division - " + erpCourseInstructorAttendanceSlot.erpcourseofferingbatch.divisionoffering.division.name + " , slot " + slot + " is already filled by " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor?.person?.fullname_as_per_previous_marksheet + "-" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor.uid + " , for  subject -" + erpCourseInstructorAttendanceSlot.erpcourseofferingbatch.erpcourseoffering.erpcourse.course_name+" for Period No : "+erpCourseInstructorAttendanceSlot?.period_number)
                                return hm
//                                render "1101 : Attendence for " + erpCourseInstructorAttendanceSlot.erpcourseofferingbatch.erpcourseoffering.program.name + ",   division - " + erpCourseInstructorAttendanceSlot.erpcourseofferingbatch.divisionoffering.division.name + " , slot " + slot + " is already filled by " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor?.person?.fullname_as_per_previous_marksheet + "-" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor.uid + " , for  subject -" + erpCourseInstructorAttendanceSlot.erpcourseofferingbatch.erpcourseoffering.erpcourse.course_name+" for Period No : "+erpCourseInstructorAttendanceSlot?.period_number
//                                return
                            }
                        }
                    }
                }
            }
        }else{
            attslot = erpTimeTableService.deleteExtraSlot(erpcrsbatch, Double.parseDouble(period_no), slotlist)
            if(erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.courserule.name != "EL") {
                for (def i = 0; i < slotlist.size(); i++) {
                    Slot slot = Slot.findById(slotlist[i])

                    ERPCourseInstructorAttendanceSlot erpCourseInstructorAttendanceSlot =
                            ERPCourseInstructorAttendanceSlot.findByDivisionofferingAndSlotAndAcademicyearAndSemesterAndExecution_date(
                                    erpcrsbatch.erpcourseofferingbatch.divisionoffering, slot,
                                    erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.academicyear,
                                    erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.semester, lecdate)

                    if (erpCourseInstructorAttendanceSlot != null) {
                        def isbatchcreated = false
                        if (erpcrsbatch.erpcourseofferingbatch.loadtype.type.type == "Theory") {
                            isbatchcreated = true
                        } else {
                            if (erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch?.batch_number == erpcrsbatch?.erpcourseofferingbatch?.batch_number)
                                isbatchcreated = true
                        }
                        if (isbatchcreated) {
                            HashMap hm = new HashMap()
                            hm.put("msg", "1129 : Attendence for " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.erpcourseoffering.program.name + ", division - " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.divisionoffering.division.name + " , slot " + slot + " is already filled by " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor?.person?.fullname_as_per_previous_marksheet + "-" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor.uid + " , for  subject -" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.erpcourseoffering.erpcourse.course_name+" for Period No : "+erpCourseInstructorAttendanceSlot?.period_number)
                            return hm
//                            render " 1129 : Attendence for " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.erpcourseoffering.program.name + ", division - " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.divisionoffering.division.name + " , slot " + slot + " is already filled by " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor?.person?.fullname_as_per_previous_marksheet + "-" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor.uid + " , for  subject -" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.erpcourseoffering.erpcourse.course_name+" for Period No : "+erpCourseInstructorAttendanceSlot?.period_number
//                            return
                        }
                    }
                }
            }

            if(erpcrsbatch.erpcourseofferingbatch.loadtype.type.type == "Theory")
            {
                def period_number  = Double.parseDouble(period_no)
                def current = period_number?.toString()?.split("[.]")
                def lectno = Integer.parseInt(current[0])
                def erpcourseofferingplan = ERPCourseOfferingPlan.findByErpcourseofferingbatchinstructorAndLecture_number_planned(erpcrsbatch, lectno)

                for(def i = 0 ; i<slotlist.size(); i++){
                    Slot slot = Slot.findById(slotlist[i])

                    ERPCourseInstructorAttendanceSlot erpCourseInstructorAttendanceSlot = new ERPCourseInstructorAttendanceSlot()

                    erpCourseInstructorAttendanceSlot.username = username
                    erpCourseInstructorAttendanceSlot.creation_date = new java.util.Date()
                    erpCourseInstructorAttendanceSlot.updation_date = new java.util.Date()
                    erpCourseInstructorAttendanceSlot.creation_ip_address = ipaddr
                    erpCourseInstructorAttendanceSlot.updation_ip_address = ipaddr

                    erpCourseInstructorAttendanceSlot.erpcourseofferingplan = erpcourseofferingplan
                    erpCourseInstructorAttendanceSlot.slot = slot
                    erpCourseInstructorAttendanceSlot.erpcourseofferingbatchinstructor = erpcrsbatch
                    erpCourseInstructorAttendanceSlot.erpcourseofferingbatch = erpcrsbatch.erpcourseofferingbatch
                    erpCourseInstructorAttendanceSlot.period_number = period_number
                    erpCourseInstructorAttendanceSlot.execution_date = lecdate
                    erpCourseInstructorAttendanceSlot.erpcourseoffering = erpcrsbatch.erpcourseofferingbatch.erpcourseoffering
                    erpCourseInstructorAttendanceSlot.divisionoffering = erpcrsbatch.erpcourseofferingbatch.divisionoffering
                    erpCourseInstructorAttendanceSlot.instructor = erpcrsbatch.instructor
                    erpCourseInstructorAttendanceSlot.academicyear = erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.academicyear
                    erpCourseInstructorAttendanceSlot.semester = erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.semester
                    erpCourseInstructorAttendanceSlot.organization = erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.organization
                    erpCourseInstructorAttendanceSlot.loadtypecategory = erpcrsbatch.erpcourseofferingbatch.loadtype.type
                    erpCourseInstructorAttendanceSlot.save(flush: true, failOnError: true)

                    period_number ++
                }
            }else if(erpcrsbatch.erpcourseofferingbatch.loadtype.type.type == "Lab" || erpcrsbatch.erpcourseofferingbatch.loadtype.type.type ==  "Tutorial") {

                if(slotlist.size()%roletypesettingpracticalhr != 0){
                    HashMap hm = new HashMap()
                    hm.put("msg", "Please select each "+erpcrsbatch.erpcourseofferingbatch.loadtype.type.type+" of "+roletypesettingpracticalhr+" hours.")
                    return hm
//                    render "Please select each "+erpcrsbatch.erpcourseofferingbatch.loadtype.type.type+" of "+roletypesettingpracticalhr+" hours."
//                    return
                }
                totalperiod = slotlist.size()/2
                def period_number  = Double.parseDouble(period_no)
                def current = period_number?.toString()?.split("[.]")
                def lectno = Integer.parseInt(current[0])
                def erpcourseofferingplan = ERPCourseOfferingPlan.findByErpcourseofferingbatchinstructorAndLecture_number_planned(erpcrsbatch, lectno)

                for(def i = 0 ; i<slotlist.size(); i++){
                    Slot slot = Slot.findById(slotlist[i])
                    if(i%2 == 0 && i>0)
                        period_number ++

                    ERPCourseInstructorAttendanceSlot erpCourseInstructorAttendanceSlot = new ERPCourseInstructorAttendanceSlot()

                    erpCourseInstructorAttendanceSlot.username = username
                    erpCourseInstructorAttendanceSlot.creation_date = new java.util.Date()
                    erpCourseInstructorAttendanceSlot.updation_date = new java.util.Date()
                    erpCourseInstructorAttendanceSlot.creation_ip_address = ipaddr
                    erpCourseInstructorAttendanceSlot.updation_ip_address = ipaddr

                    erpCourseInstructorAttendanceSlot.erpcourseofferingplan = erpcourseofferingplan
                    erpCourseInstructorAttendanceSlot.slot = slot
                    erpCourseInstructorAttendanceSlot.erpcourseofferingbatchinstructor = erpcrsbatch
                    erpCourseInstructorAttendanceSlot.erpcourseofferingbatch = erpcrsbatch.erpcourseofferingbatch
                    erpCourseInstructorAttendanceSlot.period_number = period_number
                    erpCourseInstructorAttendanceSlot.execution_date = lecdate
                    erpCourseInstructorAttendanceSlot.erpcourseoffering = erpcrsbatch.erpcourseofferingbatch.erpcourseoffering
                    erpCourseInstructorAttendanceSlot.divisionoffering = erpcrsbatch.erpcourseofferingbatch.divisionoffering
                    erpCourseInstructorAttendanceSlot.instructor = erpcrsbatch.instructor
                    erpCourseInstructorAttendanceSlot.academicyear = erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.academicyear
                    erpCourseInstructorAttendanceSlot.semester = erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.semester
                    erpCourseInstructorAttendanceSlot.organization = erpcrsbatch.erpcourseofferingbatch.erpcourseoffering.organization
                    erpCourseInstructorAttendanceSlot.loadtypecategory = erpcrsbatch.erpcourseofferingbatch.loadtype.type
                    erpCourseInstructorAttendanceSlot.save(flush: true, failOnError: true)
                }
            }
        }

        def i = 0
        for(a in attslot){
            a.slot = Slot.findById(slotlist[i])
            a.execution_date = lecdate
            a.save(flush: true, failOnError: true)
            i++
        }

        for (n in studlist) {
            def val = Double.parseDouble(period_no)
            def current = val?.toString()?.split("[.]")
            def lectno = Integer.parseInt(current[0])
            def erpcourseofferingplan = ERPCourseOfferingPlan.findByErpcourseofferingbatchinstructorAndLecture_number_planned(erpcrsbatch, lectno)

            def p = ERPCourseOfferingBatchLearner.findById(n)
            ERPCourseOfferingBatchLearnerAttendance present = ERPCourseOfferingBatchLearnerAttendance.findByErpcourseofferingbatchlearnerAndErpcourseofferingbatchAndPeriod_number(p, erpcrsbatch.erpcourseofferingbatch, Double.parseDouble(period_no))
            ERPCourseOfferingBatchLearnerAttendance e
            if(present)
                e  = present
            else {
                e = new ERPCourseOfferingBatchLearnerAttendance()
                e.creation_date = new java.util.Date()
                e.creation_ip_address = ipaddr
                e.period_number = Double.parseDouble(period_no)
                e.erpcourseofferingbatchlearner = p
                e.erpcourseofferingbatchinstructor = erpcrsbatch
                e.erpcourseofferingbatch = erpcrsbatch.erpcourseofferingbatch
                e.erpcourseoffering = erpcrsbatch.erpcourseoffering
                e.learner=p.learner
                e.instructor=erpcrsbatch.instructor
            }
            if (checkedStudent.contains('' + n))
                e.is_present = true
            else {
                e.is_present = false
            }
            e.erpcourseofferingplan = erpcourseofferingplan
            e.execution_date = lecdate
            e.username = username
            e.updation_date = new java.util.Date()
            e.updation_ip_address = ipaddr
            e.save(flush: true, failOnError: true)

            if(erpcourseofferingplan) {
                erpcourseofferingplan.execution_date = lecdate
                erpcourseofferingplan.username = username
                erpcourseofferingplan.updation_date = new java.util.Date()
                erpcourseofferingplan.updation_ip_address = ipaddr
                erpcourseofferingplan.save(flush: true, failOnError: true)
            }

            LearnerDivision learnerdivision = LearnerDivision.findByLearnerAndAcademicyearAndSemesterAndIsdeleted(p?.learner, p?.erpcourseoffering?.academicyear, p?.erpcourseoffering?.semester, false)
            if(learnerdivision && !e.is_present) {
//                commonService.sendAttendanceEmail( learnerdivision, e, username, ipaddr )
//                commonService.sendAttendanceSMS( learnerdivision, e, username, ipaddr )
            }
        }

        int lec = Double.parseDouble(period_no)

        // Add Attendance Load Per Instructor
        def instlist = []
        for(inst in checkedInst) {
            instlist.add(Instructor.findById(inst))
        }
        def slotlistAttendance = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, lec)
        if(slotlistAttendance && instlist)
            erpTimeTableService.addInstructorAttendanceLoad(instlist, slotlistAttendance, username, ipaddr)


        // Add Attendance Timesheet
        def totalstudents = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, lec)?.size()
        def presentstudents = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_numberAndIs_present(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, lec, true).size()
        def slotList = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, lec)
        def starttime = ""
        def endtime = ""
        if(slotList)
        {
            starttime = slotList[0]?.slot?.start_time
            endtime = slotList[slotList?.size()-1]?.slot?.end_time
        }
        def coursename = erpcrsbatch?.erpcourseoffering?.erpcourse?.course_code +":"+ erpcrsbatch?.erpcourseoffering?.erpcourse?.course_name
        def taskname = ""
        if(!erpcrsbatch?.erpcourseofferingbatch?.divisionoffering)
            taskname = "Attendance of Elective Course-"+coursename+" Batch No. "+erpcrsbatch?.erpcourseofferingbatch?.batch_number +" Lecture No. "+lec
        else{
            if(erpcrsbatch?.erpcourseofferingbatch?.batch_number == '-1')
                taskname = "Attendance of Theory Division-"+erpcrsbatch?.erpcourseofferingbatch?.divisionoffering +" Course-"+coursename+" Lecture No. "+lec
            else{
                if(erpcrsbatch?.erpcourseofferingbatch?.loadtype?.type?.type == 'Theory')
                    taskname = "Attendance of Theory Division-"+erpcrsbatch?.erpcourseofferingbatch?.divisionoffering +" Course-"+coursename+ " Batch No. "+erpcrsbatch?.erpcourseofferingbatch?.batch_number + " Lecture No."+lec
                else
                    taskname = "Attendance of "+erpcrsbatch.erpcourseofferingbatch.loadtype.type.type+" Division-"+erpcrsbatch?.erpcourseofferingbatch?.divisionoffering +" Course-"+coursename+" Batch No. "+erpcrsbatch?.erpcourseofferingbatch?.batch_number + " Lecture No. "+lec
            }
        }

        def description = "Task Added By System On Attendance Filling"
        erpTimeTableService.addTimeSheet(erpcrsbatch?.instructor, erpcrsbatch?.erpcourseoffering?.program, erpcrsbatch?.erpcourseoffering?.year,
                coursename, presentstudents, totalstudents, lecdate, starttime, endtime, taskname, description, ipaddr)

//        flash.message = "Student attendance updated successfully..."
//        redirect(action: 'editStudentAttendanceRecords', params: [period_number: period_no, id: batchinstid, lec: lect])

        HashMap hm = new HashMap()
//        hm.put("msg", "Student attendance updated successfully")
        hm.put("msg", "Success")

        return hm
    }

}