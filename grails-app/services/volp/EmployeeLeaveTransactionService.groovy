package volp

import grails.gorm.services.Service

@Service(EmployeeLeaveTransaction)
interface EmployeeLeaveTransactionService {

    EmployeeLeaveTransaction get(Serializable id)

    List<EmployeeLeaveTransaction> list(Map args)

    Long count()

    void delete(Serializable id)

    EmployeeLeaveTransaction save(EmployeeLeaveTransaction employeeLeaveTransaction)

}