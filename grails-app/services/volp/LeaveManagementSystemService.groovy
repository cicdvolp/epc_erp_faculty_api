package volp

import grails.gorm.transactions.Transactional

import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoField

@Transactional
class LeaveManagementSystemService {
    SendMailService sendMailService
    EmployeeLeaveTransactionService employeeLeaveTransactionService
    LeaveManagementSystemService leaveManagementSystemService

    def getLeaveInprogressCount(LeaveType leavetype, Instructor instructor,Organization organization, AcademicYear ay, def checked, def fromdate, def todate, def ay_specific) {

        double count = 0
        def results
        def leaveStatus = LeaveApprovalStatus.findByStatus("inprocess")
        def c = EmployeeLeaveTransaction.createCriteria()

        if(leavetype.leaveexpirytype.type == 'No Expiry' || leavetype.leaveexpirytype.type == 'Expiry Date') {
            if (leavetype.type != 'CL' && leavetype.type != 'HD_CL' && leavetype.type != 'ML' && leavetype.type != 'HPL' && leavetype.type != 'VACATION') {
                if(checked) {
                    if(ay_specific){
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("academicyear", ay)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'CL' || leavetype.type == 'HD_CL') {
                def leaveTypeList = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['CL', 'HD_CL'])
                            }
                        }

                if(checked) {
                    if(ay_specific) {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'VACATION') {
                if(checked) {
                    if(ay_specific) {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                'in'("academicyear", ay)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }
                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'ML' || leavetype.type == 'HPL') {
                def hplresult
                def mlresult = []

                def leaveTypeML = LeaveType.createCriteria().list(){
                    like("organization", organization)
                    and {
                        'in'("type", ['ML'])
                    }
                }
                def leaveTypeHPL = LeaveType.createCriteria().list(){
                    like("organization", organization)
                    and {
                        'in'("type", ['HPL'])
                    }
                }

                if(ay_specific) {
                    if(leaveTypeML) {
                        mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeML)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }else {
                    if(leaveTypeML) {
                        mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeML)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                if(ay_specific) {
                    if(leaveTypeHPL) {
                        hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeHPL)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }else {
                    if(leaveTypeHPL) {
                        hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeHPL)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                count = (hplresult[0] ? hplresult[0] : 0) + (mlresult[0] ? (mlresult[0] * leaveTypeML[0]?.aditionalinfo1) : 0)
            }
        }else{
            if (leavetype.type != 'CL' && leavetype.type != 'HD_CL' && leavetype.type != 'ML' && leavetype.type != 'HPL' && leavetype.type != 'VACATION') {
                if(checked) {
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'CL' || leavetype.type == 'HD_CL') {
                def leaveTypeList = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['CL', 'HD_CL'])
                            }
                        }

                if(checked) {
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leaveTypeList)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leaveTypeList)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'VACATION') {
                if(checked) {
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }
                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'ML' || leavetype.type == 'HPL') {
                def hplresult
                def mlresult

                def leaveTypeML = LeaveType.createCriteria().list(){
                    like("organization", organization)
                    and {
                        'in'("type", ['ML'])
                    }
                }
                def leaveTypeHPL = LeaveType.createCriteria().list(){
                    like("organization", organization)
                    and {
                        'in'("type", ['HPL'])
                    }
                }

                if(leaveTypeML) {
                    mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leaveTypeML)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                    projections {
                        sum('numberofdays')
                    }
                    'in'("leavetype", leaveTypeHPL)
                    and {
                        'in'("instructor", instructor)
                        'in'("academicyear", ay)
                        'in'("leaveapprovalstatus", leaveStatus)
                        'in'("organization", organization)
                    }
                }

                count = (hplresult[0] ? hplresult[0] : 0) + (mlresult[0] ? (mlresult[0] * leaveTypeML[0]?.aditionalinfo1) : 0)
            }
        }

        return count
    }


    def getLeaveApprovedCount(LeaveType leavetype, Instructor instructor,Organization organization, AcademicYear ay, def checked, def fromdate, def todate, def ay_specific) {

        double count = 0
        def results
        def leaveStatus = LeaveApprovalStatus.findByStatus("approved")
        def c = EmployeeLeaveTransaction.createCriteria()

        if(leavetype.leaveexpirytype.type == 'No Expiry' || leavetype.leaveexpirytype.type == 'Expiry Date') {
            if (leavetype.type != 'CL' && leavetype.type != 'HD_CL' && leavetype.type != 'ML' && leavetype.type != 'HPL' && leavetype.type != 'VACATION') {
                if(checked) {
                    if(ay_specific) {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                'in'("academicyear", ay)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'CL' || leavetype.type == 'HD_CL') {
                def leaveTypeList = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['CL', 'HD_CL'])
                            }
                        }

                if(checked) {
                    if(ay_specific) {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'VACATION') {
                if(checked) {
                    if(ay_specific) {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'ML' || leavetype.type == 'HPL') {
                def hplresult
                def mlresult = []

                def leaveTypeML = LeaveType.createCriteria().list(){
                    like("organization", organization)
                    and {
                        'in'("type", ['ML'])
                    }
                }
                def leaveTypeHPL = LeaveType.createCriteria().list(){
                    like("organization", organization)
                    and {
                        'in'("type", ['HPL'])
                    }
                }

                if(ay_specific) {
                    if(leaveTypeML) {
                        mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeML)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }else {
                    if(leaveTypeML) {
                        mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                            projections {
                                sum('numberofdays')
                            }
                            'in'("leavetype", leaveTypeML)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                if(ay_specific) {
                    hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leaveTypeHPL)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }else {
                    hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leaveTypeHPL)
                        and {
                            'in'("instructor", instructor)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                count = (hplresult[0] ? hplresult[0] : 0) + (mlresult[0] ? (mlresult[0] * leaveTypeML[0]?.aditionalinfo1) : 0)
            }

        }else{

            if (leavetype.type != 'CL' && leavetype.type != 'HD_CL' && leavetype.type != 'ML' && leavetype.type != 'HPL' && leavetype.type != 'VACATION') {
                if(checked) {
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'CL' || leavetype.type == 'HD_CL') {
                def leaveTypeList = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['CL', 'HD_CL'])
                            }
                        }

                if(checked) {
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leaveTypeList)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leaveTypeList)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'VACATION') {
                if(checked) {
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                count = results[0] ? results[0] : 0
            } else if (leavetype.type == 'ML' || leavetype.type == 'HPL') {
                def hplresult
                def mlresult = []

                def leaveTypeML = LeaveType.createCriteria().list(){
                    like("organization", organization)
                    and {
                        'in'("type", ['ML'])
                    }
                }
                def leaveTypeHPL = LeaveType.createCriteria().list(){
                    like("organization", organization)
                    and {
                        'in'("type", ['HPL'])
                    }
                }

                if(leaveTypeML) {
                    mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                        projections {
                            sum('numberofdays')
                        }
                        'in'("leavetype", leaveTypeML)
                        and {
                            'in'("instructor", instructor)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("academicyear", ay)
                            'in'("organization", organization)
                        }
                    }
                }

                hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                    projections {
                        sum('numberofdays')
                    }
                    'in'("leavetype", leaveTypeHPL)
                    and {
                        'in'("instructor", instructor)
                        'in'("leaveapprovalstatus", leaveStatus)
                        'in'("academicyear", ay)
                        'in'("organization", organization)
                    }
                }

                count = (hplresult[0] ? hplresult[0] : 0) + (mlresult[0] ? (mlresult[0] * leaveTypeML[0]?.aditionalinfo1) : 0)
            }

        }

            return count
    }

    def insertEmployeeAttendance(Instructor instructor,
                                 AcademicYear ay,
                                 String intime,
                                 String outtime,
                                 Date todaysdate,
                                 String expected_in_time,
                                 String expected_out_time,
                                 double expectednoofhours,
                                 Boolean hasappliedforleavefortoday,
                                 Boolean isnonworkingday,
                                 ERPMonth erpMonth,
                                 ERPDay erpDay ,
                                 ERPCalendarYear erpCalendarYear,
                                 Department department,
                                 Organization organization,
                                 LeaveType leaveType)
    {
        /*println("Ins AcademicYear "+ay)
        println("Ins intime "+intime)
        println("Ins outtime "+outtime)
        println("Ins todaysdate "+todaysdate)
        println("Ins expected_in_time "+expected_in_time)
        println("Ins expected_out_time "+expected_out_time)
        println("Ins expectednoofhours "+expectednoofhours)
        println("Ins hasappliedforleavefortoday "+hasappliedforleavefortoday)
        println("Ins erpMonth "+erpMonth)
        println("Ins erpDay "+erpDay)
        println("Ins erpCalendarYear "+erpCalendarYear)
        println("Ins leaveType "+leaveType)
*/
        // println("I am in insertEmployeeAttendance..:" + params)
        EmployeeAttendance employeeAttendance=new EmployeeAttendance()
        // Department department=instructor.department
        employeeAttendance.intime=intime
        employeeAttendance.outtime=outtime
        //String todaysdate=params.date
        //Date todaysdate = params.date('date', 'dd-MM-yyyy')
        LocalDateTime datetime1 = LocalDateTime.now();
        //println("datetime1" +datetime1);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String formatDateTime = datetime1.format(format);
        //println("formatDateTime" +formatDateTime);
        def day=(datetime1.get(ChronoField.DAY_OF_MONTH));
       // println("day" +day)

        employeeAttendance.erpcalenderintegerday=day
        //Date from= new SimpleDateFormat("yyyy-MM-dd").parse(todaysdate);
        employeeAttendance.todaysdate=todaysdate
        employeeAttendance.expectedintime=expected_in_time
        employeeAttendance.expectedouttime=expected_out_time
        employeeAttendance.expectednoofhours=expectednoofhours
        //AcademicYear ay=AcademicYear.findByAy(params.ay)
        println("ay" +ay)
        employeeAttendance.instructor=instructor
        employeeAttendance.academicyear=ay
        employeeAttendance.hasappliedforleavefortoday=hasappliedforleavefortoday
        employeeAttendance.isnonworkingday=isnonworkingday

        // ERPMonth erpMonth=ERPMonth.findByMonthname(params.erpmonth)
        employeeAttendance.erpmonth=erpMonth
        //ERPDay erpDay=ERPDay.findByDay(params.erpday)
        employeeAttendance.erpday=erpDay
        // ERPCalendarYear erpCalendarYear=ERPCalendarYear.findByYear(params.calenderyear)
        employeeAttendance.erpcalenderyear=erpCalendarYear
        // Organization organization=Organization.findByOrganization_name(params.org)

        // LeaveType leaveType=LeaveType.findByType(params.leavetype)
        employeeAttendance.leavetype=leaveType
        employeeAttendance.organization=organization
        employeeAttendance.department=department

        employeeAttendance.username =  "test"
        employeeAttendance.creation_date = new java.util.Date()
        employeeAttendance.updation_date = new java.util.Date()
        employeeAttendance.creation_ip_address = '1'
        employeeAttendance.updation_ip_address = '1'

        employeeAttendance.save(failOnError: true, flush: true)
        // render "saved"
        //redirect( action: "employeeAttendance")

    }


    def insertIntoAttendanceOnLeave(Instructor instructor,
                                    AcademicYear ay,
                                    LeaveType leavetype,
                                    Date fromdate,
                                    Date todate,
                                    LeaveApprovalStatus leaveapprovalstatus,
                                    EmployeeLeaveTransaction employeeleavetransaction,
                                    boolean isLeave){

        (fromdate..todate).each { today ->

            def erpday = ERPDay.findByDaysequence(today.getDay()+1)
            def erpmonth = ERPMonth.findByMonthnumber(today.getMonth()+1)
            def year = today.getYear()+1900
            def erpyear = ERPCalendarYear.findByYear(""+year)

            def attendance=EmployeeAttendance.findByInstructorAndErpcalenderintegerdayAndErpmonthAndErpcalenderyear(
                    instructor,today.getDate(),erpmonth,erpyear)

            if(attendance!=null){
                attendance.hasappliedforleavefortoday = isLeave
                attendance.leavetype = leavetype
                attendance.leaveapprovalstatus = leaveapprovalstatus
                attendance.employeeleavetransaction = employeeleavetransaction
                attendance.save(failOnError: true, flush: true)
            }else{

                EmployeeAttendance a = new EmployeeAttendance()
                a.intime = ""
                a.outtime = ""
                a.todaysdate = today
                a.hasappliedforleavefortoday = isLeave
                a.expectednoofhours = 0.0
                a.expectedintime = ""
                a.expectedouttime = ""
                a.erpcalenderintegerday = today.getDate()
                a.instructor = instructor
                a.academicyear = ay
                a.erpmonth = erpmonth
                a.erpday = erpday
                a.erpcalenderyear = erpyear
                a.department = instructor.department
                a.organization = instructor.organization
                a.leavetype = leavetype
                a.leaveapprovalstatus = leaveapprovalstatus
                a.employeeleavetransaction = employeeleavetransaction
                a.username='test'
                a.creation_date=new java.util.Date()
                a.updation_date=new java.util.Date()
                a.creation_ip_address='1'
                a.updation_ip_address='1'
                a.save(failOnError:true,flush:true)
            }
        }
    }

    def setIntoAttendanceOnLeaveCancelOrApprove(Instructor instructor,
                                                Date fromdate,
                                                Date todate,
                                                String leaveStatus){

        (fromdate..todate).each { today ->

            def erpmonth = ERPMonth.findByMonthnumber(today.getMonth()+1)
            def year = today.getYear()+1900
            def erpyear = ERPCalendarYear.findByYear(""+year)

            EmployeeAttendance att = EmployeeAttendance.findByInstructorAndErpcalenderintegerdayAndErpmonthAndErpcalenderyearAndHasappliedforleavefortoday(
                    instructor,today.getDate(),erpmonth,erpyear,true)
            //println("att::"+att)
            def leaveapprovalstatus = LeaveApprovalStatus.findByStatus(leaveStatus)

            if(att!=null)
            {
                att.leaveapprovalstatus = leaveapprovalstatus
                if(leaveStatus=='cancelled' || leaveStatus=='rejected'){
                    att.hasappliedforleavefortoday = false
                }
                att.save(failOnError: true, flush: true)
            }
        }
    }

    def setIntoAttendanceRemoveLeave(Instructor instructor, EmployeeLeaveTransaction employeeleavetransaction){
        def attendance = EmployeeAttendance.findAllByInstructorAndEmployeeleavetransaction(instructor,employeeleavetransaction)
        for(att in attendance) {
            att.leaveapprovalstatus = null
            att.hasappliedforleavefortoday = false
            att.leavetype = null
            att.leaveapprovalstatus = null
            att.employeeleavetransaction = null
            att.save(failOnError: true, flush: true)
        }
    }

    def getTotalWeeklyOffAndHoliday(Instructor instructor,
                                    ERPMonth erpmonth, ERPCalendarYear erpyear){

        def sumWeeklyOff  = EmployeeAttendance.findAllByInstructorAndErpmonthAndErpcalenderyearAndIsweeklyoff(instructor, erpmonth, erpyear, true).size()
        def sumOfHoliday  = EmployeeAttendance.findAllByInstructorAndErpmonthAndErpcalenderyearAndIsnonworkingday(instructor, erpmonth, erpyear, true).size()
        def sumOfWeeklyOffAndHoliday  = EmployeeAttendance.findAllByInstructorAndErpmonthAndErpcalenderyearAndIsweeklyoffAndIsnonworkingday(instructor, erpmonth, erpyear, true, true).size()
        return sumWeeklyOff + sumOfHoliday - sumOfWeeklyOffAndHoliday

    }

    def getLeaveTotal(Instructor instructor,
                      ERPMonth erpmonth, ERPCalendarYear erpyear, LeaveType leavetype, LeaveApprovalStatus leaveStatus){

        def leaveCount = EmployeeAttendance.findAllByInstructorAndErpmonthAndErpcalenderyearAndHasappliedforleavefortodayAndLeavetypeAndLeaveapprovalstatus(instructor, erpmonth, erpyear, true, leavetype, leaveStatus).size()
        return leaveCount

    }


    def autoApproveOnMinDaysOfLeave(def e, def ip, def user){
        def approvedby = e?.approvedby
        InstructorLeaveAprovalHierarchy instleaveapprovalhierarchy = InstructorLeaveAprovalHierarchy.findByInstructorAndLeavetypeAndOrganization(e.employeeleavetransaction.instructor, e.employeeleavetransaction.leavetype, e.employeeleavetransaction.instructor.organization)
        if(instleaveapprovalhierarchy) {
            LeaveApprovalHierarchy leaveapprovalhierarchy = LeaveApprovalHierarchy.findByLevelAndLeaveapprovalhierarchymasterAndOrganization(e.leaveapprovalhierarchy.level + 1, instleaveapprovalhierarchy?.leaveapprovalhierarchymaster, e.employeeleavetransaction.instructor.organization)
            if (e.employeeleavetransaction.instructor.reportingorganization != null && leaveapprovalhierarchy.leaveapporvalauthority.authority != "Registrar") {
                leaveapprovalhierarchy = LeaveApprovalHierarchy.findByLevelAndLeaveapprovalhierarchymasterAndOrganization(e.leaveapprovalhierarchy.level + 1, instleaveapprovalhierarchy?.leaveapprovalhierarchymaster, e.employeeleavetransaction.instructor.reportingorganization)
            }

            def approvedbynew
            if(leaveapprovalhierarchy.leaveapporvalauthority.authority == "HOD") {
                if (e?.employeeleavetransaction?.instructor.reporttingauthority != null) {
                    approvedbynew = e?.employeeleavetransaction?.instructor?.reporttingauthority
                } else if (e?.employeeleavetransaction?.instructor?.reportingdepartment != null) {
                    approvedbynew = e?.employeeleavetransaction?.instructor?.reportingdepartment.hod
                } else if (e?.employeeleavetransaction?.instructor?.department != null) {
                    approvedbynew = e?.employeeleavetransaction?.instructor?.department.hod
                }
            }else if(leaveapprovalhierarchy.leaveapporvalauthority.authority == "Dean"){
                approvedbynew = e?.employeeleavetransaction?.instructor?.department?.stream?.dean
            }

            EmployeeLeaveTransactionApproval employeeleavetransactionapproval = new EmployeeLeaveTransactionApproval()
            employeeleavetransactionapproval.application_push_date = new java.util.Date()
            employeeleavetransactionapproval.username = user
            employeeleavetransactionapproval.approvedby = approvedbynew
            employeeleavetransactionapproval.creation_date = new java.util.Date()
            employeeleavetransactionapproval.updation_date = new java.util.Date()
            employeeleavetransactionapproval.creation_ip_address = ip
            employeeleavetransactionapproval.updation_ip_address = ip
            employeeleavetransactionapproval.organization = e.organization
            employeeleavetransactionapproval.employeeleavetransaction = e.employeeleavetransaction
            LeaveApprovalStatus leaveapprovalstatus
            if(leaveapprovalhierarchy?.leaveapporvalauthority?.minnoofleavesforinstanceapproval >= e?.employeeleavetransaction?.numberofdays) {
                leaveapprovalstatus = LeaveApprovalStatus.findByStatus( "approved" )
                employeeleavetransactionapproval.remark = "Auto Approved By System"
            }else {
                leaveapprovalstatus = LeaveApprovalStatus.findByStatus( "inprocess" )
                employeeleavetransactionapproval.remark = ""
            }
            employeeleavetransactionapproval.leaveapprovalstatus = leaveapprovalstatus
            employeeleavetransactionapproval.leaveapprovalhierarchy = leaveapprovalhierarchy
            employeeleavetransactionapproval.save(failOnError: true, flush: true)

            if(employeeleavetransactionapproval?.leaveapprovalhierarchy?.is_last_approval && leaveapprovalstatus.status == "approved"){
                def leavet = employeeleavetransactionapproval?.employeeleavetransaction?.leavetype?.type

                if(leavet == "ML" || leavet == "EL" || leavet == "LWP" || leavet == "HPL" || leavet == "Maternity Leave"){

                    def year = new Date().getYear() + 1900
                    def erpyear = ERPCalendarYear.findByYear("" + year)

                    LeaveRegistrationSrno ispresent = LeaveRegistrationSrno.findByLeavetypeAndErpcalendaryearAndOrganization(employeeleavetransactionapproval?.employeeleavetransaction?.leavetype, erpyear, employeeleavetransactionapproval.organization)

                    if(ispresent){
                        def leavesrno = ispresent?.leave_registation_srno + 1

                        ispresent.leave_registation_srno = leavesrno
                        ispresent.username = user
                        ispresent.updation_date = new java.util.Date()
                        ispresent.updation_ip_address = ip
                        ispresent.save(failOnError: true, flush: true)

                        employeeleavetransactionapproval.employeeleavetransaction.leave_registation_srno = leavesrno

                    }else{
                        def leavesrno = 1

                        ispresent = new LeaveRegistrationSrno()
                        ispresent.leave_registation_srno = leavesrno
                        ispresent.leavetype = employeeleavetransactionapproval?.employeeleavetransaction?.leavetype
                        ispresent.erpcalendaryear = erpyear
                        ispresent.organization = employeeleavetransactionapproval.organization
                        ispresent.username = user
                        ispresent.creation_date = new java.util.Date()
                        ispresent.updation_date = new java.util.Date()
                        ispresent.creation_ip_address = ip
                        ispresent.updation_ip_address = ip
                        ispresent.save(failOnError: true, flush: true)

                        e.employeeleavetransaction.leave_registation_srno = leavesrno
                    }
                }

                employeeleavetransactionapproval.employeeleavetransaction.leaveapprovalstatus = leaveapprovalstatus
                employeeleavetransactionapproval.approvedby = approvedby
                employeeleavetransactionapproval.updation_date = new Date()
                employeeleavetransactionapproval.save(failOnError: true, flush: true)

                def toDateNew = employeeleavetransactionapproval.employeeleavetransaction.todate.toString().split(' ')
                def fromDateNew = employeeleavetransactionapproval.employeeleavetransaction.fromdate.toString().split(' ')
                Date fromdate = new SimpleDateFormat("yyyy-MM-dd").parse(fromDateNew[0])
                Date todate = new SimpleDateFormat("yyyy-MM-dd").parse(toDateNew[0])
                leaveManagementSystemService.setIntoAttendanceOnLeaveCancelOrApprove(employeeleavetransactionapproval.employeeleavetransaction.instructor,
                        fromdate,
                        todate,
                        employeeleavetransactionapproval.leaveapprovalstatus.status)
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy")
                employeeleavetransactionapproval.employeeleavetransaction.leaveapprovalstatus = employeeleavetransactionapproval.leaveapprovalstatus
                employeeleavetransactionapproval.save(failOnError: true, flush: true)
                LeaveNotificationHistory leavenotificationhistory = LeaveNotificationHistory.findByAcademicyearAndOrganizationAndEmployeeleavetransactionapproval(employeeleavetransactionapproval?.employeeleavetransaction?.academicyear, employeeleavetransactionapproval.organization, employeeleavetransactionapproval)
                if (leavenotificationhistory == null) {
                    leavenotificationhistory = new LeaveNotificationHistory()
                    leavenotificationhistory.academicyear = employeeleavetransactionapproval?.employeeleavetransaction?.academicyear
                    leavenotificationhistory.organization = employeeleavetransactionapproval.organization
                    leavenotificationhistory.employeeleavetransactionapproval = employeeleavetransactionapproval
                    leavenotificationhistory.mailsentdate = new java.util.Date()
                    leavenotificationhistory.username = user
                    leavenotificationhistory.creation_date = new java.util.Date()
                    leavenotificationhistory.updation_date = new java.util.Date()
                    leavenotificationhistory.creation_ip_address = ip
                    leavenotificationhistory.updation_ip_address = ip
                    leavenotificationhistory.save(failOnError: true, flush: true)
                    if(employeeleavetransactionapproval?.organization?.establishment_email) {
                        def leaveemail = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Send Leave Application Email', employeeleavetransactionapproval?.organization)?.value
                        if(leaveemail == 'true') {
                            if (employeeleavetransactionapproval.leaveapprovalstatus.status == "rejected")
                                sendMailService.sendmail( employeeleavetransactionapproval.organization.establishment_email, employeeleavetransactionapproval.organization.establishment_email_credentials, employeeleavetransactionapproval.employeeleavetransaction.instructor.uid, "Your Leave Application dated:" + formatter.format( employeeleavetransactionapproval.employeeleavetransaction.application_date ) + " is NOT Sanctioned..", "Your Leave Application Dated:" + formatter.format( employeeleavetransactionapproval.employeeleavetransaction.application_date ) + " is rejected by TRUST.\n\nDetails are as below:" + "\n\n Leave Type:" + employeeleavetransactionapproval.employeeleavetransaction.leavetype.type + "\n\n From Date:" + formatter.format( employeeleavetransactionapproval.employeeleavetransaction.fromdate ) + " \nTo Date:" + formatter.format( employeeleavetransactionapproval.employeeleavetransaction.todate ) + "\n\nNumber of Days:" + employeeleavetransactionapproval.employeeleavetransaction.numberofdays + "\n\nRemarks : " + employeeleavetransactionapproval.remark + " \n\n Thanks", "", "" )
                            else
                                sendMailService.sendmail( employeeleavetransactionapproval.organization.establishment_email, employeeleavetransactionapproval.organization.establishment_email_credentials, employeeleavetransactionapproval.employeeleavetransaction.instructor.uid, "Your Leave Application dated:" + formatter.format( employeeleavetransactionapproval.employeeleavetransaction.application_date ) + " is Sanctioned..", "Your Leave Application Dated:" + formatter.format( employeeleavetransactionapproval.employeeleavetransaction.application_date ) + " is Sanctioned by TRUST.\n\nDetails are as below:" + "\n\n Leave Type:" + employeeleavetransactionapproval.employeeleavetransaction.leavetype.type + "\n\n From Date:" + formatter.format( employeeleavetransactionapproval.employeeleavetransaction.fromdate ) + " \nTo Date:" + formatter.format( employeeleavetransactionapproval.employeeleavetransaction.todate ) + "\n\nNumber of Days:" + employeeleavetransactionapproval.employeeleavetransaction.numberofdays + "\n\nRemarks : " + employeeleavetransactionapproval.remark + " \n\n Thanks", "", "" )
                        }
                    }
                }
            }

            if(employeeleavetransactionapproval?.leaveapprovalstatus?.status == 'approved' && employeeleavetransactionapproval?.leaveapprovalhierarchy?.is_last_approval == false)
                autoApproveOnMinDaysOfLeave(employeeleavetransactionapproval, ip, user)
        }
    }

    def getTimeTableLoad(def employeeleavetransaction, def aay){
        def org = employeeleavetransaction?.organization
        def inst = employeeleavetransaction?.instructor
        def fromdate = employeeleavetransaction?.fromdate
        def todate = employeeleavetransaction?.todate
        def erpTimeTablelistArray = []
        (fromdate..todate).each { today ->
            def erpday = ERPDay.findByDaysequence(today.getDay()+1)
            def loadflag = isweeklyoffOrholiday(today, inst)
            if(loadflag) {
                def erpTimeTableVersion = ERPTimeTableVersion.findByOrganizationAndAcademicyearAndSemesterAndDepartmentAndIsCurrent( org, aay?.academicyear, aay?.semester, inst?.department, true )
                def erpTimeTablelist = ERPTimeTable.findAllByErpdayAndInstructorAndErptimetableversionAndAcademicyearAndSemesterAndOrganization( erpday, inst, erpTimeTableVersion, aay?.academicyear, aay?.semester, org )
                if (erpTimeTablelist) {
                    def array = []
                    for(load in erpTimeTablelist) {
                        LeaveLoadAdjustment leaveloadadjustment = LeaveLoadAdjustment.findByLeave_dateAndStart_timeAndEnd_timeAndEmployeeleavetransactionAndIscustomloadAndIsdeleted( today, load?.slot?.start_time, load?.slot?.end_time, employeeleavetransaction, false, false )
                        array.add(load : load, leaveloadadjustment : leaveloadadjustment)
                    }
                    erpTimeTablelistArray.add( date: today, erpTimeTablelist: array )
                }
            }
        }
        return erpTimeTablelistArray
    }

    def getCustomeLoad(def employeeleavetransaction){
        def inst = employeeleavetransaction?.instructor
        def fromdate = employeeleavetransaction?.fromdate
        def todate = employeeleavetransaction?.todate
        def loadflag = false
        (fromdate..todate).each { today ->
            if(isweeklyoffOrholiday(today, inst))
                loadflag = true
        }
        return loadflag
    }

    def isweeklyoffOrholiday(def today, def inst){
        def erpmonth = ERPMonth.findByMonthnumber(today.getMonth()+1)
        def year = today.getYear()+1900
        def erpyear = ERPCalendarYear.findByYear(""+year)
        def attendance = EmployeeAttendance.findByInstructorAndErpcalenderintegerdayAndErpmonthAndErpcalenderyear(inst, today.getDate(), erpmonth, erpyear)
        def loadflag = false
        if(!attendance?.isweeklyoff && !attendance?.isnonworkingday)
            loadflag = true

        return loadflag
    }

    def getCustomeLoadDetails(def employeeleavetransaction){
        def inst = employeeleavetransaction?.instructor
        def fromdate = employeeleavetransaction?.fromdate
        def todate = employeeleavetransaction?.todate
        def array = []
        (fromdate..todate).each { today ->
            if(isweeklyoffOrholiday(today, inst))
            {
                def leaveloadadjustment = LeaveLoadAdjustment.findAllByLeave_dateAndEmployeeleavetransactionAndIscustomloadAndIsdeleted( today, employeeleavetransaction, true, false)
                array.add(date : today, leaveloadadjustment : leaveloadadjustment)
            }
        }
        return array
    }

    def nextInstanceApproveLast(def e){
        InstructorLeaveAprovalHierarchy instleaveapprovalhierarchy = InstructorLeaveAprovalHierarchy.findByInstructorAndLeavetypeAndOrganization(e.employeeleavetransaction.instructor, e.employeeleavetransaction.leavetype, e.employeeleavetransaction.instructor.organization)
        if(instleaveapprovalhierarchy) {
            LeaveApprovalHierarchy leaveapprovalhierarchy = LeaveApprovalHierarchy.findByLevelAndLeaveapprovalhierarchymasterAndOrganization( e.leaveapprovalhierarchy.level + 1, instleaveapprovalhierarchy?.leaveapprovalhierarchymaster, e.employeeleavetransaction.instructor.organization )
            if (e.employeeleavetransaction.instructor.reportingorganization != null && leaveapprovalhierarchy?.leaveapporvalauthority?.authority != "Registrar") {
                leaveapprovalhierarchy = LeaveApprovalHierarchy.findByLevelAndLeaveapprovalhierarchymasterAndOrganization( e.leaveapprovalhierarchy.level + 1, instleaveapprovalhierarchy?.leaveapprovalhierarchymaster, e.employeeleavetransaction.instructor.reportingorganization )
            }
            if (leaveapprovalhierarchy?.leaveapporvalauthority?.minnoofleavesforinstanceapproval >=  e?.employeeleavetransaction?.numberofdays) {
                if(leaveapprovalhierarchy?.is_last_approval)
                    return 1
            }
        }
        return 0
    }


    // COMP-OFF
    def nextInstanceApproveCompOffLast(def e){
        InstructorLeaveAprovalHierarchy instleaveapprovalhierarchy = InstructorLeaveAprovalHierarchy.findByInstructorAndLeavetypeAndOrganization(e.employeeleavemaster.instructor, e.employeeleavemaster.leavetype, e.employeeleavemaster.instructor.organization)
        if(instleaveapprovalhierarchy) {
            LeaveApprovalHierarchy leaveapprovalhierarchy = LeaveApprovalHierarchy.findByLevelAndLeaveapprovalhierarchymasterAndOrganization( e.leaveapprovalhierarchy.level + 1, instleaveapprovalhierarchy?.leaveapprovalhierarchymaster, e.employeeleavemaster.instructor.organization )
            if (e.employeeleavemaster.instructor.reportingorganization != null && leaveapprovalhierarchy?.leaveapporvalauthority?.authority != "Registrar") {
                leaveapprovalhierarchy = LeaveApprovalHierarchy.findByLevelAndLeaveapprovalhierarchymasterAndOrganization( e.leaveapprovalhierarchy.level + 1, instleaveapprovalhierarchy?.leaveapprovalhierarchymaster, e.employeeleavemaster.instructor.reportingorganization )
            }
            if (leaveapprovalhierarchy?.leaveapporvalauthority?.minnoofleavesforinstanceapproval >=  e?.employeeleavemaster?.numberofdays) {
                if(leaveapprovalhierarchy?.is_last_approval)
                    return 1
            }
        }
        return 0
    }

    def autoApproveOnMinDaysOfCOMP_OFF(def e, def ip, def user){
        def approvedby = e?.approvedby
        InstructorLeaveAprovalHierarchy instleaveapprovalhierarchy = InstructorLeaveAprovalHierarchy.findByInstructorAndLeavetypeAndOrganization(e.employeeleavemaster.instructor, e.employeeleavemaster.leavetype, e.employeeleavemaster.instructor.organization)
        if(instleaveapprovalhierarchy) {
            LeaveApprovalHierarchy leaveapprovalhierarchy = LeaveApprovalHierarchy.findByLevelAndLeaveapprovalhierarchymasterAndOrganization(e.leaveapprovalhierarchy.level + 1, instleaveapprovalhierarchy?.leaveapprovalhierarchymaster, e.employeeleavemaster.instructor.organization)
            if (e.employeeleavemaster.instructor.reportingorganization != null && leaveapprovalhierarchy?.leaveapporvalauthority?.authority != "Registrar") {
                leaveapprovalhierarchy = LeaveApprovalHierarchy.findByLevelAndLeaveapprovalhierarchymasterAndOrganization(e.leaveapprovalhierarchy.level + 1, instleaveapprovalhierarchy?.leaveapprovalhierarchymaster, e.employeeleavemaster.instructor.reportingorganization)
            }

            def approvedbynew
            if(leaveapprovalhierarchy.leaveapporvalauthority.authority == "HOD") {
                if (e?.employeeleavemaster?.instructor.reporttingauthority != null) {
                    approvedbynew = e?.employeeleavemaster?.instructor?.reporttingauthority
                } else if (e?.employeeleavemaster?.instructor?.reportingdepartment != null) {
                    approvedbynew = e?.employeeleavemaster?.instructor?.reportingdepartment.hod
                } else if (e?.employeeleavemaster?.instructor?.department != null) {
                    approvedbynew = e?.employeeleavemaster?.instructor?.department.hod
                }
            }else if(leaveapprovalhierarchy.leaveapporvalauthority.authority == "Dean"){
                approvedbynew = e?.employeeleavemaster?.instructor?.department?.stream?.dean
            }

            CompOffTransactionApproval compofftransactionapproval = new CompOffTransactionApproval()
            compofftransactionapproval.application_push_date = new java.util.Date()
            compofftransactionapproval.updation_date = new java.util.Date()
            compofftransactionapproval.updation_username = user
            compofftransactionapproval.creation_username = user
            compofftransactionapproval.approvedby = approvedbynew
            compofftransactionapproval.creation_date = new java.util.Date()
            compofftransactionapproval.creation_ip_address = ip
            compofftransactionapproval.updation_ip_address = ip
            compofftransactionapproval.organization = e.organization
            compofftransactionapproval.employeeleavemaster = e.employeeleavemaster
            LeaveApprovalStatus leaveapprovalstatus
            if(leaveapprovalhierarchy?.leaveapporvalauthority?.minnoofleavesforinstanceapproval >= e?.employeeleavemaster?.numberofdays) {
                leaveapprovalstatus = LeaveApprovalStatus.findByStatus( "approved" )
                compofftransactionapproval.remark = "Auto Approved By System"
            }else {
                leaveapprovalstatus = LeaveApprovalStatus.findByStatus( "inprocess" )
                compofftransactionapproval.remark = ""
            }
            compofftransactionapproval.leaveapprovalstatus = leaveapprovalstatus
            compofftransactionapproval.leaveapprovalhierarchy = leaveapprovalhierarchy
            compofftransactionapproval.save(failOnError: true, flush: true)

            if(compofftransactionapproval?.leaveapprovalhierarchy?.is_last_approval && leaveapprovalstatus.status == "approved"){
                compofftransactionapproval.employeeleavemaster.leaveapprovalstatus = CompOffStatus.findByStatus("Available")
                compofftransactionapproval.employeeleavemaster.updation_ip_address = ip
                compofftransactionapproval.employeeleavemaster.updation_date = new java.util.Date()
                compofftransactionapproval.employeeleavemaster.username = user
                compofftransactionapproval.save(failOnError: true, flush: true)
            }

            if(compofftransactionapproval?.leaveapprovalstatus?.status == 'approved' && compofftransactionapproval?.leaveapprovalhierarchy?.is_last_approval == false)
                autoApproveOnMinDaysOfCOMP_OFF(compofftransactionapproval, ip, user)
        }
    }

    def disableBackdatedLeave(def instructor, def ip, def user){
        instructor?.apply_backdated_leave = false
        instructor.username = user
        instructor.updation_date = new java.util.Date()
        instructor.updation_ip_address = ip
        instructor.save(failOnError: true, flush: true)
    }


    //Leave List
    def getLeaveInprogressList(LeaveType leavetype, Instructor instructor,Organization organization, AcademicYear ay, def checked, def fromdate, def todate, def ay_specific) {

        def List = []
        def results
        def leaveStatus = LeaveApprovalStatus.findByStatus("inprocess")
        def c = EmployeeLeaveTransaction.createCriteria()

        if(leavetype.leaveexpirytype.type == 'No Expiry' || leavetype.leaveexpirytype.type == 'Expiry Date') {
            if (leavetype.type != 'CL' && leavetype.type != 'HD_CL' && leavetype.type != 'ML' && leavetype.type != 'HPL' && leavetype.type != 'VACATION') {
                if(checked) {
                    if(ay_specific){
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("academicyear", ay)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                if(results)
                    List.addAll(results)
            } else if (leavetype.type == 'CL' || leavetype.type == 'HD_CL') {
                def leaveTypeList = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['CL', 'HD_CL'])
                            }
                        }

                if(checked) {
                    if(ay_specific) {
                        results = c.list() {
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                if(results)
                    List.addAll(results)
            } else if (leavetype.type == 'VACATION') {
                if(checked) {
                    if(ay_specific) {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                'in'("academicyear", ay)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                if(results)
                    List.addAll(results)

            } else if (leavetype.type == 'ML' || leavetype.type == 'HPL') {
                def hplresult
                def mlresult

                def leaveTypeML = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['ML'])
                            }
                        }
                def leaveTypeHPL = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['HPL'])
                            }
                        }

                if(ay_specific) {
                    mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                        'in'("leavetype", leaveTypeML)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }else {
                    mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                        'in'("leavetype", leaveTypeML)
                        and {
                            'in'("instructor", instructor)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                if(ay_specific) {
                    hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                        'in'("leavetype", leaveTypeHPL)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }else {
                    hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                        'in'("leavetype", leaveTypeHPL)
                        and {
                            'in'("instructor", instructor)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                if(hplresult)
                    List.addAll(hplresult)
                if(mlresult)
                    List.addAll(mlresult)
            }
        }else{
            if (leavetype.type != 'CL' && leavetype.type != 'HD_CL' && leavetype.type != 'ML' && leavetype.type != 'HPL' && leavetype.type != 'VACATION') {
                if(checked) {
                    results = c.list() {
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                if(results)
                    List.addAll(results)

            } else if (leavetype.type == 'CL' || leavetype.type == 'HD_CL') {
                def leaveTypeList = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['CL', 'HD_CL'])
                            }
                        }

                if(checked) {
                    results = c.list() {
                        'in'("leavetype", leaveTypeList)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        'in'("leavetype", leaveTypeList)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                if(results)
                    List.addAll(results)

            } else if (leavetype.type == 'VACATION') {
                if(checked) {
                    results = c.list() {
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }
                if(results)
                    List.addAll(results)

            } else if (leavetype.type == 'ML' || leavetype.type == 'HPL') {
                def hplresult
                def mlresult

                def leaveTypeML = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['ML'])
                            }
                        }
                def leaveTypeHPL = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['HPL'])
                            }
                        }

                mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                    'in'("leavetype", leaveTypeML)
                    and {
                        'in'("instructor", instructor)
                        'in'("academicyear", ay)
                        'in'("leaveapprovalstatus", leaveStatus)
                        'in'("organization", organization)
                    }
                }

                hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                    'in'("leavetype", leaveTypeHPL)
                    and {
                        'in'("instructor", instructor)
                        'in'("academicyear", ay)
                        'in'("leaveapprovalstatus", leaveStatus)
                        'in'("organization", organization)
                    }
                }

                if(hplresult)
                    List.addAll(hplresult)
                if(mlresult)
                    List.addAll(mlresult)
            }
        }

        return List
    }


    def getLeaveApprovedList(LeaveType leavetype, Instructor instructor,Organization organization, AcademicYear ay, def checked, def fromdate, def todate, def ay_specific) {

        def List = []
        def results
        def leaveStatus = LeaveApprovalStatus.findByStatus("approved")
        def c = EmployeeLeaveTransaction.createCriteria()

        if(leavetype.leaveexpirytype.type == 'No Expiry' || leavetype.leaveexpirytype.type == 'Expiry Date') {
            if (leavetype.type != 'CL' && leavetype.type != 'HD_CL' && leavetype.type != 'ML' && leavetype.type != 'HPL' && leavetype.type != 'VACATION') {
                if(checked) {
                    if(ay_specific) {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                'in'("academicyear", ay)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                if(results)
                    List.addAll(results)
            } else if (leavetype.type == 'CL' || leavetype.type == 'HD_CL') {
                def leaveTypeList = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['CL', 'HD_CL'])
                            }
                        }

                if(checked) {
                    if(ay_specific) {
                        results = c.list() {
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leaveTypeList)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                if(results)
                    List.addAll(results)
            } else if (leavetype.type == 'VACATION') {
                if(checked) {
                    if(ay_specific) {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                                ge("fromdate", fromdate)
                                le("todate", todate)
                            }
                        }
                    }
                }else{
                    if(ay_specific) {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("academicyear", ay)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }else {
                        results = c.list() {
                            'in'("leavetype", leavetype)
                            and {
                                'in'("instructor", instructor)
                                'in'("leaveapprovalstatus", leaveStatus)
                                'in'("organization", organization)
                            }
                        }
                    }
                }

                if(results)
                    List.addAll(results)
            } else if (leavetype.type == 'ML' || leavetype.type == 'HPL') {
                def hplresult
                def mlresult

                def leaveTypeML = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['ML'])
                            }
                        }
                def leaveTypeHPL = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['HPL'])
                            }
                        }

                if(ay_specific) {
                    mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                        'in'("leavetype", leaveTypeML)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }else {
                    mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                        'in'("leavetype", leaveTypeML)
                        and {
                            'in'("instructor", instructor)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                if(ay_specific) {
                    hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                        'in'("leavetype", leaveTypeHPL)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }else {
                    hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                        'in'("leavetype", leaveTypeHPL)
                        and {
                            'in'("instructor", instructor)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                if(hplresult)
                    List.addAll(hplresult)
                if(mlresult)
                    List.addAll(mlresult)
            }

        }else{

            if (leavetype.type != 'CL' && leavetype.type != 'HD_CL' && leavetype.type != 'ML' && leavetype.type != 'HPL' && leavetype.type != 'VACATION') {
                if(checked) {
                    results = c.list() {
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                if(results)
                    List.addAll(results)
            } else if (leavetype.type == 'CL' || leavetype.type == 'HD_CL') {
                def leaveTypeList = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['CL', 'HD_CL'])
                            }
                        }

                if(checked) {
                    results = c.list() {
                        'in'("leavetype", leaveTypeList)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        'in'("leavetype", leaveTypeList)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                if(results)
                    List.addAll(results)
            } else if (leavetype.type == 'VACATION') {
                if(checked) {
                    results = c.list() {
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }
                }else{
                    results = c.list() {
                        'in'("leavetype", leavetype)
                        and {
                            'in'("instructor", instructor)
                            'in'("academicyear", ay)
                            'in'("leaveapprovalstatus", leaveStatus)
                            'in'("organization", organization)
                        }
                    }
                }

                if(results)
                    List.addAll(results)
            } else if (leavetype.type == 'ML' || leavetype.type == 'HPL') {
                def hplresult
                def mlresult

                def leaveTypeML = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['ML'])
                            }
                        }
                def leaveTypeHPL = LeaveType.createCriteria().list()
                        {
                            like("organization", organization)
                            and {
                                'in'("type", ['HPL'])
                            }
                        }

                mlresult = EmployeeLeaveTransaction.createCriteria().list() {
                    'in'("leavetype", leaveTypeML)
                    and {
                        'in'("instructor", instructor)
                        'in'("leaveapprovalstatus", leaveStatus)
                        'in'("academicyear", ay)
                        'in'("organization", organization)
                    }
                }

                hplresult = EmployeeLeaveTransaction.createCriteria().list() {
                    'in'("leavetype", leaveTypeHPL)
                    and {
                        'in'("instructor", instructor)
                        'in'("leaveapprovalstatus", leaveStatus)
                        'in'("academicyear", ay)
                        'in'("organization", organization)
                    }
                }

                if(hplresult)
                    List.addAll(hplresult)
                if(mlresult)
                    List.addAll(mlresult)
            }
        }

        return List
    }

}