package volp

import grails.gorm.transactions.Transactional
import java.text.SimpleDateFormat;
import java.util.Date;

@Transactional
class ERPTimeTableService {
    InformationService InformationService

    def markStudentAttendanceTeacher(Instructor instructor, String id, String flag, String ip , def erpcourseofferingbatch){

        println("markStudentAttendanceTeacher ")

        def attendanceflag = false
        if(flag == 'true')
            attendanceflag = true

        def roletypesetting = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Online Attendance Time Buffer', instructor.organization)

        def erptimetable = ERPTimeTable.findById(id)
        Date lecdate = new Date()
        def erpday = ERPDay.findByDaysequence(lecdate.getDay() + 1)
        def start_time = erptimetable?.slot?.start_time.split(':')
        def end_time = erptimetable?.slot?.end_time.split(':')
        def date1 = new Date()
        date1.setHours(Integer.parseInt(start_time[0]));
        if(roletypesetting)
            date1.setMinutes(Integer.parseInt(start_time[1])- Integer.parseInt(roletypesetting?.value));
        else
            date1.setMinutes(Integer.parseInt(start_time[1])-15);
        def date2 = new Date()
        date2.setHours(Integer.parseInt(end_time[0]));
        date2.setMinutes(Integer.parseInt(end_time[1]));

        def startedlecture = false
        if(erpday?.id == erptimetable?.erpday?.id) {
            if (lecdate >= date1 && lecdate <= date2)
                startedlecture = true
        }

        if(startedlecture) {
            ERPCourseOfferingBatchInstructor insbatch = ERPCourseOfferingBatchInstructor.findByErpcourseofferingbatch(erpcourseofferingbatch)

            def erpCourseOffering = erpcourseofferingbatch?.erpcourseoffering

            if(erpCourseOffering?.courserule?.name == 'EL' && erpCourseOffering?.erpcoursecategory?.name != 'EL-Div'){
                if(!erpcourseofferingbatch?.batch_number?.contains(erpCourseOffering.erpcourse.course_code)) {
                    ERPCourseTypeAcademicsLoadTypes lt = erpcourseofferingbatch?.erpcoursetypeacademicsloadtype
                    def typelist = ERPCourseTypeAcademicsLoadTypes.findAllByCoursetypeacademicsAndLoadtype(erpCourseOffering?.coursetypeacademics, lt?.loadtype)
                    typelist.sort { it?.id }
                    def number = 0
                    def p = 1
                    for (t in typelist) {
                        if (lt?.id == t?.id)
                            number = p
                        p++
                    }
                    def batchno = Integer.parseInt(erpcourseofferingbatch?.batch_number)
                    if(batchno == -1)
                        batchno = 1
                    String batchnumber = electivebatchname(erpCourseOffering.erpcourse.course_code, erpCourseOffering?.erpcourse?.courseAbbr, erptimetable?.loadtypecategory.type, batchno, number)
                    ERPCourseOfferingBatch b = ERPCourseOfferingBatch.findByBatch_numberAndErpcourseofferingAndLoadtype(batchnumber, erpCourseOffering, erpcourseofferingbatch?.loadtype)
                    insbatch = ERPCourseOfferingBatchInstructor.findByErpcourseofferingbatch(b)
                }
            }

            if (insbatch) {
                ERPCourseOfferingBatch batch = insbatch?.erpcourseofferingbatch

                def periodlist = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatch(insbatch?.erpcourseofferingbatch)
                TreeSet period = []
                TreeSet periodwithextralecture = []
                for(item in periodlist){
                    if(!item?.erpcourseofferingplan?.isextralecture)
                        period.add(item?.period_number)
                    periodwithextralecture.add(item?.period_number)
                }
                def lec
                if (period.size() == 0)
                    lec = InformationService.getFirstLecture(insbatch, 1)
                else
                    lec = InformationService.getLectureno(insbatch, period?.last())

                lec = Double.parseDouble(lec+"")

                if(periodwithextralecture?.contains(lec)){
                    if (periodwithextralecture.size() == 0)
                        lec = InformationService.getFirstLecture(insbatch, 1)
                    else
                        lec = InformationService.getLectureno(insbatch, periodwithextralecture?.last())
                }

                lec = Double.parseDouble(lec+"")

                def current = lec?.toString()?.split("[.]")
                def lectno = Integer.parseInt(current[0])
                def erpcourseofferingplan = ERPCourseOfferingPlan.findByErpcourseofferingbatchinstructorAndLecture_number_planned(insbatch, lectno)

                if (insbatch.erpcourseofferingbatch.erpcourseoffering.courserule.name != "EL") {
                    Slot slot = erptimetable?.slot
                    ERPCourseInstructorAttendanceSlot erpCourseInstructorAttendanceSlot =
                            ERPCourseInstructorAttendanceSlot.findByDivisionofferingAndSlotAndAcademicyearAndSemesterAndExecution_date(
                                    insbatch.erpcourseofferingbatch.divisionoffering, slot,
                                    insbatch.erpcourseofferingbatch.erpcourseoffering.academicyear,
                                    insbatch.erpcourseofferingbatch.erpcourseoffering.semester, lecdate.clearTime())

                    if (erpCourseInstructorAttendanceSlot != null) {
                        def isbatchcreated = false
                        if (insbatch?.erpcourseofferingbatch?.id != erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch?.id) {
                            if (insbatch.erpcourseofferingbatch.loadtype.type.type == "Theory") {
                                isbatchcreated = true
                            } else {
                                if (erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch?.batch_number == insbatch?.erpcourseofferingbatch?.batch_number)
                                    isbatchcreated = true
                            }
                        }
                        if (isbatchcreated) {
                            return  "153 : Attendence for " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.erpcourseoffering.program.name + ", division - " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.divisionoffering.division.name + " , slot " + slot + " is already filled by " + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor?.person?.fullname_as_per_previous_marksheet + "-" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatchinstructor?.instructor.uid + " , for  subject -" + erpCourseInstructorAttendanceSlot?.erpcourseofferingbatch.erpcourseoffering.erpcourse.course_name
                        }
                    }
                }

                ERPCourseInstructorAttendanceSlot existingslot = ERPCourseInstructorAttendanceSlot.findByErpcourseofferingbatchinstructorAndSlotAndExecution_date(insbatch, erptimetable?.slot, lecdate.clearTime())
                def period_number = lec
                if (!existingslot) {
                    if (insbatch.erpcourseofferingbatch.loadtype.type.type == "Theory") {
                        Slot slot = erptimetable?.slot
                        ERPCourseInstructorAttendanceSlot erpCourseInstructorAttendanceSlot = new ERPCourseInstructorAttendanceSlot()
                        erpCourseInstructorAttendanceSlot.username = instructor?.uid
                        erpCourseInstructorAttendanceSlot.creation_date = new java.util.Date()
                        erpCourseInstructorAttendanceSlot.updation_date = new java.util.Date()
                        erpCourseInstructorAttendanceSlot.creation_ip_address = ip
                        erpCourseInstructorAttendanceSlot.updation_ip_address = ip
                        erpCourseInstructorAttendanceSlot.start_attendance = attendanceflag
                        erpCourseInstructorAttendanceSlot.erpcourseofferingplan = erpcourseofferingplan
                        erpCourseInstructorAttendanceSlot.slot = slot
                        erpCourseInstructorAttendanceSlot.erpcourseofferingbatchinstructor = insbatch
                        erpCourseInstructorAttendanceSlot.erpcourseofferingbatch = insbatch.erpcourseofferingbatch
                        erpCourseInstructorAttendanceSlot.period_number = lec
                        erpCourseInstructorAttendanceSlot.execution_date = lecdate.clearTime()
                        erpCourseInstructorAttendanceSlot.erpcourseoffering = insbatch.erpcourseofferingbatch.erpcourseoffering
                        erpCourseInstructorAttendanceSlot.divisionoffering = insbatch.erpcourseofferingbatch.divisionoffering
                        erpCourseInstructorAttendanceSlot.instructor = insbatch.instructor
                        erpCourseInstructorAttendanceSlot.academicyear = insbatch.erpcourseofferingbatch.erpcourseoffering.academicyear
                        erpCourseInstructorAttendanceSlot.semester = insbatch.erpcourseofferingbatch.erpcourseoffering.semester
                        erpCourseInstructorAttendanceSlot.organization = insbatch.erpcourseofferingbatch.erpcourseoffering.organization
                        erpCourseInstructorAttendanceSlot.loadtypecategory = insbatch.erpcourseofferingbatch.loadtype.type
                        erpCourseInstructorAttendanceSlot.save(flush: true, failOnError: true)
                        period_number = erpCourseInstructorAttendanceSlot?.period_number
                    } else if (insbatch.erpcourseofferingbatch.loadtype.type.type == "Lab" || insbatch.erpcourseofferingbatch.loadtype.type.type == "Tutorial") {
                        def firstslot = erptimetable?.slot
                        def slotArray = []
                        slotArray.add(firstslot)

                        def roletypesettingpracticalhr = ERPRoleTypeSettings.findByNameIlikeAndOrganization('No. of Practical Hours Per Turn', instructor.organization)?.value
                        if(roletypesettingpracticalhr)
                            roletypesettingpracticalhr = Integer.parseInt(roletypesettingpracticalhr)
                        else
                            roletypesettingpracticalhr = 2

                        for(def pp = 1; pp < roletypesettingpracticalhr; pp++) {
                            def secondslot = Slot.findByOrganizationAndAcademicyearAndSemesterAndSlottypeAndSlot_number(firstslot?.organization, firstslot?.academicyear, firstslot?.semester, firstslot?.slottype, firstslot?.slot_number + pp)
                            if (secondslot)
                                slotArray.add(secondslot)
                        }

                        for (def i = 0; i < slotArray.size(); i++) {
                            Slot slot = slotArray[i]
                            ERPCourseInstructorAttendanceSlot erpCourseInstructorAttendanceSlot = new ERPCourseInstructorAttendanceSlot()
                            erpCourseInstructorAttendanceSlot.username = instructor?.uid
                            erpCourseInstructorAttendanceSlot.creation_date = new java.util.Date()
                            erpCourseInstructorAttendanceSlot.updation_date = new java.util.Date()
                            erpCourseInstructorAttendanceSlot.creation_ip_address = ip
                            erpCourseInstructorAttendanceSlot.updation_ip_address = ip
                            erpCourseInstructorAttendanceSlot.start_attendance = attendanceflag
                            erpCourseInstructorAttendanceSlot.erpcourseofferingplan = erpcourseofferingplan
                            erpCourseInstructorAttendanceSlot.slot = slot
                            erpCourseInstructorAttendanceSlot.erpcourseofferingbatchinstructor = insbatch
                            erpCourseInstructorAttendanceSlot.erpcourseofferingbatch = insbatch.erpcourseofferingbatch
                            erpCourseInstructorAttendanceSlot.period_number = lec
                            erpCourseInstructorAttendanceSlot.execution_date = lecdate.clearTime()
                            erpCourseInstructorAttendanceSlot.erpcourseoffering = insbatch.erpcourseofferingbatch.erpcourseoffering
                            erpCourseInstructorAttendanceSlot.divisionoffering = insbatch.erpcourseofferingbatch.divisionoffering
                            erpCourseInstructorAttendanceSlot.instructor = insbatch.instructor
                            erpCourseInstructorAttendanceSlot.academicyear = insbatch.erpcourseofferingbatch.erpcourseoffering.academicyear
                            erpCourseInstructorAttendanceSlot.semester = insbatch.erpcourseofferingbatch.erpcourseoffering.semester
                            erpCourseInstructorAttendanceSlot.organization = insbatch.erpcourseofferingbatch.erpcourseoffering.organization
                            erpCourseInstructorAttendanceSlot.loadtypecategory = insbatch.erpcourseofferingbatch.loadtype.type
                            erpCourseInstructorAttendanceSlot.save(flush: true, failOnError: true)
                            period_number = erpCourseInstructorAttendanceSlot?.period_number
                        }
                    }
                }else{
                    period_number = existingslot?.period_number
                }

                // Add Attendance Load Per Instructor
                def slotlistAttendance = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(insbatch, insbatch.erpcourseofferingbatch, lec)
                if(slotlistAttendance) {
                    addInstructorAttendanceLoad(insbatch?.instructor, slotlistAttendance, instructor?.uid, ip)
                }


                // Add Attendance Timesheet
                if(insbatch) {
                    def totalstudents = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(insbatch, insbatch.erpcourseofferingbatch, lec)?.size()
                    def presentstudents = ERPCourseOfferingBatchLearnerAttendance.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_numberAndIs_present(insbatch, insbatch.erpcourseofferingbatch, lec, true).size()
                    def slotList = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(insbatch, insbatch.erpcourseofferingbatch, lec)
                    def starttime = ""
                    def endtime = ""
                    if (slotList) {
                        starttime = slotList[0]?.slot?.start_time
                        endtime = slotList[slotList?.size() - 1]?.slot?.end_time
                    }
                    def coursename = insbatch?.erpcourseoffering?.erpcourse?.course_code + ":" + insbatch?.erpcourseoffering?.erpcourse?.course_name
                    def taskname = ""
                    if (!insbatch?.erpcourseofferingbatch?.divisionoffering)
                        taskname = "Attendance of Elective Course-" + coursename + " Batch No. " + insbatch?.erpcourseofferingbatch?.batch_number + " Lecture No. " + lec
                    else {
                        if (insbatch?.erpcourseofferingbatch?.batch_number == '-1')
                            taskname = "Attendance of Theory Division-" + insbatch?.erpcourseofferingbatch?.divisionoffering + " Course-" + coursename + " Lecture No. " + lec
                        else {
                            if (insbatch?.erpcourseofferingbatch?.loadtype?.type?.type == 'Theory')
                                taskname = "Attendance of Theory Division-" + insbatch?.erpcourseofferingbatch?.divisionoffering + " Course-" + coursename + " Batch No. " + insbatch?.erpcourseofferingbatch?.batch_number + " Lecture No." + lec
                            else
                                taskname = "Attendance of "+insbatch?.erpcourseofferingbatch?.loadtype?.type?.type+" Division-" + insbatch?.erpcourseofferingbatch?.divisionoffering + " Course-" + coursename + " Batch No. " + insbatch?.erpcourseofferingbatch?.batch_number + " Lecture No. " + lec
                        }
                    }

                    def description = "Task Added By System On Attendance Filling"
                    addTimeSheet(insbatch?.instructor, insbatch?.erpcourseoffering?.program, insbatch?.erpcourseoffering?.year,
                            coursename, presentstudents, totalstudents, lecdate.clearTime(), starttime, endtime, taskname, description, ip)
                }

                def repectiveBatchLearner = ERPCourseOfferingBatchLearner.findAllByErpcourseofferingbatch(insbatch.erpcourseofferingbatch)

                if (repectiveBatchLearner) {
                    for (ERPCourseOfferingBatchLearner obj : repectiveBatchLearner) {
                        def learner = obj?.learner
                        ERPCourseOfferingBatchLearner lbatch = ERPCourseOfferingBatchLearner.findByLearnerAndErpcourseofferingbatch(learner, batch)
                        ERPCourseOfferingBatchLearnerAttendance la = ERPCourseOfferingBatchLearnerAttendance.findByErpcourseofferingbatchlearnerAndErpcourseofferingbatchinstructorAndPeriod_number(lbatch, insbatch, period_number)
                        if (la == null) {
                            la = new ERPCourseOfferingBatchLearnerAttendance()
                            la.username = instructor?.uid
                            la.creation_date = new java.util.Date()
                            la.updation_date = new java.util.Date()
                            la.creation_ip_address = ip
                            la.updation_ip_address = ip

                            la.erpcourseofferingplan = erpcourseofferingplan
                            la.period_number = period_number
                            la.execution_date = lecdate.clearTime()
                            la.is_present = false
                            la.erpcourseofferingbatchlearner = lbatch
                            la.erpcourseofferingbatchinstructor = insbatch
                            la.erpcourseofferingbatch = insbatch.erpcourseofferingbatch
                            la.erpcourseoffering = insbatch.erpcourseoffering
                            la.learner = learner
                            la.instructor = insbatch.instructor
                            la.save(flush: true, failOnError: true)

                            if(erpcourseofferingplan) {
                                erpcourseofferingplan.execution_date = lecdate.clearTime()
                                erpcourseofferingplan.username = instructor?.uid
                                erpcourseofferingplan.updation_date = new java.util.Date()
                                erpcourseofferingplan.updation_ip_address = ip
                                erpcourseofferingplan.save(flush: true, failOnError: true)
                            }
                        }
                    }
                } else {
                    return "No students assigned to this Course/Subject by class teacher."
                }
            }else{
                return "Course Offering Batch Instrcutor Not Set"
            }
        }
        return 1
    }

    def electivebatchname(String course_code, String course_name, String type, int batch, def num){
        def val = 'B'
        if(type == "Theory")
            val = 'D'
        return course_code+'_'+course_name+'_'+type+'_'+num+'_'+val+(batch)
    }

    def getTimeTableSlot(def academicyear, def semester, def program, def year, def slottype, def org){
        ERPTimeTableVersion currenttimetableversion = ERPTimeTableVersion.findByAcademicyearAndSemesterAndIsCurrentAndDepartment(academicyear, semester, true, program?.department)
        def programyear = ProgramYear.findByOrganizationAndProgramAndYear(org, program, year)
        def slotmaster = ProgramYearSlotMaster.findByAcademicyearAndSemesterAndOrganizationAndProgramyearAndErptimetableversionAndIslocked(academicyear, semester, org, programyear, currenttimetableversion, true)?.slotmaster
        def slotlist
        if(slotmaster) {
            if(slottype)
                slotlist = Slot.findAllByOrganizationAndAcademicyearAndSemesterAndSlottypeAndSlotmaster(org, academicyear, semester, slottype, slotmaster)
            else
                slotlist = Slot.findAllByOrganizationAndAcademicyearAndSemesterAndSlotmaster(org, academicyear, semester, slotmaster)
        }
        return slotlist
    }

    def getTimeTableProgramYearSlot(def academicyear, def semester, def program, def year, def org){
        ERPTimeTableVersion currenttimetableversion = ERPTimeTableVersion.findByAcademicyearAndSemesterAndIsCurrentAndDepartment(academicyear, semester, true, program?.department)
        def programyear = ProgramYear.findByOrganizationAndProgramAndYear(org, program, year)
        def slotmaster = ProgramYearSlotMaster.findByAcademicyearAndSemesterAndOrganizationAndProgramyearAndErptimetableversionAndIslocked(academicyear, semester, org, programyear, currenttimetableversion, true)
        return slotmaster
    }

    def addTimeSheet(def instructor, def program, def year, def coursename, def presentstudents,  def totalstudents, def date,
                     def starttime, def endtime, def taskname, def description, def ip){

        def org = instructor?.organization

        def erptimesheet = ERPTimeSheet.findByInstructorAndOrganizationAndName(instructor, org, taskname)

        if(!erptimesheet) {
            erptimesheet = new ERPTimeSheet()
            erptimesheet.name = taskname
            erptimesheet.description = description
            erptimesheet.instructor = instructor
            erptimesheet.organization = org
            erptimesheet.year = year
            erptimesheet.program = program
            erptimesheet.creation_username = instructor.uid
            erptimesheet.creation_ip_address = ip
            erptimesheet.creation_date = new java.util.Date()
        }

        erptimesheet.date = date
        erptimesheet.starttime = starttime
        erptimesheet.endtime = endtime
        erptimesheet.coursename = coursename
        erptimesheet.presentstudents = presentstudents
        erptimesheet.totalstudents = totalstudents
        erptimesheet.updation_username = instructor.uid
        erptimesheet.updation_ip_address = ip
        erptimesheet.updation_date = new java.util.Date()
        erptimesheet.save(flush: true, failOnError: true)

    }

    def displaymarkattendanceButton(String id , def erpcourseofferingbatch){
//        println("displaymarkattendanceButton ")

        def erptimetable = ERPTimeTable.findById(id)
        def roletypesetting = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Online Attendance Time Buffer', erptimetable?.organization)

        Date lecdate = new Date()
        def erpday = ERPDay.findByDaysequence(lecdate.getDay() + 1)
        def start_time = erptimetable?.slot?.start_time.split(':')
        def end_time = erptimetable?.slot?.end_time.split(':')

        def date1 = new Date()
        date1.setHours(Integer.parseInt(start_time[0]));
        if(roletypesetting)
            date1.setMinutes(Integer.parseInt(start_time[1])- Integer.parseInt(roletypesetting?.value));
        else
            date1.setMinutes(Integer.parseInt(start_time[1])-15);

        def date2 = new Date()
        date2.setHours(Integer.parseInt(end_time[0]));
        date2.setMinutes(Integer.parseInt(end_time[1]));

        def startedlecture = false
        if(erpday?.id == erptimetable?.erpday?.id) {
            if (lecdate >= date1 && lecdate <= date2)
                startedlecture = true
        }

        ERPCourseOfferingBatchInstructor insbatch = ERPCourseOfferingBatchInstructor.findByErpcourseofferingbatch(erpcourseofferingbatch)
        def erpCourseOffering = erpcourseofferingbatch?.erpcourseoffering
        if(erpCourseOffering?.courserule?.name == 'EL' && erpCourseOffering?.erpcoursecategory?.name != 'EL-Div'){
            if(!erpcourseofferingbatch?.batch_number?.contains(erpCourseOffering.erpcourse.course_code)) {
                ERPCourseTypeAcademicsLoadTypes lt = erpcourseofferingbatch?.erpcoursetypeacademicsloadtype
                def typelist = ERPCourseTypeAcademicsLoadTypes.findAllByCoursetypeacademicsAndLoadtype(erpCourseOffering?.coursetypeacademics, lt?.loadtype)
                typelist.sort { it?.id }
                def number = 0
                def p = 1
                for (t in typelist) {
                    if (lt?.id == t?.id)
                        number = p
                    p++
                }
                def batchno = Integer.parseInt(erpcourseofferingbatch?.batch_number)
                if(batchno == -1)
                    batchno = 1

                String batchnumber = electivebatchname(erpCourseOffering.erpcourse.course_code, erpCourseOffering?.erpcourse?.courseAbbr, erptimetable?.loadtypecategory.type, batchno, number)
                ERPCourseOfferingBatch b = ERPCourseOfferingBatch.findByBatch_numberAndErpcourseofferingAndLoadtype(batchnumber, erpCourseOffering, erpcourseofferingbatch?.loadtype)
                insbatch = ERPCourseOfferingBatchInstructor.findByErpcourseofferingbatch(b)
            }
        }
        ERPCourseInstructorAttendanceSlot slotadded = ERPCourseInstructorAttendanceSlot.findByErpcourseofferingbatchinstructorAndSlotAndExecution_date(insbatch, erptimetable?.slot, lecdate.clearTime())

        def array = []
        array.add(lecturestarted : startedlecture, attendanceslotpresent : slotadded, attendancestarted : slotadded?.start_attendance)

        return array
    }

    def addInstructorAttendanceLoad(def instructorlist, def slotlist, def user, def ip){

        def removeload = ERPCourseExtraInstructorAttendanceSlot.createCriteria().list(){
            'in'('erpcourseinstructorattendanceslot', slotlist)
        }

        for(slot in slotlist){
            for(inst in instructorlist){
                def attendanceslot = ERPCourseExtraInstructorAttendanceSlot.findByInstructorAndErpcourseinstructorattendanceslot(inst, slot)
                if(!attendanceslot){
                    attendanceslot = new ERPCourseExtraInstructorAttendanceSlot()
                    attendanceslot.creation_username = user
                    attendanceslot.creation_ip_address = ip
                    attendanceslot.creation_date = new java.util.Date()
                }
                attendanceslot.instructor = inst
                attendanceslot.erpcourseinstructorattendanceslot = slot
                attendanceslot.updation_username = user
                attendanceslot.updation_ip_address = ip
                attendanceslot.updation_date = new java.util.Date()
                attendanceslot.save(flush: true, failOnError: true)
                removeload.remove(attendanceslot)
            }
        }

        for(rm in removeload)
            rm.delete(flush: true, failOnError: true)
    }

    def deleteExtraSlot(def erpcrsbatch, def period_number, def slotlist){
        def attslot = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, period_number)
        if(attslot?.size() > slotlist?.size()){
            for(def i = slotlist?.size()-1; i < attslot?.size(); i++){
                def extrainstructorlist = ERPCourseExtraInstructorAttendanceSlot.findAllByErpcourseinstructorattendanceslot(attslot[i])
                for(ex in extrainstructorlist)
                    ex.delete(flush: true, failOnError: true)
                attslot[i].delete(flush: true, failOnError: true)
            }
        }
        attslot = ERPCourseInstructorAttendanceSlot.findAllByErpcourseofferingbatchinstructorAndErpcourseofferingbatchAndPeriod_number(erpcrsbatch, erpcrsbatch.erpcourseofferingbatch, period_number)
        return  attslot
    }

    def getMyTimeTable(String username, AcademicYear ay, Semester sem, Department dept, SlotMaster slotMasterId) {
        Instructor instructor = Instructor.findByUid(username)
        Organization org = instructor.organization
        ApplicationType at = ApplicationType.findByApplication_type("ERP")
        RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "TimeTable", org)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true, org)
        if (!aay) {
            //flash.error = "Please set Application Academic Year For Role Type : " + rt.type
            //redirect(controller: "learner", action: "infomsg")
            HashMap hm = new HashMap()
            hm.put("flash_error", "Please set Application Academic Year For Role Type : " + rt.type)
            hm.put("controller", "learner")
            hm.put("action", "infomsg")
            return hm
        } else {

            def daywisetimetable = []

            def erpDayList = ERPDay.list()
            AcademicYear academicYear = aay.academicyear
            if (ay != null) {
                academicYear = ay
            }
            Semester semester = aay.semester
            if (sem != null) {
                semester = sem
            }

            Department department = null
            if (dept != null)
                department = dept
            if (instructor != null && !department) {
                department = instructor.department
            }

            def erpcourseoffering = ERPCourseOffering.createCriteria().list() {
                'in'('organization', org)
                and {
                    'in'('academicyear', academicYear)
                    'in'('semester', semester)
                    eq('isDeleted', false)
                }
            }

            def erpcourseofferingbatch
            if (erpcourseoffering) {
                erpcourseofferingbatch = ERPCourseOfferingBatch.createCriteria().list() {
                    'in'('erpcourseoffering', erpcourseoffering)
                }
            }

            def erpcourseofferingbatchinstructor
            if (erpcourseofferingbatch) {
                erpcourseofferingbatchinstructor = ERPCourseOfferingBatchInstructor.createCriteria().list() {
                    'in'('erpcourseofferingbatch', erpcourseofferingbatch)
                }
            }

            def batchlist = []
            for (batch in erpcourseofferingbatchinstructor) {
                if (batch?.extrainstructor?.id?.contains(instructor?.id) || batch?.instructor?.id == instructor?.id)
                    batchlist.add(batch?.erpcourseofferingbatch)
            }

            HashSet slotmaster = []
            if (slotMasterId != null) {
                slotmaster.add(SlotMaster.findById(slotMasterId.id))
            } else {
                HashSet erpTimeTablelist = []
                if (batchlist) {
                    def list = ERPTimeTable.createCriteria().list() {
                        'in'('organization', org)
                        and {
                            'in'('academicyear', academicYear)
                            'in'('semester', semester)
                            'in'('erpcourseofferingbatch', batchlist)
                        }
                    }

                    if (list)
                        erpTimeTablelist.addAll(list)
                }

                def list = ERPTimeTable.findAllByOrganizationAndAcademicyearAndSemesterAndInstructor(
                        org, academicYear, semester, instructor)

                if (list)
                    erpTimeTablelist.addAll(list)

                if (erpTimeTablelist)
                    slotmaster.addAll(erpTimeTablelist?.slot?.slotmaster)
            }

            HashSet slotList = []
            if (academicYear != null && semester != null && org != null && slotmaster) {
                def list = Slot.findAllByOrganizationAndAcademicyearAndSemesterAndSlotmasterInList(org, academicYear, semester, slotmaster)
                for (item in list) {
                    slotList.add(item?.start_time + "-" + item?.end_time)
                }
            }

            def finalSlotList = []
            for (slt in slotList) {
                def sltname = slt?.split('-')
                def slot
                if (slotmaster)
                    slot = Slot.findAllByOrganizationAndAcademicyearAndSemesterAndStart_timeAndend_timeAndSlotmasterInList(org, academicYear, semester, sltname[0], sltname[1], slotmaster)
                def number = 0
                if (slot)
                    number = slot[0]?.slot_number
                finalSlotList.add(slot: slot, time: slt, number: number)
            }
            finalSlotList?.sort { it?.number }

            ERPTimeTableVersion erpTimeTableVersion = null
            if (academicYear != null && semester != null && department != null && org != null) {
                erpTimeTableVersion = ERPTimeTableVersion.findByOrganizationAndAcademicyearAndSemesterAndDepartmentAndIsCurrent(
                        org, academicYear, semester, department, true)
            }

            Set subjectList = []
            def loadmap = [:]
            def loadtypecategorylist = LoadTypeCategory.findAllByOrganization(org)
            for (LoadTypeCategory loadTypeCategory : loadtypecategorylist) {
                loadmap.put(loadTypeCategory.type, 0)
            }

            if (erpDayList != null && slotList != null) {
                for (ERPDay erpDay : erpDayList) {
                    def maxrow = 0
                    def slottimetable = []
                    for (slot in finalSlotList) {
                        HashSet erpTimeTablelist = []
                        if (slot?.slot) {
                            if (batchlist) {
                                def list = ERPTimeTable.createCriteria().list() {
                                    'in'('organization', org)
                                    and {
                                        'in'('academicyear', academicYear)
                                        'in'('semester', semester)
                                        'in'('erpday', erpDay)
                                        'in'('slot', slot?.slot)
                                        'in'('erpcourseofferingbatch', batchlist)
                                    }
                                }

                                if (list)
                                    erpTimeTablelist.addAll(list)
                            }

                            def list = ERPTimeTable.findAllByOrganizationAndAcademicyearAndSemesterAndErpdayAndSlotInListAndInstructor(
                                    org, academicYear, semester, erpDay, slot?.slot, instructor)

                            if (list)
                                erpTimeTablelist.addAll(list)

                        }

                        def timetableArray = []
                        for (item in erpTimeTablelist) {
                            if (item.erptimetableversion.isCurrent) {
                                def batchinst = ERPCourseOfferingBatchInstructor.findByErpcourseofferingbatch(item?.erpcourseofferingbatch)
                                def name = batchinst?.instructor?.employeeabbr
                                for (extra in batchinst?.extrainstructor) {
                                    name = name + "/" + extra?.employeeabbr
                                }
                                timetableArray.add(timetable: item, name: name)


                                if (item.instructor == null || item.loadtype == null || item.loadtype.type == null || item?.erpcourseofferingbatch == null || item?.erpcourseofferingbatch?.erpcourseoffering == null || item?.erpcourseofferingbatch?.erpcourseoffering?.erpcourse == null) {
                                    subjectList.add(null)
                                } else {
                                    subjectList.add(item?.instructor?.employee_code + " " + item?.instructor?.employeeabbr + "( " + item?.instructor?.person?.fullname_as_per_previous_marksheet + ")  : " + item?.loadtype?.type?.type + " : " +
                                            item?.erpcourseofferingbatch?.erpcourseoffering?.erpcourse?.courseAbbr + " - " + item?.erpcourseofferingbatch?.erpcourseoffering?.erpcourse?.course_code + " - " + item?.erpcourseofferingbatch?.erpcourseoffering?.erpcourse?.course_name)
                                }
                                if (item.loadtype != null && item.loadtype.type != null) {
                                    loadmap[item?.loadtype?.type?.type] = loadmap[item?.loadtype?.type?.type] + 1
                                }
                            }
                        }

                        timetableArray?.sort { it?.timetable?.erpcourseofferingbatch?.batch_number }
                        slottimetable.add(erpTimeTable: timetableArray, slot: slot)

                        if (maxrow < erpTimeTablelist?.size())
                            maxrow = erpTimeTablelist?.size()
                    }
                    daywisetimetable.add(slottimetable: slottimetable, erpDay: erpDay, maxrow: maxrow)
                }
            }

            println("daywisetimetable :: " + daywisetimetable[0].slottimetable[0])
            println("daywisetimetable :: " + daywisetimetable[0].slottimetable[1])
            println("daywisetimetable :: " + daywisetimetable[0].erpDay)
            println("daywisetimetable :: " + daywisetimetable[0].maxrow)

            HashMap hm = new HashMap()
            hm.put("daywisetimetable", daywisetimetable)
            hm.put("erpTimeTableVersion", erpTimeTableVersion)
            hm.put("slotList", finalSlotList)
            hm.put("loadmap", loadmap)
            hm.put("subjectList", subjectList)
            hm.put("instructor", instructor)
            hm.put("erpDayList", erpDayList)
            hm.put("academicYear", academicYear)
            hm.put("semester", semester)
            hm.put("organization", org)
            return hm
        }
    }

    def saveOnlineClassUrl(String username, String tt_version_id, String erpTimeTable_id, String meet_link_url, String ip_addr) {
        Instructor instructor = Instructor.findByUid(username)
        Organization org = instructor.organization
        ApplicationType at = ApplicationType.findByApplication_type("ERP")
        RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "TimeTable", org)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true, org)
        if(!aay) {
            HashMap error_map = new HashMap()
            error_map.put("flash_error", "Application Academic Year not set for " + rt.type_displayname)
            return error-map
        }
        AcademicYear ay = aay.academicyear
        Semester sem = aay.semester
        ERPTimeTableVersion version = ERPTimeTableVersion.findById(tt_version_id)
        ERPTimeTable timetable = ERPTimeTable.findById( erpTimeTable_id)

        timetable.isonlinesession = true
        timetable.onlinesesionurl = meet_link_url
        timetable.username = username
        timetable.updation_date = new java.util.Date()
        timetable.updation_ip_address = ip_addr
        timetable.save(flush: true, failOnError: true)

        HashMap hm = new HashMap()
        hm.put("ay", ay)
        hm.put("sem", sem)
        hm.put("tt_version", version)
        hm.put("timetable", timetable)

        return hm
    }
}
