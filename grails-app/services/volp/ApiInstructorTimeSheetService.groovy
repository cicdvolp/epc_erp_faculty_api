package volp

import java.text.SimpleDateFormat

class ApiInstructorTimeSheetService {

    InformationService InformationService

    def getTimeSheets(String username, String editid, def today){
        println("editid : " + editid)

        Instructor instructor = Instructor.findByUid(username)
        Organization org = instructor.organization

        def timesheet
        if(editid){
            timesheet = ERPTimeSheet.findById(editid)
            today = timesheet?.date
        } else {
            today = today
        }

        def timesheetlist = ERPTimeSheet.findAllByInstructorAndOrganizationAndDate(instructor, org, today)

        today = InformationService.getDate(today)

        def instructortimesheetmaster = ERPInstructorTaskMaster.findAllByOrganizationAndInstructorAndIsdeleteAndIsactive(org, instructor, false, true)

        HashMap hm = new HashMap()
        hm.put("today", today)
        hm.put("instructortimesheetmaster", instructortimesheetmaster)
        hm.put("timesheetlist", timesheetlist)
        hm.put("instructor", instructor)
        hm.put("timesheet", timesheet)

        return hm
    }

    def saveInstructorTimeSheetMaster(String username, String erpTaskMasterId, String taskName, String taskDescription, String requestIp) {
        Instructor instructor = Instructor.findByUid(username)
        Organization org = instructor.organization

        def task = ERPTaskMaster.findById(erpTaskMasterId)

        def erpinstructortaskmaster = ERPInstructorTaskMaster.findByInstructorAndOrganizationAndName(instructor, org, taskName)

        if(erpinstructortaskmaster) {
            if(erpinstructortaskmaster?.isdelete) {
                erpinstructortaskmaster.name = taskName
                erpinstructortaskmaster.description = taskDescription
                erpinstructortaskmaster.isdelete = false
                erpinstructortaskmaster.save(flush: true, failOnError: true)
            }
            if(!erpinstructortaskmaster?.isactive) {
                erpinstructortaskmaster.name = taskName
                erpinstructortaskmaster.description = taskDescription
                erpinstructortaskmaster.isactive = true
                erpinstructortaskmaster.save(flush: true, failOnError: true)
            }
        } else {
            erpinstructortaskmaster = new ERPInstructorTaskMaster()
            erpinstructortaskmaster.name = taskName
            erpinstructortaskmaster.description = taskDescription
            erpinstructortaskmaster.erptaskmaster = task
            erpinstructortaskmaster.instructor = instructor
            erpinstructortaskmaster.organization = org
            erpinstructortaskmaster.isactive = true
            erpinstructortaskmaster.creation_username = instructor.uid
            erpinstructortaskmaster.updation_username = instructor.uid
            erpinstructortaskmaster.updation_ip_address = requestIp
            erpinstructortaskmaster.creation_ip_address = requestIp
            erpinstructortaskmaster.updation_date = new java.util.Date()
            erpinstructortaskmaster.creation_date = new java.util.Date()
            erpinstructortaskmaster.save(flush: true, failOnError: true)
        }

        def instructortimesheetmaster = ERPInstructorTaskMaster.findAllByOrganizationAndInstructorAndIsdeleteAndIsactive(org, instructor, false, true)

        HashMap map = new HashMap()
        map.put("instructortimesheetmaster", instructortimesheetmaster)

        return map
    }

    def deleteERPInstructorTaskMaster(String username, String id, String requestIp) {
        Instructor instructor = Instructor.findByUid(username)
        Organization org = instructor.organization
        ERPInstructorTaskMaster erpTaskMaster = ERPInstructorTaskMaster.findById(id)
        if(erpTaskMaster) {
            erpTaskMaster.isdelete = true
            erpTaskMaster.updation_ip_address = requestIp
            erpTaskMaster.updation_date = new java.util.Date()
            erpTaskMaster.save(flush: true, failOnError: true)
            return 1
        } else {
            return 0
        }
    }

    def activeERPInstructorTaskMaster(String username, String id, String requestIp) {
        Instructor instructor = Instructor.findByUid(username)
        Organization org = instructor.organization
        ERPInstructorTaskMaster erpTaskMaster = ERPInstructorTaskMaster.findById(id)

        if(erpTaskMaster) {
            if(erpTaskMaster.isactive) {
                erpTaskMaster.isactive = false
                erpTaskMaster.updation_ip_address = requestIp
                erpTaskMaster.updation_date = new java.util.Date()
                erpTaskMaster.save(flush: true, failOnError: true)
            } else {
                erpTaskMaster.isactive = true
                erpTaskMaster.updation_ip_address = requestIp
                erpTaskMaster.updation_date = new java.util.Date()
                erpTaskMaster.save(flush: true, failOnError: true)
            }
            return  1
        } else {
            return 0
        }
    }

    def saveInstructorTimesheet(String username, String id, String requestIp, def date, String timesheet_id, String starttime, String endtime, String taskname, String task_description) {
        Instructor instructor = Instructor.findByUid(username)
        Organization org = instructor.organization

//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//        Date date = formatter.parse(params?.date)
        def task = ERPInstructorTaskMaster.findById(id)
        if(timesheet_id){
            def erptimesheet = ERPTimeSheet.findById(timesheet_id)
            erptimesheet.date = date
            erptimesheet.starttime = starttime
            erptimesheet.endtime = endtime

            erptimesheet.name = taskname
            erptimesheet.description = task_description

            erptimesheet.erpinstructortaskmaster = task
            erptimesheet.instructor = instructor
            erptimesheet.organization = org

            erptimesheet.creation_username = instructor.uid
            erptimesheet.updation_username = instructor.uid
            erptimesheet.updation_ip_address = requestIp
            erptimesheet.creation_ip_address = requestIp
            erptimesheet.updation_date = new java.util.Date()
            erptimesheet.creation_date = new java.util.Date()
            erptimesheet.save(flush: true, failOnError: true)

            return 1
        } else {
            def erptimesheet = new ERPTimeSheet()
            erptimesheet.date = date
            erptimesheet.starttime = starttime
            erptimesheet.endtime = endtime

            erptimesheet.name = taskname
            erptimesheet.description = task_description

            erptimesheet.erpinstructortaskmaster = task
            erptimesheet.instructor = instructor
            erptimesheet.organization = org

            erptimesheet.creation_username = instructor.uid
            erptimesheet.updation_username = instructor.uid
            erptimesheet.updation_ip_address = requestIp
            erptimesheet.creation_ip_address = requestIp
            erptimesheet.updation_date = new java.util.Date()
            erptimesheet.creation_date = new java.util.Date()
            erptimesheet.save(flush: true, failOnError: true)

            return 1
        }
        return 0
    }

    def getAllTaskList(String username) {
        Instructor instructor = Instructor.findByUid(username)
        Organization org = instructor.organization

        def timesheetmaster = ERPTaskMaster.findAllByOrganizationAndIsdeleteAndIsactive(org, false, true)
        def timesheetmaster1 = ERPInstructorTaskMaster.findAllByOrganizationAndIsdelete(org,false)

        HashMap map = new HashMap()
        map.put("timesheetmaster",timesheetmaster)
        map.put("timesheetmaster1",timesheetmaster1)

        return map
    }

}
