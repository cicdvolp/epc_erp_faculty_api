package volp
import grails.gorm.transactions.Transactional
import volp.CourseAnnouncement

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import volp.Course
import volp.CourseAnnouncement
import volp.CourseMaterial
import volp.CourseOutline
import volp.CourseTopic
import volp.CourseVideos
import volp.LearnerCourseProgress
import grails.gorm.transactions.Transactional

@Transactional
class PasswordEncryptionService {

    def serviceMethod() {
    }
    String encrypwd(String pwd)
    {
        MessageDigest md;
        String pass=pwd
        String securepassword="";
        try
        {
            md = MessageDigest.getInstance("MD5");
            byte[] passBytes = pass.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for(int i=0;i<digested.length;i++)
            {
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            securepassword=sb.toString();
        }
        catch(NoSuchAlgorithmException ex)
        {
            println("Error:"+ex);
        }
        //println("Secure Password::"+securepassword);
        return securepassword
    }
}
