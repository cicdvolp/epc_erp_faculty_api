package volp

import grails.gorm.transactions.Transactional

import java.text.SimpleDateFormat

@Transactional
class EmployeeAttendanceService {

    InformationService InformationService

    def markEmployeeAttendance(Instructor instructor, Date todate, String in_time, String out_time, double expectednoofhours, String ipaddress, String username, ERPDay erpday, ERPMonth erpMonth, ERPCalendarYear erpyear)
    {
        println("markEmployeeAttendance :: " + instructor + ", " + todate +  ", " + in_time + ", " + out_time + ", " + expectednoofhours +  ", " + ipaddress +  ", " +  username + ", "+ erpday + ", " + erpMonth +  ", "+ erpyear)
        ApplicationType at = ApplicationType.findByApplication_type("ERP")
        RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "Leave Management", instructor.organization)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true, instructor.organization)
        println("aay :: " + aay)
        EmployeeAttendance employee_attendance = EmployeeAttendance.findByTodaysdateAndInstructorAndAcademicyearAndOrganization(todate, instructor, aay.academicyear, instructor.organization)
        println("employee_attendance  :: " + employee_attendance )
        if(employee_attendance ) {
            employee_attendance.intime = in_time
            employee_attendance.outtime = out_time

            employee_attendance.username = username
            employee_attendance.updation_date = new Date()
            employee_attendance.updation_ip_address = ipaddress
            employee_attendance.save(flush: true, failOnError: true)
            println("updated")
            return 1
        } else {

            employee_attendance.intime = in_time
            employee_attendance.outtime = out_time
            employee_attendance.todaysdate = todate
            employee_attendance.expectednoofhours = expectednoofhours

            employee_attendance.instructor = instructor
            employee_attendance.academicyear = aay.academicyear
            employee_attendance.erpmonth = erpMonth
            employee_attendance.erpday = erpday
            employee_attendance.erpcalenderyear = erpyear
            employee_attendance.department = instructor.department
            employee_attendance.organization = instructor.organization

            employee_attendance.username = username
            employee_attendance.creation_date = new Date()
            employee_attendance.updation_date = new Date()
            employee_attendance.creation_ip_address = ipaddress
            employee_attendance.updation_ip_address = ipaddress
            employee_attendance.save(flush: true, failOnError: true)
            println("saved :: " + employee_attendance)

            return 1
        }
        println("failed")
        return 0
    }

    def getEmployeeData(def organization)
    {
        println("getEmployeeData  :: ")

        def attendancedata = AttendanceBiometricLog.findAllByOrganizationInList(organization)?.attendance_id
        attendancedata = attendancedata.toString()
        attendancedata = ","+attendancedata.substring(1, attendancedata.length() - 1);
        println("attendancedata  :: " + attendancedata )
        return attendancedata
    }

    //For PCCOE
    def getInstructorData(def organization)
    {
//        println("getInstructorData  :: ")
        def instructor = Instructor.createCriteria().list(){
            projections{
                distinct('aditionalinfo1')
            }
            'in'('organization', organization)
            and{
                isNotNull('aditionalinfo1')
                not {
                    eq('aditionalinfo1', "")
                }
            }
        }
        instructor?.sort{it}
        instructor = instructor.toString()
        instructor = instructor.substring(1, instructor.length() - 1);
        return instructor
    }

    def getEmployeeDataPccoe(def inst)
    {
//        println("getEmployeeDataPccoe  :: : ")

//        def monthArray = month.split('-')
//        def year = Integer.parseInt(monthArray[0]) - 1900
//        def monthcount = Integer.parseInt(monthArray[1])
//        monthcount = monthcount
//        def daysInMonth = new Date(year, monthcount, 0).getDate()
//        def startdate = new Date(year, monthcount-1, 1)
//        def enddate = new Date(year, monthcount-1, daysInMonth)

//        year = Integer.parseInt(year) - 1900
//        def startdate = new Date(year, 0, 1)
//        def enddate = new Date(year, 11, 31)

//        println("startdate  :: "+startdate)
//        println("enddate  :: "+enddate)

//        startdate.setHours(0);
//        startdate.setMinutes(0);
//        startdate.setSeconds(0);
//
//        enddate.setHours(23);
//        enddate.setMinutes(59);
//        enddate.setSeconds(59);

        def attendancedata = ""
        if(inst) {
            attendancedata = AttendanceBiometricLog.createCriteria().list() {
                'in'('instructor', inst)
//            and{
//                between('punchdatetime', startdate, enddate)
//            }
            }
            if(attendancedata?.size()>0) {
                attendancedata = attendancedata?.attendance_id
                attendancedata = attendancedata.toString()
                attendancedata = attendancedata.substring(1, attendancedata.length() - 1);
            }else{
                attendancedata = ""
            }
        }

//        println("attendancedata  :: " + attendancedata )
        return attendancedata
    }

    def markAttendance(Instructor inst, def datetime, Organization organization, def id, def empno, def ticketno, def deviceaddress)
    {
//        println("markAttendance :: ticketno :: " + ticketno)

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        Date today = formatter.parse(datetime)
//        println("today : "+today)
        def erpday = ERPDay.findByDaysequence(today.getDay() + 1)
        def erpmonth = ERPMonth.findByMonthnumber(today.getMonth() + 1)
        def year = today.getYear() + 1900
        def erpyear = ERPCalendarYear.findByYear("" + year)
        def attendance = EmployeeAttendance.findByInstructorAndOrganizationAndErpcalenderintegerdayAndErpmonthAndErpcalenderyear(inst, inst?.organization, today.getDate(), erpmonth, erpyear)

//        println("attendance : "+attendance)

        if(deviceaddress != '20') {
            if (attendance) {
                def timestring = getTime(today)
                def inArray = timestring.split(':')
                def starttime = 0
                if (inArray.size() > 1) {
                    starttime = new Date(2019, 04, 03, Integer.parseInt(inArray[0]), Integer.parseInt(inArray[1]))
                }

                def previousin = attendance?.intime
                def starttimepreviousin = 0
                if (previousin) {
                    def inArraypreviousin = previousin.split(':')
                    if (inArraypreviousin.size() > 1) {
                        starttimepreviousin = new Date(2019, 04, 03, Integer.parseInt(inArraypreviousin[0]), Integer.parseInt(inArraypreviousin[1]))
                    }
                }

                def previousout = attendance?.outtime
                def starttimepreviousout = 0
                if (previousout) {
                    def inArraypreviousout = previousin.split(':')
                    if (inArraypreviousout.size() > 1) {
                        starttimepreviousout = new Date(2019, 04, 03, Integer.parseInt(inArraypreviousout[0]), Integer.parseInt(inArraypreviousout[1]))
                    }
                }

                def intime
                def outime
                if (!previousin && !previousout) {
                    intime = timestring
                } else {
                    if (!attendance.intime)
                        intime = timestring
                    else
                        outime = timestring
                }

                if (starttimepreviousin && starttimepreviousout) {
                    if (starttime < starttimepreviousin && starttimepreviousin) {
                        intime = timestring
                        if (starttimepreviousin > starttimepreviousout)
                            outime = attendance.intime
                    } else if (starttime > starttimepreviousout && starttimepreviousout) {
                        outime = timestring
                        if (starttimepreviousin > starttimepreviousout)
                            intime = attendance.outime
                    }
                }

                if (intime)
                    attendance.intime = intime
                if (outime)
                    attendance.outtime = outime
                attendance.isattendancebyestablishment = false
                attendance.username = "Biometric"
                attendance.updation_date = new java.util.Date()
                attendance.updation_ip_address = "1.1.1.1"
                attendance.save(failOnError: true, flush: true)
            } else {
                def aay = InformationService.currentAySem("ERP", "Leave Management", inst?.organization)

                EmployeeAttendance a = new EmployeeAttendance()
                a.intime = getTime(today)
                a.outtime = ""
                a.todaysdate = today
                a.hasappliedforleavefortoday = false
                a.isattendancebyestablishment = false
                a.isexpectedinoutapproved = true
                a.expectedintime = ""
                a.expectedouttime = ""
                a.expectednoofhours = 0
                a.erpcalenderintegerday = today.getDate()
                a.instructor = inst
                a.academicyear = aay?.academicyear
                a.erpmonth = erpmonth
                a.erpday = erpday
                a.erpcalenderyear = erpyear
                a.department = inst.department
                a.organization = inst?.organization
                a.leavetype = null
                a.employeeleavetransaction = null
                a.username = "Biometric"
                a.creation_date = new java.util.Date()
                a.updation_date = new java.util.Date()
                a.creation_ip_address = '1.1.1.1'
                a.updation_ip_address = '1.1.1.1'
                a.save(failOnError: true, flush: true)
            }
        }

        AttendanceBiometricLog ab = new AttendanceBiometricLog()
        ab.employee_code = empno
        if(ticketno != 0) {
            ab.ticketno = Integer.parseInt(ticketno)
        }
        ab.attendance_id = Integer.parseInt(id)
        ab.deviceaddress = deviceaddress
        ab.time = getTime(today)
        ab.punchdatetime = today.clearTime()
        ab.instructor = inst
        ab.organization = inst?.organization
        ab.creation_username = "Biometric"
        ab.updation_username = "Biometric"
        ab.creation_date = new java.util.Date()
        ab.updation_date = new java.util.Date()
        ab.creation_ip_address = '1.1.1.1'
        ab.updation_ip_address = '1.1.1.1'
        ab.save(failOnError: true, flush: true)

//        println("attendance ::  "+attendance )
//        println("ab ::  "+ab )
    }

    def getTime(today)
    {
        def hour = today.getHours()>9 ? today.getHours() : '0'+today.getHours()
        def minute = today.getMinutes()>9 ? today.getMinutes() : '0'+today.getMinutes()
        def second = today.getSeconds()>9 ? today.getSeconds() : '0'+today.getSeconds()
        return hour + ":" +minute + ":"+second
    }

    def markMovement(date)
    {

        def prev = date.previous()
        prev.setHours(23);
        prev.setMinutes(59);
        prev.setSeconds(59);

        def nextdate = date
        nextdate.setHours(23);
        nextdate.setMinutes(59);
        nextdate.setSeconds(59);


        def instructorlist = AttendanceBiometricLog.createCriteria().list(){
             projections{
                 distinct('instructor')
             }
            eq('deviceaddress', '20')
            and{
                between("punchdatetime", prev, nextdate)
            }
        }

        for(inst in instructorlist) {

            def erpmonth = ERPMonth.findByMonthnumber(date?.getMonth() + 1)
            def year = date.getYear() + 1900
            def erpyear = ERPCalendarYear.findByYear("" + year)
            EmployeeAttendance attendance = EmployeeAttendance.findByInstructorAndOrganizationAndErpcalenderintegerdayAndErpmonthAndErpcalenderyear(inst, inst?.organization, date.getDate(), erpmonth, erpyear)

            if(attendance?.intime!= null && attendance?.intime!= "") {
                def inArray = attendance?.intime?.split(':')
                def outArray = attendance?.outtime?.split(':')
                def prevMovement = prev?.next()
                prevMovement.setHours(Integer.parseInt(inArray[0]));
                prevMovement.setMinutes(Integer.parseInt(inArray[1]));
                def nextMovement = nextdate
                if(outArray?.size()>0 && attendance?.outtime) {
                    nextMovement.setHours(Integer.parseInt(outArray[0]));
                    nextMovement.setMinutes(Integer.parseInt(outArray[1]));
                }else{
                    nextMovement.setHours(23);
                    nextMovement.setMinutes(59);
                }


                def movementlist = AttendanceBiometricLog.createCriteria().list() {
                    eq('deviceaddress', '20')
                    and {
                        between("punchdatetime", prev, nextdate)
                        eq('instructor', inst)
                    }
                }

                def finalMovementList = []
                for(mo in movementlist){
                    def momentArray = mo?.time?.split(':')
                    def movementtime = prev?.next()
                    movementtime.setHours(Integer.parseInt(momentArray[0]));
                    movementtime.setMinutes(Integer.parseInt(momentArray[1]));
                    if(movementtime > prevMovement && movementtime < nextMovement) {
                        finalMovementList.add(mo)
                    }
                }

                finalMovementList?.sort { it?.punchdatetime }
                def k = 1
                for (def i = 0; k <= finalMovementList?.size(); i = i + 2) {
                    def fromtime = ""
                    if (i < finalMovementList?.size())
                        fromtime = finalMovementList[i]?.time
                    def totime = ""
                    if (i < finalMovementList?.size())
                        totime = finalMovementList[k]?.time
                    if (!totime)
                        totime = ""
                    if (fromtime)
                        ouoffofficehours(finalMovementList[i].instructor, finalMovementList[i].punchdatetime, fromtime, totime)
                    k = k + 2
                }
            }
        }
    }

    def ouoffofficehours(instructor, today, fromtime, totime)
    {
        def organization = instructor?.organization
        def erpmonth = ERPMonth.findByMonthnumber(today.getMonth() + 1)
        def year = today.getYear() + 1900
        def erpyear = ERPCalendarYear.findByYear("" + year)
        EmployeeAttendance attendance = EmployeeAttendance.findByInstructorAndOrganizationAndErpcalenderintegerdayAndErpmonthAndErpcalenderyear(instructor, organization, today.getDate(), erpmonth, erpyear)
        if (attendance) {
            if(!totime)
                totime = attendance?.outtime ? attendance?.outtime : ""
            OutOfOfficeHours officehours  = OutOfOfficeHours.findByFromtimeAndTotimeAndDateAndInstructor(fromtime, totime, today, instructor)
            if(!officehours) {
                officehours = new OutOfOfficeHours()
                officehours.fromtime = fromtime
                officehours.totime = totime
                officehours.date = today
                officehours.reason = "System Generated By RF Reader"
                officehours.addToEmployeeattendance(attendance)
                officehours.instructor = instructor
                officehours.organization = organization
                officehours.save(failOnError: true, flush: true)
            }
        }
    }

    //Movement Register Dashboard By Pankaj
}
