package volp

import grails.converters.JSON
import grails.gorm.transactions.Transactional

import java.util.regex.Matcher
import java.util.regex.Pattern

@Transactional
class ApiInstructorLoginService {

    def checkValidEmailId(String username) {
        String regex = '^(.+)@(.+)$'
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(username);
        if(matcher.matches()) {
            return true
        }
        return false
    }

    def generatedOTP(String username) {
        try {
            java.util.Random random = new java.util.Random()
            String otp = ""
            int min = 0, max = 0, n
            for (int i = 1; i <= 6; i++) {
                if (i == 1)
                    min = 1
                else
                    min = 0
                max = 9
                n = random.nextInt(max) + min
                otp = otp + n
            }

            Otp otpobj = Otp.findByEmail(username)
            Date date = new java.util.Date()
            if (otpobj == null) {
                //insert otp
                otpobj = new Otp()
                otpobj.email = username
                otpobj.otp = otp
                otpobj.otpgenerationtime = date
                otpobj.save(failOnError: true, flush: true)
            } else {
                //update otp
                otpobj.otp = otp
                otpobj.otpgenerationtime = date
                otpobj.save(failOnError: true, flush: true)
            }
            println("otpobj :: " + otpobj)
            return otp
        } catch(Exception e) {
            return "000000"
        }
    }

    def checkAuthentication(String username, String token) {
        try {
            Login login = Login.findByUsername(username)
            if(login && login.access_token.equals(token)) {
                return true
            } else {
                return false
            }
        } catch(Exception e) {
            return false
        }
    }

    def compareAndSavePassword(Login login, String password, String confirmPassword) {
        try {
            if (password.equals(confirmPassword)) {
                login.password = password
                login.updation_date = new Date()
                login.updation_ip_address = login.username
                login.save(failOnError: true, flush: true)
                return true
            } else {
                return false
            }
        } catch(Exception e) {
            return  false
        }
    }
}
