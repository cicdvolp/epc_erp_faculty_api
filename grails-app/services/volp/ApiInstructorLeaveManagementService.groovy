package volp

import grails.gorm.transactions.Transactional

import javax.servlet.http.Part
import java.nio.file.Paths
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

@Transactional
class ApiInstructorLeaveManagementService {

    LeaveManagementSystemService leaveManagementSystemService

    def getLeaveDetails(String username) {
        Instructor instructor = Instructor.findByUid(username)
        Organization organization = instructor.organization

        ArrayList leavetypelist = new ArrayList()
        def ltlist = LeaveType.findAllByOrganizationAndIsactive(instructor.organization, true)
        ltlist?.sort{it.sortorder}

        for (LeaveType l : ltlist) {
            if (l.type == "Maternity Leave" && instructor?.gender?.type == 'Male')
                continue
            leavetypelist.add(display_name : l.display_name, type : l?.type)
        }

        RoleType roletype = RoleType.findByTypeAndOrganization("Leave Management", organization)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByIsActiveAndRoletypeAndOrganization(true, roletype, instructor.organization)
        AcademicYear ay = aay.academicyear

        def leavetype = LeaveType.findAllByOrganizationAndTypeInListAndIsactive(organization, ['CL', 'HPL', 'VACATION', 'EL', 'COMP-OFF', 'LWP'], true)
        leavetype?.sort{it.sortorder}

        def creditedleave = LeaveType.findAllByOrganizationAndTypeInListAndIsactiveAndCan_be_credited(organization, ['On Duty'], true, true)
        if(creditedleave)
            leavetype.addAll(creditedleave)

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        def employeeleavebalancesheet = []
        for (lt in leavetype) {
            def elm
            def elt
            def leaveTypeList
            if(lt.type == 'CL'){
                leaveTypeList = LeaveType.createCriteria().list() {
                    like("organization", instructor.organization)
                    and {
                        'in'("type", ['CL', 'HD_CL'])
                    }
                }
            }else if(lt.type == 'HPL'){
                leaveTypeList = LeaveType.createCriteria().list() {
                    like("organization", instructor.organization)
                    and {
                        'in'("type", ['ML', 'HPL'])
                    }
                }
            }else {
                leaveTypeList = lt
            }

            if(lt.type == 'VACATION') {
                if (lt.leaveexpirytype.type == 'Academic Year') {
                    elm = EmployeeVaccationMaster.findAllByInstructorAndOrganizationAndLeavetypeAndAcademicyear(instructor, instructor.organization, lt, ay)

                    elt = EmployeeLeaveTransaction.findAllByInstructorAndOrganizationAndLeavetypeAndAcademicyear(instructor, instructor.organization, lt, ay)
                } else if (lt.leaveexpirytype.type == 'Slot') {

                    elm = EmployeeVaccationMaster.findAllByInstructorAndOrganizationAndLeavetypeAndAcademicyear(instructor, instructor.organization, lt, ay)

                } else if (lt.leaveexpirytype.type == 'No Expiry') {
                    elm = EmployeeVaccationMaster.findAllByInstructorAndOrganizationAndLeavetype(instructor, instructor.organization, lt)

                    elt = EmployeeLeaveTransaction.findAllByInstructorAndOrganizationAndLeavetype(instructor, instructor.organization, lt)
                }else if (lt.leaveexpirytype.type == 'Expiry Date') {
                }
            }else{
                if (lt.leaveexpirytype.type == 'Academic Year') {
                    elm = EmployeeLeaveMaster.findAllByInstructorAndOrganizationAndLeavetypeAndAcademicyear(instructor, instructor.organization, lt, ay)

                    elt = EmployeeLeaveTransaction.createCriteria().list() {
                        like("instructor", instructor)
                        and {
                            'in'("leavetype", leaveTypeList)
                            like("organization", instructor.organization)
                            like("academicyear", ay)
                        }
                    }
                } else if (lt.leaveexpirytype.type == 'Slot') {
                    elm = EmployeeLeaveMaster.findAllByInstructorAndOrganizationAndLeavetypeAndAcademicyear(instructor, instructor.organization, lt, ay)

                } else if (lt.leaveexpirytype.type == 'No Expiry') {
                    elm = EmployeeLeaveMaster.findAllByInstructorAndOrganizationAndLeavetype(instructor, instructor.organization, lt)

                    elt = EmployeeLeaveTransaction.createCriteria().list() {
                        like("instructor", instructor)
                        and {
                            'in'("leavetype", leaveTypeList)
                            like("organization", instructor.organization)
                        }
                    }
                }else if (lt.leaveexpirytype.type == 'Expiry Date') {
                    def compoffstatus = CompOffStatus.findAllByStatusInList(['Available', 'Availed'])
                    if(compoffstatus) {
                        elm = EmployeeLeaveMaster.createCriteria().list() {
                            'in'('organization', instructor.organization)
                            and {
                                'in'('instructor', instructor)
                                'in'('leavetype', lt)
                                'in'('compoffstatus', compoffstatus)
                            }
                        }
                    }

                    elt = EmployeeLeaveTransaction.createCriteria().list() {
                        like("instructor", instructor)
                        and {
                            'in'("leavetype", leaveTypeList)
                            like("organization", instructor.organization)
                        }
                    }
                }
            }

            if (lt.leaveexpirytype.type == 'Slot') {
                for (e in elm) {
                    def slotrange = ''
                    def total = 0.0
                    def inprocess = 0.0
                    def approved = 0.0

                    slotrange = formatter.format(e?.leavemaster?.fromdate) + " - " + formatter.format(e?.leavemaster?.todate)
                    if(lt.type == 'VACATION')
                        total = e.allowedDays
                    else
                        total = e.numberofdays

                    elt = EmployeeLeaveTransaction.createCriteria().list() {
                        like("instructor", instructor)
                        and {
                            'in'("leavetype", lt)
                            like("organization", instructor.organization)
                            ge("fromdate", e?.leavemaster?.fromdate)
                            le("todate", e?.leavemaster?.todate)
                        }
                    }
                    for(et in elt){
                        if(et.leaveapprovalstatus.status == 'inprocess')
                        {
                            if(et.leavetype.type == 'ML')
                                inprocess = inprocess + (et.numberofdays * et?.leavetype?.aditionalinfo1)
                            else
                                inprocess = inprocess + et.numberofdays
                        }else if(et.leaveapprovalstatus.status == 'approved'){
                            if(et.leavetype.type == 'ML')
                                approved = approved + (et.numberofdays * et?.leavetype?.aditionalinfo1)
                            else
                                approved = approved + et.numberofdays
                        }
                    }

                    def vacancy = total - inprocess - approved
                    employeeleavebalancesheet.add(leavetype: e.leavetype, total: total, inprocess:inprocess, approved:approved, vacancy:vacancy, slotrange:slotrange)
                }
            } else {
                def total = 0.0
                def inprocess = 0.0
                def approved = 0.0
                for (e in elm) {
                    if(lt.type == 'VACATION')
                        total = total + e.allowedDays
                    else
                        total = total + e.numberofdays
                }
                for(et in elt){
                    if(et.leaveapprovalstatus.status == 'inprocess')
                    {
                        if(et.leavetype.type == 'ML')
                            inprocess = inprocess + (et.numberofdays * et?.leavetype?.aditionalinfo1)
                        else
                            inprocess = inprocess + et.numberofdays
                    }else if(et.leaveapprovalstatus.status == 'approved'){
                        if(et.leavetype.type == 'ML')
                            approved = approved + (et.numberofdays * et?.leavetype?.aditionalinfo1)
                        else
                            approved = approved + et.numberofdays
                    }
                }
                def vacancy = total - inprocess - approved
                def color_code = LeaveType.findByTypeAndOrganization(lt, organization)
                employeeleavebalancesheet.add(leavetype: lt, total: total, inprocess:inprocess, approved:approved, vacancy:vacancy, color:color_code?.leave_type_color)
            }
        }

        HashMap hm  = new HashMap()
        hm.put("employeeleavebalancesheet", employeeleavebalancesheet)
        hm.put("instructor", instructor)
        hm.put("leavetypelist", leavetypelist)

        return hm
    }

    def applyLeave(String username, String backto, String instid, String leavetypename, String academicYear) {
        Instructor instructor = Instructor.findByUid(username)
        if(backto == 'EB' && instid) {
            instructor = Instructor.findById(instid)
        }
        def lbinporcess = 0
        def lbaprroved = 0

        LeaveType leavetype = LeaveType.findByTypeAndOrganizationAndIsactive(leavetypename, instructor.organization, true)
        def acdyear = AcademicYear.list().sort{it.ay}.reverse(true)
        RoleType roletype = RoleType.findByTypeAndOrganization("Leave Management", instructor.organization)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByIsActiveAndRoletypeAndOrganization(true, roletype, instructor.organization)
        AcademicYear ay
        if(academicYear)
            ay = AcademicYear.findById(academicYear)
        else
            ay = aay.academicyear
        def total_allowed_leaves = 0

        if(leavetype.type == "VACATION"){
//            redirect(controller: "EmployeeLeaveTransaction", action: "process_add_availed_vaccation_with_leave_expiry", params:[leavetype:leavetype.type,redirect:'leave', backto: backto, instid : instid])
//            return
            return process_add_availed_vaccation_with_leave_expiry(username, backto, instid, leavetype.type, null, null, 'leave')
        }

        LeaveType lt
        if (leavetype.type == "CL" || leavetype.type == "HD_CL") {
            lt = LeaveType.findByTypeAndOrganization("CL", instructor.organization)
        }else if (leavetype.type == "ML" || leavetype.type == "HPL") {
            lt = LeaveType.findByTypeAndOrganizationAndIsactive("HPL", instructor.organization, true)
        }else{
            lt = leavetype
        }

        def elm = []
        if(lt) {
            if (lt.leaveexpirytype.type == 'Academic Year') {
                elm = EmployeeLeaveMaster.findAllByInstructorAndOrganizationAndLeavetypeAndAcademicyear(instructor, instructor.organization, lt, ay)
            } else if (lt.leaveexpirytype.type == 'Slot') {
                elm = EmployeeLeaveMaster.createCriteria().list() {
                    'in'('organization', instructor.organization)
                    and {
                        'in'('instructor', instructor)
                        'in'('leavetype', lt)
                        le("fromdate", new java.util.Date())
                        ge("todate", new java.util.Date())
                    }
                }
            } else if (lt.leaveexpirytype.type == 'No Expiry') {
                elm = EmployeeLeaveMaster.findAllByInstructorAndOrganizationAndLeavetype(instructor, instructor.organization, lt)
            }else if (lt.leaveexpirytype.type == 'Expiry Date') {
                def compoffstatus = CompOffStatus.findAllByStatusInList(['Available', 'Availed'])
                if(compoffstatus) {
                    elm = EmployeeLeaveMaster.createCriteria().list() {
                        'in'('organization', instructor.organization)
                        and {
                            'in'('instructor', instructor)
                            'in'('leavetype', lt)
                            'in'('compoffstatus', compoffstatus)
                        }
                    }
                }
            }

            def  allowed_leaves = 0.0
            if(lt?.leaveexpirytype?.type == 'No Expiry' || lt?.leaveexpirytype?.type == 'Expiry Date'){
                for (e in elm) {
                    allowed_leaves = allowed_leaves + e.numberofdays
                }
            }else if(elm){
                allowed_leaves = allowed_leaves + elm[0].numberofdays
            }
            total_allowed_leaves = allowed_leaves
        }

        int myflag = 0
        def elt
        def leaveTypeList
        println("leavetype :: " + leavetype)
        println("instructor.organization :: " + instructor.organization)
        if (leavetype.type == 'CL' || leavetype.type == 'HD_CL'){
            leaveTypeList = LeaveType.createCriteria().list() {
                        like("organization", instructor.organization)
                        and {
                            'in'("type", ['CL', 'HD_CL'])
                        }
                    }
        }else if (leavetype.type == 'ML' || leavetype.type == 'HPL'){
            leaveTypeList = LeaveType.createCriteria().list()
                    {
                        like("organization", instructor.organization)
                        and {
                            'in'("type", ['ML', 'HPL'])
                        }
                    }
        }else{
            leaveTypeList = leavetype
        }

        def mindate
        def maxdate

        if(leaveTypeList){
            if (lt?.leaveexpirytype?.type == 'Academic Year') {
                elt = EmployeeLeaveTransaction.createCriteria().list() {
                    like("instructor", instructor)
                    and {
                        'in'("leavetype", leaveTypeList)
                        like("organization", instructor.organization)
                        like("academicyear", ay)
                    }
                }

                if(elm[0]) {
                    def fromdate = elm[0].fromdate
                    def todate = elm[0].todate

                    mindate = dateFormatting(fromdate)
                    maxdate = dateFormatting(todate)
                }
            }else if (lt?.leaveexpirytype?.type == 'Slot') {
                if(elm[0]) {
                    def fromdate = elm[0].fromdate
                    def todate = elm[0].todate

                    elt = EmployeeLeaveTransaction.createCriteria().list() {
                        like("instructor", instructor)
                        and {
                            'in'("leavetype", leaveTypeList)
                            like("organization", instructor.organization)
                            ge("fromdate", fromdate)
                            le("todate", todate)
                        }
                    }

                    mindate = dateFormatting(fromdate)
                    maxdate = dateFormatting(todate)
                }
            }else if (lt?.leaveexpirytype?.type == 'No Expiry' || lt?.leaveexpirytype?.type == 'Expiry Date') {
                elt = EmployeeLeaveTransaction.createCriteria().list() {
                    like("instructor", instructor)
                    and {
                        'in'("leavetype", leaveTypeList)
                        like("organization", instructor.organization)
                    }
                }
            }
        }

        ArrayList<String> aprovingAuthority = new ArrayList<String>();
        def adjusted = []
        def loadRequired = []


        double inprocessleave = 0
        double aprovedleaves = 0
        for (e in elt) {
            if (e.leaveapprovalstatus.status == 'inprocess') {
                if(e.leavetype.type == 'ML')
                {
                    inprocessleave += (e.numberofdays * e?.leavetype?.aditionalinfo1 )
                }
                else {
                    inprocessleave += e.numberofdays
                }
            }
            if (e.leaveapprovalstatus.status == 'approved') {
                if(e.leavetype.type == 'ML')
                {
                    aprovedleaves += (e.numberofdays * e?.leavetype?.aditionalinfo1 )
                }
                else {
                    aprovedleaves += e.numberofdays
                }
            }
        }
        lbinporcess = inprocessleave
        lbaprroved = aprovedleaves

        for (EmployeeLeaveTransaction employeeLeaveTransaction : elt) {
            if (employeeLeaveTransaction.leaveapprovalstatus.status == "inprocess") {
                def c = EmployeeLeaveTransactionApproval.createCriteria()
                def eltaList = c.list() {
                    like("employeeleavetransaction", employeeLeaveTransaction)
                    and {
                        like("organization", instructor.organization)
                    }
                    order("id", "asc")
                }
                if (eltaList.size() > 0) {
                    def elta = eltaList.last()
                    aprovingAuthority.add(elta.leaveapprovalhierarchy.leaveapporvalauthority.display_name)
                } else {
                    aprovingAuthority.add("")
                }
            } else {
                aprovingAuthority.add("")
            }

            def loadadjust = isloadadjusted(employeeLeaveTransaction)
            if(loadadjust)
                loadRequired.add(true)
            else
                loadRequired.add(false)

            def loadadjustlist = LeaveLoadAdjustment.findAllByEmployeeleavetransactionAndOrganizationAndIsloadadjustedAndIsdeleted( employeeLeaveTransaction, instructor?.organization, false , false)
            def loadadjustlistcomplete = LeaveLoadAdjustment.findAllByEmployeeleavetransactionAndOrganizationAndIsdeleted( employeeLeaveTransaction, instructor?.organization, false )
            if(loadadjust) {
                if (!loadadjustlist && loadadjustlistcomplete.size() > 0)
                    adjusted.add( true )
                else
                    adjusted.add( false )
            }else{
                adjusted.add( true )
            }

        }

        def balance = total_allowed_leaves - lbinporcess - lbaprroved

        def showAcademicYear = false
        if(leavetype.leaveexpirytype.type == 'Academic Year')
            showAcademicYear = true

        def loadadjust = false
        def timetableload = ERPRoleTypeSettings.findByNameIlikeAndOrganization('While Applying Leave is Load Adjustment required for each lecture/practial as per time table?', instructor.organization)?.value
        def customload = ERPRoleTypeSettings.findByNameIlikeAndOrganization('While Applying Leave is Load Adjustment required for customized slot entered by Employee?', instructor.organization)?.value
        if((timetableload == 'true' || customload == 'true') && instructor?.isloadadustmentrequired) {
            if(backto != 'EB')
                loadadjust = true
            else{
                def isloadAdjusttrue = ERPRoleTypeSettings.findByNameIlikeAndOrganization('EST Leave Apply should go for load adjustment?', instructor.organization)?.value
                if(isloadAdjusttrue == 'true')
                    loadadjust = true
            }
        }

        def CompoffArray = []
        def availcompoff = false
        if(leavetype.type == "COMP-OFF") {
            for(item in elm){
                if(item?.employeeleavetransaction) {
                    if (item?.employeeleavetransaction?.leaveapprovalstatus?.status == 'rejected' || item?.employeeleavetransaction?.leaveapprovalstatus?.status == 'cancelled'){
                        CompOffStatus compoffstatus = CompOffStatus.findByStatus("Available")
                        item.compoffstatus = compoffstatus
                        item.employeeleavetransaction = null
                        item.updation_ip_address = request.getRemoteAddr()
                        item.updation_date = new java.util.Date()
                        item.username = username
                        item.save(failOnError: true, flush: true)
                    }
                }
            }
            elm?.sort { it?.expirydate }
            def today = new Date()
            today.setHours(00);
            today.setMinutes(00);
            today.setSeconds(00);
            if (elm?.size() > 0) {
                elm[0]?.fromdate.setHours(23);
                elm[0]?.fromdate.setMinutes(59);
                elm[0]?.fromdate.setSeconds(00);
                if (today > elm[0]?.fromdate && today <= elm[0]?.expirydate) {
                    CompoffArray.add(elm: elm[0], today: today, mapped : true)
                    availcompoff = true
                }
            }
            if(!availcompoff)
                CompoffArray.add(elm: null, today: today, mapped: false)
        }

        if(backto != 'EB' && !instructor?.apply_backdated_leave && !leavetype?.is_backdated_leave_allowed){
            def date = new Date()
            date.setDate(date.getDate() - leavetype?.backdated_leave_variable);
            mindate =  dateFormatting(date)
        }

        def nameofbutton = "Apply"
        if(loadadjust)
            nameofbutton = 'Adjust Load'

        HashMap hm = new HashMap()
        hm.put("lbaprroved", lbaprroved)
        hm.put("showAcademicYear",showAcademicYear)
        hm.put("academicYearData",acdyear)
        hm.put("lbinporcess",lbinporcess)
        hm.put("aprovingAuthority",aprovingAuthority)
        hm.put("elt",elt)
        hm.put("instructor",instructor)
        hm.put("color",leavetype?.leave_type_color)
        hm.put("leavetype",leavetype)
        hm.put("total_allowed_leaves",total_allowed_leaves)
        hm.put("mindate",mindate)
        hm.put("maxdate",maxdate)
        hm.put("balance",balance)
        hm.put("yearayy",ay.id)
        hm.put("backto",backto)
        hm.put("loadadjust",loadadjust)
        hm.put("loadadjust",loadadjust)
        hm.put("adjusted",adjusted)
        hm.put("loadRequired",loadRequired)
        hm.put("CompoffArray",CompoffArray)
        hm.put("availcompoff",availcompoff)
        hm.put("nameofbutton",nameofbutton)

        return hm
    }

    /*
    params.backto = backto
    params.instid = instid
    params.leavetype = ltype
    params.academicYear = academicYear
    params.slot = slotid
    params.redirect = redirect
     */
    def process_add_availed_vaccation_with_leave_expiry(String username, String backto, String instid, String ltype, String academicYear, String slotid, String redirect){
        HashMap hm = new HashMap()
        Login login = Login.findById(username)
        int max_number_of_days = 0
        def slotrange

        def balanceVacation
        def inprocessVacation = 0
        def approvedVacation = 0
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        Instructor instructor = Instructor.findByUid(username)
        if(backto == 'EB'){
            instructor = Instructor.findById(instid)
        }

        LeaveType leavetype = LeaveType.findByDisplay_nameAndOrganization(ltype, instructor.organization)
        def acdyear = AcademicYear.list().sort{it.ay}.reverse(true)
        RoleType roletype = RoleType.findByTypeAndOrganization("Leave Management", instructor.organization)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByIsActiveAndRoletypeAndOrganization(true, roletype, instructor.organization)

        AcademicYear ay
        if(academicYear)
            ay = AcademicYear.findById(academicYear)
        else
            ay = aay.academicyear

        def slotlist
        def employeeVaccationMaster = null
        def leaveMaster
        def finalslotlist = []

        if (leavetype.leaveexpirytype.type == 'Academic Year') {
            leaveMaster = LeaveMaster.findByOrganizationAndLeavetypeAndAcademicyear(instructor.organization, leavetype, ay)
            employeeVaccationMaster = EmployeeVaccationMaster.findByOrganizationAndLeavetypeAndAcademicyearAndInstructorAndLeavemaster(instructor.organization, leavetype, ay, instructor, leaveMaster)
        }else if (leavetype.leaveexpirytype.type == 'Slot') {
            slotlist = EmployeeVaccationMaster.createCriteria().list(){
                projections {
                    distinct("leavemaster")
                }
                and{
                    'in'('organization', instructor.organization)
                    'in'('instructor', instructor)
                    'in'('leavetype', leavetype)
                    isNotNull("leavemaster")
                }
            }
            for(s in slotlist)
                finalslotlist.add(id : s?.id, name : "AY "+ s?.academicyear?.ay +" : "+ formatter.format(s?.fromdate) + " - " + formatter.format(s?.todate))

            if(slotid)
                leaveMaster = LeaveMaster.findById(slotid)
            else
                leaveMaster = LeaveMaster.createCriteria().list() {
                    'in'('organization', instructor.organization)
                    and {
                        'in'('leavetype', leavetype)
//                            'in'('employeetype', instructor.employeetype)
                        le('fromdate', new Date())
                        ge('todate', new Date())
                    }
                }[0]

            if(!leaveMaster){
                leaveMaster = slotlist[0]
            }

            if(leaveMaster) {
                employeeVaccationMaster = EmployeeVaccationMaster.createCriteria().list() {
                    'in'('organization', instructor.organization)
                    and {
                        'in'('instructor', instructor)
                        'in'('leavetype', leavetype)
                        'in'('leavemaster', leaveMaster)
                    }
                }[0]

                if(!employeeVaccationMaster){
                    employeeVaccationMaster = EmployeeVaccationMaster.createCriteria().list() {
                        'in'('organization', instructor.organization)
                        and {
                            'in'('instructor', instructor)
                            'in'('leavetype', leavetype)
                            'in'('leavemaster', slotlist[0])
                        }
                    }[0]

                    leaveMaster = slotlist[0]
                }
            }
        }else if (leavetype.leaveexpirytype.type == 'No Expiry') {
            leaveMaster = LeaveMaster.findByOrganizationAndLeavetype(instructor.organization, leavetype)
            employeeVaccationMaster = EmployeeVaccationMaster.findByOrganizationAndLeavetypeAndInstructor(instructor.organization, leavetype, instructor)
        }else if (lt.leaveexpirytype.type == 'Expiry Date') {
        }
        for (e in employeeVaccationMaster) {
            max_number_of_days = max_number_of_days + e.allowedDays
        }
        def fromdate
        def todate

        if(leaveMaster){
            slotrange = formatter.format(leaveMaster.fromdate) + " - " + formatter.format(leaveMaster.todate)
            fromdate = leaveMaster.fromdate
            todate = leaveMaster.todate
        }

        def employeeleavetransactionList
        if (employeeVaccationMaster) {
            if (leavetype.leaveexpirytype.type == 'Academic Year') {
                employeeleavetransactionList = EmployeeLeaveTransaction.createCriteria().list() {
                    like("instructor", instructor)
                    and {
                        'in'("leavetype", leavetype)
                        like("organization", instructor.organization)
                        like("academicyear", ay)
                    }
                }
            }else if (leavetype.leaveexpirytype.type == 'Slot') {
                employeeleavetransactionList = EmployeeLeaveTransaction.createCriteria().list() {
                    like("instructor", instructor)
                    and {
                        'in'("leavetype", leavetype)
                        like("organization", instructor.organization)
                        ge("fromdate", fromdate)
                        le("todate", todate)
                    }
                }
            }else if (leavetype.leaveexpirytype.type == 'No Expiry') {
                employeeleavetransactionList = EmployeeLeaveTransaction.createCriteria().list() {
                    like("instructor", instructor)
                    and {
                        'in'("leavetype", leavetype)
                        like("organization", instructor.organization)
                    }
                }
            }else if (leavetype.leaveexpirytype.type == 'Expiry Date') {
            }

            for (EmployeeLeaveTransaction employeeLeaveTransaction : employeeleavetransactionList) {
                if (employeeLeaveTransaction.leaveapprovalstatus.status == "inprocess") {
                    inprocessVacation += employeeLeaveTransaction.numberofdays
                } else if (employeeLeaveTransaction.leaveapprovalstatus.status == "approved") {
                    approvedVacation += employeeLeaveTransaction.numberofdays
                }
            }
        } else
        {
            hm.put("msg","Please ask Establishment Section to add vacation slots....")
            return hm
//            flash.message="Please ask Establishment Section to add vacation slots...."
        }

        if(redirect)
//            session.redirect = 'leave'
        ArrayList<String> aprovingAuthority = new ArrayList<String>();
        for (EmployeeLeaveTransaction employeeLeaveTransaction : employeeleavetransactionList) {
            if (employeeLeaveTransaction.leaveapprovalstatus.status == "inprocess") {
                def c = EmployeeLeaveTransactionApproval.createCriteria()
                def eltaList = c.list() {
                    like("employeeleavetransaction", employeeLeaveTransaction)
                    and {
                        like("organization", instructor.organization)
                    }
                    order("id", "asc")
                }
                if (eltaList.size() > 0) {
                    aprovingAuthority.add(eltaList.last().leaveapprovalhierarchy.leaveapporvalauthority.authority)
                } else {
                    aprovingAuthority.add("")
                }
            } else {
                aprovingAuthority.add("")
            }
        }
        def min;
        def max;
        if(slotrange!=null){
            def slot = slotrange.split('-')
            min = slot[0].split('/')
            min = min[2].trim()+'-'+min[1].trim()+'-'+min[0].trim()
            max = slot[1].split('/')
            max = max[2].trim()+'-'+max[1].trim()+'-'+max[0].trim()
        }

        def showAcademicYear
        if(leavetype.leaveexpirytype.type == 'Academic Year')
            showAcademicYear = 'A'
        else if(leavetype.leaveexpirytype.type == 'Slot')
            showAcademicYear = 'S'

        if(backto != 'EB' && !instructor?.apply_backdated_leave && !leavetype?.is_backdated_leave_allowed){
            def date = new Date()
            date.setDate(date.getDate() - leavetype?.backdated_leave_variable);
            min =  dateFormatting(date)
        }

        hm.put("msg","Please ask Establishment Section to add vacation slots....")
        hm.put("balanceVacation", balanceVacation)
        hm.put("max_number_of_days", max_number_of_days)
        hm.put("inprocessVacation", inprocessVacation)
        hm.put("aprovingAuthority", aprovingAuthority)
        hm.put("elt", employeeleavetransactionList)
        hm.put("slotrange", slotrange)
        hm.put("instructor", instructor)
        hm.put("leavetype", leavetype)
        hm.put("max_number_of_days", max_number_of_days)
        hm.put("min", min)
        hm.put("max", max)
        hm.put("slotlist", finalslotlist)
        hm.put("academicYearData", acdyear)
        hm.put("yearayy", ay.id)
        hm.put("showAcademicYear", showAcademicYear)
        hm.put("defaultslot", leaveMaster?.id)
        hm.put("backto", backto)
        hm.put("instructor", instructor)
        return hm
    }

    /*
        params.numberofdays = numofdays
        params.backto = backto
        params.instid = instid
        params.academicYear =academicYear
        params.leavetype = ltype
        params.balanceleaves = bleaves
        params.from = from
        params.to = to
        params.reason = reason
        params.numberofhours = numofhours
        params.is_load_adjustment_done = is_load_adjustment_done
        params.contact_phone_number = contact_phone_number
        params.alternate_arragement_person_name = alternate_arragement_person_name
        params.alternate_arragement_person_contact_number = alternate_arragement_person_contact_number
        params.nature_of_work = nature_of_work
        params.place_of_work = place_of_work
        params.halfLeave = halfLeave
     */
    def saveleaveWithLeaveExpiry(String username, String backto, String instid, String academicYear, Date fromdate, Date todate,
                                 String ltype, String bleaves, String from, String to, String reason, String is_load_adjustment_done,
                                 String contact_phone_number, String alternate_arragement_person_name, String alternate_arragement_person_contact_number,
                                 String nature_of_work, String place_of_work, String halfLeave, def ipAddr){

//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//        Date firstDate = sdf.parse(fromdate.toString());
//        Date secondDate = sdf.parse(todate.toString());
        long diffInMillies = Math.abs(todate.getTime() - fromdate.getTime());
        def numofdays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        println("numofdays :: " + numofdays)

        def numofhours = 0
        if(from && to) {
            numofhours = timediff(from, to)
        }

        println("1")

        HashMap hm = new HashMap()
        double balaceleaves = 0
        double numberofdays = 0
        if (numofdays != null)
            numberofdays = Double.parseDouble(numofdays+"")

        Instructor instructor = Instructor.findByUid(username)

        println("2")

        if(backto == 'EB'){
            instructor = Instructor.findById(instid)
        }

        RoleType roletype = RoleType.findByTypeAndOrganization("Leave Management", instructor.organization)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByIsActiveAndRoletypeAndOrganization(true, roletype, instructor.organization)

        println("3")

        AcademicYear ay
        if(academicYear)
            ay = AcademicYear.findById(academicYear)
        else
            ay = aay.academicyear

        println("4")

        LeaveType leavetype = LeaveType.findByDisplay_nameAndOrganizationAndIsactive(ltype, instructor.organization, true)

        if(leavetype?.min_leave_allowed > 0) {
            if (numberofdays < leavetype?.min_leave_allowed) {
                hm.put("msg", leavetype.display_name + " can not be applied for less than "+leavetype?.min_leave_allowed+" days.")
                hm.put("leavetype", leavetype.type)
                hm.put("academicYear", ay.id)
                hm.put("instid", instid)
                hm.put("backto", backto)
                return hm
//                flash.message = leavetype.display_name + " can not be applied for less than "+leavetype?.min_leave_allowed+" days."
//                redirect(controller: "EmployeeLeaveMaster", action: "processApplyForLeaveWithLeaveExpiry", params: [leavetype: leavetype.type, academicYear: ay.id, instid: instid, backto: backto])
//                return
            }
        }

        println("5")

        if(leavetype?.max_leave_allowed > 0) {
            if (numberofdays > leavetype?.max_leave_allowed) {
                hm.put("msg", leavetype.display_name + " can not be applied for more than "+leavetype?.max_leave_allowed+" days.")
                hm.put("leavetype", leavetype.type)
                hm.put("academicYear", ay.id)
                hm.put("instid", instid)
                hm.put("backto", backto)
                return hm
//                flash.message = leavetype.display_name + " can not be applied for more than "+leavetype?.max_leave_allowed+" days."
//                redirect(controller: "EmployeeLeaveMaster", action: "processApplyForLeaveWithLeaveExpiry", params: [leavetype: leavetype.type, academicYear: ay.id, instid: instid, backto: backto])
//                return
            }
        }

        def COMPOffavaileddate = []
        if(leavetype.type=="COMP-OFF")
        {
            println("6")
            def compoffstatus = CompOffStatus.findByStatus('Available')
            def elm = []
            if(compoffstatus) {
                elm = EmployeeLeaveMaster.createCriteria().list() {
                    'in'('organization', instructor.organization)
                    and {
                        'in'('instructor', instructor)
                        'in'('leavetype', leavetype)
                        'in'('compoffstatus', compoffstatus)
                    }
                }
            }
            elm?.sort { it?.expirydate }
            def availcompoff = true
            HashSet monthYearArray = []
            def count = 0
            (fromdate..todate).each { today ->
                count++
                def year = today.getYear() + 1900
                def month = today.getMonth() + 1
                monthYearArray.add(month+"-"+year)
                today.setHours(00);
                today.setMinutes(00);
                today.setSeconds(00);
                def canavail = false
                for (day in elm) {
                    if(!COMPOffavaileddate?.contains(day?.id)) {
                        day?.fromdate.setHours(23);
                        day?.fromdate.setMinutes(59);
                        day?.fromdate.setSeconds(00);
                        if (today > day?.fromdate && today <= day?.expirydate) {
                            canavail = true
                            COMPOffavaileddate.add(day?.id)
                            break
                        }
                    }
                }
                if(!canavail) {
                    availcompoff = false
                }
            }

            def maxcompoffpermonth = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Maximum number of compensatory offs can be availed in a month by an employee', instructor.organization)

            def maxcompofftaken = false
            for(item in monthYearArray) {
                item = item.split('-')
                def month = Integer.parseInt(item[0])
                def year = Integer.parseInt(item[1])
                def daysInMonth = new Date(year, month, 0).getDate()
                def leavefromdate = new Date(year - 1900, month - 1, 1, 0, 0)
                def leavetodate = new Date(year - 1900, month - 1, daysInMonth, 23, 59)

                def leaveapprovalstatus = LeaveApprovalStatus.findAllByStatusInList(['inprocess', 'Approved'])

                def elt = []
                if (leaveapprovalstatus) {
                    elt = EmployeeLeaveTransaction.createCriteria().list() {
                        like("instructor", instructor)
                        and {
                            'in'("leavetype", leavetype)
                            'in'("leaveapprovalstatus", leaveapprovalstatus)
                            like("organization", instructor.organization)
                        }
                    }
                }

                if((elt.size()+count) > Integer.parseInt(maxcompoffpermonth?.value)){
                    maxcompofftaken = true
                }
            }

            println("7")
            if(maxcompofftaken) {
                hm.put("msg", "Cannot Avail More Than " + maxcompoffpermonth?.value + " "+ leavetype?.display_name+" Per Month")
                hm.put("leavetype", leavetype.type)
                hm.put("academicYear", ay.id)
                hm.put("instid", instid)
                hm.put("backto", backto)
                return hm
//                flash.message = "Cannot Avail More Than " + maxcompoffpermonth?.value + " "+ leavetype?.display_name+" Per Month"
//                redirect(controller: "EmployeeLeaveMaster", action: "processApplyForLeaveWithLeaveExpiry", params: [leavetype: leavetype.type, academicYear: ay.id, instid: instid, backto: backto])
//                return
            }
            println("8")
            if(!availcompoff) {
                hm.put("msg", "Can't Avail COMP-OFF On Given Date.. Please Check you are applying COMP-OFF On Valid Date")
                hm.put("leavetype", leavetype.type)
                hm.put("academicYear", ay.id)
                hm.put("instid", instid)
                hm.put("backto", backto)
                return hm
//                flash.message = "Can't Avail COMP-OFF On Given Date.. Please Check you are applying COMP-OFF On Valid Date"
//                redirect(controller: "EmployeeLeaveMaster", action: "processApplyForLeaveWithLeaveExpiry", params: [leavetype: leavetype.type, academicYear: ay.id, instid: instid, backto: backto])
//                return
            }
            println("9")
        }

        LeaveApprovalStatus leaveapprovalstatus = LeaveApprovalStatus.findByStatus("inprocess")

        if (leavetype.type == 'HD_CL')
            todate = fromdate

        println("10")

        def leaveTakenFlag = false
        def leaveDays = ''
        (fromdate..todate).each { today ->
            def erpmonth = ERPMonth.findByMonthnumber(today.getMonth()+1)
            def year = today.getYear()+1900
            def erpyear = ERPCalendarYear.findByYear(""+year)

            def att = EmployeeAttendance.findByInstructorAndErpcalenderintegerdayAndErpmonthAndErpcalenderyearAndHasappliedforleavefortoday(
                    instructor,today.getDate(),erpmonth,erpyear,true)
            if(att!=null)
            {
                if(((att.leavetype.type == 'OD' || att.leavetype.type == 'On Duty') && leavetype?.type == "HD_CL") || (att.leavetype.type == "HD_CL" && (leavetype?.type=="OD" || leavetype?.type=="On Duty"))){
                }else {
                    def day = today.getDate() > 9 ? today.getDate() : '0' + today.getDate()
                    def month = (today.getMonth() + 1) > 9 ? (today.getMonth() + 1) : '0' + (today.getMonth() + 1)
                    def leaveDate = day + '-' + month + '-' + year
                    if (att.leavetype.type == 'VACATION')
                        leaveDays = leaveDays + ', vacation' + ' on ' + leaveDate
                    else
                        leaveDays = leaveDays + ', ' + att.leavetype.type + ' on ' + leaveDate
                    leaveTakenFlag = true
                }
            }
        }

        println("11")

        if(leaveTakenFlag){
            hm.put("msg", "You have already applied "+leaveDays+". Please select appropriate date.")
            return hm
//            flash.message="You have already applied "+leaveDays+". Please select appropriate date."
//            if(backto == 'EB'){
//                redirect(controller: "EmployeeLeaveMaster", action: "applyforleaveByEstablishment")
//            }else {
//                redirect(controller: "EmployeeLeaveMaster", action: "applyforleave")
//            }
//            return
        }

        println("12")

        if (leavetype.type != 'LWP' && leavetype.type != 'OD') {
            balaceleaves = Double.parseDouble(bleaves)
            if(leavetype.type == 'ML'){
                if ((numberofdays*leavetype?.aditionalinfo1) > balaceleaves) {
                    hm.put("msg", "You are trying to apply for " + numberofdays + " " + leavetype.display_name + "(1ML = "+leavetype?.aditionalinfo1+"HPL), but your balance HPL is " + balaceleaves)
                    hm.put("leavetype", leavetype.type)
                    hm.put("academicYear", ay.id)
                    hm.put("instid", instid)
                    hm.put("backto", backto)
                    return hm
//                    flash.message = "You are trying to apply for " + numberofdays + " " + leavetype.display_name + "(1ML = "+leavetype?.aditionalinfo1+"HPL), but your balance HPL is " + balaceleaves
//                    redirect(controller: "EmployeeLeaveMaster", action: "processApplyForLeaveWithLeaveExpiry", params: [leavetype: leavetype.type, academicYear: ay.id,  instid: instid, backto:backto])
//                    return
                }
            }else {
                if (numberofdays > balaceleaves) {
                    hm.put("msg", "You are trying to apply for " + numberofdays + " " + leavetype.display_name + " , but your balance " + leavetype.display_name + " is " + balaceleaves)
                    hm.put("leavetype", leavetype.type)
                    hm.put("academicYear", ay.id)
                    hm.put("instid", instid)
                    hm.put("backto", backto)
                    return hm
//                    flash.message = "You are trying to apply for " + numberofdays + " " + leavetype.display_name + " , but your balance " + leavetype.display_name + " is " + balaceleaves
//                    redirect(controller: "EmployeeLeaveMaster", action: "processApplyForLeaveWithLeaveExpiry", params: [leavetype: leavetype.type, academicYear: ay.id,  instid: instid, backto:backto])
//                    return
                }
            }
        }

        println("13")

        EmployeeLeaveTransaction employeeleavetransaction = new EmployeeLeaveTransaction()
        employeeleavetransaction.application_date = new java.util.Date()
        employeeleavetransaction.fromdate = fromdate
        employeeleavetransaction.todate = todate
        if (leavetype.type != 'HD_CL')
            employeeleavetransaction.numberofdays = Double.parseDouble(numofdays + "")
        else
            employeeleavetransaction.numberofdays = 0.5
        if (leavetype.type == 'OD' || leavetype.type == 'On Duty') {
            employeeleavetransaction.fromtime=from
            employeeleavetransaction.totime=to
            employeeleavetransaction.numberofhours = Double.parseDouble(numofhours.trim())
        } else
            employeeleavetransaction.numberofhours = 0
        employeeleavetransaction.reason = reason
        if (is_load_adjustment_done != null)
            employeeleavetransaction.is_load_adjustment_done = true
        else
            employeeleavetransaction.is_load_adjustment_done = false
        employeeleavetransaction.username = instructor.username
        employeeleavetransaction.creation_date = new java.util.Date()
        employeeleavetransaction.updation_date = new java.util.Date()
        employeeleavetransaction.creation_ip_address = ipAddr
        employeeleavetransaction.updation_ip_address = ipAddr
        employeeleavetransaction.instructor = instructor
        employeeleavetransaction.leavetype = leavetype
        employeeleavetransaction.leaveapprovalstatus = leaveapprovalstatus
        employeeleavetransaction.organization = instructor.organization

        println("14")

        if (leavetype.type == "ML" || leavetype.type == "EL"  || leavetype.type == "Maternity Leave"  || leavetype.type == "HPL" || leavetype.type == "LWP" || leavetype.type == "CL" || leavetype.type == "HD_CL") {
            employeeleavetransaction.alternate_arragement_person_name = alternate_arragement_person_name
            employeeleavetransaction.alternate_arragement_person_contact_number = alternate_arragement_person_contact_number
        }
        if (leavetype.type == "OD" || leavetype.type == "On Duty") {
            employeeleavetransaction.nature_of_work = nature_of_work
            employeeleavetransaction.place_of_work = place_of_work
            employeeleavetransaction.contact_phone_number = contact_phone_number
        }

        if (leavetype.type == "ML" || leavetype.type == "OD" || leavetype.type == "On Duty") {
            def file = request.getFile("certificate_file_name")
            if (file.empty) {
                if(leavetype.type == "OD" || backto == 'EB'){
                }else {
                    hm.put("msg", "File cannot be empty")
                    hm.put("leavetype", leavetype.type)
                    hm.put("academicYear", ay.id)
                    hm.put("instid", instid)
                    hm.put("backto", backto)
                    return hm
//                    flash.message = "File cannot be empty"
//                    redirect(controller: "EmployeeLeaveMaster", action: "processApplyForLeaveWithLeaveExpiry", params: [leavetype: leavetype.type, academicYear: ay.id,  instid: instid, backto:backto])
//                    return
                }
            } else {
                AWSFolderPath awsFolderPath = AWSFolderPath.findByPath('cloud/')
                Part filePart = request.getPart("certificate_file_name")

                def fileExtention
                InputStream fs = filePart.getInputStream()
                String contentType = filePart.getContentType();
                String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                int indexOfDot = filePartName.lastIndexOf('.')
                if (indexOfDot > 0) {
                    fileExtention = filePartName.substring(indexOfDot + 1)
                }

                def folderPath = awsFolderPath.path + 'facultyprofile/leave/medicalcertificate/' + instructor.employee_code +'/'
                def fileName = 'medicalcertificate_' + instructor.employee_code + "_" + new Date().format('dd-MM-yyyy') + '.' +fileExtention
                def existsFilePath = ""
                boolean isUploaded = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)

                if (isUploaded) {
                    String certificate_file_name = fileName
                    employeeleavetransaction.certificate_file_path = 'facultyprofile/leave/medicalcertificate/' + instructor.employee_code +'/'
                    employeeleavetransaction.certificate_file_name = certificate_file_name
                }
            }
        }

        println("15")

        employeeleavetransaction.academicyear = ay
        InstructorLeaveAprovalHierarchy instleaveapprovalhierarchy = InstructorLeaveAprovalHierarchy.findByInstructorAndLeavetypeAndOrganization(instructor, leavetype, instructor.organization)
        LeaveApprovalHierarchy leaveapprovalhierarchy
        if(!instleaveapprovalhierarchy){
            hm.put("msg", "Please contact Admin : Leave Approving Hierarchy is not set for " + leavetype?.display_name)
            return hm
//            flash.message = "Please contact Admin : Leave Approving Hierarchy is not set for " + leavetype?.display_name
//            if (backto == 'EB') {
//                redirect(controller: "EmployeeLeaveMaster", action: "applyforleaveByEstablishment")
//            } else {
//                redirect(controller: "EmployeeLeaveMaster", action: "applyforleave")
//            }
//            return
        }else {
            leaveapprovalhierarchy = LeaveApprovalHierarchy.findByLevelAndLeaveapprovalhierarchymasterAndOrganization(1, instleaveapprovalhierarchy?.leaveapprovalhierarchymaster, instructor.organization)
            if (!leaveapprovalhierarchy) {
                hm.put("msg", "Please contact Admin : Leave Approving Hierarchy is not set for " + leavetype?.display_name)
                return hm
//                flash.message = "Please contact Admin : Leave Approving Hierarchy is not set for " + leavetype?.display_name
//                if (backto == 'EB') {
//                    redirect(controller: "EmployeeLeaveMaster", action: "applyforleaveByEstablishment")
//                } else {
//                    redirect(controller: "EmployeeLeaveMaster", action: "applyforleave")
//                }
//                return
            }
        }

        println("16")

        def approvedby
        if (instructor.reporttingauthority != null && leaveapprovalhierarchy.leaveapporvalauthority.authority == "HOD") {
            approvedby = instructor.reporttingauthority
        } else if (instructor.reportingdepartment != null && leaveapprovalhierarchy.leaveapporvalauthority.authority == "HOD") {
            approvedby = instructor.reportingdepartment.hod
        } else if (instructor.department != null) {
            if(leaveapprovalhierarchy.leaveapporvalauthority.authority == "HOD") {
                approvedby = instructor.department.hod
            }else if(leaveapprovalhierarchy.leaveapporvalauthority.authority == "Dean"){
                approvedby = instructor?.department?.stream?.dean
            }
        } else {
            hm.put("msg", "Please contact Admin : Your department is not set.")
            return hm
//            flash.message = "Please contact Admin : Your department is not set."
//            if(backto == 'EB'){
//                redirect(controller: "EmployeeLeaveMaster", action: "applyforleaveByEstablishment")
//            }else {
//                redirect(controller: "EmployeeLeaveMaster", action: "applyforleave")
//            }
//            return
        }
        if (leavetype.type == "HD_CL") {
            def firstHalf;
            if(Integer.parseInt(halfLeave))
                firstHalf = true
            else
                firstHalf = false
            employeeleavetransaction.isfirsthalf = firstHalf
        }
        employeeleavetransaction.save(failOnError: true, flush: true)
        if(!leavetype?.is_backdated_leave_allowed)
            leaveManagementSystemService.disableBackdatedLeave(employeeleavetransaction.instructor, ipAddr, username)
        if(leavetype.type=="COMP-OFF"){
            for(elmt in COMPOffavaileddate){
                def compest = EmployeeLeaveMaster.findById(elmt)
                CompOffStatus compoffstatus = CompOffStatus.findByStatus("Availed")
                compest.compoffstatus = compoffstatus
                compest.employeeleavetransaction = employeeleavetransaction
                compest.updation_ip_address = ipAddr
                compest.updation_date = new java.util.Date()
                compest.username = username
                compest.save(failOnError: true, flush: true)
            }
        }

        println("17")

        def isautoapproveleavebyest = ERPRoleTypeSettings.findByNameIlikeAndOrganization('EST Leave Apply should go for Approval of all hierarchy authorities?', instructor.organization)?.value
        def autoapproveest = false
        if(isautoapproveleavebyest != 'true' && backto == 'EB')
            autoapproveest = true

        def loadadjust = false
        if (backto != 'EB') {
            if (isloadadjusted(employeeleavetransaction) == 1) {
                loadadjust = true
            }else if (isloadadjusted(employeeleavetransaction) == 0) {
                loadadjust = false
            } else {
                hm.put("msg", "Please set Application Academic Year For Role Type")
                return hm
            }
        } else {
            def isloadAdjusttrue = ERPRoleTypeSettings.findByNameIlikeAndOrganization('EST Leave Apply should go for load adjustment?', instructor.organization)?.value
            if (isloadAdjusttrue == 'true')
                loadadjust = isloadadjusted(employeeleavetransaction)
        }

        println("18")

        if(!autoapproveest) {
            def remark = ""
            LeaveApprovalStatus leaveapprovalstatusapprove
            if (leaveapprovalhierarchy?.leaveapporvalauthority?.minnoofleavesforinstanceapproval >= employeeleavetransaction?.numberofdays) {
                leaveapprovalstatusapprove = LeaveApprovalStatus.findByStatus("approved")
                remark = "Auto Approved By System"
            } else {
                leaveapprovalstatusapprove = LeaveApprovalStatus.findByStatus("inprocess")
            }

            if (!loadadjust) {
                EmployeeLeaveTransactionApproval employeeleavetransactionapproval = new EmployeeLeaveTransactionApproval()
                employeeleavetransactionapproval.approvedby = approvedby
                employeeleavetransactionapproval.application_push_date = new java.util.Date()
                employeeleavetransactionapproval.username = username
                employeeleavetransactionapproval.creation_date = new java.util.Date()
                employeeleavetransactionapproval.updation_date = new java.util.Date()
                employeeleavetransactionapproval.creation_ip_address = ipAddr
                employeeleavetransactionapproval.updation_ip_address = ipAddr
                employeeleavetransactionapproval.organization = instructor.organization
                employeeleavetransactionapproval.employeeleavetransaction = employeeleavetransaction
                employeeleavetransactionapproval.remark = remark
                employeeleavetransactionapproval.leaveapprovalstatus = leaveapprovalstatusapprove
                employeeleavetransactionapproval.leaveapprovalhierarchy = leaveapprovalhierarchy
                employeeleavetransactionapproval.save(failOnError: true, flush: true)
                if (leaveapprovalhierarchy?.leaveapporvalauthority?.minnoofleavesforinstanceapproval >= employeeleavetransaction?.numberofdays)
                    leaveManagementSystemService.autoApproveOnMinDaysOfLeave(employeeleavetransactionapproval, ipAddr, username)
            }
        }else{
            leaveapprovalstatus = LeaveApprovalStatus.findByStatus("approved")
            employeeleavetransaction.leaveapprovalstatus = leaveapprovalstatus
            employeeleavetransaction.save(failOnError: true, flush: true)
        }

        println("19")

        leaveManagementSystemService.insertIntoAttendanceOnLeave(instructor,
                ay,
                leavetype,
                fromdate,
                todate,
                leaveapprovalstatus,
                employeeleavetransaction,
                true)

        println("20")

        if(!loadadjust || autoapproveest) {
            def loadadjustrequired = false
            def timetableload = ERPRoleTypeSettings.findByNameIlikeAndOrganization('While Applying Leave is Load Adjustment required for each lecture/practial as per time table?', instructor.organization)?.value
            def customload = ERPRoleTypeSettings.findByNameIlikeAndOrganization('While Applying Leave is Load Adjustment required for customized slot entered by Employee?', instructor.organization)?.value
            if((timetableload == 'true' || customload == 'true') && instructor?.isloadadustmentrequired)
                loadadjustrequired = true

            def msg = ""
            if(loadadjustrequired)
                msg = '(No Load Found For Current Date)'
            hm.put("msg", "Leave Applied Successfully..." + msg)
            hm.put("leavetype", leavetype.type)
            hm.put("academicYear", ay.id)
            hm.put("instid", instid)
            hm.put("backto", backto)
            return hm
//            flash.message = "Leave Applied Successfully..." + msg
//            redirect(controller: "EmployeeLeaveMaster", action: "processApplyForLeaveWithLeaveExpiry", params: [leavetype: leavetype.type, academicYear: ay.id, instid: instid, backto: backto])
        } else {
            hm.put("msg", "go to loadadjustment")
            hm.put("e", employeeleavetransaction?.id)
            hm.put("academicYear", ay.id)
            hm.put("backto", backto)
            return hm
//            redirect(controller: "EmployeeLeaveMaster", action: "loadadjustment", params: [e: employeeleavetransaction?.id, backto: backto, ay: ay.id])
        }
        println("21")
    }

    /*
    params.e = employeeLeaveTransactionId
     */
    def leaveapplicaionhistory(def employeeLeaveTransactionId) {
        HashMap hm = new HashMap()

        EmployeeLeaveTransaction employeeLeaveTransaction = EmployeeLeaveTransaction.findById(employeeLeaveTransactionId)

        def employeeLeaveTransactionApprovalList = EmployeeLeaveTransactionApproval.findAllByEmployeeleavetransaction(employeeLeaveTransaction)

        Contact contact
        if(employeeLeaveTransactionApprovalList)
            contact = Contact.findByPerson(employeeLeaveTransactionApprovalList.get(0).employeeleavetransaction.instructor.person)

        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        AWSFolderPath awsFolderPath = AWSFolderPath.findByPath('cloud/')

        def loadadjust = isloadadjusted(employeeLeaveTransaction)
        if(loadadjust == 1 || loadadjust == 0 ) {
            def loadadjustlist = LeaveLoadAdjustment.findAllByEmployeeleavetransactionAndOrganizationAndIsdeletedAndIsloadadjusted( employeeLeaveTransaction, employeeLeaveTransaction?.organization, false, true)

            hm.put("contact", contact)
            if(loadadjust == 1 ) {
                hm.put("loadadjust", true)
            } else {
                hm.put("loadadjust", false)
            }
            hm.put("employeeLeaveTransaction", employeeLeaveTransaction)
            hm.put("loadadjustlist", loadadjustlist)
            hm.put("employeeLeaveTransactionApprovalList", employeeLeaveTransactionApprovalList)
            hm.put("link", awsBucket.aws_path+awsFolderPath.path)
        } else {
            hm.put("msg", "Please set Application Academic Year For Role Type")
        }
        return hm
    }


    /*
    params.e = employeeLeaveTransactionId
     */
    def loadadjustment(def username, def employeeLeaveTransactionId){
        HashMap hm = new HashMap()
        Instructor instructor = Instructor.findByUid(username)
        def org = instructor?.organization

        EmployeeLeaveTransaction employeeLeaveTransaction = EmployeeLeaveTransaction.findById(employeeLeaveTransactionId)
        def inst = employeeLeaveTransaction?.instructor

        def timetableload = ERPRoleTypeSettings.findByNameIlikeAndOrganization('While Applying Leave is Load Adjustment required for each lecture/practial as per time table?', instructor.organization)?.value
        def customload = ERPRoleTypeSettings.findByNameIlikeAndOrganization('While Applying Leave is Load Adjustment required for customized slot entered by Employee?', instructor.organization)?.value

        def erpTimeTablelistArray
        if(timetableload == 'true')
        {
            ApplicationType applicationType = ApplicationType.findByApplication_type("ERP")
            RoleType roleType = RoleType.findByApplicationtypeAndTypeAndOrganization(applicationType, "TimeTable", org)
            ApplicationAcademicYear aay = ApplicationAcademicYear.findByOrganizationAndRoletypeAndIsActiveAndIsDeleted(org, roleType, true, false)
            if(!aay) {
//                render "Please set Application Academic Year For Role Type : " + roleType.type
                hm.put("msg", "Please set Application Academic Year For Role Type : " + roleType.type)
                return hm
            }
            erpTimeTablelistArray = leaveManagementSystemService.getTimeTableLoad(employeeLeaveTransaction, aay)
        }

        def customloadad = false
        def customloadArray = []
        if(customload == 'true'){
            customloadad = leaveManagementSystemService.getCustomeLoad(employeeLeaveTransaction)
            if(customloadad)
                customloadArray = leaveManagementSystemService.getCustomeLoadDetails(employeeLeaveTransaction)
        }

        def list = []
        list.add(inst?.id)
        def instlist = Instructor.findAllByOrganizationAndIscurrentlyworkingAndIdNotInList(org, true, list)

        def e = employeeLeaveTransaction
        InstructorLeaveAprovalHierarchy instleaveapprovalhierarchy = InstructorLeaveAprovalHierarchy.findByInstructorAndLeavetypeAndOrganization(e?.instructor, e.leavetype, e.instructor.organization)
        LeaveApprovalStatus leaveapprovalstatusapproved = LeaveApprovalStatus.findByStatus("approved")
        LeaveApprovalHierarchy leaveapprovalhierarchy = LeaveApprovalHierarchy.findByLevelAndLeaveapprovalhierarchymasterAndOrganization(1, instleaveapprovalhierarchy?.leaveapprovalhierarchymaster, instructor.organization)
        def employeeleavetransactionapproval = EmployeeLeaveTransactionApproval.findAllByOrganizationAndEmployeeleavetransactionAndLeaveapprovalstatusAndLeaveapprovalhierarchy(instructor.organization, e, leaveapprovalstatusapproved, leaveapprovalhierarchy)

        def leaveapproved = false
        if(employeeleavetransactionapproval?.size() > 0)
            leaveapproved = true

        if(employeeLeaveTransaction?.leaveapprovalstatus?.status == 'rejected' || employeeLeaveTransaction?.leaveapprovalstatus?.status == 'cancelled')
            leaveapproved = true

        hm.put("instructor", inst)
        hm.put("employeeLeaveTransaction", employeeLeaveTransaction)
        hm.put("erpTimeTablelistArray", erpTimeTablelistArray)
        hm.put("instlist", instlist)
        hm.put("leaveapproved", leaveapproved)
        hm.put("customloadad", customloadad)
        hm.put("customloadArray", customloadArray)
        hm.put("timetableload", timetableload)
        hm.put("customload", customload)
//        hm.put("params", params)
        hm.put("msg", "Success")
        return hm
    }


    // common services
    def dateFormatting(changeDate){
        def today = changeDate
        def year = today.getYear() + 1900
        def day = today.getDate() > 9 ? today.getDate() : '0' + today.getDate()
        def month = (today.getMonth() + 1) > 9 ? (today.getMonth() + 1) : '0' + (today.getMonth() + 1)
        return year + '-' + month + '-' + day
    }

    def isloadadjusted(def employeeleavetransaction){
        def organization = employeeleavetransaction?.organization
        def timetableload = ERPRoleTypeSettings.findByNameIlikeAndOrganization('While Applying Leave is Load Adjustment required for each lecture/practial as per time table?', organization)?.value
        def customload = ERPRoleTypeSettings.findByNameIlikeAndOrganization('While Applying Leave is Load Adjustment required for customized slot entered by Employee?', organization)?.value
        def loadadjust = false
        if((timetableload == 'true' || customload == 'true') && employeeleavetransaction?.instructor?.isloadadustmentrequired){
            if(timetableload == 'true'){
                ApplicationType applicationType = ApplicationType.findByApplication_type("ERP")
                RoleType roleType = RoleType.findByApplicationtypeAndTypeAndOrganization(applicationType, "TimeTable", organization)
                ApplicationAcademicYear aaytimetable = ApplicationAcademicYear.findByOrganizationAndRoletypeAndIsActiveAndIsDeleted(organization, roleType, true, false)
                if(!aaytimetable) {
                    loadadjust = false
//                    msg = "Please set Application Academic Year For Role Type : " + roleType.type
//                    render "Please set Application Academic Year For Role Type : " + roleType.type
//                    return
                    return 2
                }
                def list = leaveManagementSystemService.getTimeTableLoad(employeeleavetransaction, aaytimetable)
                if(list.size() > 0)
                    loadadjust = true
            }
            if(customload == 'true'){
                loadadjust = leaveManagementSystemService.getCustomeLoad(employeeleavetransaction)
            }
        }
        if(loadadjust == true) {
            return 1
        } else {
            return 0
        }
    }

    def timediff(outtime, intime) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Date date1 = format.parse(intime + ":00");
        Date date2 = format.parse(outtime + ":00");
        long difference
        if (date2 > date1)
            difference = (date2.getTime() - date1.getTime()) / 1000;
        else
            difference = (date1.getTime() - date2.getTime()) / 1000;
        int hours = difference / 3600;
        difference = difference % 3600;
        def mins = difference / 60;
        mins = mins > 9 ? mins : "0" + mins
        return Double.parseDouble(hours + '.' + mins)
    }
}

