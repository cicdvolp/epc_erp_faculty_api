package volp

import grails.gorm.transactions.Transactional

import javax.servlet.http.Part
import java.nio.file.Paths
import java.text.SimpleDateFormat

@Transactional
class TaskService {

    def serviceMethod() {

    }

    def addNewTask(request,instructor,hm)
    {
        println "in addNewTask service"
        String abbr=instructor?.employeeabbr
        String activityname="General"
        activityname=abbr+"-"+activityname
        TaskActivity taskActivity=TaskActivity.findByNameAndOrganizationAndInstructor(activityname,instructor?.organization,instructor)
        if(!taskActivity)
        {
            def taskactivity=createNewTaskActivity(activityname,request.getRemoteAddr(),instructor)
            TaskStatusType taskStatusType=TaskStatusType.findByType("Default")
            if(!taskStatusType)
            {
                taskStatusType=addNewTaskStatusType("Default",request.getRemoteAddr(),instructor.uid)
            }
            TaskStatusType taskStatusType1=TaskStatusType.findByType("User Defined")
            if(!taskStatusType1)
            {
                taskStatusType1=addNewTaskStatusType("User Defined",request.getRemoteAddr(),instructor.uid)
            }
            if(taskStatusType)
                addNewTaskStatus("To-Do",instructor,1,request.getRemoteAddr(),taskactivity,taskStatusType)
            if(taskStatusType1)
                addNewTaskStatus("In-Process",instructor,2,request.getRemoteAddr(),taskactivity,taskStatusType1)
            if(taskStatusType)
                addNewTaskStatus("Done",instructor,3,request.getRemoteAddr(),taskactivity,taskStatusType)
        }
        def taskActivity1=TaskActivity.findAllByIsactiveAndOrganizationAndInstructor(true,instructor?.organization,instructor)
        ArrayList arrayList=new ArrayList()
        for(eachtask in taskActivity1)
        {
            HashMap temp=new HashMap()
            temp.put("id",eachtask?.id)
            temp.put("name",eachtask?.name)
            def task=Task.findAllByTaskactivityAndOrganizationAndTaskIsNull(eachtask,instructor?.organization).size()
            if(task==0)
                temp.put("maintasksize",findUniqueMainTaskName(eachtask?.name)+"-01")
            else {
                def task1=task+1
                if(task1 < 10)
                    task1="0"+task1
                temp.put("maintasksize", findUniqueMainTaskName(eachtask?.name) + task1)
            }
            if(activityname==eachtask?.name && task==0) {
                hm.put("maintask",abbr + "-GEN-01")
                temp.put("maintasksize", abbr + "-GEN-01")
            }
            else if(activityname==eachtask?.name && task>0) {
                def task1=task+1
                if(task1 < 10)
                    task1="0"+task1
                temp.put("maintasksize", abbr + "-GEN-" + task1)
                hm.put("maintask", abbr + "-GEN-" + task1)
            }
            arrayList.add(temp)
        }

        hm.put("activity",arrayList)
        ArrayList arrayList1=new ArrayList()
        for(eachinst in Instructor.findAllByOrganizationAndIscurrentlyworking(instructor?.organization, true))
        {
            HashMap temp=new HashMap()
            temp.put("instId",eachinst?.id)
            temp.put("name",eachinst?.employee_code +":"+eachinst?.person.fullname_as_per_previous_marksheet)
            arrayList1.add(temp)
        }
        hm.put("instructor",arrayList1)
        hm.put("status","200")
        hm.put("message","success")
        hm.put("assignbyinstructor",instructor?.employee_code +":"+instructor?.person.fullname_as_per_previous_marksheet)
        return
    }

    def createNewTaskActivity(activityname,ip,instructor)
    {
        TaskActivity taskActivity=new TaskActivity()
        taskActivity.name=activityname
        taskActivity.creation_date=new Date()
        taskActivity.updation_date=new Date()
        taskActivity.creation_ip_address=ip
        taskActivity.updation_ip_address=ip
        taskActivity.updation_username=instructor.uid
        taskActivity.creation_username=instructor.uid
        taskActivity.isactive=true
        taskActivity.organization=instructor.organization
        taskActivity.instructor=instructor
        taskActivity.save(failOnError: true, flush: true)
        return taskActivity
    }

    def addNewTaskStatus(name,instructor,seqno,ip,taskactivity,taskstatustype)
    {
        println("In addNewTaskStatus TaskService")

        TaskStatus taskStatus=new TaskStatus()
        taskStatus.name=name
        taskStatus.sequence_no=seqno
        taskStatus.creation_date=new Date()
        taskStatus.updation_date=new Date()
        taskStatus.creation_ip_address=ip
        taskStatus.updation_ip_address=ip
        taskStatus.updation_username=instructor.uid
        taskStatus.creation_username=instructor.uid
        taskStatus.isactive=true
        taskStatus.organization=instructor.organization
        taskStatus.taskactivity=taskactivity
        taskStatus.taskstatustype=taskstatustype
        taskStatus.taskstatustype=taskstatustype
        taskStatus.save(failOnError: true, flush: true)

        def taskStatusList = TaskStatus.findAllByTaskactivityAndIsactive(taskactivity,true)
        //println("taskStatusList =>>"+ taskStatusList)
        if(taskStatusList){
            for(status in taskStatusList){
                //println("status =>>"+ status.name)
                if(status.name == "Done"){
                    TaskStatus taskStatus1 = TaskStatus.findById(status?.id)
                    //println("taskStatus1 =>>"+ taskStatus1)
                    if(taskStatus1){
                        taskStatus1.sequence_no = taskStatusList.size()
                        taskStatus1.updation_ip_address= ip
                        taskStatus1.updation_date=new Date()
                        taskStatus1.updation_username=instructor?.uid
                        taskStatus1.save(flush: true ,failOnError:true )
                    }
                }
            }
        }

        return
    }

    def addNewTaskStatusType(name,ip,uid)
    {
        TaskStatusType taskStatusType=new TaskStatusType()
        taskStatusType.updation_ip_address=ip
        taskStatusType.creation_ip_address=ip
        taskStatusType.creation_date=new Date()
        taskStatusType.updation_date=new Date()
        taskStatusType.updation_username=uid
        taskStatusType.type=name
        taskStatusType.save(failOnError: true, flush: true)
        return  taskStatusType
    }

    def findUniqueMainTaskName(name)
    {
        name=name.trim()
        def test
        def test1
        String nametoret=""
        if(name.contains("-"))
        {
            test=name.split("-")
            for(eachtest in test) {
                if(eachtest.contains(" "))
                {
                    test1=eachtest.split(" ")
                    for(eachtest1 in test1)
                        nametoret += eachtest1.charAt(0)
                }else{
                    nametoret += eachtest.charAt(0)
                }
            }
        }
        else if(name.contains(" "))
        {
            test=name.split(" ")
            for(eachtest in test) {
                nametoret += eachtest.charAt(0)
            }
        }
        else{
            if (name.length() > 4 )
            {
                nametoret = name.substring( 0 , 4 ); }
            else{
                nametoret = name; }
        }
        nametoret= nametoret.toUpperCase()
        return nametoret
    }

    def getTaskNo(request,instructor,hm,params)
    {
        String abbr=instructor?.employeeabbr
        String activityname="General"
        activityname=abbr+"-"+activityname
        TaskActivity taskActivity=TaskActivity.findById(params?.atid)
        def task=Task.findAllByTaskactivityAndOrganizationAndTaskIsNull(taskActivity,instructor?.organization).size()
        if(task==0)
            hm.put("maintasksize",findUniqueMainTaskName(taskActivity?.name)+"-01")
        else {
            def task1=task+1
            if(task1 < 10)
                task1="0"+task1
            hm.put("maintasksize", findUniqueMainTaskName(taskActivity?.name)+"-" +task1)
        }
        if(activityname==taskActivity?.name && task==0) {
            hm.put("maintasksize",abbr + "-GEN-01")
        }
        else if(activityname==taskActivity?.name && task>0) {
            def task1=task+1
            if(task1 < 10)
                task1="0"+task1
            hm.put("maintasksize", abbr + "-GEN-" + task1)
        }
        return
    }

    def saveTask(request,instructor,hm,params)
    {
        println "in saveTask"+params
        TaskActivity taskActivity=TaskActivity.findById(params?.activity)
        if(!taskActivity)
        {
            hm.put("status","400")
            hm.put("message","Task Activity Not Found")
            return
        }
        Instructor instructor1=Instructor.findById(params?.assignto)
        if(!instructor1)
        {
            hm.put("status","400")
            hm.put("message","Assigned To Instructor Not Found")
            return
        }
        def starttime = (params.starttime).trim()
        starttime = starttime.split(":")
        def endtime = (params.endtime).trim()
        endtime = endtime.split(":")
        Date startdate = new SimpleDateFormat("yyyy-MM-dd").parse(params.startdate)
        Date enddate = new SimpleDateFormat("yyyy-MM-dd").parse(params.enddate)
        Calendar c1 = Calendar.getInstance();
        c1.setTime(startdate)
        c1.add(Calendar.HOUR, Integer. parseInt(starttime[0]));
        c1.add(Calendar.MINUTE, Integer. parseInt(starttime[1]));
        startdate = c1.getTime();
        c1.setTime(enddate)
        c1.add(Calendar.HOUR, Integer. parseInt(endtime[0]));
        c1.add(Calendar.MINUTE, Integer. parseInt(endtime[1]));
        enddate = c1.getTime();
        if(params.task && (params?.task.trim()=="" || params?.task==null))
        {
            hm.put("status","400")
            hm.put("message","Please Enter Task")
            return
        }
        if(params.taskdescription && (params?.taskdescription.trim()=="" || params?.taskdescription==null))
        {
            hm.put("status","400")
            hm.put("message","Please Enter Task Description")
            return
        }
        def file = request.getFile("importfile")
        Part filePart = request.getPart("importfile")
        println "file--"+file
        println "filePart--"+filePart
        hm.put("status","200")
        hm.put("message","success")
        hm.put("flashmsg",params.maintask+" Task Successfully added for Activity "+taskActivity?.name)
        def task=saveNewTask(params.task,params.maintask,params.taskdescription,startdate,enddate,instructor1,taskActivity,request.getRemoteAddr(),instructor,null)
        if(task) {
            TaskStatus taskStatus = TaskStatus.findByNameAndOrganizationAndTaskactivity("To-Do", instructor.organization, taskActivity)
            saveNewTaskTransaction(task, instructor, taskStatus, request.getRemoteAddr())
            if (file != null && !file.empty) {
                boolean fileuploadstat = saveUploadAttachment(task, instructor, taskActivity, request.getRemoteAddr(), file, filePart)
                if (!fileuploadstat) {
                    hm.put("status", "400")
                    hm.put("message", "File Upload has been failed,please try agian")
                }
            }
        }else{
            hm.put("status", "400")
            hm.put("message", "Unable to create task,please try again")
        }
        return
    }

    def saveNewTask(taskname,task_no,taskdescription,startdate,enddate,instructor1,taskActivity,ip,instructor,subtask)
    {
        println "in  saveNewTask"
        TaskStatus taskStatus= TaskStatus.findByNameAndOrganizationAndTaskactivity("To-Do",instructor.organization,taskActivity)
        Task task=new Task()
        task.creation_username=instructor?.uid
        task.updation_username=instructor?.uid
        task.creation_date=new Date()
        task.updation_date=new Date()
        task.creation_ip_address=ip
        task.updation_ip_address=ip
        task.description=taskdescription
        task.task_no=task_no
        task.title=taskname
        task.isactive=true
        task.start_date=startdate
        task.expected_end_date=enddate
        task.completion_date=null
        task.organization=instructor.organization
        task.assignedby=instructor
        task.assignedto=instructor1
        task.taskactivity=taskActivity
        task.taskstatus=taskStatus
        if(subtask!=null)
            task.task=subtask
        task.actionby=instructor
        task.save(failOnError: true, flush: true)
        return  task


    }

    def saveNewTaskTransaction(task,instructor,taskStatus,ip)
    {
        TaskTransaction taskTransaction=TaskTransaction.findByIscurrentAndTaskAndOrganization(true,task,instructor.organization)
        TaskTransaction taskTransaction1=new TaskTransaction()
        taskTransaction1.creation_username=instructor?.uid
        taskTransaction1.updation_username=instructor?.uid
        taskTransaction1.creation_date=new Date()
        taskTransaction1.updation_date=new Date()
        taskTransaction1.creation_ip_address=ip
        taskTransaction1.updation_ip_address=ip
        taskTransaction1.action_date=new Date()
        taskTransaction1.iscurrent=true
        taskTransaction1.organization=instructor.organization
        taskTransaction1.task=task
        taskTransaction1.taskstatus=taskStatus
        taskTransaction1.actionby=instructor
        taskTransaction1.prev_task_transaction=taskTransaction
        taskTransaction1.save(failOnError: true, flush: true)
        if(taskTransaction)
        {
            taskTransaction.updation_username=instructor?.uid
            taskTransaction.updation_date=new Date()
            taskTransaction.updation_ip_address=ip
            taskTransaction.iscurrent=false
            taskTransaction.save(failOnError: true, flush: true)
        }

    }

    def saveUploadAttachment(task,instructor,taskActivity,ip,file,filePart)
    {
        boolean  isUploadfile=false
        if(file!=null) {
            if (filePart) {
                def fileExtention
                InputStream fs = filePart.getInputStream()
                String contentType = filePart.getContentType();
                String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                int indexOfDot = filePartName.lastIndexOf('.')
                if (indexOfDot > 0) {
                    fileExtention = filePartName.substring(indexOfDot + 1)
                }
                AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                def folderPath = awsFolderPath.path + "TaskManagement/"+ instructor?.organization.organization_code +"/"+taskActivity?.name+"_"+taskActivity?.id+"/"+task?.task_no+"_"+task?.id+"/"
                folderPath = folderPath.replace(' ', '_');
                def fileName =  getSaltString()+task?.task_no+"_"+task?.id+ "." +fileExtention
                def existsFilePath = ""
                AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                isUploadfile = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                if(isUploadfile)
                {
                    TaskAttachment taskAttachment=new TaskAttachment()
                    taskAttachment.updation_ip_address=ip
                    taskAttachment.creation_ip_address=ip
                    taskAttachment.creation_username=instructor?.uid
                    taskAttachment.updation_username=instructor?.uid
                    taskAttachment.creation_date=new Date()
                    taskAttachment.updation_date=new Date()
                    taskAttachment.filename=fileName
                    taskAttachment.filepath=folderPath
                    taskAttachment.isdeleted=false
                    taskAttachment.upload_date=new Date()
                    taskAttachment.organization=instructor.organization
                    taskAttachment.task=task
                    taskAttachment.attachmentby=instructor
                    taskAttachment.save(failOnError: true, flush: true)
                }
                else{
                    isUploadfile=false
                }
            }
        }
        return  isUploadfile
    }

    def getSaltString()
    {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    def saveNewActivity(request,instructor,hm,params)
    {
        def activityname=""
        activityname=params?.addnewactivity
        activityname=activityname.trim()
        if(!activityname)
        {
            hm.put("status","400")
            hm.put("message","Please Enter Activity Name..")
            return
        }
        TaskActivity taskActivity=TaskActivity.findByNameAndOrganizationAndInstructor(activityname,instructor?.organization,instructor)
        if(taskActivity)
        {
            hm.put("status", "400")
            if(taskActivity?.isactive) {
                hm.put("message", "Activity Already Exist!")
            }
            else if(!taskActivity?.isactive)
            {
                hm.put("message", "Activity Already Exist!,Please Active active this use.")
            }
            else{
                hm.put("message", "Activity Already Exist!")
            }
            return
        }
        def taskactivity=createNewTaskActivity(activityname,request.getRemoteAddr(),instructor)
        if(taskactivity) {
            TaskStatusType taskStatusType = TaskStatusType.findByType("Default")
            if (!taskStatusType) {
                taskStatusType = addNewTaskStatusType("Default", request.getRemoteAddr(), instructor.uid)
            }
            TaskStatusType taskStatusType1 = TaskStatusType.findByType("User Defined")
            if (!taskStatusType1) {
                taskStatusType1 = addNewTaskStatusType("User Defined", request.getRemoteAddr(), instructor.uid)
            }
            if (taskStatusType)
                addNewTaskStatus("To-Do", instructor, 1, request.getRemoteAddr(), taskactivity, taskStatusType)
            if (taskStatusType1)
                addNewTaskStatus("In-Process", instructor, 2, request.getRemoteAddr(), taskactivity, taskStatusType1)
            if (taskStatusType)
                addNewTaskStatus("Done", instructor, 3, request.getRemoteAddr(), taskactivity, taskStatusType)
            hm.put("status","200")
            hm.put("message","success")
            hm.put("flashmsg",activityname+" Activity has been successfully added.")
        }
        else {
            hm.put("status","400")
            hm.put("message","Activity Not Created Please,Try Again.")
        }
        return
    }

    def fetchActivityWorkflowSettings(request,instructor,hm)
    {
        println "In fetchActivityWorkflowSettings TaskService"

        def TaskActivityList = TaskActivity.findAllByInstructorAndOrganizationAndIsactive(instructor,instructor.organization,true)
        //println("TaskActivityList =>>"+ TaskActivityList)
        def taskStatusList = TaskStatus.findAllByTaskactivityAndOrganization(TaskActivityList[0],instructor.organization)
        taskStatusList?.sort{it.sequence_no}
        hm.put("taskStatusList",taskStatusList)
        hm.put("TaskActivityList",TaskActivityList)
        hm.put("status","200")
        hm.put("message","success")
    }

    def fetchActivityStatus(request,instructor,hm,activity)
    {
        println "In fetchActivityStatus TaskService"

        def taskStatusList = TaskStatus.findAllByTaskactivityAndOrganizationAndIsactive(activity,instructor.organization,true)
        taskStatusList?.sort{it.sequence_no}
        //println("taskStatusList =>>"+ taskStatusList)
        def taskList = Task.findAllByTaskactivityAndOrganizationAndIsactive(activity,instructor.organization,true)
        def is_block_delete
        if(taskList.isEmpty())
            is_block_delete = true
        else
            is_block_delete = false

        hm.put("taskStatusList",taskStatusList)
        hm.put("is_block_delete",is_block_delete)
        hm.put("status","200")
        hm.put("message","success")
        return
    }

    def ActivityDashBoard(request,instructor,hm)
    {
        println "in addNewTask service"
//        def taskActivity1=TaskActivity.findAllByIsactiveAndOrganizationAndInstructor(true,instructor?.organization,instructor)
//        if(!taskActivity1)
//        {
//            hm.put("status","400")
//            hm.put("message","Please Add Activity to view Dashboard.")
//            return
//        }
        def taskActivity1=Task.createCriteria().list() {
            projections {
                distinct('taskactivity')
            }
            and {
                eq('isactive', true)
                eq('organization', instructor?.organization)
                or {
                    'in'('assignedby', instructor)
                    'in'('assignedto', instructor)
                }
            }
        }
        ArrayList arrayList=new ArrayList()
        for(eachtask in taskActivity1)
        {
            HashMap temp=new HashMap()
            temp.put("id",eachtask?.id)
            temp.put("name",eachtask?.name)
            arrayList.add(temp)
        }
        hm.put("status","200")
        hm.put("message","success")
        hm.put("activity",arrayList)
        return
    }

    def getDashboardDetails(request,instructor,hm,params)
    {
        println "in service "+params
        TaskActivity taskActivity=TaskActivity.findById(params?.maintask)
        if(!taskActivity)
        {
            hm.put("status","400")
            hm.put("message","Activity Not found,please try again")
            return
        }
        def taskStatus=TaskStatus.findAllByOrganizationAndTaskactivityAndIsactive(instructor?.organization,taskActivity,true)
        taskStatus.sort{ it?.sequence_no }
        ArrayList arrayList=new ArrayList()
        for(eachtaskStatus in taskStatus)
        {
            HashMap taskStatus1=new HashMap()
            taskStatus1?.put("name",eachtaskStatus?.name)
            def task1=Task.findAllByOrganizationAndTaskactivityAndIsactiveAndTaskstatus(instructor?.organization,taskActivity,true,eachtaskStatus)
            ArrayList arrayList1=new ArrayList()
            for(eachtask in task1)
            {
                HashMap task=new HashMap()
                getTaskdetails(task,eachtask)
                arrayList1.add(task)
            }
            taskStatus1?.put("alltask",arrayList1)
            taskStatus1?.put("alltasksize",task1?.size())
            arrayList.add(taskStatus1)
        }
        hm.put("task",arrayList)
        hm.put("instructorid",instructor?.id)
        hm.put("status","200")
        hm.put("message","success")
        return
    }

    def getTaskdetails(taskmap,eachtask)
    {
        taskmap?.put("name",eachtask?.title)
        taskmap.put("type",!eachtask?.task?"Main Task":"Sub Task")
        taskmap.put("taskno",eachtask?.task_no)
        taskmap.put("description",eachtask?.description)
        taskmap.put("status",eachtask?.taskstatus?.name)
        taskmap.put("title",eachtask?.title)
        taskmap.put("assignedto",eachtask?.assignedto?.employeeabbr)
        taskmap.put("fname",eachtask?.assignedto?.person.fullname_as_per_previous_marksheet)
        taskmap.put("assignedbyname",eachtask?.assignedby?.employee_code+" : "+eachtask?.assignedby?.person.fullname_as_per_previous_marksheet)
        taskmap.put("taskactivityName",eachtask?.taskactivity?.name)
        taskmap.put("taskid",eachtask?.id)
        taskmap.put("enctaskid",aesEncrypt(eachtask?.id))
        taskmap.put("taskactivityid",eachtask?.taskactivity?.id)
        taskmap.put("taskdescription",eachtask?.description)
        taskmap.put("taskstatusid",eachtask?.taskstatus?.id)
        taskmap.put("assignedtoid",eachtask?.assignedto) //object
        taskmap.put("taskassignedtoid",eachtask?.assignedto.id) //id
        taskmap.put("assignedbyid",eachtask?.assignedby)//object
        taskmap.put("taskassignedbyid",eachtask?.assignedby.id)//id
        taskmap.put("taskactivity",eachtask?.taskactivity?.id)
        taskmap.put("subtaskid",eachtask?.task?.id)
        taskmap.put("actionbyid",eachtask?.actionby?.id)
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm")
        taskmap.put("start_date",sdf.format(eachtask?.start_date))
        taskmap.put("endtime",simpleDateFormat.format(eachtask?.expected_end_date))
        taskmap.put("expected_end_date",sdf.format(eachtask?.expected_end_date))
        //for the date comparision
        SimpleDateFormat  sdfo = new SimpleDateFormat("yyyy-MM-dd");
        Date d2 = sdfo.parse(sdfo.format(new java.util.Date()));
        Date d3 = sdfo.parse(sdfo.format(eachtask?.expected_end_date));
        taskmap.put("end_date_expired",false)
        if(d3.compareTo(d2)<0)
        {
            taskmap.put("end_date_expired",true)
        }
        //end
        return  taskmap
    }

    def taskTracking(request,instructor,hm,params)
    {
        println "In taskTracking service =>> "+params

        Task task=Task.findById(aesDecrypt(params?.taskid))
        if(!task)
        {
            hm.put("status","400")
            hm.put("message","Task Not found,please try again")
            return
        }
        getTaskdetails(hm,task)
        ArrayList arrayList1=new ArrayList()
        for(eachinst in Instructor.findAllByOrganizationAndIscurrentlyworking(instructor?.organization, true))
        {
            HashMap temp=new HashMap()
            temp.put("instId",eachinst?.id)
            temp.put("name",eachinst?.employee_code +":"+eachinst?.person.fullname_as_per_previous_marksheet)
            arrayList1.add(temp)
        }
        def task1=Task.findAllByOrganizationAndTaskAndIsactive(instructor?.organization,task,true)
        ArrayList arrayList=new ArrayList()
        for(eachtask in task1)
        {
            HashMap taskmap=new HashMap()
            getTaskdetails(taskmap,eachtask)
            arrayList.add(taskmap)
        }
        hm.put("instructor",arrayList1)
        hm.put("subtask",arrayList)
        hm.put("taskstatus",getTaskStatus(task?.taskactivity))
        hm.put("disableflag",true)
        if(instructor?.id==task?.assignedby?.id)
            hm.put("disableflag",false)
        hm.put("disableflagother",false)
        if(instructor?.id!=task?.assignedby?.id && instructor?.id!=task?.assignedto?.id)
            hm.put("disableflagother",true)
        hm.put("instructorid",instructor?.id)
        hm.put("status","200")
        hm.put("message","success")
        return
    }

    def getTaskStatus(activity)
    {
        ArrayList arrayList=new ArrayList()
        def taskStatus1=TaskStatus.findAllByTaskactivity(activity)
        for(eachtaskstatus in taskStatus1)
        {
            HashMap temp=new HashMap()
            temp.put("name",eachtaskstatus?.name)
            temp.put("id",eachtaskstatus?.id)
            arrayList.add(temp)
        }
        return arrayList
    }

    def getAttachMents(task)
    {
        def taskAttachment=TaskAttachment.findAllByTaskAndIsdeleted(task,false)
        ArrayList arrayList=new ArrayList()
        AWSBucketService awsBucketService=new AWSBucketService()
        int i=1;
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd")
        for(eachattachment in taskAttachment)
        {
            HashMap temp=new HashMap()
            def path = eachattachment?.filepath+eachattachment?.filename
            temp.put("file",awsBucketService.getPresignedUrl(awsBucket.bucketname,path, awsBucket.region))
            temp.put("filename","Attachement "+i)
            temp.put("uploaddate",sdf.format(eachattachment?.upload_date))
            temp.put("attachid",eachattachment?.id)
            temp.put("attachmentbyid",eachattachment?.attachmentby?.id)
            temp.put("attachmentby",eachattachment?.attachmentby?.employee_code +":"+eachattachment?.attachmentby?.person.fullname_as_per_previous_marksheet)
            arrayList.add(temp)
        }
        return  arrayList

    }

    def editTask(request,instructor,hm,params)
    {
        println "in service "+params
        Task task=Task.findById(params?.taskid)

        if(!task)
        {
            hm.put("status","400")
            hm.put("message","Task Not found,please try again")
            return
        }
        hm.put("taskid",task?.id)
        hm.put("enctaskid",aesEncrypt(task?.id))
        if(params?.taskdescription)
            task.description=params?.taskdescription
        if(params?.taskname)
            task.title=params?.taskname
        task.updation_username=instructor?.uid
        task.updation_date=new Date()
        task.updation_ip_address=request.getRemoteAddr()
        if(params.assignto && params.assignto!=null && params.assignto!="")
        {
            Instructor instructor2=Instructor.findById(params?.assignto)
            task.assignedto=instructor2
        }

        if(params.taskstatus && params.taskstatus!=null && params.taskstatus!="")
        {
            TaskStatus taskStatus1=TaskStatus.findByIdAndTaskactivity(params.taskstatus,task.taskactivity)
            if(taskStatus1) {
                if(task.taskstatus?.id!=taskStatus1?.id)
                    saveNewTaskTransaction(task, instructor, taskStatus1, request.getRemoteAddr())
                task.taskstatus = taskStatus1
            }
            if(taskStatus1?.name=="Done")
                task.completion_date=new Date()
        }
        if(params.endtime  && params.endtime!=null && params.endtime!="" && params.enddate  && params.enddate!=null && params.enddate!="" ) {
            def endtime = (params.endtime).trim()
            endtime = endtime.split(":")
            Date enddate = new SimpleDateFormat("yyyy-MM-dd").parse(params.enddate)
            Calendar c1 = Calendar.getInstance();
            c1.setTime(enddate)
            c1.add(Calendar.HOUR_OF_DAY, Integer.parseInt(endtime[0]));
            c1.add(Calendar.MINUTE, Integer.parseInt(endtime[1]));
            enddate = c1.getTime();
            task.expected_end_date=enddate
        }
        task.save(failOnError: true, flush: true)
        if(params.taskcomment.trim() && params.taskcomment!=null && params.taskcomment.trim()!="")
        {
            saveComment(task,instructor,request.getRemoteAddr(),params.taskcomment)
        }
        def file = request.getFile("importfile")
        Part filePart = request.getPart("importfile")
        hm.put("status","200")
        hm.put("message","success")
        hm.put("flashmsg","Task Edited Successfully")
        if (file != null && !file.empty) {
            boolean fileuploadstat = saveUploadAttachment(task, instructor, task.taskactivity, request.getRemoteAddr(), file, filePart)
            if (!fileuploadstat) {
                hm.put("status", "400")
                hm.put("message", "File Upload has been failed,please try agian")
            }
        }

        return
    }

    def saveComment(task,instructor,ip,comment)
    {
        TaskComment taskComment =new TaskComment()
        taskComment.updation_ip_address=ip
        taskComment.creation_ip_address=ip
        taskComment.creation_username=instructor?.uid
        taskComment.updation_username=instructor?.uid
        taskComment.creation_date=new Date()
        taskComment.updation_date=new Date()
        taskComment.comment=comment
        taskComment.comment_date=new Date()
        taskComment.organization=instructor.organization
        taskComment.task=task
        taskComment.commentby=instructor
        taskComment.save(failOnError: true, flush: true)
    }

    def addSubTask(request,instructor,hm,params)
    {
        println "in service "+params
        Task task=Task.findById(aesDecrypt(params?.taskid))
        if(!task)
        {
            hm.put("status","400")
            hm.put("message","Task Not found,please try again")
            return
        }
        getTaskdetails(hm,task)
        ArrayList arrayList1=new ArrayList()
        for(eachinst in Instructor.findAllByOrganizationAndIscurrentlyworking(instructor?.organization, true))
        {
            HashMap temp=new HashMap()
            temp.put("instId",eachinst?.id)
            temp.put("name",eachinst?.employee_code +":"+eachinst?.person.fullname_as_per_previous_marksheet)
            arrayList1.add(temp)
        }
        def task1=Task.findAllByOrganizationAndTaskAndIsactive(instructor?.organization,task,true)?.size()
        if(task1==0)
            hm.put("maintasksize",task?.task_no+"-01")
        else {
            def task2=task1+1
            if(task2 < 10)
                task2="0"+task2
            hm.put("maintasksize", task?.task_no+"-" + task2)
        }
        hm.put("instructor",arrayList1)
        hm.put("activityname",task?.taskactivity?.name)
        hm.put("taskstatus",getTaskStatus(task?.taskactivity))
        hm.put("disableflagother",false)
        if(instructor?.id!=task?.assignedby?.id )
            hm.put("disableflagother",true)
        hm.put("status","200")
        hm.put("message","success")
        return
    }

    def saveSubTask(request,instructor,hm,params)
    {
        println "in saveTask"+params
        Task task=Task.findById(params?.taskid)
        if(!task)
        {
            hm.put("status","400")
            hm.put("message","Task Not found,please try again")
            return
        }
        TaskActivity taskActivity=task?.taskactivity
        if(!taskActivity)
        {
            hm.put("status","400")
            hm.put("message","Task Activity Not found,please try again")
            return
        }
        Instructor instructor1=Instructor.findById(params?.assignto)
        if(!instructor1)
        {
            hm.put("status","400")
            hm.put("message","Assigned To Instructor Not Found")
            return
        }
        if(params.task && (params?.task.trim()=="" || params?.task==null))
        {
            hm.put("status","400")
            hm.put("message","Please Enter Task")
            return
        }
        if(params.taskdescription && (params?.taskdescription.trim()=="" || params?.taskdescription==null))
        {
            hm.put("status","400")
            hm.put("message","Please Enter Task Description")
            return
        }
        def starttime = (params.starttime).trim()
        starttime = starttime.split(":")
        def endtime = (params.endtime).trim()
        endtime = endtime.split(":")
        Date startdate = new SimpleDateFormat("yyyy-MM-dd").parse(params.startdate)
        Date enddate = new SimpleDateFormat("yyyy-MM-dd").parse(params.enddate)
        Calendar c1 = Calendar.getInstance();
        c1.setTime(startdate)
        c1.add(Calendar.HOUR, Integer. parseInt(starttime[0]));
        c1.add(Calendar.MINUTE, Integer. parseInt(starttime[1]));
        startdate = c1.getTime();
        c1.setTime(enddate)
        c1.add(Calendar.HOUR, Integer. parseInt(endtime[0]));
        c1.add(Calendar.MINUTE, Integer. parseInt(endtime[1]));
        enddate = c1.getTime();
        def file = request.getFile("importfile")
        Part filePart = request.getPart("importfile")
        hm.put("status","200")
        hm.put("message","success")
        hm.put("taskid",task?.id)
        hm.put("enctaskid",aesEncrypt(task?.id))
        hm.put("flashmsg",params.maintask+" Sub Task Successfully added for Task "+task?.title)
        def subtask=saveNewTask(params.task,params.maintask,params.taskdescription,startdate,enddate,instructor1,taskActivity,request.getRemoteAddr(),instructor,task)
        if(subtask) {
            TaskStatus taskStatus = TaskStatus.findByNameAndOrganizationAndTaskactivity("To-Do", instructor.organization, taskActivity)
            saveNewTaskTransaction(subtask, instructor, taskStatus, request.getRemoteAddr())
            if (file != null && !file.empty) {
                boolean fileuploadstat = saveUploadAttachment(subtask, instructor, taskActivity, request.getRemoteAddr(), file, filePart)
                if (!fileuploadstat) {
                    hm.put("status", "400")
                    hm.put("message", "File Upload has been failed,please try agian")
                }
            }
        }else{
            hm.put("status", "400")
            hm.put("message", "Unable to create task,please try again")
        }
        return
    }

    def viewAttachment(request,instructor,hm,params)
    {
        Task task=Task.findById(aesDecrypt(params?.taskid))
        if(!task)
        {
            hm.put("status","400")
            hm.put("message","Task Not found,please try again")
            return
        }
        getTaskdetails(hm,task)
        hm.put("taskattachments", getAttachMents(task))
        hm.put("activityname",task?.taskactivity?.name)
        hm.put("instructorid",instructor?.id)
        hm.put("disableflagother",false)
        if(instructor?.id!=task?.assignedby?.id && instructor?.id!=task?.assignedto?.id)
            hm.put("disableflagother",true)
        hm.put("status","200")
        hm.put("message","success")
        return
    }

    def editAttchment(request,instructor,hm,params)
    {
        TaskAttachment taskAttachment=TaskAttachment.findById(params?.attachmentid)
        if(!taskAttachment)
        {
            hm.put("status","400")
            hm.put("message","Attchment Not Found,please try again")
            return
        }
        def file = request.getFile("editimportfile")
        Part filePart = request.getPart("editimportfile")
        boolean isUploadfile = false
        hm.put("status","200")
        hm.put("message","success")
        hm.put("taskid",taskAttachment?.task?.id)
        hm.put("enctaskid",aesEncrypt(taskAttachment?.task?.id))
        hm.put("flashmsg","File Edited SuccessFully")
        if (file != null && !file.empty) {
            if (file != null) {
                if (filePart) {
                    def fileExtention
                    InputStream fs = filePart.getInputStream()
                    String contentType = filePart.getContentType();
                    String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                    int indexOfDot = filePartName.lastIndexOf('.')
                    if (indexOfDot > 0) {
                        fileExtention = filePartName.substring(indexOfDot + 1)
                    }
                    AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
                    AWSBucket awsBucket = AWSBucket.findByContent("documents")
                    def folderPath = taskAttachment?.filepath
                    folderPath = folderPath.replace(' ', '_');
                    def fileName = taskAttachment?.filename
                    def existsFilePath =folderPath+fileName
                    AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                    isUploadfile = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                    if (isUploadfile) {
                        taskAttachment.updation_ip_address = request.getRemoteAddr()
                        taskAttachment.updation_username = instructor?.uid
                        taskAttachment.updation_date = new Date()
                        taskAttachment.filename = fileName
                        taskAttachment.filepath = folderPath
                        taskAttachment.upload_date = new Date()
                        taskAttachment.attachmentby = instructor
                        taskAttachment.save(failOnError: true, flush: true)
                    } else {
                        isUploadfile = false
                    }
                }
            }
        }
        if (!isUploadfile) {
            hm.put("status", "400")
            hm.put("message", "File Upload has been failed,please try agian")
        }
        return
    }

    def saveNewAttachment(request,instructor,hm,params)
    {
        println "in service "+params
        Task task=Task.findById(params?.taskid)
        if(!task)
        {
            hm.put("status","400")
            hm.put("message","Task Not found,please try again")
            return
        }
        TaskActivity taskActivity=task?.taskactivity
        if(!taskActivity)
        {
            hm.put("status","400")
            hm.put("message","Task Activity Not found,please try again")
            return
        }
        def file = request.getFile("newimportfile")
        Part filePart = request.getPart("newimportfile")
        hm.put("status","200")
        hm.put("message","success")
        hm.put("taskid",task?.id)
        hm.put("taskid",task?.id)
        hm.put("enctaskid",aesEncrypt(task?.id))
        hm.put("flashmsg","Attachment Added Successfully for "+task?.title)
        if(task) {
            if (file != null && !file.empty) {
                boolean fileuploadstat = saveUploadAttachment(task, instructor, taskActivity, request.getRemoteAddr(), file, filePart)
                if (!fileuploadstat) {
                    hm.put("status", "400")
                    hm.put("message", "File Upload has been failed,please try agian")
                }
            }
        }else{
            hm.put("status", "400")
            hm.put("message", "Unable to create task,please try again")
        }
        return
    }

    def aesEncrypt(data)
    {
        AesCryptUtil aesUtil=new AesCryptUtil("CODETEST11");
        String encRequest = aesUtil.encrypt(data.toString());
        return encRequest
    }

    def aesDecrypt(data)
    {
        String decResp=""
        try {
            AesCryptUtil aesUtil = new AesCryptUtil("CODETEST11");
            decResp = aesUtil.decrypt(data.toString());
            return decResp
        }
        catch (Exception e)
        {
            return  decResp
        }
        finally {
            return  decResp
        }
    }

    def viewComments(request,instructor,hm,params)
    {
        Task task=Task.findById(aesDecrypt(params?.taskid))
        if(!task)
        {
            hm.put("status","400")
            hm.put("message","Task Not found,please try again")
            return
        }
        getTaskdetails(hm,task)
        hm.put("comments", getComments(task))
        hm.put("instructorid", instructor?.id)
        hm.put("activityname",task?.taskactivity?.name)
        hm.put("disableflagother",false)
        if(instructor?.id!=task?.assignedby?.id && instructor?.id!=task?.assignedto?.id)
            hm.put("disableflagother",true)
        hm.put("status","200")
        hm.put("message","success")
        return
    }

    def getComments(task)
    {
        def taskComment=TaskComment.findAllByTask(task)
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy")
        ArrayList arrayList=new ArrayList()
        taskComment.sort{it.comment_date}
        for(eachcomment in taskComment)
        {
            HashMap temp =new HashMap()
            temp.put("comment",eachcomment?.comment)
            temp.put("commentid",eachcomment?.id)
            temp.put("commentdate",sdf.format(eachcomment?.comment_date))
            temp.put("commentbyid",eachcomment?.commentby?.id)
            temp.put("commentby",eachcomment?.commentby?.employee_code +":"+eachcomment?.commentby?.person.fullname_as_per_previous_marksheet)
            arrayList.add(temp)
        }
        return arrayList
    }

    def editComment(request,instructor,hm,params)
    {
        println "in editcomment"+params
        TaskComment taskComment=TaskComment.findById(params?.cmtid)
        if(!taskComment)
        {
            hm.put("status","400")
            hm.put("message","Comment Not Found,please try again")
            return
        }
        taskComment.comment=params?.taskcomment
        taskComment.updation_ip_address= request.getRemoteAddr()
        taskComment.updation_date=new Date()
        taskComment.save(flush: true ,failOnError:true )
        hm.put("status","200")
        hm.put("message","success")
        hm.put("flashmsg","Comment edited successfully!!")
        hm.put("taskid",taskComment?.task?.id)
        hm.put("enctaskid",aesEncrypt(taskComment?.task?.id))
        return
    }

    def saveNewComment(request,instructor,hm,params)
    {
        println "in service "+params
        Task task=Task.findById(params?.taskid)
        if(!task)
        {
            hm.put("status","400")
            hm.put("message","Task Not found,please try again")
            return
        }
        TaskActivity taskActivity=task?.taskactivity
        if(!taskActivity)
        {
            hm.put("status","400")
            hm.put("message","Task Activity Not found,please try again")
            return
        }
        hm.put("status","200")
        hm.put("message","success")
        hm.put("taskid",task?.id)
        hm.put("enctaskid",aesEncrypt(task?.id))
        hm.put("flashmsg","Comment Added Successfully for "+task?.title)
        if(task) {
            if(params.newtaskcomment.trim() && params.newtaskcomment!=null && params.newtaskcomment.trim()!="")
            {
                saveComment(task,instructor,request.getRemoteAddr(),params.newtaskcomment)
            }
        }
        else{
            hm.put("status", "400")
            hm.put("message", "Unable to create task,please try again")
        }
        return
    }

    def deleteAttachment(request,instructor,hm,params)
    {
        TaskAttachment taskAttachment=TaskAttachment.findById(params?.attachid)
        if(!taskAttachment)
        {
            hm.put("status","400")
            hm.put("message","Attchment Not Found,please try again")
            return
        }
        hm.put("status","200")
        hm.put("message","success")
        hm.put("taskid",taskAttachment?.task?.id)
        hm.put("enctaskid",aesEncrypt(taskAttachment?.task?.id))
        hm.put("flashmsg","File Deleted SuccessFully")
        taskAttachment.updation_ip_address = request.getRemoteAddr()
        taskAttachment.updation_username = instructor?.uid
        taskAttachment.isdeleted = true
        taskAttachment.updation_date = new Date()
        taskAttachment.save(failOnError: true, flush: true)
        return
    }

    def deleteComment(request,instructor,hm,params)
    {
        TaskComment taskComment=TaskComment.findById(params?.attachid)
        if(!taskComment)
        {
            hm.put("status","400")
            hm.put("message","Comment Not Found,please try again")
            return
        }
        hm.put("status","200")
        hm.put("message","success")
        hm.put("taskid",taskComment?.task?.id)
        hm.put("enctaskid",aesEncrypt(taskComment?.task?.id))
        hm.put("flashmsg","Comment Deleted SuccessFully")
        taskComment.delete(failOnError: true, flush: true)

        return
    }

    def fetchMyTask(instructor,hm,date)
    {
        println "In fetchMyTask TaskService"

        def assignByMeList = ["Assigned To Me","Assigned By Me"]
        def taskList = Task.createCriteria().list() {
            'in'('organization', instructor?.organization)
            and {
                'in'('organization', instructor?.organization)
                'in'('assignedto', instructor)
                eq("isactive",true)
            }
        }
        //println("taskList =>>"+ taskList)
        ArrayList arrayList=new ArrayList()
        for(eachTask in taskList)
        {
            SimpleDateFormat  sdfo = new SimpleDateFormat("yyyy-MM-dd");
            String start_date = sdfo.format(eachTask?.start_date)
            if(!eachTask?.task && start_date==date){
                HashMap task=new HashMap()
                getTaskdetails(task,eachTask)
                def subTaskList = Task.findAllByOrganizationAndIsactiveAndTask(instructor?.organization,true,eachTask)
                task.put("subTaskSize",subTaskList.size())
                arrayList.add(task)
            }
        }

        hm.put("taskList",arrayList)
        hm.put("assignByMeList",assignByMeList)
        hm.put("status","200")
        hm.put("message","success")
    }

    def updateMyTask(instructor,hm,params)
    {
        println "In updateMyTask TaskService"

        def taskList
        if(params.assignBy=="Assigned To Me"){
            taskList = Task.createCriteria().list() {
                'in'('organization', instructor?.organization)
                and {
                    'in'('organization', instructor?.organization)
                    'in'('assignedto', instructor)
                    eq("isactive",true)
                }
            }
            //println("taskList =>>"+ taskList)
        }
        else {
            taskList = Task.createCriteria().list() {
                'in'('organization', instructor?.organization)
                and {
                    'in'('organization', instructor?.organization)
                    'in'('assignedby', instructor)
                    eq("isactive",true)
                }
            }
            //println("taskList =>>"+ taskList)
        }
        ArrayList arrayList=new ArrayList()
        for(eachTask in taskList)
        {
            SimpleDateFormat  sdfo = new SimpleDateFormat("yyyy-MM-dd");
            String start_date = sdfo.format(eachTask?.start_date)
            if(params.fromdate <= start_date && start_date <= params.todate){
                HashMap task=new HashMap()
                getTaskdetails(task,eachTask)
                def subTaskList = Task.findAllByOrganizationAndIsactiveAndTask(instructor?.organization,true,eachTask)
                task.put("subTaskSize",subTaskList.size())
                arrayList.add(task)
            }
        }

        hm.put("taskList",arrayList.reverse())
        hm.put("AssignBy",params.assignBy)
        hm.put("status","200")
        hm.put("message","success")
    }

    def userTaskBoard(login,instructor,hm)
    {
        println "In userTaskBoard TaskService"

        def instructorFinalList = []
        def instructorList = Instructor.findAllByOrganizationAndIscurrentlyworking(instructor?.organization, true)
        for (Instructor i : instructorList) {
            instructorFinalList.add(i)
        }

        def role = login.roles
        def isRoleAssign
        for(r in role){
            if(r?.role=="Authority Task Board"){
                println("Role =>>"+ r?.role)
                isRoleAssign = r?.role
            }
        }

        //println("isRoleAssign =>>"+ isRoleAssign)

        hm.put("isRoleAssign", isRoleAssign)
        hm.put("instructorFinalList", instructorFinalList)
        hm.put("status", "200")
        hm.put("message", "success")
    }

    def getUserBoardDetails(request,instructor,hm,params)
    {
        println "In getUserBoardDetails TaskService =>> "+params

        TaskActivity taskActivity = TaskActivity.findById(params.activity)
        if(!taskActivity)
        {
            hm.put("status","400")
            hm.put("message","Activity Not found,please try again")
            return
        }
        def taskStatus=TaskStatus.findAllByOrganizationAndTaskactivityAndIsactive(instructor?.organization,taskActivity,true)
        taskStatus.sort{ it?.sequence_no }
        ArrayList arrayList=new ArrayList()
        for(eachtaskStatus in taskStatus)
        {
            //println("eachtaskStatus =>>"+ eachtaskStatus)
            HashMap taskStatus1=new HashMap()
            taskStatus1?.put("name",eachtaskStatus?.name)
            def task1=Task.findAllByOrganizationAndTaskactivityAndIsactiveAndTaskstatus(instructor?.organization,taskActivity,true,eachtaskStatus)
            ArrayList arrayList1=new ArrayList()
            for(eachtask in task1)
            {
                HashMap task=new HashMap()
                getTaskdetails(task,eachtask)
                arrayList1.add(task)
            }
            taskStatus1?.put("alltask",arrayList1)
            taskStatus1?.put("alltasksize",task1?.size())
            arrayList.add(taskStatus1)
        }
        //println("arrayList =>>"+ arrayList)
        hm.put("task",arrayList)
        hm.put("instructorid",instructor?.id)
        hm.put("status","200")
        hm.put("message","success")
        return
    }

    def getUserActivity(request,instructor,hm,params)
    {
        println "In getUserActivity TaskService =>> "+params

        Instructor faculty = Instructor.findById(params.user)
        def taskActivityList = TaskActivity.createCriteria().list() {
            'in'('instructor', faculty)
            and {
                'in'('organization', instructor?.organization)
                eq("isactive",true)
            }
        }

        hm.put("taskActivityList",taskActivityList)
        hm.put("status","200")
        hm.put("message","success")
        return
    }

    def saveEditedActivityStatus(request,instructor,hm,params)
    {
        println "In saveEditedActivityStatus =>>"+params

        TaskStatus taskStatus = TaskStatus.findById(params?.itemId)
        if(!taskStatus)
        {
            hm.put("status","400")
            hm.put("message","Task Status Not Found,please try again")
            return
        }
        taskStatus.name=params.status
        taskStatus.updation_ip_address= request.getRemoteAddr()
        taskStatus.updation_date=new Date()
        taskStatus.updation_username=instructor?.uid
        taskStatus.save(flush: true ,failOnError:true )

        hm.put("status","200")
        hm.put("message","success")
        hm.put("flashmsg","Task status edited successfully!!")
        return
    }

    def subTaskDashboard(request,instructor,hm,params)
    {
        println "In subTaskDashboard TaskService"

        Task task=Task.findById(aesDecrypt(params?.taskid))
        if(task){
            def taskList = Task.createCriteria().list() {
                'in'('organization', instructor?.organization)
                and {
                    'in'('task', task)
                    eq("isactive",true)
                }
            }
            //println("taskList =>>"+ taskList)
            ArrayList arrayList=new ArrayList()
            for(eachTask in taskList)
            {
                if(eachTask?.task){
                    HashMap taskhm=new HashMap()
                    getTaskdetails(taskhm,eachTask)
                    def subTaskList = Task.findAllByOrganizationAndIsactiveAndTask(instructor?.organization,true,eachTask)
                    taskhm.put("subTaskSize",subTaskList.size())
                    arrayList.add(taskhm)
                }
            }

            hm.put("taskList",arrayList)
            hm.put("task",task)
            hm.put("status","200")
            hm.put("message","success")
        }
        else {
            hm.put("status","404")
            hm.put("message","Task Not Found")
        }
    }

    def getTaskListAssignTome(request,instructor,hm)
    {
        try {
            def alltask = Task.createCriteria().list() {
                eq('isactive', true)
                and {
                    eq('organization', instructor?.organization)
                    eq('assignedto', instructor)
                    ge('expected_end_date', new Date())
                    le('start_date', new Date())
                }
            }
            alltask?.sort { it?.taskstatus?.sequence_no }
            ArrayList arrayList = new ArrayList()
            for (eachtask in alltask) {
                if(eachtask?.taskstatus?.name!="Done") {
                    HashMap taskmap = new HashMap()
                    taskmap?.put("name", eachtask?.title)
                    taskmap.put("type", !eachtask?.task ? "Main Task" : "Sub Task")
                    taskmap.put("assignby", eachtask?.assignedby?.employee_code + " : " + eachtask?.assignedby?.person.fullname_as_per_previous_marksheet)
                    taskmap.put("enctaskid", aesEncrypt(eachtask?.id))
                    taskmap.put("status", eachtask?.taskstatus?.name)
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY")
                    taskmap.put("expected_end_date", sdf.format(eachtask?.expected_end_date))
                    arrayList.add(taskmap)
                }
            }
            hm.put("status","200")
            hm.put("message","success")
            hm.put("task",arrayList)
        }
        catch (Exception e)
        {
            hm.put("status","404")
            hm.put("message","INTERNAL SERVER ERROR..! Please try again..")
        }
        return

    }

}
