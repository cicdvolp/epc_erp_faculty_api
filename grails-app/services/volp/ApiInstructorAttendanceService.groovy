package volp

import grails.converters.JSON
import grails.gorm.transactions.Transactional
import groovy.json.JsonSlurper
import org.springframework.http.HttpStatus

import java.text.SimpleDateFormat

@Transactional
class ApiInstructorAttendanceService {

    InformationService informationService
    EmployeeAttendanceService employeeAttendanceService
    AWSBucketService awsBucketService

    def getalllocationinformationbyorg(def orgid) {
        HashMap hm = new HashMap()

        Organization organization = Organization.findById(orgid)
        if(organization) {
            def mobilebiometriclocationcordinator_list = MobileBiometricLoactionCoordinate.findAllByOrganization(organization)

            if (mobilebiometriclocationcordinator_list.size() > 0) {
                def list = []
                for (cordinator in mobilebiometriclocationcordinator_list) {
                    def mobileBiometricRegistration = MobileBiometricRegistration.findAllByMobilebiometriclocationmasterAndIsdeleted(cordinator.mobilebiometriclocationmaster, false)
                    HashMap map = new HashMap()
                    if (mobileBiometricRegistration.size() == 0) {
                        map.put('isdelete', true)
                    } else {
                        map.put('isdelete', false)
                    }
                    map.put('registrationcount', mobileBiometricRegistration?.size())
                    map.put('latitude', cordinator?.latitude)
                    map.put('longitude', cordinator?.longitude)
                    HashMap orgmap = new HashMap()
                    orgmap.put('organization_name', cordinator?.organization?.organization_name)
                    orgmap.put('id', cordinator?.organization?.id)
                    map.put('organization', orgmap)
                    HashMap mastermap = new HashMap()
                    mastermap.put('location', cordinator?.mobilebiometriclocationmaster?.location)
                    mastermap.put('id', cordinator?.mobilebiometriclocationmaster?.id)
                    map.put('locationmaster', mastermap)
                    list.add(map)
                }
                hm.put("msg", "success")
                hm.put('location_coordinate_list', list)
                return hm
            } else {
                hm.put("msg", "registered locations not found")
                return hm
            }
        } else {
            hm.put("msg", "Organization not found")
            return hm
        }
    }

    def getalllocationmaster(def orgid) {
        HashMap hm = new HashMap()

        Organization organization = Organization.findById(orgid)
        if (organization) {
            def mobilebiometriclocationmasterlist = MobileBiometricLocationMaster.findAllByOrganization(organization)
            if (mobilebiometriclocationmasterlist.size() > 0) {
                def list = []
                for (locationmaster in mobilebiometriclocationmasterlist) {
                    MobileBiometricLocationMaster mobileBiometricLocationMaster=MobileBiometricLocationMaster.findById(locationmaster.id)
                    def mobileBiometricRegistration=MobileBiometricRegistration.findAllByMobilebiometriclocationmasterAndIsdeleted(mobileBiometricLocationMaster, false)
                    HashMap map = new HashMap()
                    if(mobileBiometricRegistration.size() == 0)
                    {
                        map.put('isdelete', true)
                    }
                    else {
                        map.put('isdelete', false)
                    }
                    map.put('registrationcount',mobileBiometricRegistration?.size())
                    map.put('id', locationmaster.id)
                    map.put('location', locationmaster.location)
                    HashMap orgmap = new HashMap()
                    orgmap.put('organization_name', locationmaster.organization.organization_name)
                    orgmap.put('id', locationmaster.organization.id)
                    map.put('organization', orgmap)
                    list.add(map)
                }
                hm.put("msg", "success")
                hm.put('location_coordinate_master_list', list)
                return hm
            } else {
                hm.put("msg", "No Location Master Found Can You Add it first")
                return hm
            }
        } else {
            hm.put("msg", "Organization not found")
            return hm
        }
    }

    def deletelocationmaster(def locationmasterid, Organization org) {
        HashMap hm = new HashMap()
        if(locationmasterid) {
            MobileBiometricLocationMaster mobileBiometricLocationMaster = MobileBiometricLocationMaster.findById(locationmasterid)
            def mobileBiometricRegistration = MobileBiometricRegistration.findAllByMobilebiometriclocationmaster(mobileBiometricLocationMaster)
            if (mobileBiometricRegistration.size() > 0) {
                def mobileBiometricRegistrationlistwithdeletedreg = MobileBiometricRegistration.findAllByMobilebiometriclocationmasterAndIsdeleted(mobileBiometricLocationMaster, true)
                if (mobileBiometricRegistrationlistwithdeletedreg.size() == mobileBiometricRegistration.size()) {
                    for (mob in mobileBiometricRegistrationlistwithdeletedreg)
                        mob.delete(flush: true, failOnError: true)
                } else {
                    hm.put("msg", "Employee Registration data present for this Location... You cannot delete it.");
                    return hm
                }
            }
            if (mobileBiometricLocationMaster != null) {
                MobileBiometricLoactionCoordinate mobileBiometricLoactionCoordinate1 = MobileBiometricLoactionCoordinate.findByMobilebiometriclocationmasterAndOrganization(mobileBiometricLocationMaster, org)
                mobileBiometricLoactionCoordinate1.delete(flush: true, failOnError: true)

                mobileBiometricLocationMaster.delete(flush: true, failOnError: true)
                hm.put("msg", "success");
                return hm
            } else {
                hm.put("msg", "mobile location Biometric master not found...");
                return hm
            }
        } else {
            hm.put("msg", "biometric location Master id is not valid")
            return hm
        }
    }

    def getalllocationinformation(def locationmasterid, def organization) {
        HashMap hm = new HashMap()
        if (locationmasterid) {
            MobileBiometricLocationMaster biometricLocationMaster = MobileBiometricLocationMaster.findById(locationmasterid)
            if (biometricLocationMaster) {
                def biometricLoactionCoordinatelist = MobileBiometricLoactionCoordinate.findAllByMobilebiometriclocationmasterAndOrganization(biometricLocationMaster, organization)
                if (biometricLoactionCoordinatelist.size() > 0) {
                    def list = []
                    for (coordinate in biometricLoactionCoordinatelist) {
                        println("coordinate :: " + coordinate)
                        def mobileBiometricRegistration=MobileBiometricRegistration.findAllByMobilebiometriclocationmasterAndIsdeleted(coordinate?.mobilebiometriclocationmaster, false)
                        println("mobileBiometricRegistration :: " + mobileBiometricRegistration)
                        HashMap map = new HashMap()
                        if(mobileBiometricRegistration.size() == 0) {
                            map.put('isdelete', true)
                        } else {
                            map.put('isdelete', false)
                        }
                        map.put('registrationcount',mobileBiometricRegistration?.size())
                        map.put('id', coordinate?.id)
                        map.put('latitude', coordinate?.latitude)
                        map.put('longitude', coordinate?.longitude)
                        HashMap orgmap = new HashMap()
                        orgmap.put('organization_name', coordinate?.organization?.organization_name)
                        orgmap.put('id', coordinate?.organization?.id)
                        map.put('organization', orgmap)
                        HashMap mastermap = new HashMap()
                        mastermap.put('location', coordinate?.mobilebiometriclocationmaster?.location)
                        mastermap.put('id', coordinate?.mobilebiometriclocationmaster?.id)
                        map.put('locationmaster', mastermap)
                        list.add(map)
                        println("list :: " + list)
                    }
                    hm.put('location_coordinate_list', list)
                    hm.put('msg', "success")
                    return hm
                } else {
                    hm.put('msg', "Location Cordinates not register for selected location master..")
                    return hm
                }
            } else {
                hm.put('msg', "Location Master Not Found..")
                return hm
            }
        } else {
            hm.put('msg', "Location Master Not Found..")
            return hm
        }
    }

    def registerlocationmaster(def masterid, def org, def location_name, def latitude, def longitude, def username, def ipaddr) {
        HashMap hm = new HashMap()

        Instructor instructor = Instructor.findByUsername(username)
        if(masterid) {
            MobileBiometricLocationMaster mobileBiometricLocationMaster = MobileBiometricLocationMaster.findById(masterid)
            mobileBiometricLocationMaster.location = location_name

            mobileBiometricLocationMaster.updation_username = instructor.uid
            mobileBiometricLocationMaster.updation_date = new Date()
            mobileBiometricLocationMaster.updation_ip_address = ipaddr
            mobileBiometricLocationMaster.save(flush: true, failOnError: true)

            MobileBiometricLoactionCoordinate mobileBiometricLoactionCoordinate1 = MobileBiometricLoactionCoordinate.findByMobilebiometriclocationmasterAndOrganization(mobileBiometricLocationMaster,org)
            mobileBiometricLoactionCoordinate1.latitude = latitude.toString()
            mobileBiometricLoactionCoordinate1.longitude = longitude.toString()

            mobileBiometricLoactionCoordinate1.creation_username = instructor.uid
            mobileBiometricLoactionCoordinate1.updation_username = instructor.uid
            mobileBiometricLoactionCoordinate1.creation_date = new Date()
            mobileBiometricLoactionCoordinate1.updation_date = new Date()
            mobileBiometricLoactionCoordinate1.creation_ip_address = ipaddr
            mobileBiometricLoactionCoordinate1.updation_ip_address = ipaddr

            mobileBiometricLoactionCoordinate1.organization = org
            mobileBiometricLoactionCoordinate1.mobilebiometriclocationmaster = mobileBiometricLocationMaster
            mobileBiometricLoactionCoordinate1.save(flush: true, failOnError: true)

            hm.put("msg", "Successfully updated");
            return hm
//            hm.put("masterid", mobileBiometricLocationMaster.id);
//            hm.put("status", "200");
//            render hm as JSON
//            return
        } else {
            MobileBiometricLocationMaster mobileBiometricLocationMaster = MobileBiometricLocationMaster.findByLocationAndOrganization(location_name, org)
            if(mobileBiometricLocationMaster != null) {
                hm.put("msg", "location name already present");
                return hm
            } else {
                mobileBiometricLocationMaster = new MobileBiometricLocationMaster()
                mobileBiometricLocationMaster.location = location_name
                mobileBiometricLocationMaster.creation_username = instructor.uid
                mobileBiometricLocationMaster.updation_username = instructor.uid
                mobileBiometricLocationMaster.creation_date = new Date()
                mobileBiometricLocationMaster.updation_date = new Date()
                mobileBiometricLocationMaster.creation_ip_address = ipaddr
                mobileBiometricLocationMaster.updation_ip_address = ipaddr
                mobileBiometricLocationMaster.organization = org
                mobileBiometricLocationMaster.save(flush: true, failOnError: true)

                MobileBiometricLoactionCoordinate mobileBiometricLoactionCoordinate1 = new MobileBiometricLoactionCoordinate()
                mobileBiometricLoactionCoordinate1.latitude = latitude.toString()
                mobileBiometricLoactionCoordinate1.longitude = longitude.toString()

                mobileBiometricLoactionCoordinate1.creation_username = instructor.uid
                mobileBiometricLoactionCoordinate1.updation_username = instructor.uid
                mobileBiometricLoactionCoordinate1.creation_date = new Date()
                mobileBiometricLoactionCoordinate1.updation_date = new Date()
                mobileBiometricLoactionCoordinate1.creation_ip_address = ipaddr
                mobileBiometricLoactionCoordinate1.updation_ip_address = ipaddr

                mobileBiometricLoactionCoordinate1.organization = org
                mobileBiometricLoactionCoordinate1.mobilebiometriclocationmaster = mobileBiometricLocationMaster
                mobileBiometricLoactionCoordinate1.save(flush: true, failOnError: true)
                hm.put("msg", "Successfully added");
                return hm
//                hm.put("masterid", mobileBiometricLocationMaster.id);
//                hm.put("status", "200");
//                render hm as JSON
//                return
            }
        }
    }

    def getRegEmployeeList(def locationmasterid) {
        HashMap hm = new HashMap()
        def mobilebiometriclocationmaster = MobileBiometricLocationMaster.findById(locationmasterid)

        if(mobilebiometriclocationmaster) {
            def instructor = MobileBiometricRegistration.createCriteria().list(){
                projections{
                    distinct('instructor')
                }
                'in'('mobilebiometriclocationmaster', mobilebiometriclocationmaster)
                eq('isdeleted', false)
            }
            instructor.sort{it?.employee_code}

            def employeelist = []
            def index = 1
            for(inst in instructor){
                employeelist.add(index++ +") " + inst?.employee_code + " : " + inst?.person?.firstName + " " + inst?.person?.lastName)
            }
            hm.put("employeelist", employeelist)
            hm.put("msg", "success")
            return hm
        } else {
            hm.put("msg", "No Location Master Found ...")
            return hm
        }
    }

    def getemployeeinfo(def username, def org) {
        HashMap hm = new HashMap()
        Instructor instructor = null
        instructor = Instructor.findByUidAndOrganization(username, org)
        if (instructor == null) {
            instructor = Instructor.findByEmployee_codeAndOrganization(username, org)
        }
        if (instructor) {
            Contact contact = Contact.findByPerson(instructor.person)

            def biometricRegistrationlist = MobileBiometricRegistration.findAllByOrganizationAndInstructorAndIsdeleted(instructor.organization, instructor, false)
            println("biometricRegistrationlist :: " + biometricRegistrationlist)
            if (biometricRegistrationlist.size() > 0) {
                biometricRegistrationlist.forEach() {
                    obj ->
                        def locationcoordinate = MobileBiometricLoactionCoordinate.findAllByOrganizationAndMobilebiometriclocationmaster(org, obj?.mobilebiometriclocationmaster)
                        println("locationcoordinate :: " + locationcoordinate)
                }
//
//                HashMap hm1 = new HashMap()
//                hm1.put("mobile_no", biometricRegistration.mobile_no)
//                hm1.put("imei_no", biometricRegistration.mobile_imei_no)
//                hm1.put("geo_location", biometricRegistration?.mobilebiometriclocationmaster?.location)
//                hm1.put("geo_location_lat", locationcoordinate?.latitude)
//                hm1.put("geo_location_lng", locationcoordinate?.longitude)
//                hm1.put("biometric_master", biometricRegistration?.mobilebiometriclocationmaster?.id)
//                hm.put("biometric_details", hm1)
            } else {
//                HashMap hm1 = new HashMap()
//                hm1.put("mobile_no", "")
//                hm1.put("imei_no", "")
//                hm1.put("geo_location", "")
//                hm1.put("geo_location_lat", "")
//                hm1.put("geo_location_lng", "")
//                hm1.put("biometric_master", "")
//                hm.put("biometric_details", hm1)
            }








            ///////////////////////////////////////////////////
            MobileBiometricRegistration biometricRegistration = MobileBiometricRegistration.findByOrganizationAndInstructorAndIsdeleted(instructor.organization, instructor, false)
            if (biometricRegistration) {
                def locationcoordinate = MobileBiometricLoactionCoordinate.findByOrganizationAndMobilebiometriclocationmaster(org, biometricRegistration?.mobilebiometriclocationmaster)
                HashMap hm1 = new HashMap()
                hm1.put("mobile_no", biometricRegistration.mobile_no)
                hm1.put("imei_no", biometricRegistration.mobile_imei_no)
                hm1.put("geo_location", biometricRegistration?.mobilebiometriclocationmaster?.location)
                hm1.put("geo_location_lat", locationcoordinate?.latitude)
                hm1.put("geo_location_lng", locationcoordinate?.longitude)
                hm1.put("biometric_master", biometricRegistration?.mobilebiometriclocationmaster?.id)
                hm.put("biometric_details", hm1)
            } else {
                HashMap hm1 = new HashMap()
                hm1.put("mobile_no", "")
                hm1.put("imei_no", "")
                hm1.put("geo_location", "")
                hm1.put("geo_location_lat", "")
                hm1.put("geo_location_lng", "")
                hm1.put("biometric_master", "")
                hm.put("biometric_details", hm1)
            }

//            def biometricRegistrationlist = MobileBiometricRegistration.findAllByOrganizationAndInstructorAndIsdeleted(instructor.organization, instructor, false)
            def list = []
//            for(loc in biometricRegistrationlist) {
//                list.add(loc?.mobilebiometriclocationmaster?.id)
//            }

            hm.put("biometricRegistrationlist", list)
            hm.put("full_name", instructor.person?.firstName + " " + instructor.person?.lastName )
//            hm.put("full_name", instructor.person?.fullname_as_per_previous_marksheet)
            hm.put("employee_code", instructor.employee_code)
            hm.put("orgaization_name", instructor.organization?.organization_name)
            hm.put("department", instructor.department?.name==null?"":instructor.department?.name)
            hm.put("employeetype", instructor.employeetype?.display_name)
            hm.put("designation", instructor.designation?.name)
            hm.put("shift", instructor.currentshift?.type)
            hm.put("email", instructor.uid)
            if (instructor.mobile_no != "" && instructor.mobile_no != null) {
                hm.put("mobile", instructor.mobile_no)
            } else if (contact.mobile_no != "" && contact.mobile_no != null) {
                hm.put("mobile", instructor.mobile_no)
            } else {
                hm.put("mobile", "-")
            }

            Facultyidcard facultyidcard = Facultyidcard.findByInstructorAndOrganization(instructor, instructor.organization)
            if (facultyidcard) {
                AWSBucketService awsBucketService = new AWSBucketService()
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSFolderPath awsFolderPath = AWSFolderPath.findByPath('cloud/')
                String path = awsFolderPath.path + facultyidcard.icardPath + facultyidcard.icardPhotoFile
                String url = awsBucketService.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
                if (url) {
                    hm.put("profile_photo", url)
                } else {
                    hm.put("profile_photo", "-")
                }
            } else {
                hm.put("profile_photo", "-")
            }
            hm.put("msg", "success")
        } else {
            hm.put("msg", "Instructor information not found...")
        }
        return hm
    }

    def savemobiledeviceinfo(def username, def imei, def mobile, def org, def mobilebiometriclocationid, def ipaddr) {
        println("imei :: "+ imei)
        println("mobilebiometriclocationid :: "+ mobilebiometriclocationid)
        HashMap hm = new HashMap()
        Instructor inst = Instructor.findByUidAndOrganization(username, org)
        if (inst) {
            def locationmasterarray = []
            if(mobilebiometriclocationid.getClass().isArray()) {
                locationmasterarray = mobilebiometriclocationid
            } else {
                locationmasterarray.add(mobilebiometriclocationid)
            }
            def imeino = imei
            if (imeino) {
                def locationstring = ""
                def i = 0
                def mobilelocationarray = []
                for(id in locationmasterarray) {
                    def mobilebiometriclocationmaster = MobileBiometricLocationMaster.findById( id )
                    if (mobilebiometriclocationmaster) {
                        mobilelocationarray.add(mobilebiometriclocationmaster)
                        if(i == 0) {
                            locationstring = locationstring + mobilebiometriclocationmaster?.location
                        } else {
                            locationstring = locationstring + ", " + mobilebiometriclocationmaster?.location
                        }

                        def mobileno = null
                        if (mobile) {
                            mobileno = mobile
                        }

                        MobileBiometricRegistration biometricRegistration = MobileBiometricRegistration.findByOrganizationAndInstructorAndMobilebiometriclocationmasterAndMobile_imei_no( org, inst, mobilebiometriclocationmaster, imei )
                        if (biometricRegistration) {
                            biometricRegistration.mobile_no = mobileno
                            biometricRegistration.mobile_imei_no = imeino
                            biometricRegistration.isdeleted = false
                            biometricRegistration.mobilebiometriclocationmaster = mobilebiometriclocationmaster
                            biometricRegistration.updation_username = username
                            biometricRegistration.updation_date = new Date()
                            biometricRegistration.updation_ip_address = ipaddr
                            biometricRegistration.save( failOnError: true, flush: true )

                            hm.put("msg", inst?.employee_code + ":" + inst?.person?.firstName + " " + inst?.person?.lastName + "'s Face Registered Successfully at " + locationstring)
//                            status_code = HttpStatus.OK
//                            message = inst?.employee_code + ":" + inst?.person?.firstName + " " + inst?.person?.lastName + "'s Face Registered Successfully at " + locationstring
                        } else {
                            biometricRegistration = new MobileBiometricRegistration()
                            biometricRegistration.mobile_no = mobileno
                            biometricRegistration.mobile_imei_no = imeino
                            biometricRegistration.isdeleted = false
                            biometricRegistration.mobilebiometriclocationmaster = mobilebiometriclocationmaster
                            biometricRegistration.creation_username = username
                            biometricRegistration.updation_username = username
                            biometricRegistration.creation_date = new Date()
                            biometricRegistration.updation_date = new Date()
                            biometricRegistration.creation_ip_address = ipaddr
                            biometricRegistration.updation_ip_address = ipaddr
                            biometricRegistration.organization = org
                            biometricRegistration.instructor = inst
                            biometricRegistration.save( failOnError: true, flush: true )

                            hm.put("msg", inst?.employee_code + ":" + inst?.person?.firstName + " " + inst?.person?.lastName + "'s Face Registered Successfully at " + locationstring)
//
//                            status_code = HttpStatus.OK
//                            message = inst?.employee_code + ":" + inst?.person?.firstName + " " + inst?.person?.lastName + "'s Face Registered Successfully at " + locationstring
                        }
                    } else {
                        hm.put("msg", "Location Not Found")
//                        status_code = HttpStatus.INTERNAL_SERVER_ERROR
//                        message = "Location Not Found"
                    }
                    i++
                }

                def biometricRegistrationremovallist = []
                if(mobilelocationarray) {
                    biometricRegistrationremovallist = MobileBiometricRegistration.createCriteria().list() {
                        'in'( 'organization', org )
                        and {
                            'in'( 'instructor', inst )
                            'in'( 'mobile_imei_no', imei )
                            not {
                                'in'( 'mobilebiometriclocationmaster', mobilelocationarray)
                            }
                        }
                    }
                }

                for(mob in biometricRegistrationremovallist){
                    mob.isdeleted = true
                    mob.updation_username = username
                    mob.updation_date = new Date()
                    mob.updation_ip_address = ipaddr
                    mob.save( failOnError: true, flush: true )
                }

            } else {
                hm.put("msg", "Primary Mobile no not found...")
//                status_code = HttpStatus.INTERNAL_SERVER_ERROR
//                message = "IMEI no not found..."
            }
        } else {
            hm.put("msg", "Instructor not Found")
//            status_code = HttpStatus.NOT_FOUND
//            message = "Instructor not Found"
        }
        return hm
    }

    def getRegisteredMobileLocation(def mobileno, def organization) {
        println("mobileno :: " + mobileno)
        HashMap hm = new HashMap()
        def mobilereglist = MobileBiometricRegistration.findAllByOrganizationAndMobile_imei_noAndIsdeleted(organization, mobileno, false)

        if(mobilereglist) {
            def isGeolocation = ERPRoleTypeSettings.findByNameIlikeAndOrganization( 'Compair Geolocation While Mobile App Biometic Attendance', organization )?.value
            if (isGeolocation == 'true') {
                ArrayList locationArray = new ArrayList()
                for (mobilereg in mobilereglist) {
                    def locationcoordinate = MobileBiometricLoactionCoordinate.findAllByOrganizationAndMobilebiometriclocationmaster( organization, mobilereg?.mobilebiometriclocationmaster )
                    for (loc in locationcoordinate) {
                        HashMap temp = new HashMap()
                        temp.put("latitude", loc?.latitude)
                        temp.put("longitude", loc?.longitude)
                        temp.put("name", loc?.mobilebiometriclocationmaster?.location)
                        locationArray.add(temp)
                    }
                }
                hm.put("locationArray", locationArray)
                hm.put("compairGeolocation", true)
            }else{
                hm.put("compairGeolocation", false)
            }
            hm.put("msg", "success")
        } else {
            hm.put("msg", "User Not Registered On This Device")
        }
        return hm
    }

    def markMobileAppAttendance(def username, def organization, def imeino, def latitude, def longitude, def ipaddr) {
        HashMap hm = new HashMap()
        Instructor inst = Instructor.findByUidAndOrganization(username, organization)
        if(inst==null)
            inst = Instructor.findByIdAndOrganization(instid, organization)
        if (inst) {
            def mobilereglist = MobileBiometricRegistration.findAllByInstructorAndMobile_imei_noAndIsdeleted(inst, imeino, false)
            RoleType roleType=RoleType.findByOrganizationAndType(inst?.organization,"Biometric")
            ERPRoleTypeSettings erpRoleTypeSettings =ERPRoleTypeSettings.findByRoletypeAndOrganizationAndName(roleType,inst?.organization,"geolocation_attedance_app_compare_distance")
            def compare_distance=20
            if(erpRoleTypeSettings) {
                compare_distance = Integer.parseInt(erpRoleTypeSettings?.value)
            }
            compare_distance=10000
            if(mobilereglist) {
                for (mobilereg in mobilereglist) {
                    def actuallatitude = latitude
                    def actuallongitude = longitude

                    def isGeolocation = ERPRoleTypeSettings.findByNameIlikeAndOrganization( 'Compair Geolocation While Mobile App Biometic Attendance', inst?.organization )?.value

                    def geolocationmatch = false
                    def matchlocation
                    if (isGeolocation == 'true') {
                        if (actuallatitude != "UNKNOWN" && actuallongitude != "UNKNOWN") {
                            actuallatitude = Double.parseDouble( latitude )
                            actuallongitude = Double.parseDouble( longitude )

                            def locationcoordinate = MobileBiometricLoactionCoordinate.findAllByOrganizationAndMobilebiometriclocationmaster( organization, mobilereg?.mobilebiometriclocationmaster )

                            for (loc in locationcoordinate) {
                                geolocationmatch = distance( Double.parseDouble(loc?.latitude.toString()), Double.parseDouble(loc?.longitude.toString()), actuallatitude, actuallongitude , compare_distance)
                                if (geolocationmatch) {
                                    matchlocation = loc
                                    break
                                }
                            }
                        } else
                            geolocationmatch = false
                    } else
                        geolocationmatch = true

                    if (geolocationmatch) {
//                        try {
                            Date today = new Date()
                            def erpday = ERPDay.findByDaysequence( today.getDay() + 1 )
                            def erpmonth = ERPMonth.findByMonthnumber( today.getMonth() + 1 )
                            def year = today.getYear() + 1900
                            def erpyear = ERPCalendarYear.findByYear( "" + year )
                            def attendance = EmployeeAttendance.findByInstructorAndOrganizationAndErpcalenderintegerdayAndErpmonthAndErpcalenderyear( inst, organization, today.getDate(), erpmonth, erpyear )

                            if (attendance) {
                                def time = employeeAttendanceService.getTime( today )
                                if (!attendance?.intime) {
                                    attendance.intime = time
                                } else {
                                    attendance.outtime = time
                                }
                                attendance.isattendancebyestablishment = false
                                attendance.username = "Mobile Biometric - " + username
                                attendance.updation_date = new java.util.Date()
                                attendance.updation_ip_address = ipaddr
                                attendance.save( failOnError: true, flush: true )

                                hm.put("msg", inst?.employee_code + ":"+inst?.person?.firstName+" "+inst?.person?.lastName+"'s Attendance Recorded Successfully at Time : "+time+" and Location : "+matchlocation?.mobilebiometriclocationmaster?.location)
//                                message = inst?.employee_code + ":"+inst?.person?.firstName+" "+inst?.person?.lastName+"'s Attendance Recorded Successfully at Time : "+time+" and Location : "+matchlocation?.mobilebiometriclocationmaster?.location
                                break;
                            } else {
                                def aay = informationService.currentAySem( "ERP", "Leave Management", organization )

                                EmployeeAttendance a = new EmployeeAttendance()
                                a.intime = employeeAttendanceService.getTime( today )
                                a.outtime = ""
                                a.todaysdate = today
                                a.hasappliedforleavefortoday = false
                                a.isattendancebyestablishment = false
                                a.isexpectedinoutapproved = true
                                a.expectedintime = ""
                                a.expectedouttime = ""
                                a.expectednoofhours = 0
                                a.erpcalenderintegerday = today.getDate()
                                a.instructor = inst
                                a.academicyear = aay?.academicyear
                                a.erpmonth = erpmonth
                                a.erpday = erpday
                                a.erpcalenderyear = erpyear
                                a.department = inst.department
                                a.organization = organization
                                a.leavetype = null
                                a.employeeleavetransaction = null
                                a.username = "Mobile Biometric - " + username
                                a.creation_date = new java.util.Date()
                                a.updation_date = new java.util.Date()
                                a.creation_ip_address = ipaddr
                                a.updation_ip_address = ipaddr
                                a.save( failOnError: true, flush: true )

                                hm.put("msg", inst?.employee_code + ":"+inst?.person?.firstName+" "+inst?.person?.lastName+"'s Attendance Recorded Successfully at Time : "+a?.intime+" and Location : "+matchlocation?.mobilebiometriclocationmaster?.location)
//                                message = inst?.employee_code + ":"+inst?.person?.firstName+" "+inst?.person?.lastName+"'s Attendance Recorded Successfully at Time : "+a?.intime+" and Location : "+matchlocation?.mobilebiometriclocationmaster?.location

                                break;
                            }
//                        } catch (Exception e) {
//                            hm.put("msg", "Internal Server Error..")
////                            status_code = HttpStatus.NOT_FOUND
////                            message = "Internal Server Error.."
//                        }
                    } else {
                        hm.put("msg", "User Location Does not Match with Registered Department Location, Please make sure your GPS locations is on..")
//                        status_code = HttpStatus.NOT_FOUND
//                        message = "User Location Does not Match with Registered Department Location, Please make sure your GPS locations is on.."
                    }
                }
            } else {
                hm.put("msg", "User Not Registered On This Device")
//                status_code = HttpStatus.NOT_FOUND
//                message = "User Not Registered On This Device"
            }
        } else {
            hm.put("msg", "User Not Found")
//            status_code = HttpStatus.NOT_FOUND
//            message = "User Not Found"
        }

        def erpmonth1 = ERPMonth.findByMonthnumber(new Date().getMonth() + 1)
        def year1 = new Date().getYear() + 1900
        def erpyear1 = ERPCalendarYear.findByYear("" + year1)
        Calendar calendar = Calendar.getInstance()
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        def day= ERPDay.findByDaysequence(dayOfWeek)

//        println("day :: " + day.id)
//        println("erpmonth1 :: " + erpmonth1)
//        println("erpyear1 :: " + erpyear1)
//        println("inst.organization :: " + inst.organization)
//        println("inst :: " + inst)

        EmployeeAttendance employeeAttendance=EmployeeAttendance.findByErpdayAndErpmonthAndErpcalenderyearAndOrganizationAndInstructor(day,erpmonth1,erpyear1,inst.organization,inst)
        def intime="";
        def outtime="";
        if(employeeAttendance) {
            if(employeeAttendance.intime!=null && employeeAttendance.intime!="")
                intime=employeeAttendance?.intime
            if(employeeAttendance.outtime!=null && employeeAttendance.outtime!="")
                outtime=employeeAttendance?.outtime
        }
        hm.put("intime",intime)
        hm.put("outtime",outtime)
        hm.put("totaltime", getTotalTime(intime, outtime))

        Facultyidcard facultyidcard = Facultyidcard.findByInstructorAndOrganization(inst, inst.organization)
        if (facultyidcard) {
            AWSBucketService awsBucketService = new AWSBucketService()
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSFolderPath awsFolderPath = AWSFolderPath.findByPath('cloud/')
            String path = awsFolderPath.path + facultyidcard.icardPath + facultyidcard.icardPhotoFile
            String url = awsBucketService.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
            if (url) {
                hm.put("profile_photo", url)
            } else {
                hm.put("profile_photo", "")
            }
        } else {
            hm.put("profile_photo", "")
        }
        hm.put("name", inst.person.firstName + " " + inst.person.lastName)
        return hm
    }

    def distance(lat1, lon1, lat2, lon2,compare_distance) {
//        println("Dept lat1  :: "+lat1)
//        println("Dept lon1  :: "+lon1)
//        println("User lat2  :: "+lat2)
//        println("User lon2  :: "+lon2)

        if ((lat1 == lat2) && (lon1 == lon2)) {
            return true;
        } else {
//            def R = 6371 ; // Radius of earth in KM
//            def dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
//            def dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
//            def a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
//                    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
//                    Math.sin(dLon / 2) * Math.sin(dLon / 2);
//            def c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//            def d = R * c;
//            d = d * 100
//            println("d :: " + d)
            lon1 = Math.toRadians(lon1);
            lon2 = Math.toRadians(lon2);
            lat1 = Math.toRadians(lat1);
            lat2 = Math.toRadians(lat2);
            double dlon = lon2 - lon1;
            double dlat = lat2 - lat1;
            double a = Math.pow(Math.sin(dlat / 2), 2)+ Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2),2);

            double c = 2 * Math.asin(Math.sqrt(a));
            double r = 6371;

            def d=(c * r)*1000
            println("disaptance " +d)
            if (d <= compare_distance)
                return true
            else
                return false
        }
    }

    def editregisterlocationmastername(def masterid, def org, def location_name, def username, def ipaddr) {
        HashMap hm = new HashMap()

        MobileBiometricLocationMaster mobileBiometricLocationMaster = MobileBiometricLocationMaster.findByLocationAndOrganization(location_name, org)
        if(mobileBiometricLocationMaster != null) {
            hm.put("msg", "location name already present, We can not update duplicate location master name");
            return hm
        }

        Instructor instructor = Instructor.findByUsername(username)
        if(masterid) {
            mobileBiometricLocationMaster = MobileBiometricLocationMaster.findById(masterid)
            mobileBiometricLocationMaster.location = location_name

            mobileBiometricLocationMaster.updation_username = instructor.uid
            mobileBiometricLocationMaster.updation_date = new Date()
            mobileBiometricLocationMaster.updation_ip_address = ipaddr
            mobileBiometricLocationMaster.save(flush: true, failOnError: true)

            hm.put("msg", "Successfully updated location master name");
            return hm
        }
    }

    def getTotalTime(String intime, String outtime) {
        println("outtime :: " + outtime)
        if(!outtime.equals("")) {
            def intimesplit = intime.split(":")
            println("intimesplit :: " + intimesplit)
            def outtimesplit = outtime.split(":")
            println("outtimesplit :: " + outtimesplit)

            def totalhrtime = Integer.parseInt(outtimesplit[0]) - Integer.parseInt(intimesplit[0])
            def totalmintime = Integer.parseInt(outtimesplit[1]) - Integer.parseInt(intimesplit[1])
            def totaltime = totalhrtime.toString() + ":" + Math.abs(totalmintime).toString();
            println("totaltime :: " + totaltime)
            return totaltime
        } else {
            return ""
        }
    }
}
