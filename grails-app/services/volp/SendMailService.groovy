package volp

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sns.AmazonSNSClientBuilder
import com.amazonaws.services.sns.model.MessageAttributeValue
import com.amazonaws.services.sns.model.PublishRequest
import com.amazonaws.services.sns.model.PublishResult
import grails.gorm.transactions.Transactional
import grails.gorm.services.Service


import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.Multipart;
import javax.mail.BodyPart;
@Transactional
class SendMailService
{
    // boolean transactional = false
    def serviceMethod() {
    }
    // def SendMailService sendMailService
    // sendMailService.sendmail("volp.vu@gmail.com","volp@108","deepak.pawar@vit.edu","This is final tesing","hi hardworking guy","", "")
    def sendmail(String frommail,String frommailpassword,String sendto,String subject,String bodymessage,String attachmentfile, def cc_email)
    {
        //d:/trust_office_ip.png
        final String username = frommail
        final String password = frommailpassword;
        //println("username >>"+ username)
        //println("password >>"+ password)

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(frommail));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(sendto));

            if(cc_email !="") {
                message.setRecipients(Message.RecipientType.CC,
                        InternetAddress.parse(cc_email))
            }

            message.setSubject(subject);
            if(attachmentfile.equals("")) {
                message.setText(bodymessage);
            }
            else {
                // Create the message part
                BodyPart messageBodyPart = new MimeBodyPart();
                // Now set the actual message
                messageBodyPart.setText(bodymessage);
                // Create a multipar message
                Multipart multipart = new MimeMultipart();
                // Set text message part
                multipart.addBodyPart(messageBodyPart);

                // Part two is attachment
                messageBodyPart = new MimeBodyPart();
                String filename = attachmentfile
                DataSource source = new FileDataSource(filename);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(filename);
                multipart.addBodyPart(messageBodyPart);
                // Send the complete message parts
                message.setContent(multipart);
            }
            Transport.send(message);
            System.out.println("Mail Sent Successfully.....");
            return 1
        } catch (MessagingException e) {
            // throw new RuntimeException(e);
            System.out.println("Error::Mail NOT Sent....");
            return 0
        }
    }


    def sendmailwithcss(String frommail,String frommailpassword,String sendto,String subject,String bodymessage,String attachmentfile, def cc_email)
    {
        //d:/trust_office_ip.png
        final String username = frommail
        final String password = frommailpassword;

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(frommail));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(sendto));

            if(cc_email) {
                message.setRecipients(Message.RecipientType.CC,
                        InternetAddress.parse(cc_email))
            }

            message.setSubject(subject);
            if(attachmentfile.equals(""))
            {
                message.setContent(bodymessage, "text/html");
            }
            else
            {
                // Create the message part
                BodyPart messageBodyPart = new MimeBodyPart();

                // Now set the actual message
                messageBodyPart.setContent(bodymessage, "text/html");

                // Create a multipar message
                Multipart multipart = new MimeMultipart();

                // Set text message part
                multipart.addBodyPart(messageBodyPart);

                // Part two is attachment
                messageBodyPart = new MimeBodyPart();
                String filename = attachmentfile
                DataSource source = new FileDataSource(filename);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(filename);
                multipart.addBodyPart(messageBodyPart);
                // Send the complete message parts
                message.setContent(multipart);
            }
            Transport.send(message);
            System.out.println("Mail Sent Successfully.....");

        } catch (MessagingException e) {
            // throw new RuntimeException(e);
            System.out.println("Error::Mail NOT Sent...."+e);
            return false
        }
        return true
    }

    def sendSMS_withAWS(msg, phonenos){
        AWSCredential cred = AWSCredential.findByNameAndIsactive("SNS", true)
        if(cred==null)
            return 0
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(cred.accesskey, cred.secretkey);
        AmazonSNS snsClient = AmazonSNSClientBuilder.standard()
                .withRegion(Regions.fromName("ap-south-1"))
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).build();

        Map<String, MessageAttributeValue> smsAttributes =
                new HashMap<String, MessageAttributeValue>();
        smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                .withStringValue("mySenderID")
                .withDataType("String"));
        smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue()
                .withStringValue("Transactional")
                .withDataType("String"));

        for(no in phonenos) {
            PublishResult result
            try {
                result = snsClient.publish(new PublishRequest()
                        .withMessage(msg)
                        .withPhoneNumber(no)
                        .withMessageAttributes(smsAttributes));
            }catch(Exception e)
            {
                println e
            }finally {
                System.out.println(result);
            }
        }
        return 1
    }

    def sesmail_withAWS(String from, String to, String body, String subject){
        println "in sesmail msg:"+body

        final String FROM = from
        final String TO = to
        AWSCredential awsc=AWSCredential.findByNameAndIsactive("SES", true)
        final String BODY = body
        final String SUBJECT = subject

        final String SMTP_USERNAME = awsc.accesskey
        final String SMTP_PASSWORD = awsc.secretkey

        final String HOST = awsc.host

        final int PORT = awsc.port

        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", PORT);

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.starttls.required", "true");

        Session session = Session.getDefaultInstance(props);

        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(FROM));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
        msg.setSubject(SUBJECT);
        msg.setContent(BODY,"text/html");
        Transport transport = session.getTransport();
        try
        {
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
            transport.sendMessage(msg, msg.getAllRecipients());
            println("Email sent!");
        }catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }finally
        {
            transport.close();
        }
    }

}
