package volp

import grails.gorm.transactions.Transactional

@Transactional
class ApiMobileAppLinkService {

    AWSBucketService awsBucketService = new AWSBucketService()
    InformationService informationService = new InformationService()

    def getMobileLinks(String username, Organization org) {
        Instructor inst = Instructor.findByUsername(username)
        ApplicationType applicationType = ApplicationType.findByApplication_type("ERP")
        UserType userType = UserType.findByTypeAndOrganizationAndApplication_type("Employee",org,applicationType)
        def mobileAppLink = MobileAppLink.findAllByOrganizationAndUsertypeAndIsactive(org, userType, true)
        def mobileAppLinkList = []
        if(mobileAppLink.size() > 0) {
            mobileAppLink.forEach({ obj ->
                HashMap link = new HashMap()
                link.put("id", obj.id)
                link.put("displayname", obj.displayname)
                link.put("linkname", obj.linkname)
                link.put("isactive", obj.isactive)
                link.put("sort_order", obj.sort_order)
                link.put("usertype", obj.usertype.type)
                link.put("icon_name", obj.mobileAppLinkMaster.icon_path_name)
                link.put("mobileAppLinkMasterName", obj.mobileAppLinkMaster.name)
                link.put("mobileAppLinkMasterId", obj.mobileAppLinkMaster.id)
                mobileAppLinkList.add(link)
            })
        }
        return mobileAppLinkList
    }

    def getAyList() {
        HashMap ayMap = new HashMap()
        AcademicYear.list().sort{it.ay}.reverse().forEach() {
            obj ->
                ayMap.put(obj.id.toString(), obj.ay)
        }
        return ayMap
    }

    def getSemList(Organization org) {
        HashMap semMap = new HashMap()
        Semester.findAllByOrganization(org).sort{it?.sequence}.forEach() {
            obj ->
                semMap.put(obj.id.toString(), obj.display_name)
        }
        return semMap
    }

    def getCurrentApplicationAcademicYear(String roleTypeName, Organization org) {
        RoleType roleType = RoleType.findByTypeAndOrganization(roleTypeName, org)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndOrganizationAndIsActive(roleType, org, true)
        HashMap hm = new HashMap()
        HashMap cayMap = new HashMap()
        cayMap.put(aay?.academicyear?.id, aay?.academicyear?.ay)
        HashMap csemMap = new HashMap()
        csemMap.put(aay?.semester?.id, aay?.semester?.display_name)

        hm.put("ay", cayMap)
        hm.put("sem", csemMap)
        return hm
    }

    def getBiometricLinks(def username, def org) {
        HashMap hm = new HashMap()
        Login login = Login.findByUsernameAndIsloginblocked(username, false)
        Instructor instructor = Instructor.findByUid(username)
        println("instructoir :: " + instructor.username)
        Facultyidcard facultyidcard = Facultyidcard.findByInstructorAndOrganization(instructor, instructor.organization)
        if (facultyidcard) {
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSFolderPath awsFolderPath = AWSFolderPath.findByPath('cloud/')
            String path = awsFolderPath.path + facultyidcard.icardPath + facultyidcard.icardPhotoFile
            String url = awsBucketService.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
            if (url) {
                hm.put("profile_photo", url)
            } else {
                hm.put("profile_photo", "")
            }
        } else {
            hm.put("profile_photo", "")
        }
        def isregistration = informationService.isregistration(login, instructor)
        def isregistrationlocation = informationService.isregistrationlocation(login, instructor)
        def isattadance = informationService.isattadance(login, instructor)

        if (isregistration) {
            hm.put("isregistration", true);
        } else {
            hm.put("isregistration", false);
        }

        if (isregistrationlocation) {
            hm.put("isregistrationlocation", true);
        } else {
            hm.put("isregistrationlocation", false);
        }

        if (isattadance) {
            hm.put("isattadance", true);
        } else {
            hm.put("isattadance", false);
        }

        def erpmonth = ERPMonth.findByMonthnumber(new Date().getMonth() + 1)
        def year = new Date().getYear() + 1900
        def erpyear = ERPCalendarYear.findByYear("" + year)
        Calendar calendar = Calendar.getInstance()
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        def day= ERPDay.findByDaysequence(dayOfWeek)

        println("day :: " + day)
        println("erpmonth :: " + erpmonth)
        println("erpyear :: " + erpyear)
        println("instructor.organization :: " + instructor.organization)
        println("instructor :: " + instructor)

        EmployeeAttendance employeeAttendance=EmployeeAttendance.findByErpdayAndErpmonthAndErpcalenderyearAndOrganizationAndInstructor(day,erpmonth,erpyear,instructor.organization,instructor)
        println("employeeAttendance :: " + employeeAttendance)
        def intime="";
        def outtime="";
        if(employeeAttendance) {
            if(employeeAttendance.intime!=null && employeeAttendance.intime!="") {
                intime = employeeAttendance?.intime
            }
            if(employeeAttendance.outtime!=null && employeeAttendance.outtime!="") {
                outtime = employeeAttendance?.outtime
            }
        }

        hm.put("intime", intime);
        hm.put("outtime", outtime);
        ApiInstructorAttendanceService apiInstructorAttendanceService = new ApiInstructorAttendanceService()
        hm.put("totaltime", apiInstructorAttendanceService.getTotalTime(intime, outtime))

        RoleType roleType=RoleType.findByOrganizationAndType(instructor?.organization,"Biometric")
        ERPRoleTypeSettings erpRoleTypeSettings =ERPRoleTypeSettings.findByRoletypeAndOrganizationAndName(roleType,instructor?.organization,"geolocation_attedance_app_compare_distance")
        ERPRoleTypeSettings erpRoleTypeSettings1 =ERPRoleTypeSettings.findByRoletypeAndOrganizationAndName(roleType,instructor?.organization,"no_of_geoloaction_fetch")
        def compare_distance = 20
        def no_of_geolocation_fetch = 3.0
        if(erpRoleTypeSettings) {
            compare_distance = Integer.parseInt(erpRoleTypeSettings?.value)
        }
        if(erpRoleTypeSettings1) {
            no_of_geolocation_fetch = Double.parseDouble(erpRoleTypeSettings1?.value)
        }
        hm.put("compare_distance", compare_distance);
        hm.put("no_of_geolocation_fetch", no_of_geolocation_fetch);

        return hm
    }

}
