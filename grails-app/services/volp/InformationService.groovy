package volp

import grails.gorm.transactions.Transactional
import org.apache.poi.xwpf.extractor.XWPFWordExtractor
import org.apache.poi.xwpf.usermodel.XWPFDocument

//

//import org.apache.poi.hwpf.HWPFDocument;
//import org.apache.poi.hwpf.extractor.WordExtractor;
//aws

import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.text.SimpleDateFormat

//AWS SNS

//sms txt local

@Transactional
class InformationService {
    AWSBucketService awsService =  new AWSBucketService()
    SendMailService SendMailService


    def calculateresult(ERPMCQExamSecretCode secretCode,String ip)
    {
        println secretCode
        def allQue = ERPMCQQuestionAllocationtoApplicant.findAllByOrganizationAndRecapplicantAndRecdeptgroup(secretCode.organization,secretCode.recapplicant,secretCode.recdeptgroup)
        int correct = 0
        double totalmarks = 0
        TreeSet crses = new TreeSet()
        for(que in allQue){
            crses.add(que.reccourse.id)
            if(que.studentselectedoption!=null) {
                if (que.studentselectedoption.iscorrecetoption == true)
                    correct++
            }
        }
        ArrayList result = new ArrayList()
        for(crswise in crses){
            RecCourse coff = RecCourse.findById(crswise)
            ArrayList crsres = new ArrayList()
            crsres.add(coff)
            def tempque = ERPMCQQuestionAllocationtoApplicant.findAllByOrganizationAndRecapplicantAndReccourseAndRecdeptgroup(secretCode.organization,secretCode.recapplicant,coff,secretCode.recdeptgroup)
            double mark=0.0
            double outoff=0.0
            for(temp in tempque){
                outoff+=temp.erpmcqquestionbank.weightage
                if(temp.studentselectedoption!=null) {

                    if (temp.studentselectedoption.iscorrecetoption == true) {
                        println temp.erpmcqquestionbank.weightage
                        mark += temp.erpmcqquestionbank.weightage
                        totalmarks +=temp.erpmcqquestionbank.weightage
                    }
                }
            }
            //    ////println("mark:"+mark)
            crsres.add(mark)
            crsres.add(outoff)
            result.add(crsres)
        }
        println "result"
        secretCode.obtained_score = totalmarks
        secretCode.examgivendate = new Date()
        secretCode.end_time = new Date()
        secretCode.isexamgiven = true
        secretCode.save(flush: true,failOnError: true)
        def ename = ERPMCQExamName.findByNameAndOrganization("VIT-FACULTY-RECRUITMENT-ONLINE-TEST",secretCode.organization)
        RecEvaluationParameter recevaluationparameter = RecEvaluationParameter.findByParameterAndOranization('Written Test',secretCode.organization)
        RecExpertType recexperttype = RecExpertType.findByOranizationAndType(secretCode.organization,'COMMON')
        RecApplicationEvaluation recapplicationevaluation = RecApplicationEvaluation.findByOranizationAndRecversionAndRecapplicantAndRecapplicationAndRecevaluationparameterAndRecexperttype(secretCode.organization,secretCode.recversion,secretCode.recapplicant,secretCode.recapplication,recevaluationparameter,recexperttype)
        if(recapplicationevaluation==null)
        {
            recapplicationevaluation = new RecApplicationEvaluation()
            double m = (recevaluationparameter.maxmarks * secretCode.obtained_score)/ename.max_score
            recapplicationevaluation.obtained_marks = m
            recapplicationevaluation.evaluation_date =new java.util.Date()
            recapplicationevaluation.oranization = secretCode.organization
            recapplicationevaluation.recversion = secretCode.recversion
            recapplicationevaluation.recapplication = secretCode.recapplication
            recapplicationevaluation.recapplicant = secretCode.recapplicant
            recapplicationevaluation.recevaluationparameter = recevaluationparameter
            recapplicationevaluation.recexperttype = recexperttype
            recapplicationevaluation.recdeptexpertgroup = null
            recapplicationevaluation.recexpert = null
            recapplicationevaluation.username = secretCode.recapplicant.email
            recapplicationevaluation.creation_date=new java.util.Date()
            recapplicationevaluation.updation_date=new java.util.Date()
            recapplicationevaluation.creation_ip_address=ip
            recapplicationevaluation.updation_ip_address=ip
            recapplicationevaluation.save(flush:true,failOnError: true)
        }
        else
        {
            double m = (recevaluationparameter.maxmarks * secretCode.obtained_score)/ename.max_score
            recapplicationevaluation.obtained_marks = m
            recapplicationevaluation.username = secretCode.recapplicant.email
            recapplicationevaluation.updation_date=new java.util.Date()
            recapplicationevaluation.updation_ip_address=ip
            recapplicationevaluation.save(flush:true,failOnError: true)
        }
    }

    //AWS file
    def comparefile(ERPCoursePlanProblemStatementAllocation curr){
        //println curr
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        def allps = ERPCoursePlanProblemStatementAllocation.findAllByErpcourseplanproblemstatementsAndErpcourseofferingAndLoadtype(curr.erpcourseplanproblemstatements,curr.erpcourseoffering,curr.loadtype)
//        println curr.id
//        println allps.size()
//        println allps.erpcourseplanproblemstatements.id
        ArrayList compps = new ArrayList()
        String currps = ""
        boolean toproceed = true
        if(!awsService.doesObjectExist(awsBucket.bucketname, awsBucket.region, curr.answer_file_path+""+curr.answer_file_name)){
            return
        }
        InputStream object
        File tmp
        def fileInputStream
        XWPFDocument doc
        XWPFWordExtractor extract
        if(curr.answer_file_name.endsWith(".docx")) {
            object = awsService.downloadContentFromBucket(awsBucket.bucketname, awsBucket.region, curr.answer_file_path + "" + curr.answer_file_name)
            tmp = File.createTempFile("asps3test", "");
            Files.copy(object, tmp.toPath(), StandardCopyOption.REPLACE_EXISTING);
            fileInputStream = new FileInputStream(tmp)
            doc = new XWPFDocument(fileInputStream);
            extract = new XWPFWordExtractor(doc);
            currps = extract.getText()
            fileInputStream.close()
            object.close()
    //        tmp.deleteOnExit()
            println("1 deleteOnExit:"+tmp.delete())
            toproceed = false

        }
        else if(curr.answer_file_name.endsWith(".txt")){
            object = awsService.downloadContentFromBucket(awsBucket.bucketname, awsBucket.region, curr.answer_file_path + "" + curr.answer_file_name)
            tmp = File.createTempFile("asps3test", "");
            Files.copy(object, tmp.toPath(), StandardCopyOption.REPLACE_EXISTING);
            Scanner scan = new Scanner(tmp);
            scan.useDelimiter("\\Z");
            currps = scan.next();
            scan.close()
            object.close()
          //  tmp.deleteOnExit()
            println("2deleteOnExit:"+tmp.delete())
            toproceed = false
        }
        if(toproceed==true)
            return
        for(ps in allps){
            if(ps.studentsubmisiondate==null){
                continue
            }
            if(ps.id==curr.id){
                continue
            }
            if(!awsService.doesObjectExist(awsBucket.bucketname, awsBucket.region,  ps.answer_file_path+""+ps.answer_file_name)){
                continue
            }
            if(ps.answer_file_name.endsWith(".docx")) {
                object = awsService.downloadContentFromBucket(awsBucket.bucketname, awsBucket.region, ps.answer_file_path + "" + ps.answer_file_name)
                tmp = File.createTempFile("asps3test", "");
                Files.copy(object, tmp.toPath(), StandardCopyOption.REPLACE_EXISTING);
                fileInputStream = new FileInputStream(tmp)
                doc = new XWPFDocument(fileInputStream);
                extract = new XWPFWordExtractor(doc);
                compps.add(extract.getText())
                object.close()
                fileInputStream.close()
  //              tmp.deleteOnExit()
                println("3deleteOnExit:"+tmp.delete())

            }
            else if(ps.answer_file_name.endsWith(".txt")){
                object = awsService.downloadContentFromBucket(awsBucket.bucketname, awsBucket.region, ps.answer_file_path + "" + ps.answer_file_name)
                tmp = File.createTempFile("asps3test", "");
                Files.copy(object, tmp.toPath(), StandardCopyOption.REPLACE_EXISTING);
                Scanner scan = new Scanner(tmp);
                scan.useDelimiter("\\Z");
                String content = scan.next();
                compps.add(content)
                scan.close()
                object.close()
//                tmp.deleteOnExit()
                println("4deleteOnExit:"+tmp.delete())

            }
        }
        println("compps:"+compps.size())
        ArrayList perlist = new ArrayList()
       // double per = 0.0
        for(it in compps){
            perlist.add(similarity(currps,it))
        }
        if(perlist.size()>0) {
            //println "Max:"+Collections.max(perlist)
            double temp_per = (1-Collections.max(perlist))*curr.maxmarks
            if(temp_per<0)
                temp_per = 0
            curr.plagarisum_percentage =temp_per
            curr.save(failOnError: true, flush: true)
        }
        else{
            curr.plagarisum_percentage = curr.maxmarks
            curr.save(failOnError: true, flush: true)
        }
        //println("per:"+perlist)
    }
    //1
    public static double similarity(String s1, String s2) {
        String longer = s1, shorter = s2;
        if (s1.length() < s2.length()) { // longer should always have greater length
            longer = s2; shorter = s1;
        }
        int longerLength = longer.length();
        if (longerLength == 0) { return 1.0; /* both strings are zero length */ }

        return (longerLength - editDistance(longer, shorter)) / (double) longerLength;

    }
    public static int editDistance(String s1, String s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        int[] costs = new int[s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++) {
            int lastValue = i;
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0)
                    costs[j] = j;
                else {
                    if (j > 0) {
                        int newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1))
                            newValue = Math.min(Math.min(newValue, lastValue),
                                    costs[j]) + 1;
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0)
                costs[s2.length()] = lastValue;
        }
        return costs[s2.length()];
    }

//2
    public static int computeEditDistance(String s1, String s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        int[] costs = new int[s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++) {
            int lastValue = i;
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0) {
                    costs[j] = j;
                } else {
                    if (j > 0) {
                        int newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1)) {
                            newValue = Math.min(Math.min(newValue, lastValue),
                                    costs[j]) + 1;
                        }
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0) {
                costs[s2.length()] = lastValue;
            }
        }
        return costs[s2.length()];
    }
    public static void printDistance(String s1, String s2) {
        double similarityOfStrings = 0.0;
        int editDistance = 0;
        if (s1.length() < s2.length()) { // s1 should always be bigger
            String swap = s1;
            s1 = s2;
            s2 = swap;
        }
        int bigLen = s1.length();
        editDistance = computeEditDistance(s1, s2);
        if (bigLen == 0) {
            similarityOfStrings = 1.0; /* both strings are zero length */
        } else {
            similarityOfStrings = (bigLen - editDistance) / (double) bigLen;
        }
        //////////////////////////
        //System.out.println(s1 + "-->" + s2 + ": " +
        //      editDistance + " (" + similarityOfStrings + ")");
        System.out.println(editDistance + " (" + similarityOfStrings + ")");
    }

    def getallcols(Semester sem,AcademicYear ay, Organization org, ProgramType progtype, Year yr)//regular+backlog
    {
        println("in reg+RR")
        def learners = ERPCourseOfferingLearner.findAllByAcademicyearAndSemesterAndOrganization(ay,sem,org)
        learners = learners.findAll{it?.program?.programtype?.id==progtype.id}
        learners = learners.findAll{it.erpcourseoffering.year.id==yr.id}
        return learners
    }
    def getonlyregcols(Semester sem,AcademicYear ay, Organization org, ProgramType progtype, Year yr)//regular
    {
        println("in only reg")
        def learners = ERPCourseOfferingLearner.findAllByAcademicyearAndSemesterAndOrganization(ay,sem,org)
        learners = learners.findAll{it?.program?.programtype?.id==progtype.id}
        learners = learners.findAll{it.erpcourseoffering.year.id==yr.id}
        ERPExamConductType reg = ERPExamConductType.findByTypeAndOrganization("Regular",org)
        learners = learners.findAll{it.erpexamconducttype.id==reg.id}
        return learners
    }
    def getonlybackcols()//backlog
    {}

    def smsCommonService(String msg, def phoneno, def organization, def template_id){
        def smsprovider = SMSServiceProvider.findByIs_activeAndOrganization(true, organization)
        def responce
        if(smsprovider?.sms_provider_name == 'TEXTLOCAL')
            responce = sendSmslcl(msg, phoneno)
        else if(smsprovider?.sms_provider_name == 'SMS123')
            responce = sendSms123(msg, phoneno, organization, smsprovider?.sender, smsprovider?.username, smsprovider?.password)
        else if(smsprovider?.sms_provider_name == 'GCS')
            responce = sendSmsGCS(msg, phoneno, organization, smsprovider?.sender, smsprovider?.username, smsprovider?.password, smsprovider?.header, template_id)
        else if(smsprovider?.sms_provider_name == 'AMAZON') {
            def phonenos = []
            if(!phoneno?.contains("+91"))
                phonenos.add("+91"+phoneno)
            else
                phonenos.add(phoneno)
            responce = SendMailService.sendSMS_withAWS(msg, phonenos)
        }
        println("responce SMS : "+responce)
        return responce
    }

    // sms123 sms send service
    def sendSms123(String msg, def phoneno, def organization, def sender, def username, def password) {
        println("sendSms123")
        try {
            String usernamestr = "?username="+username
            String passwordstr = "&password="+password
            String senderstr = "&sender="+sender

//            if(organization?.organization_code == 'VIT')
//            {
//                username = "?username="+'vitpnq'
//                password = "&password="+"vitpnq12"
//                sender = "&sender="+"VISWAK"
//            }else if(organization?.organization_code == 'VIIT'){
//                username = "?username="+'viitkondhwa'
//                password = "&password="+"viitkondhwa123"
//                sender = "&sender="+"INFSMS"
//            }

            if(usernamestr&&passwordstr&&senderstr) {
                def mobno = "&mob=" + phoneno
                msg = msg.replaceAll(" ", "%20")
                String message = "&msg=" + msg
                String data = usernamestr + passwordstr + mobno + message + senderstr
                HttpURLConnection conn = (HttpURLConnection) new URL("http://www.sms123.in/QuickSend.aspx" + data).openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
                conn.getOutputStream().write(data.getBytes("UTF-8"));
                final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                final StringBuffer stringBuffer = new StringBuffer();
                String line;
                while ((line = rd.readLine()) != null) {
                    stringBuffer.append(line);
                }
                rd.close();
                if (stringBuffer.toString().contains('Error code- 000')) {
                    return 1
                } else
                    return 0
            }else{
                return 0
            }
        } catch (Exception e) {
            System.out.println("Error SMS "+e);
            return 0
        }
    }

    // local text sms send service
    def sendSmslcl(String msg, def phoneno) {
        try {
            // Construct data
            String apiKey = "apikey=" + "N5aplNRZl88-lnM6Y1GtC1zP02kNsK7Ou6Jt7cQFHa";
            String message = "&message=" + msg//"This is your message";
            String sender = "&sender=" + "EPSCAM";
            String numbers = "&numbers=" + phoneno//"919762340668";
            //String test = "&test=" + "true"; //for testing
            String test = "&test=" + "false"; //for testing

            // Send data
            HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
            String data = apiKey + numbers + message + sender +test;
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuffer.append(line);
            }
            rd.close();
            println "txtlcl:"+stringBuffer.toString();
            return 1
        } catch (Exception e) {
            System.out.println("Error SMS "+e);
            return 0;
        }
    }

    // local text sms send service
    def sendSmsGCS(String msg, def phoneno, def organization, def sender, def username, def password, def header, def template_id) {
        println("sendSmsGCS"+header)
        try {
            phoneno = phoneno.trim()
            String regex = "(0/91)?[7-9][0-9]{9}";
            def valid = phoneno.matches(regex)
            if(phoneno.size()>=10 && valid) {
                // Construct data
                String usernamestr = "username=" + username
                String passwordstr = "&password=" + password
                String sourcestr = "&source=" + sender;
                String type = "&type=0";
                String dlr = "&dlr=1"
                String destination = "&destination=" + phoneno
                String message = "&message=" + msg
                String headerstr = "&entityid=" + header
                String templatestr = "&tempid=" + template_id

                // Send data
                if (usernamestr && passwordstr && sourcestr) {
                    HttpURLConnection conn = (HttpURLConnection) new URL("http://103.16.101.52:8080/sendsms/bulksms?").openConnection();
                    String data = usernamestr + passwordstr + type + dlr + destination + sourcestr + message + templatestr;
                    if (header)
                        data = data + headerstr
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
                    conn.getOutputStream().write(data.getBytes("UTF-8"));
                    final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuffer stringBuffer = new StringBuffer();
                    String line;
                    while ((line = rd.readLine()) != null) {
                        stringBuffer.append(line);
                    }
                    rd.close();
                    def str = stringBuffer.toString()
                    println "GCSSMS:" + str;
                    def val = str.split('\\|')
                    if (val[0].trim() == '1701')
                        return 1
                    else
                        return 0
                } else {
                    return 0
                }
            }else{
                return 0
            }
        } catch (Exception e) {
            System.out.println("Error SMS "+e);
            return 0
        }
    }

    def getsentsmslog(max)
    {
        try {
            // Construct data
            //SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
            String apiKey = "apikey=" + "N5aplNRZl88-lnM6Y1GtC1zP02kNsK7Ou6Jt7cQFHa";
            String min_time="&min_time="+max;
            //String max_time="&max_time="+max;
            //def startDate = Date.parse("yyyy-MM-dd",min_time)
            //def endDate = Date.parse("yyyy-MM-dd", max_time)

            // Send data
            HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/get_history_api/?").openConnection();
            //String data = apiKey //+ min_time + max_time;
            String data = apiKey + min_time// + max_time
            println data
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuffer.append(line);
            }
            rd.close();
            return stringBuffer.toString();
        } catch (Exception e) {
            System.out.println("Error SMS "+e);
            return "Error "+e;
        }
    }


    def aylist(){
        return AcademicYear.findAllByIsactive(true).sort { it.ay }.reverse(true)
    }

    def getAy(currentay, yeararray){
        if(currentay?.previousyear) {
            def year = currentay?.previousyear
            yeararray.add(year)
            getAy(year, yeararray)
        }else{
            return yeararray
        }
    }

    def currentAySem(String atname, String rtname, Organization org){
        ApplicationType at = ApplicationType.findByApplication_type(atname)
        RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, rtname, org)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true, org)
        if(aay)
            return aay
    }

    def isinstitutimetablecoordinator(Login login){
        boolean isinstitutimetablecoordinator=false
        for (Role r1 : login.roles) {
            if (r1.role?.trim() == "ITTC")
            {
                isinstitutimetablecoordinator = true
            }
        }
        return isinstitutimetablecoordinator
    }

    def isdepartmenttimetablecoordinator(Login login){
        boolean isdepartmenttimetablecoordinator=false
        for (Role r1 : login.roles) {
            if(r1.role?.trim()=="DTTC") {
                isdepartmenttimetablecoordinator=true
            }
        }
        return isdepartmenttimetablecoordinator
    }

    def isdepartmentacademiccoordinator(Login login){
        boolean isdepartmentacademiccoordinator=false
        for (Role r1 : login.roles) {
            if(r1.role?.trim()=="DAC") {
                isdepartmentacademiccoordinator=true
            }
        }
        return isdepartmentacademiccoordinator
    }

    def isSpecificRole(Login login, Instructor inst, def role, def roletype){
        boolean isrolepresent = false
        for (Role r1 : login.roles) {
            if (r1?.role?.trim() == role && r1?.roletype?.type?.trim() == roletype) {
                isrolepresent = true
            }
        }
        return isrolepresent
    }


    def isManagement(def login, def inst, def role){
        boolean ismanagement = false
        if(inst?.ismanagementroleapplicable) {
//            for (Role r1 : login.roles) {
//                if (r1?.role?.trim() == role && r1?.roletype?.type?.trim() == "Management") {
                    ismanagement = true
//                }
//            }
        }
        return ismanagement
    }

    def isManagementWithRole(Login login, Instructor inst, def role){
        boolean ismanagement = false
        if(inst?.ismanagementroleapplicable) {
            for (Role r1 : login.roles) {
                if (r1?.role?.trim() == role && r1?.roletype?.type?.trim() == "Management") {
                    ismanagement = true
                }
            }
        }
        return ismanagement
    }

    def isManagement1(Login login, Instructor inst){
        boolean ismanagement = false
        if(inst?.ismanagementroleapplicable) {
            for (Role r1 : login.roles) {
                if (r1?.role?.trim() == "Management" && r1?.roletype?.type?.trim() == 'Academics') {
                    ismanagement = true
                }
            }
        }
        return ismanagement
    }
    def isregistration(Login login, Instructor inst){
        boolean ismanagement = false
            for (Role r1 : login.roles) {
                if (r1?.role?.trim() == "Register Employee" && r1?.roletype?.type?.trim() == 'Biometric') {
                    ismanagement = true
                }
            }

        return ismanagement
    }

    def isregistrationlocation(Login login, Instructor inst){
        boolean ismanagement = false

            for (Role r1 : login.roles) {
                if (r1?.role == "Register Location" && r1?.roletype?.type == 'Biometric') {
                    ismanagement = true
                }
            }

        return ismanagement
    }

    def isattadance(Login login, Instructor inst){
        boolean ismanagement = false

            for (Role r1 : login.roles) {
                if (r1?.role?.trim() == "Mark Attendance" && r1?.roletype?.type?.trim() == 'Biometric') {
                    ismanagement = true
                }
            }

        return ismanagement
    }

    def isAcademicsDAC(Login login, Instructor inst){
        println("isAcademicsDAC")
        boolean isrolepresent = false

        for (Role r1 : login.roles) {
            if (r1?.role?.trim() == "DAC" && r1?.roletype?.type?.trim() == 'Academics') {
                isrolepresent = true
            }
        }

        return isrolepresent
    }

    def getDate(changeDate)
    {
        def today = changeDate
        def year = today.getYear() + 1900
        def day = today.getDate() > 9 ? today.getDate() : '0' + today.getDate()
        def month = (today.getMonth() + 1) > 9 ? (today.getMonth() + 1) : '0' + (today.getMonth() + 1)
        return year + '-' + month + '-' + day
    }

    def getDateddmmyy(changeDate)
    {
        def today = changeDate
        def year = today.getYear() + 1900
        def day = today.getDate() > 9 ? today.getDate() : '0' + today.getDate()
        def month = (today.getMonth() + 1) > 9 ? (today.getMonth() + 1) : '0' + (today.getMonth() + 1)
        return day + '-' +month + '-' + year
    }

    def getDateddmmyy2(changeDate)
    {
        def today = changeDate
        def year = today.getYear() + 1900
        def day = today.getDate() > 9 ? today.getDate() : '0' + today.getDate()
        def month = (today.getMonth() + 1) > 9 ? (today.getMonth() + 1) : '0' + (today.getMonth() + 1)
        return day + '/' +month + '/' + year
    }

    def getTime(today)
    {
        def hour = today.getHours()>9 ? today.getHours() : '0'+today.getHours()
        def minute = today.getMinutes()>9 ? today.getMinutes() : '0'+today.getMinutes()
        return hour + ":" +minute
    }

    def assignrole(def login, def role){
        login.addToRoles(role)
        login.save(flush: true, failOnError: true)
    }

    def removerole(def login, def role){
        login.removeFromRoles(role)
        login.save(flush: true, failOnError: true)
    }

    def password_generation(){
        java.util.Random random=new java.util.Random()
        String otp=""
        int min=0,max=0,n
        for(int i=1;i<=8;i++)
        {
            if(i==1)
                min=1
            else
                min=0
            max=9
            n=random.nextInt(max)+min
            otp=otp+n
        }

        return otp
    }


    def getrollnumbersuffix(def prog, def year, def organization, def division){
        def rollprog = ERPGRNumberProgramCode.findByProgramAndOrganization(prog, organization)
        return year?.year +""+rollprog?.rollno_program_code+""+division?.name
    }

    def electivebucketcolor(index){
        def colorarray = ['black',  'brown', 'blue', 'red', '#104E8B', '#820BBB', '#5C246E', '#800080', '#C12474', '#C30000',
                          '#668014', '#00688B', 'purple', '#0D4F8B' ];
        return colorarray[index]
    }

    def timediff(outtime, intime) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Date date1 = format.parse(intime + ":00");
        Date date2 = format.parse(outtime + ":00");
        long difference
        if (date2 > date1)
            difference = (date2.getTime() - date1.getTime()) / 1000;
        else
            difference = (date1.getTime() - date2.getTime()) / 1000;
        int hours = difference / 3600;
        difference = difference % 3600;
        def mins = difference / 60;
        mins = mins > 9 ? mins : "0" + mins
        return Double.parseDouble(hours + '.' + mins)
    }


    //Send Job Opening Email
    def sendJobEnabledEmail(def jobname){
        def organizationlist = Organization.list()
        for(organization in organizationlist){
            if(organization.establishment_email && organization.establishment_email_credentials) {
                def result = SendMailService.sendmail(organization.establishment_email, organization.establishment_email_credentials, "nilesh2886@gmail.com", "Congratulations : " + jobname + " Job Running for " + organization?.organizationgroup?.name + "", "Thank you", "", "komal@edupluscampus.com")
                if (result)
                    break
            }
        }
    }

    // School redierct
    def getSchoolRedirectURL(def login){
        PayUMoneyService payuMoneyService = new PayUMoneyService()
        def hash = payuMoneyService.hashCal("SHA-256", "dpuav" + login?.username);
        def url = "https://sbpps.pceterp.in/check-eps-management-home?uid=" + login?.username + "&hash=" + hash + "&tenant=sbpps&backurl="
        return url
    }

    def getOrganizationForManagementDashboard(def login, def instructor, def role){
        def ismanagement = isManagement(login, instructor, role)
        def organizationlist = []
        if(ismanagement) {
            organizationlist = OrganizationOrgGroupMapping.createCriteria().list(){
                projections{
                    distinct('organization')
                }
                'in'('organizationgroup', instructor?.organization?.organizationgroup)
            }
            organizationlist?.sort{it?.sort_order}
        }else
            organizationlist = instructor?.organization


        def organizationlistfinal = []
        for(org in organizationlist){
            def roletypesetting = 'true'
            if(role == 'Biometric')
                roletypesetting = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Is Biomatric Management Dashboard on this ERP?', org)?.value
            if(role == 'Leave Management')
                roletypesetting = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Is Payroll Management Dashboard on this ERP?', org)?.value
            if(role == 'Payroll')
                roletypesetting = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Is Biomatric Management Dashboard on this ERP?', org)?.value

            def isschool = org?.isschool
            if(org?.isschool == true && roletypesetting == 'true')
                isschool = false
            organizationlistfinal.add(id : org?.id, organization_code : org?.organization_code, organization_name : org?.organization_code, isschool : isschool)
        }

        return organizationlistfinal
    }

    def sentenceCase(text){
        text = text?.trim()
        String finaltext  = "";
        if(text) {
            text = text?.split(" ")
            for(item in text) {
                item = item?.replaceAll("[^a-zA-Z0-9]", "");
                if(item?.size() > 0) {
                    item = item.substring(0, 1).toUpperCase() + item.substring(1).toLowerCase();
                    finaltext = finaltext + " " + item
                }
            }
        }
        return finaltext
    }

    // First Lecture
    def getLectureno(def insbatch, def period){
        def current = period?.toString()?.split("[.]")
        def lectno = Integer.parseInt(current[0])
        def erpcourseofferingplan = ERPCourseOfferingPlan.findByErpcourseofferingbatchinstructorAndLecture_number_planned(insbatch, lectno)
        def erpcourseofferingplannext = ERPCourseOfferingPlan.findByErpcourseofferingbatchinstructorAndLecture_number_planned(insbatch, lectno+1)

        if(erpcourseofferingplan){
            if(erpcourseofferingplan?.no_lecture_per_plan == 1) {
                if(erpcourseofferingplannext) {
                    if (erpcourseofferingplannext?.no_lecture_per_plan == 1)
                        period = lectno + 1
                    else {
                        period = lectno + "." + 1
                    }
                }else
                    period = lectno + 1
            }else{
                def currectactual = Integer.parseInt(current[1])
                if(erpcourseofferingplan?.no_lecture_per_plan > currectactual){
                    period = erpcourseofferingplan.lecture_number_planned + "." + (currectactual+1)
                }else{
                    if(erpcourseofferingplannext) {
                        if (erpcourseofferingplannext?.no_lecture_per_plan == 1)
                            period = erpcourseofferingplannext?.lecture_number_planned
                        else {
                            period = erpcourseofferingplannext.lecture_number_planned + "." + 1
                        }
                    }else{
                        period = lectno + 1
                    }
                }
            }
        }else
            period = lectno + 1

        return period
    }

    def getFirstLecture(def insbatch, def lectno){
        def erpcourseofferingplan = ERPCourseOfferingPlan.findByErpcourseofferingbatchinstructorAndLecture_number_planned(insbatch, lectno)
        if(erpcourseofferingplan?.no_lecture_per_plan == 1) {
            return 1
        }else{
            return 1.1
        }
    }


    // Offer Role Group To User
    def removeRoleGroup(def rolegroupuser, def login){
        def rolegrouprole = RoleGroupRole.findAllByEmployeerolegroup(rolegroupuser?.employeerolegroup)
        for (item in rolegrouprole) {
            removerole(login, item?.role)
        }
        rolegroupuser.delete(flush: true, failOnError: true)
    }

    def offerRoleGroup(def rolegroup, def login, def user, def username, def ip, def usertype, def roleval){
        def rolegrouprole = RoleGroupRole.findAllByEmployeerolegroup(rolegroup)
        for (item in rolegrouprole) {
            assignrole(login, item?.role)
        }
        def rolegroupuser
        if(usertype == 'Employee') {
            if(rolegroup?.rolelevel?.name?.trim() == 'DepartmentLevel') {
                rolegroupuser = RoleGroupUser.findByInstructorAndOrganizationAndEmployeerolegroupAndDepartment(user, user?.organization, rolegroup, roleval)
            }else if(rolegroup?.rolelevel?.name?.trim() == 'BranchLevel') {
                rolegroupuser = RoleGroupUser.findByInstructorAndOrganizationAndEmployeerolegroupAndProgram(user, user?.organization, rolegroup, roleval)
            }else if(rolegroup?.rolelevel?.name?.trim() == 'StreamLevel') {
                rolegroupuser = RoleGroupUser.findByInstructorAndOrganizationAndEmployeerolegroupAndStream(user, user?.organization, rolegroup, roleval)
            }else if(rolegroup?.rolelevel?.name?.trim() == 'BranchYearLevel') {
                rolegroupuser = RoleGroupUser.findByInstructorAndOrganizationAndEmployeerolegroupAndProgramyear(user, user?.organization, rolegroup, roleval)
            }else if(rolegroup?.rolelevel?.name?.trim() == 'CourseLevel') {
                rolegroupuser = RoleGroupUser.findByInstructorAndOrganizationAndEmployeerolegroupAndErpcourse(user, user?.organization, rolegroup, roleval)
            }else if(rolegroup?.rolelevel?.name?.trim() == 'DivisionLevel') {
                rolegroupuser = RoleGroupUser.findByInstructorAndOrganizationAndEmployeerolegroupAndDivisionoffering(user, user?.organization, rolegroup, roleval)
            }else {
                rolegroupuser = RoleGroupUser.findByInstructorAndOrganizationAndEmployeerolegroup(user, user?.organization, rolegroup)
            }
        }else
            rolegroupuser = RoleGroupUser.findByLearnerAndOrganizationAndEmployeerolegroup(user, user?.organization, rolegroup)

        if (!rolegroupuser) {
            rolegroupuser = new RoleGroupUser()
            rolegroupuser?.employeerolegroup = rolegroup
            if(usertype == 'Employee') {
                rolegroupuser?.instructor = user
                if(rolegroup?.rolelevel?.name?.trim() == 'DepartmentLevel') {
                    rolegroupuser?.department = roleval
                }else if(rolegroup?.rolelevel?.name?.trim() == 'BranchLevel') {
                    rolegroupuser?.program = roleval
                }else if(rolegroup?.rolelevel?.name?.trim() == 'StreamLevel') {
                    rolegroupuser?.stream = roleval
                }else if(rolegroup?.rolelevel?.name?.trim() == 'BranchYearLevel') {
                    rolegroupuser?.programyear = roleval
                }else if(rolegroup?.rolelevel?.name?.trim() == 'CourseLevel') {
                    rolegroupuser?.erpcourse = roleval
                }else if(rolegroup?.rolelevel?.name?.trim() == 'DivisionLevel') {
                    rolegroupuser?.divisionoffering = roleval
                }
            }else{
                rolegroupuser?.learner = user
            }
            rolegroupuser?.organization = user?.organization
            rolegroupuser.creation_username = username
            rolegroupuser.updation_username = username
            rolegroupuser.creation_date = new java.util.Date()
            rolegroupuser.updation_date = new java.util.Date()
            rolegroupuser.creation_ip_address = ip
            rolegroupuser.updation_ip_address = ip
            rolegroupuser.save(flush: true, failOnError: true)
        }
    }

    def addRolesToUser(def rolename, def user, def org, def username, def ip, def usertype, def roleval){
        println("addRolesToUser : "+rolename)
        if(user) {
            Login login = Login.findByUsername(user?.uid)
            if(login) {
                ApplicationType at = ApplicationType.findByApplication_type("ERP")
                UserType ut = UserType.findByApplication_typeAndTypeAndOrganization(at, usertype, org)
                def rolegroup = RoleGroup.findByNameIlikeAndOrganizationAndUsertype(rolename , org, ut)
                if(rolegroup) {
                    offerRoleGroup(rolegroup, login, user, username, ip, usertype, roleval)
                }else {
                    def rolelist = Role.findAllByRoleIlikeAndOrganizationAndUsertype(rolename, org, ut)
                    for (role in rolelist) {
                        assignrole(login, role)
                    }
                }
            }
        }
    }

    def removeRolesToUser(def rolename, def user, def org, def usertype){
        println("removeRolesToUser : "+rolename)
        if(user) {
            Login login = Login.findByUsername(user?.uid)
            if(login) {
                ApplicationType at = ApplicationType.findByApplication_type("ERP")
                UserType ut = UserType.findByApplication_typeAndTypeAndOrganization(at, usertype, org)
                def rolegroup = RoleGroup.findByNameIlikeAndOrganizationAndUsertype(rolename , org, ut)
                def rolegroupuser
                if(usertype == 'Employee')
                    rolegroupuser = RoleGroupUser.findByEmployeerolegroupAndInstructorAndOrganization(rolegroup, user, org)
                else
                    rolegroupuser = RoleGroupUser.findByEmployeerolegroupAndLearnerAndOrganization(rolegroup, user, org)
                if(rolegroupuser) {
                    removeRoleGroup(rolegroupuser, login)
                }else {
                    def rolelist = Role.findAllByRoleIlikeAndOrganizationAndUsertype(rolename, org, ut)
                    for (role in rolelist) {
                        removerole(login, role)
                    }
                }
            }
        }
    }



    // Check Role Group Access

    def getRoleDetails(def instructor, def rolegroupname){
        ApplicationType at = ApplicationType.findByApplication_type("ERP")
        UserType ut = UserType.findByApplication_typeAndTypeAndOrganization(at, "Employee", instructor?.organization)
        def rolegroup = RoleGroup.findByNameIlikeAndOrganizationAndUsertype(rolegroupname , instructor?.organization, ut)
        def rolegroupuser = RoleGroupUser.findAllByEmployeerolegroupAndInstructorAndOrganization(rolegroup, instructor, instructor?.organization)
        if(rolegroupuser) {
            if(rolegroup?.rolelevel?.name?.trim() == 'DepartmentLevel')
                return rolegroupuser?.department
            else if(rolegroup?.rolelevel?.name?.trim() == 'BranchLevel')
                return rolegroupuser?.program
            else if(rolegroup?.rolelevel?.name?.trim() == 'StreamLevel')
                return rolegroupuser?.stream
            else if(rolegroup?.rolelevel?.name?.trim() == 'BranchYearLevel')
                return rolegroupuser?.programyear
            else if(rolegroup?.rolelevel?.name?.trim() == 'CourseLevel')
                return rolegroupuser?.erpcourse
            else if(rolegroup?.rolelevel?.name?.trim() == 'DivisionLevel')
                return rolegroupuser?.divisionoffering
        }else
            return 0
    }

}
