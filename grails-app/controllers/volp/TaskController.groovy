package volp
import grails.converters.JSON
import groovy.json.JsonSlurper
import org.springframework.http.*
class TaskController {
    ApiInstructorLoginService apiInstructorLoginService
    def index() { }

    def getAppTaskListAssignTome()
    {
        HashMap hm=new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService=new TaskService()
        taskService.getTaskListAssignTome(request,instructor,hm)
        render hm as JSON
        return
    }
    def addAppNewTask()
    {
        HashMap hm=new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService=new TaskService()
        taskService.addNewTask(request,instructor,hm)
        render hm as JSON
        return
    }
    def getAppTaskNo() {
        HashMap hm=new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.getTaskNo(request, instructor, hm, params)
        render hm as JSON
        return
    }
    def saveAppTask() {
        HashMap hm=new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.saveTask(request, instructor, hm, params)
        render hm as JSON
        return
    }
    def saveAppNewActivity() {
        println "in save App New Activity"
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.saveNewActivity(request, instructor, hm, params)
        render hm as JSON
        return
    }
    def fetchAppActivityWorkflowSettings() {
        println "In fetchAppActivityWorkflowSettings TaskController"
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.fetchActivityWorkflowSettings(request, instructor, hm)
        render hm as JSON
        return
    }
    def fetchAppActivityStatus() {
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskActivity activity = TaskActivity.findById(params.activity)
        TaskService taskService = new TaskService()
        taskService.fetchActivityStatus(request, instructor, hm, activity)
        render hm as JSON
        return
    }
    def saveAppNewStatus() {
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        if (!params.taskActivity) {
            hm.put("status", "404")
            hm.put("message", "Please Select Activity.")
            return
        }
        TaskActivity taskActivity = TaskActivity.findById(params.taskActivity)
        TaskStatusType taskStatusType = TaskStatusType.findByType("User Defined")
        def taskList = Task.findAllByTaskactivityAndOrganization(taskActivity, instructor.organization)
        if (taskList.isEmpty()) {
            def taskStatusList = TaskStatus.findAllByTaskactivityAndIsactive(taskActivity, true)
            //println("Task Status List =>>"+ taskStatusList.size())
            taskService.addNewTaskStatus(params.status, instructor, taskStatusList.size(), request.getRemoteAddr(), taskActivity, taskStatusType)
            hm.put("status", "200")
            hm.put("message", params.status + " Status Added to " + taskActivity?.name + " Activity Successfully.")
            return
        } else {
            hm.put("status", "404")
            hm.put("message", "Can not add status if already task created.")
        }
        render hm as JSON
        return
    }
    def saveAppSwapStatus() {
        println "In saveSwapStatus TaskController =>> " + params
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        def sequence
        def sequence1
        TaskStatus taskStatus = TaskStatus.findById(params.id1)
        TaskStatus taskStatus1 = TaskStatus.findById(params.id2)
        if (taskStatus && taskStatus1) {
            if (taskStatus)
                sequence = taskStatus?.sequence_no
            if (taskStatus1)
                sequence1 = taskStatus1?.sequence_no

            if (taskStatus) {
                taskStatus.sequence_no = sequence1
                taskStatus.updation_username = instructor.uid
                taskStatus.updation_date = new Date()
                taskStatus.updation_ip_address = request.getRemoteAddr()
                taskStatus.save(flush: true, failOnError: true)
            }
            if (taskStatus1) {
                taskStatus1.sequence_no = sequence
                taskStatus1.updation_username = instructor.uid
                taskStatus1.updation_date = new Date()
                taskStatus1.updation_ip_address = request.getRemoteAddr()
                taskStatus1.save(flush: true, failOnError: true)
            }
            hm.put("status", "200")
            hm.put("message", "Status Sequence Number Swap Successfully.")
        } else {
            hm.put("status", "404")
            hm.put("message", "Task Status Not Found.")
        }
        render hm as JSON
        return
    }
    def deleteAppActivityStatus() {
        println("In deleteAppActivityStatus TaskController =>> " + params)
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskStatus taskStatus = TaskStatus.findById(params?.id)
        if (taskStatus) {
            def taskList = Task.findAllByTaskactivityAndOrganization(taskStatus.taskactivity, instructor.organization)
            if (taskList.isEmpty()) {
                taskStatus.isactive = false
                taskStatus.updation_username = instructor.uid
                taskStatus.updation_date = new Date()
                taskStatus.updation_ip_address = request.getRemoteAddr()
                taskStatus.save(flush: true, failOnError: true)

                def taskStatusList = TaskStatus.findAllByTaskactivityAndOrganizationAndIsactive(taskStatus.taskactivity, instructor.organization, true)
                taskStatusList?.sort { it.sequence_no }
                if (taskStatusList) {
                    def count = 1
                    for (status in taskStatusList) {
                        TaskStatus status1 = TaskStatus.findById(status?.id)
                        if (status1) {
                            status1.sequence_no = count
                            status1.updation_username = instructor.uid
                            status1.updation_date = new Date()
                            status1.updation_ip_address = request.getRemoteAddr()
                            status1.save(flush: true, failOnError: true)
                        }
                        count++
                    }
                }
                hm.put("status", "200")
                hm.put("message", taskStatus.taskactivity?.name + " Activity " + taskStatus?.name + " Status Deleted Successfully.")
            } else {
                hm.put("status", "404")
                hm.put("message", "Can not delete status if already task created.")
            }
        } else {
            hm.put("status", "404")
            hm.put("message", "Task Status Not Found.")
        }
        render hm as JSON
        return
    }
    def editAppTask() {
        println "In tasktracking TaskController =>>" + params

        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.editTask(request, instructor, hm, params)
        if (hm.get("status") == "200" && hm.get("message") == "success") {
            hm.put("status", "200")
            hm.put("message", hm.get("flashmsg"))
        } else if (hm.get("status") != "200") {
            hm.put("message", hm.get("flashmsg"))
        } else {
            hm.put("message", "Error Occured..")
        }
        render hm as JSON
        return
    }
    def addAppSubTask() {
        println "In addSubTask TaskController =>>" + params
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.addSubTask(request, instructor, hm, params)
        render hm as JSON
        return
    }
    def saveAppSubTask() {
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }

        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.saveSubTask(request, instructor, hm, params)
        if (hm.get("status") == "200" && hm.get("message") == "success") {
            hm.put("status", "200")
            hm.put("message", hm.get("flashmsg"))
        } else if (hm.get("status") != "200") {
            hm.put("message", hm.get("flashmsg"))
        } else {
            hm.put("message", "Error Occured..")
        }
        render hm as JSON
        return
    }
    def appActivityDashBoard() {
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.ActivityDashBoard(request, instructor, hm)
        render hm as JSON
        return
    }
    def getAppDashboardDetails() {
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.getDashboardDetails(request, instructor, hm, params)
        render hm as JSON
        return
    }
    def apptaskTracking() {
        println "In tasktracking TaskController =>> " + params
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.taskTracking(request, instructor, hm, params)
        render hm as JSON
        return
    }
    def viewAppAttachment() {
        println "in tasktracking" + params
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.viewAttachment(request, instructor, hm, params)
        render hm as JSON
        return
    }
    def editAppAttchment() {
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.editAttchment(request, instructor, hm, params)
        if (hm.get("status") == "200" && hm.get("message") == "success") {
            hm.put("status", "200")
            hm.put("message", hm.get("flashmsg"))
        } else if (hm.get("status") != "200") {
            hm.put("message", hm.get("flashmsg"))
        } else {
            hm.put("message", "Error Occured..")
        }
        render hm as JSON
        return
    }
    def saveAppNewAttachment() {

        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.saveNewAttachment(request, instructor, hm, params)
        if (hm.get("status") == "200" && hm.get("message") == "success") {
            hm.put("status", "200")
            hm.put("message", hm.get("flashmsg"))
        } else if (hm.get("status") != "200") {
            hm.put("message", hm.get("flashmsg"))
        } else {
            hm.put("message", "Error Occured..")
        }
        render hm as JSON
        return
    }
    def viewAppComments() {
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.viewComments(request, instructor, hm, params)
        render hm as JSON
        return
    }
    def editAppComment() {
        println "in editComment" + params
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.editComment(request, instructor, hm, params)
        if (hm.get("status") == "200" && hm.get("message") == "success") {
            hm.put("status", "200")
            hm.put("message", hm.get("flashmsg"))
        } else if (hm.get("status") != "200") {
            hm.put("message", hm.get("flashmsg"))
        } else {
            hm.put("message", "Error Occured..")
        }
        render hm as JSON
        return
    }
    def saveAppNewComment() {
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.saveNewComment(request, instructor, hm, params)
        if (hm.get("status") == "200" && hm.get("message") == "success") {
            hm.put("status", "200")
            hm.put("message", hm.get("flashmsg"))
        } else if (hm.get("status") != "200") {
            hm.put("message", hm.get("flashmsg"))
        } else {
            hm.put("message", "Error Occured..")
        }
        render hm as JSON
        return
    }

    def deleteAppAttachment() {
        println "In deleteAttachment TaskController"
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.deleteAttachment(request, instructor, hm, params)
        if (hm.get("status") == "200" && hm.get("message") == "success") {
            hm.put("status", "200")
            hm.put("message", hm.get("flashmsg"))
        } else if (hm.get("status") != "200") {
            hm.put("message", hm.get("flashmsg"))
        } else {
            hm.put("message", "Error Occured..")
        }
        render hm as JSON
        return
    }

    def deleteAppComment() {
        println "In deleteAttachment TaskController"
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.deleteComment(request, instructor, hm, params)
        if (hm.get("status") == "200" && hm.get("message") == "success") {
            hm.put("status", "200")
            hm.put("message", hm.get("flashmsg"))
        } else if (hm.get("status") != "200") {
            hm.put("message", hm.get("flashmsg"))
        } else {
            hm.put("message", "Error Occured..")
        }
        render hm as JSON
        return
    }
    def deleteAppTask() {
        println("In deleteTask TaskController =>> " + params)
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }

        Instructor instructor = Instructor.findByUid(login.username)
        Task task = Task.findById(params?.id)
        if (task) {
            task.isactive = false
            task.updation_username = instructor.uid
            task.updation_date = new Date()
            task.updation_ip_address = request.getRemoteAddr()
            task.save(flush: true, failOnError: true)
            flash.message = "Task Deleted Successfully."
            hm.put("status", "200")
            hm.put("message", "Task Deleted Successfully.")
        } else {
            hm.put("status", "404")
            hm.put("message", "Organization not found.")

        }
        render hm as JSON
        return
    }
    def saveAppEditedActivityStatus() {
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if (!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if (!org) {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("message", "Organization not found.")
            render hm as JSON
            return
        }
        if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("message", "You are logged out. Please re-login..!")
            render hm as JSON
            return
        }
        Instructor instructor = Instructor.findByUid(login.username)
        TaskService taskService = new TaskService()
        taskService.saveEditedActivityStatus(request, instructor, hm, params)
        if (hm.get("status") == "200" && hm.get("message") == "success") {
            hm.put("status", "200")
            hm.put("message", hm.get("flashmsg"))
        } else if (hm.get("status") != "200") {
            hm.put("message", hm.get("flashmsg"))
        } else {
            hm.put("message", "Error Occured..")
        }
        render hm as JSON
        return
    }

}
