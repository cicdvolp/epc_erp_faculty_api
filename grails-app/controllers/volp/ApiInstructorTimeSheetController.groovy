package volp

import com.google.api.client.json.Json
import grails.converters.JSON
import groovy.json.JsonSlurper
import org.springframework.http.HttpStatus

import java.text.SimpleDateFormat

class ApiInstructorTimeSheetController {

    ApiInstructorTimeSheetService apiInstructorTimeSheetService

    def addTimeSheets(){
        HashMap hm = new HashMap()
        String body1 =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body1)
        String editid = dataresponse.timesheet_id
        String date = dataresponse.timesheet_date
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if(!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if(!org) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","Organization not found.")
            render hm as JSON
            return
        }


        SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        def today = formatter.parse(date)
//        def today = new Date()
//        if (date) {
//            try {
//                SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//                date = formatter.parse(date)
////                today = date('date', 'dd-MMM-yyyy')
//                today = date
//                println("today 1 ::: " + today)
//            } catch (Exception e) {
//                SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd")
//                date = formatter2.parse(date)
////                today = params.date('date', 'dd-MMM-yyyy')
//                today = date
//                println("today 2 ::: " + today)
//            }
//        }
        HashMap newMap = apiInstructorTimeSheetService.getTimeSheets(login.username, editid, today)

        def timesheet = newMap.timesheet
        println("timesheet : " + timesheet)

        def instructortimesheetmaster = newMap.instructortimesheetmaster
        println("instructortimesheetmaster :  " + instructortimesheetmaster)

        def timesheetlist = newMap.timesheetlist
        println("timesheetlist : " + timesheetlist.size())

        def insttimesheetmasterlist = []
        instructortimesheetmaster.forEach({
            obj ->
                HashMap map = new HashMap()
                map.put("id", obj?.id)
                map.put("name", obj?.name)
                map.put("description", obj?.description)
                map.put("isdelete", obj?.isdelete)
                map.put("isactive", obj?.isactive)

                HashMap taskMaster = new HashMap()
                taskMaster.put("id", obj?.erptaskmaster?.id)
                taskMaster.put("name", obj?.erptaskmaster?.name)
                taskMaster.put("description", obj?.erptaskmaster?.description)
                taskMaster.put("isdelete", obj?.erptaskmaster?.isdelete)
                taskMaster.put("isactive", obj?.erptaskmaster?.isactive)
                map.put("task_master", taskMaster)
                insttimesheetmasterlist.add(map)
        })

        def task_list = []
        timesheetlist.forEach({
            obj ->
                HashMap map = new HashMap()
                map.put("id", obj.id)
                map.put("name", obj.name)
                map.put("description", obj.description)
                map.put("date", obj.date)
                map.put("starttime", obj.starttime)
                map.put("endtime", obj.endtime)
                map.put("coursename", obj.coursename)
                map.put("presentstudents", obj.presentstudents)
                map.put("totalstudents", obj.totalstudents)
                HashMap prog_map = new HashMap()
                prog_map.put("name", obj.program?.name)
                prog_map.put("abbrivation", obj.program?.abbrivation)
                prog_map.put("id", obj.program?.id)
                map.put("program", prog_map)
                HashMap year_map = new HashMap()
                year_map.put("id", obj.year?.id)
                year_map.put("year", obj.year?.year)
                year_map.put("year_display_name", obj.year?.display_name)
                map.put("year", year_map)
                task_list.add(map)
        })

        if(timesheet) {
            HashMap timesheet_map = new HashMap()
            timesheet_map.put("name", timesheet.name)
            timesheet_map.put("description", timesheet.description)
            timesheet_map.put("date", timesheet.date)
            timesheet_map.put("starttime", timesheet.starttime)
            timesheet_map.put("endtime", timesheet.endtime)
            timesheet_map.put("coursename", timesheet.coursename)
            timesheet_map.put("presentstudents", timesheet.presentstudents)
            timesheet_map.put("totalstudents", timesheet.totalstudents)
            HashMap prog_map = new HashMap()
            prog_map.put("name", timesheet.program?.name)
            prog_map.put("abbrivation", timesheet.program?.abbrivation)
            prog_map.put("id", timesheet.program?.id)
            timesheet_map.put("program", prog_map)
            HashMap year_map = new HashMap()
            year_map.put("id", timesheet.year?.id)
            year_map.put("year", timesheet.year?.year)
            year_map.put("year_display_name", timesheet.year?.display_name)
            timesheet_map.put("year", year_map)
            hm.put("timesheet", timesheet_map)
        }

        hm.put("inst_timesheet_master_list", insttimesheetmasterlist)
        hm.put("task_list", task_list)
        hm.put("status", HttpStatus.OK.value().toString())
        render hm as JSON
        return
    }

    def getTaskList() {
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if(!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if(!org) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","Organization not found.")
            render hm as JSON
            return
        }

        HashMap map = apiInstructorTimeSheetService.getAllTaskList(login.username)

        def timesheetmasters = map.timesheetmaster
        def timesheetmasters1 = map.timesheetmaster1
        println("timesheetmasters :: " + timesheetmasters.size())
        println("timesheetmasters :: " + timesheetmasters1.size())

        def taskmaster_list = []
        timesheetmasters.stream().forEach({
            obj ->
                println("obj :: " + obj)

                HashMap taskMaster = new HashMap()
                taskMaster.put("id", obj?.id)
                taskMaster.put("name", obj?.name)
                taskMaster.put("description", obj?.description)
                taskMaster.put("isdelete", obj?.isdelete)
                taskMaster.put("isactive", obj?.isactive)
                taskmaster_list.add(taskMaster)
        })

        def taskmaster_list1 = []
        timesheetmasters1.stream().forEach({
            obj ->
                println("obj :: " + obj)

                HashMap taskMaster = new HashMap()
                taskMaster.put("id", obj?.id)
                taskMaster.put("name", obj?.name)
                taskMaster.put("description", obj?.description)
                taskMaster.put("isdelete", obj?.isdelete)
                taskMaster.put("isactive", obj?.isactive)
                taskmaster_list1.add(taskMaster)
        })

        println("task_master_list :: " + taskmaster_list)
        println("task_master_list1 :: " + taskmaster_list1)

        hm.put("task_master_list", taskmaster_list)
        hm.put("all_task_master_list", taskmaster_list1)
        hm.put("status", HttpStatus.OK.value().toString())
        render hm as JSON
        return
    }

    def saveInstructorTask() {
        println("saveInstructorTask :: " + params)
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if(!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if(!org) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","Organization not found.")
            render hm as JSON
            return
        }
        String body1 =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body1)
        String erpTaskMasterId = dataresponse.erpTaskMasterId
        String taskName = dataresponse.taskName
        String taskDescription = dataresponse.taskDescription
        String requestIp = request.getRemoteAddr()

        HashMap inst_ts_map = apiInstructorTimeSheetService.saveInstructorTimeSheetMaster(login.username, erpTaskMasterId, taskName, taskDescription, requestIp)

        def insttimesheetmasterlist = []
        inst_ts_map.instructortimesheetmaster.forEach() {
            obj ->
                HashMap map = new HashMap()
                map.put("id", obj?.id)
                map.put("name", obj?.name)
                map.put("description", obj?.description)
                map.put("isdelete", obj?.isdelete)
                map.put("isactive", obj?.isactive)

                HashMap taskMaster = new HashMap()
                taskMaster.put("id", obj?.erptaskmaster?.id)
                taskMaster.put("name", obj?.erptaskmaster?.name)
                taskMaster.put("description", obj?.erptaskmaster?.description)
                taskMaster.put("isdelete", obj?.erptaskmaster?.isdelete)
                taskMaster.put("isactive", obj?.erptaskmaster?.isactive)
                map.put("task_master", taskMaster)
                insttimesheetmasterlist.add(map)
        }

        hm.put("time_sheet_list", insttimesheetmasterlist)
        hm.put("status", HttpStatus.OK.value().toString())
        render hm as JSON
        return
    }

    def saveInstructorTimesheet() {
        println("I am in Save Instructor Timesheet"+params)
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if(!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if(!org) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","Organization not found.")
            render hm as JSON
            return
        }
        String body1 =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body1)
        String inst_taskmaster_id = dataresponse.inst_task_master_id
        String requestIp = request.getRemoteAddr()
        String date = dataresponse.date
        String timesheet_id = dataresponse.timesheet_id
        String starttime = dataresponse.starttime
        String endtime = dataresponse.endtime
        String taskname = dataresponse.task_name
        String taskdescription = dataresponse.task_description

        SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        def today = formatter.parse(date)

        def result = apiInstructorTimeSheetService.saveInstructorTimesheet(login.username, inst_taskmaster_id, requestIp, today, timesheet_id, starttime, endtime, taskname, taskdescription)
        if(result == 1) {
            hm.put("status", HttpStatus.OK.value().toString())
            hm.put("msg", "Instructor Time Sheet Master saved successfully")
        } else {
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Instructor Time Sheet Master not found")
        }
        render hm as JSON
        return

    }

    def isDeleteERPInstructorTaskMaster() {
        println("in isDeleteERPInstructorTaskMaster " + params)
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if(!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if(!org) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","Organization not found.")
            render hm as JSON
            return
        }
        String body1 =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body1)
        String inst_taskmaster_id = dataresponse.inst_task_master_id
        String requestIp = request.getRemoteAddr()

        def result = apiInstructorTimeSheetService.deleteERPInstructorTaskMaster(login.username, inst_taskmaster_id, requestIp)
        if(result == 1) {
            hm.put("status", HttpStatus.OK.value().toString())
            hm.put("msg", "Instructor Time Sheet Master deleted successfully")
        } else {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("msg", "Instructor Time Sheet Master not found")
        }
        render hm as JSON
        return
    }

    def isactiveERPInstructorTaskMaster() {
        println("in isactiveERPInstructorTaskMaster " + params)
        HashMap hm = new HashMap()
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if(!login) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if(!org) {
            hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
            hm.put("msg","Organization not found.")
            render hm as JSON
            return
        }
        String body1 =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body1)
        String inst_taskmaster_id = dataresponse.inst_task_master_id
        String requestIp = request.getRemoteAddr()

        def result = apiInstructorTimeSheetService.activeERPInstructorTaskMaster(login.username, inst_taskmaster_id, requestIp)
        if(result == 1) {
            hm.put("status", HttpStatus.OK.value().toString())
            hm.put("msg", "Instructor Time Sheet Master activation status changed successfully")
        } else {
            hm.put("status", HttpStatus.NOT_FOUND.value().toString())
            hm.put("msg", "Instructor Time Sheet Master not found")
        }
        render hm as JSON
        return
    }
}
