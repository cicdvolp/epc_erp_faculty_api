package volp

import grails.converters.JSON
import groovy.json.JsonSlurper
import org.springframework.http.HttpStatus

class ApiInstructorTimeTableController {

    ERPTimeTableService erpTimeTableService = new ERPTimeTableService()

    def getMyTimeTable() {
        HashMap hm = new HashMap()
        String body1 =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body1)
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if(!login) {
            hm.put("status", "500")
            hm.put("msg","You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if(!org) {
            hm.put("status", "500")
            hm.put("msg","Organization not found.")
            render hm as JSON
            return
        }
        AcademicYear ay = AcademicYear.findById(dataresponse.ay) //3
        Semester sem = Semester.findById(dataresponse.sem)//21
        Department dept = Department.findById(dataresponse.department) //70
        SlotMaster slotmaster = SlotMaster.findById(dataresponse.slotmaster) //null
        HashMap data = erpTimeTableService.getMyTimeTable(login.username, ay, sem, dept, slotmaster)

        if(data.flash_error != null) {
            hm.put("status", "500")
            hm.put("msg",hm.flash_error)
            render hm as JSON
            return
        }

        def erpDays = data.erpDayList
        erpDays = erpDays.sort{it.daysequence}
        def erpDayList = []
        erpDays.stream().forEach({ obj ->
            erpDayList.add(obj.displayname)
        })

        def timetablelist = []
        def daywisetimetable = data.daywisetimetable
        daywisetimetable.forEach({ obj ->
            List timetable = []
            obj.slottimetable.forEach({ obj1 ->
                obj1.erpTimeTable.forEach({ tt ->
                    HashMap ttObj = new HashMap()
                    ttObj.put("timetable_id", tt?.timetable?.id)
                    ttObj.put("timetable_version_id", tt?.timetable?.erptimetableversion?.id)
                    ttObj.put("isonlinesession", tt?.timetable?.isonlinesession)
                    ttObj.put("onlinesesionurl", tt?.timetable?.onlinesesionurl)
                    ttObj.put("subslotnumber", tt?.timetable?.subslotnumber)
                    ttObj.put("division", tt?.timetable?.erpcourseofferingbatch?.divisionoffering?.division?.name)
                    ttObj.put("year", tt?.timetable?.year?.display_name)
                    ttObj.put("program_abbrivation", tt?.timetable?.program?.abbrivation)
                    ttObj.put("program", tt?.timetable?.program?.name)
                    ttObj.put("department", tt?.timetable?.department?.name)
                    ttObj.put("department_abbrivation", tt?.timetable?.department?.abbrivation)
                    ttObj.put("batch_number", tt?.timetable?.erpcourseofferingbatch?.batch_number)
                    ttObj.put("batch_display_name", tt?.timetable?.erpcourseofferingbatch?.display_name)
                    ttObj.put("load_type", tt?.timetable?.loadtypecategory?.display_name)
                    ttObj.put("course_code", tt?.timetable?.erpcourseofferingbatch?.erpcourseoffering?.erpcourse?.course_code)
                    ttObj.put("course_name", tt?.timetable?.erpcourseofferingbatch?.erpcourseoffering?.erpcourse?.course_name)
                    ttObj.put("course_name_abbrivation", tt?.timetable?.erpcourseofferingbatch?.erpcourseoffering?.erpcourse?.courseAbbr)
                    ttObj.put("room_no", tt?.timetable?.erptimetableroomoffering?.erproom?.roomnumber)
                    ttObj.put("room_name", tt?.timetable?.erptimetableroomoffering?.erproom?.roomname)
                    ttObj.put("slot", obj1?.slot?.time)
                    ttObj.put("time_table_version_id", obj1?.erptimetableversion?.id)
                    timetable.add(ttObj)
                })
            })

            HashMap ttMap = new HashMap()
            ttMap.put("day_sequence", obj.erpDay.daysequence)
            ttMap.put("day", obj.erpDay.day)
            ttMap.put("day_short_name", obj.erpDay.displayname)
            ttMap.put("timetablelist", timetable)
            timetablelist.add(ttMap)
        })


        HashMap body = new HashMap()
        body.put("days", erpDayList)
        body.put("timetable", timetablelist)
//        hm.put("body", data)
        hm.put("body", body)
        hm.put("status", HttpStatus.OK.value().toString())
        render hm as JSON
        return
    }


    def saveonlineclasslink() {
        HashMap hm = new HashMap()
        String body1 =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body1)
        def loginid = request.getHeader("EPC-loginid")
        Login login = Login.findById(loginid)
        if(!login) {
            hm.put("status", "500")
            hm.put("msg","You are logged out! Please login again.")
            render hm as JSON
            return
        }
        def orgid = request.getHeader("EPC-orgid")
        Organization org = Organization.findById(orgid)
        if(!org) {
            hm.put("status", "500")
            hm.put("msg","Organization not found.")
            render hm as JSON
            return
        }
        String ip_addr = request.getRemoteAddr()
        println("ip_addr :: " + ip_addr)
        String tt_version_id =  dataresponse.tt_version_id.toString()
        println("tt_version_id :: " + tt_version_id)
        String timetableid = dataresponse.timetableid.toString()
        println("timetableid  :: " + timetableid )
        String online_class_url = dataresponse.online_class_url.toString()
        println("online_class_url :: " + online_class_url)

        HashMap data = erpTimeTableService.saveOnlineClassUrl(login.username, tt_version_id, timetableid, online_class_url, ip_addr)

        if(data?.flash_error != null) {
            hm.put("status", "500")
            hm.put("msg",hm.flash_error)
            render hm as JSON
            return
        }

        hm.put("status", HttpStatus.OK.value().toString())
        hm.put("message", "Online class URL saved successfully...")
        render hm as JSON
        return
    }
}
