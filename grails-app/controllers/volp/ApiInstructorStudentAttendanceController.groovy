package volp

import grails.converters.JSON
import groovy.json.JsonSlurper
import org.springframework.http.*

import java.text.SimpleDateFormat

class ApiInstructorStudentAttendanceController {

    ApiInstructorLoginService apiInstructorLoginService
    ApiInstructorStudentAttendanceService apiInstructorStudentAttendanceService

    def getCourseList() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def ayId
            def semId
            try {
                String body =  request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                ayId = dataresponse.ayid
                semId = dataresponse.semid
            } catch(Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorStudentAttendanceService.getCourseAttendancetable(login.username, ayId, semId)

            def course_list = []
            data.allclasses.forEach() {
                obj ->
                    HashMap map = new HashMap()
                    map.put("batchinstid", obj?.cls?.id)
                    map.put("cours_code", obj?.cls?.erpcourseofferingbatch?.erpcourseoffering?.erpcourse?.course_code)
                    map.put("cours_name", obj?.cls?.erpcourseofferingbatch?.erpcourseoffering?.erpcourse?.course_name)
                    map.put("load_type", obj?.cls?.erpcourseofferingbatch?.loadtype?.type?.type)
                    map.put("program", obj?.cls?.erpcourseofferingbatch?.erpcourseoffering?.program?.abbrivation)
                    map.put("program_type", obj?.cls?.erpcourseofferingbatch?.erpcourseoffering?.program?.programtype?.displayname)
                    map.put("year", obj?.cls?.erpcourseofferingbatch?.erpcourseoffering?.year?.year)
                    map.put("division", obj?.cls?.erpcourseofferingbatch?.divisionoffering?.division?.name )
                    HashMap batchmap = new HashMap()
                    batchmap.put("name", obj.cls.erpcourseofferingbatch.display_name)
                    batchmap.put("no", obj.cls.erpcourseofferingbatch.batch_number)
                    map.put("batch", batchmap)
                    map.put("students", obj?.count)
                    map.put("lecture_count", obj?.lecnos)
                    map.put("lecture_out_off", obj?.cls?.erpcourseofferingbatch?.expectednooflecture)
                    HashMap inst_map = new HashMap()
                    inst_map.put("id", obj?.cls?.instructor?.id)
                    inst_map.put("code", obj?.cls?.instructor?.employee_code)
                    inst_map.put("name", obj?.cls?.instructor?.person?.fullname_as_per_previous_marksheet)
                    map.put("instructor", inst_map)
                    course_list.add(map)
            }
            HashMap body_map = new HashMap()
            body_map.put("course_list", course_list)
            body_map.put("graph", data.graph)
            body_map.put("roletypesetting", data.roletypesetting)

            hm.put("body", body_map)
            hm.put("msg", "Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    //batchno:3, divisionofferid:2150, controller:ERPCourseOfferingBatchInstructor, format:null, action:getStudentListMarkAttendance, id:14347]
    def getStudentListMarkAttendance() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def batchinstid
            def executeDate
            def topiccovered
            try {
                String body =  request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                batchinstid = dataresponse.batchinstid
                executeDate = dataresponse.executeDate
                topiccovered = dataresponse.topiccovered
            } catch(Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorStudentAttendanceService.getStudentListMarkAttendance(login.username, batchinstid, executeDate, topiccovered)
            if(!data.msg.equals("success")){
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            HashMap slotMap = new HashMap()
            data.slotlist.forEach() {
                obj ->
                    slotMap.put("id", obj.id.toString())
                    slotMap.put("slot", obj.start_time + " - " + obj.end_time)
//                    slotMap.put(obj.id.toString(), obj.start_time + " - " + obj.end_time)
            }

            def rollList = []
            data.rolllist.forEach() {
                obj ->
                    println("obj :: " + obj)
                    println("learnerdivision id :: " + obj[0].id)
                    println("learner roll no :: " + obj[0].rollno)
                    println("learner prn :: " + obj[0].learner.registration_number)
                    println("learner full name :: " + obj[0].learner.person.fullname_as_per_previous_marksheet)

                    println("presented_lect :: " + obj[1])
                    println("total_conducted Lect :: " + obj[2])

                    println("batch learner  id :: " + obj[3].id)

                    HashMap learnerMap = new HashMap()
                    HashMap learnerDivMap = new HashMap()

                    learnerDivMap.put("id", obj[0].id)
                    learnerDivMap.put("roll_no", obj[0].rollno)
                    learnerDivMap.put("prn", obj[0].learner.registration_number)
                    learnerDivMap.put("fullname", obj[0].learner.person.fullname_as_per_previous_marksheet)

                    learnerMap.put("present_lect", obj[1])
                    learnerMap.put("total_conducted_lect", obj[2])
                    learnerMap.put("batch_learner_id", obj[3].id)
                    learnerMap.put("ld", learnerDivMap)
                    rollList.add(learnerMap)
            }

            HashMap instMap = new HashMap()
            instMap.put("id", data?.insbatch?.id)
            instMap.put("instid", data?.insbatch?.instructor?.id)
            instMap.put("code", data?.insbatch?.instructor?.employee_code)
            instMap.put("name", data.insbatch?.instructor?.person?.fullname_as_per_previous_marksheet)

            HashMap batchMap = new HashMap()
            batchMap.put("id", data.batch?.id)
            batchMap.put("batch_no", data.batch?.batch_number)
            batchMap.put("batch_no", data.batch?.batch_number)
            batchMap.put("batch_name", data.batch?.display_name)
            batchMap.put("load_type", data.batch?.loadtype?.type?.type)
            batchMap.put("course_code", data.batch?.erpcourseoffering?.erpcourse?.course_code)
            batchMap.put("course_name", data.batch?.erpcourseoffering?.erpcourse?.course_name)
            batchMap.put("division", data.batch?.divisionoffering?.division?.name)
            batchMap.put("year", data.batch?.divisionoffering?.year?.year)
            batchMap.put("program", data.batch?.divisionoffering?.program?.name)

            HashMap body_map = new HashMap()
//            body_map.put("data", data)
            body_map.put("slot_list", slotMap)
            body_map.put("batch", batchMap)
            body_map.put("total_lectures", data?.totallecture)
            body_map.put("nextLecture_no", data?.lecnos)
            body_map.put("roll_list", rollList)
            body_map.put("instbatch", instMap)

            hm.put("body", body_map)
            hm.put("msg", "Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    def viewLearnerAttendance() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def learnerdivisionid
            def instbatchid
            try {
                String body =  request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                learnerdivisionid = dataresponse.learnerdivisionid
                instbatchid = dataresponse.instbatchid
            } catch(Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorStudentAttendanceService.getLearnerRecord(login.username, learnerdivisionid, instbatchid)
            if(!data.msg.equals("success")){
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            println("data record :: " + data.record)
            println("data ld :: " + data.ld)
            println("data insbatch :: " + data.insbatch)
            println("data learnerrollno :: " + data.learnerrollno)

            HashMap learnerMap = new HashMap()
            learnerMap.put("rollno",data?.learnerrollno?.rollno)
            learnerMap.put("name",data?.learnerrollno?.learner?.person?.fullname_as_per_previous_marksheet)
            learnerMap.put("prn",data?.learnerrollno?.learner?.registration_number)
            learnerMap.put("id",data?.learnerrollno?.id)

            HashMap courseMap = new HashMap()
            courseMap.put("code", data?.insbatch?.erpcourseoffering?.erpcourse?.course_code)
            courseMap.put("name", data?.insbatch?.erpcourseoffering?.erpcourse?.course_name)
            courseMap.put("program", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.program?.name)
            courseMap.put("year", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.year?.year)
            courseMap.put("division", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.division?.name)

            HashMap batchMap = new HashMap()
            batchMap.put("name",data?.insbatch?.erpcourseofferingbatch?.display_name)
            batchMap.put("no",data?.insbatch?.erpcourseofferingbatch?.batch_number)
            batchMap.put("id",data?.insbatch?.id)
            courseMap.put("inst_batch", batchMap)

            def attendanceList = []
            data.record.forEach() {
                obj ->
                    HashMap attendanceMap = new HashMap()
                    attendanceMap.put("date", obj?.execution_date)
                    attendanceMap.put("period_number", obj?.period_number)
                    attendanceMap.put("is_present", obj?.is_present)
                    attendanceMap.put("id", obj?.id)
                    attendanceList.add(attendanceMap)
            }

            HashMap body_map = new HashMap()
            body_map.put("learner", learnerMap)
            body_map.put("course", courseMap)
            body_map.put("attendance", attendanceList)

            hm.put("body", body_map)
            hm.put("msg", "Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    //updateLearnerRecord :: [
//                insbatch:14347, // inst batch id
//                e:460562, // learner div id
//                idlist:[6515207, 6515234, 6807777, 6807804, 6807831, 6883334, 6949905, 7042919, 7150981, 7238977],  // learner attendace id list all
//                checkedValue:[6515207, 6515234, 6807777, 6807804, 6807831, 7042919, 7150981, 7238977], // is present learner attendace id list
//                Save:Save, controller:ERPCourseOfferingBatchInstructor, format:null, action:updateLearnerRecord]
    def updateLearnerAttendance() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def presentbatchlearnerattendanceid
            def learnerdivisionid
            def batchlearnerattendanceid
            def instbatchid
            try {
                String body =  request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                batchlearnerattendanceid = dataresponse.batchlearnerattendanceid
                presentbatchlearnerattendanceid = dataresponse.presentbatchlearnerattendanceid
                learnerdivisionid = dataresponse.learnerdivisionid
                instbatchid = dataresponse.instbatchid
            } catch(Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorStudentAttendanceService.updateLearnerRecord(login.username, presentbatchlearnerattendanceid, batchlearnerattendanceid, request.getRemoteAddr())
            if(!data.msg.equals("success")){
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            HashMap body_map = new HashMap()
            body_map.put("learnerdivisionid", learnerdivisionid)
            body_map.put("instbatchid", instbatchid)

            hm.put("body", body_map)
            hm.put("msg", "Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    def getCourseAttendancetable() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def nonedittable
            def instbatchid
            try {
                String body =  request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                nonedittable = dataresponse.nonedittable
                instbatchid = dataresponse.instbatchid
            } catch(Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorStudentAttendanceService.editStudentListMarkAttendance(login.username, instbatchid, nonedittable)
            if(!data.msg.equals("success")){
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            HashMap batchMap = new HashMap()
            batchMap.put("no", data?.insbatch?.erpcourseofferingbatch?.batch_number)
            batchMap.put("name", data?.insbatch?.erpcourseofferingbatch?.display_name)
            HashMap courseMap = new HashMap()
            courseMap.put("code", data?.insbatch?.erpcourseoffering?.erpcourse?.course_code)
            courseMap.put("name", data?.insbatch?.erpcourseoffering?.erpcourse?.course_name)
            courseMap.put("program", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.program?.name)
            courseMap.put("year", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.year?.year)
            courseMap.put("batch", batchMap)
            courseMap.put("division", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.division?.name)

            def studentAttendanceList = []
            data.listArray.forEach() {
                obj ->
                    println("obj :: " + obj)
                    println("period no :: " + obj?.displayperiodno)
                    println("slot :: " + obj?.slot)
                    println("total student :: " + obj?.totalstudent)
                    println("present student :: " + obj?.presentstudent)
                    println("absent student :: " + obj?.absentStudent)
                    println("present persent :: " + obj?.presentpersent)
                    println("date :: " + obj?.period?.execution_date)

                    HashMap attendanceMap = new HashMap()
                    attendanceMap.put("period_no", obj?.displayperiodno)
                    attendanceMap.put("slot", obj?.slot)
                    attendanceMap.put("total_students", obj?.totalstudent)
                    attendanceMap.put("present_students", obj?.presentstudent)
                    attendanceMap.put("absent_students", obj?.absentStudent)
                    attendanceMap.put("present_percentage", obj?.presentpersent)
                    attendanceMap.put("date", obj?.period?.execution_date)
                    attendanceMap.put("id", obj?.period?.id)

                    studentAttendanceList.add(attendanceMap)
            }

            HashMap body_map = new HashMap()
            body_map.put("student_attendance_list", studentAttendanceList)
            body_map.put("instbatchid", instbatchid)
            body_map.put("course", courseMap)

            hm.put("body", body_map)
            hm.put("msg", "Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    def editStudentAttendanceRecords() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def period_number
            def lec
            def instbatchid
            try {
                String body =  request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                period_number = dataresponse.period_number
                lec = dataresponse.lec
                instbatchid = dataresponse.instbatchid
            } catch(Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorStudentAttendanceService.editStudentAttendanceRecords(login.username, instbatchid, period_number, lec)
            if(!data.msg.equals("success")){
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            HashMap periodMap = new HashMap()
            periodMap.put("period_number", data?.period?.period_number)
            periodMap.put("id", data?.period?.id)

            def learnerList = []
            data.studentlist.forEach() {
                obj ->
                    println("obj :: " + obj)
                    HashMap learnerMap = new HashMap()
                    learnerMap.put("batch_learner_id", obj?.student?.id)
                    learnerMap.put("roll_no", obj?.rollnumber?.rollno)
                    learnerMap.put("registration_no", obj?.rollnumber?.learner?.registration_number)
                    learnerMap.put("name", obj?.rollnumber?.learner?.person?.fullname_as_per_previous_marksheet)
                    learnerMap.put("attended_lecture", obj?.totallecture)
                    learnerMap.put("ispresent", obj?.ispresent)

                    learnerList.add(learnerMap)
            }

            HashMap batchMap = new HashMap()
            batchMap.put("id", data?.insbatch?.erpcourseofferingbatch?.id)
            batchMap.put("no", data?.insbatch?.erpcourseofferingbatch?.batch_number)
            batchMap.put("name", data?.insbatch?.erpcourseofferingbatch?.display_name)
            HashMap courseMap = new HashMap()
            courseMap.put("code", data?.insbatch?.erpcourseoffering?.erpcourse?.course_code)
            courseMap.put("name", data?.insbatch?.erpcourseoffering?.erpcourse?.course_name)
            courseMap.put("program", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.program?.name)
            courseMap.put("year", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.year?.year)
            courseMap.put("batch", batchMap)
            courseMap.put("division", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.division?.name)

            def instList = []
            data.facultylist.forEach() {
                obj ->
                    HashMap instMap = new HashMap()
                    instMap.put("id", obj.id)
                    instMap.put("code", obj.employee_code)
                    instMap.put("name", obj.person?.fullname_as_per_previous_marksheet)
                    instList.add(instMap)
            }

            HashMap currentSlotMap = new HashMap()
            data.attslot.forEach() {
                obj ->
                    currentSlotMap.put(obj.id.toString(), obj.start_time + " - " + obj.end_time)
            }

            HashMap slotMap = new HashMap()
            data.slotlist.forEach() {
                obj ->
                    println("obj :: " + obj.slotlist.id)
                    slotMap.put(obj.slotlist.id.toString(), obj.slotlist.start_time + " - " + obj.slotlist.end_time)
            }

            HashMap body_map = new HashMap()
            body_map.put("slot_list", slotMap)
            body_map.put("allotted_slot", currentSlotMap)
            body_map.put("totallecture", data?.totallecture)
            body_map.put("lec", data?.lec)
            body_map.put("roletypesettingpracticalhr", data?.roletypesettingpracticalhr)
            body_map.put("course", courseMap)
            body_map.put("instbatchid", instbatchid)
            body_map.put("inst_list", instList)
            body_map.put("learner_list", learnerList)

            hm.put("body", body_map)
            hm.put("msg", "Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }

    }

    def saveAttendance() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def insbatchid
            def instructorid
            def slotidlist
            def date
            def batchid
            def present_learnerdivisiondid
            def learnerdivisiondid
//
//        params :: [checkedInst:345, _chkslotid:[, , , , , , , , , , ], chkslotid:186, lecdate:date.struct, lecdate_day:17, lecdate_month:2, lecdate_year:2022, noofhours:1, nooflect:1, lec:11.0
//                   , batchid:29447, insbatchid:14347, untick:on, ldid:[460562, 460564, 460566, 460568, 460570, 460580, 460586, 460592, 460596, 460598, 460602, 460620, 460622, 460634, 464448, 464452, 4644
//                                                                       70, 464472, 464476, 464480, 464484, 467888, 467894, 473307, 473311, 473313, 480161], _pldid:[, , , , , , , , , , , , , , , , , , , , , , , , , , ], pldid:[460562, 460564, 460566, 46056
//                                                                                                                                                                                                                                  8, 460570, 460580, 460586, 460592, 460596, 460598, 460602, 460620, 460622, 460634, 464448, 464452, 464470, 464472, 464476, 464480, 464484, 467888, 467894, 473307], controller:ERPCourse
//                   OfferingBatchInstructor, format:null, action:saveAttendance]

            try {
                String body =  request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                insbatchid = dataresponse.insbatchid
                instructorid = dataresponse.instructorid
                slotidlist = dataresponse.slotidlist
                date = dataresponse.date
                batchid = dataresponse.batchid
                present_learnerdivisiondid = dataresponse.present_learnerdivisiondid
                learnerdivisiondid = dataresponse.learnerdivisiondid
            } catch(Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
            Date formateddate = formatter.parse(date)

            HashMap data = apiInstructorStudentAttendanceService.saveAttendance(login.username, insbatchid, instructorid, slotidlist, formateddate, batchid, present_learnerdivisiondid, learnerdivisiondid, request.getRemoteAddr())
            if(!data.msg.equals("success")){
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

//            HashMap periodMap = new HashMap()
//            periodMap.put("period_number", data?.period?.period_number)
//            periodMap.put("id", data?.period?.id)
//
//            def learnerList = []
//            data.studentlist.forEach() {
//                obj ->
//                    HashMap learnerMap = new HashMap()
//                    learnerMap.put("batch_learner_id", obj?.student?.id)
//                    learnerMap.put("roll_no", obj?.rollnumber?.rollno)
//                    learnerMap.put("registration_no", obj?.rollnumber?.learner?.registration_number)
//                    learnerMap.put("name", obj?.rollnumber?.learner?.person?.fullname_as_per_previous_marksheet)
//                    learnerMap.put("attended_lecture", obj?.totallecture)
//                    learnerMap.put("ispresent", obj?.ispresent)
//
//                    learnerList.add(learnerMap)
//            }
//
//            HashMap batchMap = new HashMap()
//            batchMap.put("no", data?.insbatch?.erpcourseofferingbatch?.batch_number)
//            batchMap.put("name", data?.insbatch?.erpcourseofferingbatch?.display_name)
//            HashMap courseMap = new HashMap()
//            courseMap.put("code", data?.insbatch?.erpcourseoffering?.erpcourse?.course_code)
//            courseMap.put("name", data?.insbatch?.erpcourseoffering?.erpcourse?.course_name)
//            courseMap.put("program", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.program?.name)
//            courseMap.put("year", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.year?.year)
//            courseMap.put("batch", batchMap)
//            courseMap.put("division", data?.insbatch?.erpcourseofferingbatch?.divisionoffering?.division?.name)
//
//            def instList = []
//            data.facultylist.forEach() {
//                obj ->
//                    HashMap instMap = new HashMap()
//                    instMap.put("id", obj.id)
//                    instMap.put("code", obj.employee_code)
//                    instMap.put("name", obj.person?.fullname_as_per_previous_marksheet)
//                    instList.add(instMap)
//            }
//
//            HashMap currentSlotMap = new HashMap()
//            data.attslot.forEach() {
//                obj ->
//                    currentSlotMap.put(obj.id.toString(), obj.start_time + " - " + obj.end_time)
//            }
//
//            HashMap slotMap = new HashMap()
//            data.slotlist.forEach() {
//                obj ->
//                    println("obj :: " + obj.slotlist.id)
//                    slotMap.put(obj.slotlist.id.toString(), obj.slotlist.start_time + " - " + obj.slotlist.end_time)
//            }
//
//            HashMap body_map = new HashMap()
//            body_map.put("slot_list", slotMap)
//            body_map.put("allotted_slot", currentSlotMap)
//            body_map.put("totallecture", data?.totallecture)
//            body_map.put("lec", data?.lec)
//            body_map.put("roletypesettingpracticalhr", data?.roletypesettingpracticalhr)
//            body_map.put("course", courseMap)
//            body_map.put("instbatchid", instbatchid)
//            body_map.put("inst_list", instList)
//            body_map.put("learner_list", learnerList)

//            hm.put("body", body_map)
            hm.put("msg", "Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    def updateStudentAttendance() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def insbatchid
            def instructorid
            def slotidlist
            def date
            def batchid
            def present_learnerdivisiondid
            def learnerdivisiondid
            def period_no
            try {
                String body =  request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                insbatchid = dataresponse.insbatchid
                instructorid = dataresponse.instructorid
                slotidlist = dataresponse.slotidlist
                date = dataresponse.date
                batchid = dataresponse.batchid
                present_learnerdivisiondid = dataresponse.present_learnerdivisiondid
                learnerdivisiondid = dataresponse.studentidlist
                period_no = dataresponse.period_no
            } catch(Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
            Date formateddate = formatter.parse(date)

            HashMap data = apiInstructorStudentAttendanceService.updateStudentAttendance(login.username, request.getRemoteAddr(), insbatchid, present_learnerdivisiondid, slotidlist, instructorid, formateddate, period_no.toString(), period_no, learnerdivisiondid)
            if(!data.msg.equals("Success")){
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            hm.put("msg", "Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }

    }

}
