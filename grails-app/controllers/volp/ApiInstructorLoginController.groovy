package volp

import grails.converters.JSON
import groovy.json.JsonSlurper
import org.springframework.http.HttpStatus

import java.util.regex.Matcher
import java.util.regex.Pattern

class ApiInstructorLoginController {

    PasswordEncryptionService passwordEncryptionService
    ApiInstructorLoginService apiInstructorLoginService
    SendMailService sendMailService

    def login() {
        try {
            String username = params.username.replaceAll(" ", "")
            String password = params.password
            HashMap hm = new HashMap()

            // check valid email id
            if(!apiInstructorLoginService.checkValidEmailId(username)) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "Please enter correct Email Id. ")
                render hm as JSON
                return
            }

            Login login = Login.findByUsernameAndPassword(username, password)
            if (login == null) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "Invalid Username and Password")
                render hm as JSON
                return
            } else {
                if (login.isloginblocked == true) {
                    hm.put("status", HttpStatus.LOCKED.value().toString())
                    hm.put("msg", "Sorry, Your account is BLOCKED!!!")
                    render hm as JSON
                    return
                } else {
                    //This is successful login
                    Random rand = new Random();
                    int x = rand.nextInt(100);
                    String token = "123" //passwordEncryptionService.encrypwd(x.toString())
                    login.access_token = token
                    login.save(failOnError: true, flush: true)
                    def inst = Instructor.findByUid(login.username)
                    AWSBucket awsBucket = AWSBucket.findByContent("documents")
                    AWSFolderPath awsFolderPath = AWSFolderPath.findById(5)
                    AWSBucketService awsBucketService = new AWSBucketService()
                    if (inst != null) {
                        //only for instructor

                        hm.put("status", HttpStatus.OK.value().toString())
                        hm.put("msg", "Successfully logged-in")
                        hm.put("loginid", login.id)
                        hm.put("access_token", login.access_token)

                        HashMap dept_map = new HashMap()
                        dept_map.put("department_id", inst?.department?.id)
                        dept_map.put("department", inst?.department?.name)
                        dept_map.put("department_abbrivation", inst?.department?.abbrivation)
                        hm.put("department", dept_map)

                        HashMap orgMap = new HashMap()
                        orgMap.put("organization_id", inst.organization?.id)
                        orgMap.put("organization_name", inst.organization?.organization_name)
                        orgMap.put("organization_display_name", inst.organization?.display_name)
                        orgMap.put("organization_organization_detailed_name", inst.organization?.organization_detailed_name)
                        orgMap.put("organization_address", inst.organization?.address)
                        hm.put("organization", orgMap)

                        HashMap inst_map = new HashMap()
                        inst_map.put("code", inst?.employee_code)
                        inst_map.put("email", login?.username)
                        inst_map.put("designation", inst?.designation?.name)
                        inst_map.put("name", inst?.person?.fullname_as_per_previous_marksheet)
                        if (inst?.person?.firstName != null || inst?.person?.lastName != null)
                            inst_map.put("flname", inst?.person?.firstName + " " + inst?.person?.lastName)
                        else
                            inst_map.put("flname", "NA")

                        Facultyidcard facultyidcard = Facultyidcard.findByInstructorAndOrganization(inst, inst.organization)
                        if (facultyidcard) {
                            if (facultyidcard.icardPath && facultyidcard.icardPhotoFile) {
                                def path = awsFolderPath.path + facultyidcard.icardPath + facultyidcard.icardPhotoFile
                                def url = awsBucketService.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
                                inst_map.put("profilephoto", url)
                            } else {
                                inst_map.put("profilephoto", "https://www.kindpng.com/picc/m/252-2524695_dummy-profile-image-jpg-hd-png-download.png")
                            }
                        } else {
                            inst_map.put("profilephoto", "https://www.kindpng.com/picc/m/252-2524695_dummy-profile-image-jpg-hd-png-download.png")
                        }
                        hm.put("instructor", inst_map)

                        render hm as JSON
                        return
                    } else {
                        hm.put("status", HttpStatus.NO_CONTENT.value().toString())
                        hm.put("msg", "Not found your details, Please contact with ERP-coordinator.")
                        hm.put("loginid", login.id)
                        hm.put("access_token", null)

                        hm.put("department", null)
                        hm.put("organization", null)
                        hm.put("instructor", null)

                        render hm as JSON
                        return
                    }
                }
            }
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }

    def verifyemail() {
        try {
            HashMap hm = new HashMap()
            String email
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                email = dataresponse.email
            } catch(Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }
            email = email.replaceAll(" ", "")

            // check valid email id
            if(!apiInstructorLoginService.checkValidEmailId(email)) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "Please enter correct Email Id.")
                render hm as JSON
                return
            }

            Login login = Login.findByUsername(email)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "Invalid Username and Password")
                render hm as JSON
                return
            }

            Instructor inst = Instructor.findByUid(email)

            if (inst == null) {
                hm.put("msg", "Not found your details, Please contact with ERP-coordinator.")
                hm.put("status", HttpStatus.NO_CONTENT.value().toString())
                render hm as JSON
                return
            } else {

                //Generate OTP
                String otp = apiInstructorLoginService.generatedOTP(email)
                println("otp :: " + otp)
                if(otp.equals("000000")) {
                    hm.put("msg", "OTP Generation failed.. Please try again..")
                    hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
                    render hm as JSON
                    return
                }
                println("1Now sending mail of otp...." + otp)

                try {
                    String msg = "Your OTP is " + otp + " \n" + "Thanks, EduPlusCampus Team.."
                    def establishment_email
                    def establishment_email_credentials
                    def username
                    def organization_code

                    if (inst) {
                        establishment_email = inst?.organization?.establishment_email
                        establishment_email_credentials = inst?.organization?.establishment_email_credentials
                        username = login?.username
                        organization_code = inst?.organization?.organization_code + "Forgot Password Otp"
                    }
//                    boolean value = true
                    boolean value = sendMailService.sendmailwithcss(establishment_email, establishment_email_credentials, username, organization_code, msg, "", "")
                    if (value) {
                        hm.put("msg", "Otp Send to your Official Email.")
                        hm.put("status", HttpStatus.OK.value().toString())
                        render hm as JSON
                        return
                    } else {
                        hm.put("msg", "Email send failed.. Please try again")
                        hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
                        render hm as JSON
                        return
                    }
                } catch (Exception e) {
                    hm.put("msg", "Email send failed.. Please try again")
                    hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
                    render hm as JSON
                    return
                }
            }
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }

    def verifyotp() {
        try {
            HashMap hm = new HashMap()
            String email
            String otp
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                email = dataresponse.email
                otp = dataresponse.otp
            } catch(Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }
            email = email.replaceAll(" ", "")

            // check valid email id
            if(!apiInstructorLoginService.checkValidEmailId(email)) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "Please enter correct Email Id.")
                render hm as JSON
                return
            }

            Otp otpobj = Otp.findByEmail(email)
            println("otpobj :: " + otpobj)
            if(otpobj) {
                if(!otpobj.otp.equals(otp)) {
                    hm.put("status", HttpStatus.UNAUTHORIZED.value().toString()) //otp expire
                    hm.put("msg","OTP not matched..")
                    render hm as JSON
                    return
                }
                Date otpgenerationtime = otpobj.otpgenerationtime
                Date currenttime = new java.util.Date()
                long diff = currenttime.getTime() - otpgenerationtime.getTime() ;
                long diffInMinutes = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(diff);
                if(diffInMinutes<=30) {
                    hm.put("email", email)
                    hm.put("status", HttpStatus.OK.value().toString()) //otp verified
                    hm.put("msg","OTP verification done")
                    render hm as JSON
                    return
                } else {
                    hm.put("status", HttpStatus.UNAUTHORIZED.value().toString()) //otp expire
                    hm.put("msg","OTP time out. Please try again..")
                    render hm as JSON
                    return
                }
            } else {
                hm.put("status",HttpStatus.NOT_FOUND.value.toString()) //otp not match
                hm.put("msg", "Generated OTP not found.. Please try again..")
                render hm as JSON
                return
            }
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }

    def savechangepassword() {
        try {
            HashMap hm = new HashMap()
            String email
            String newpassword
            String confirmpassword
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                email = dataresponse.email
                newpassword = dataresponse.newpassword
                confirmpassword = dataresponse.confirmpassword
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }
            email = email.replaceAll(" ", "")

            // check valid email id
            if (!apiInstructorLoginService.checkValidEmailId(email)) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "Please enter correct Email Id.")
                render hm as JSON
                return
            }

            Login login = Login.findByUsername(email.replaceAll(" ", ""))
            if(!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "Please enter correct Email Id.")
                render hm as JSON
                return
            }

            boolean isUpdated = apiInstructorLoginService.compareAndSavePassword(login,  newpassword.toString(), confirmpassword.toString() )

            if (isUpdated) {
                hm.put("status", HttpStatus.OK.value().toString())
                hm.put("msg", "Password Successfully changed.")//password change successfully
                render hm as JSON
                return
            } else {
                hm.put("status", HttpStatus.NOT_ACCEPTABLE.value().toString())
                hm.put("msg", "New and Confirm Password fields are not matched..") //password not match
                render hm as JSON
                return
            }
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }

    def changepassword() {
        try {
            HashMap hm = new HashMap()

            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            String email
            String oldpassword
            String newpassword
            String confirmpassword
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                email = dataresponse.email
                newpassword = dataresponse.newpassword
                confirmpassword = dataresponse.confirmpassword
                oldpassword = dataresponse.oldpassword
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }
            email = email.replaceAll(" ", "")

            // check valid email id
            if (!apiInstructorLoginService.checkValidEmailId(email)) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "Please enter correct Email Id.")
                render hm as JSON
                return
            }

            Login loginCheck = Login.findByUsernameAndPassword(email.replaceAll(" ", ""), oldpassword)
            if(!loginCheck) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "Old Password are not correct..")
                render hm as JSON
                return
            }

            boolean isUpdated = apiInstructorLoginService.compareAndSavePassword(loginCheck,  newpassword.toString(), confirmpassword.toString() )

            if (isUpdated) {
                hm.put("status", HttpStatus.OK.value().toString())
                hm.put("msg", "Password Successfully changed.")//password change successfully
                render hm as JSON
                return
            } else {
                hm.put("status", HttpStatus.NOT_ACCEPTABLE.value().toString())
                hm.put("msg", "New and Confirm Password fields are not matched..") //password not match
                render hm as JSON
                return
            }
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }
}
