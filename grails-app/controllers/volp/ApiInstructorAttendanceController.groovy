package volp

import grails.converters.JSON
import groovy.json.JsonSlurper
import org.springframework.http.HttpStatus

class ApiInstructorAttendanceController {

    ApiInstructorAttendanceService apiInstructorAttendanceService
    ApiInstructorLoginService apiInstructorLoginService

    // location registration
    def getLocationInformation() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorAttendanceService.getalllocationinformationbyorg(orgid)
            if (!data.msg.equals("success")) {
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            HashMap map = new HashMap()
            map.put("location_list", data.location_coordinate_list)

            hm.put("data", map)
            hm.put("msg", "Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    def getalllocationmaster() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorAttendanceService.getalllocationmaster(orgid)
            if (!data.msg.equals("success")) {
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            HashMap masterMap = new HashMap()
            data.location_coordinate_master_list.forEach() {
                obj ->
                    masterMap.put(obj?.id, obj?.location)
            }
            HashMap map = new HashMap()
            map.put("location_master_list", data.location_coordinate_master_list)
            map.put("location_master_drop_down_list", masterMap)

            hm.put("data", map)
            hm.put("msg", "Success")
            hm.put("status", HttpStatus.OK.value().toString())
            println("hm :: " + hm)
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    def deletelocationmaster() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def masterid
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                masterid = dataresponse.locationmasterid
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorAttendanceService.deletelocationmaster(masterid, org)
            if (!data.msg.equals("success")) {
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            hm.put("msg", "Delete Successfully..")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    def getalllocationinformation() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def masterid
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                masterid = dataresponse.locationmasterid
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorAttendanceService.getalllocationinformation(masterid, org)
            if (!data.msg.equals("success")) {
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }
            HashMap map = new HashMap()
            map.put("location_master_list", data.location_coordinate_list)

            hm.put("data", map)
            hm.put("msg", "Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    def registerlocationmaster() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def masterid
            def location_name
            def latitude
            def longitude
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                masterid = dataresponse.locationmasterid
                location_name = dataresponse.location_name
                latitude = dataresponse.latitude.toString()
                longitude = dataresponse.longitude.toString()
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorAttendanceService.registerlocationmaster(masterid, org, location_name, latitude, longitude, login.username, request.getRemoteAddr())
            if (!data.msg.equals("Successfully added") && !data.msg.equals("Successfully updated") ) {
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }
            hm.put("msg", data.msg)
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    def getRegEmployeeList() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def masterid
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                masterid = dataresponse.locationmasterid
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorAttendanceService.getRegEmployeeList(masterid)
            if (!data.msg.equals("success")) {
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }
            HashMap map = new HashMap()
            map.put("employee_list", data.employeelist)

            hm.put("data", map)
            hm.put("msg", data.msg)
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    def getemployeeinfo() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def username
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                username = dataresponse.username
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorAttendanceService.getemployeeinfo(username, org)
            if (!data.msg.equals("success")) {
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            HashMap map = new HashMap()
            map.put("employee_info", data)

            hm.put("data", map)
            hm.put("msg", data.msg)
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }

    def savemobiledeviceinfo() {
        println("savemobiledeviceinfo :: " )
//        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def username
            def mobilebiometriclocationid
            def imeino
            def mobileno
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                username = dataresponse.username
                mobilebiometriclocationid = dataresponse.mobilebiometriclocationid
                imeino = dataresponse.primarymobileno
                mobileno = dataresponse.secondarymobileno
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorAttendanceService.savemobiledeviceinfo(username, imeino, mobileno, org, mobilebiometriclocationid, request.getRemoteAddr())
            if (data.msg.equals("Location Not Found") || data.msg.equals("Primary Mobile no not found...") || data.msg.equals("IMEI no not found...") || data.msg.equals("Instructor not Found")) {
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            hm.put("msg", data.msg)
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
//        } catch (Exception e) {
//            HashMap hm = new HashMap()
//            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
//            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
//            render hm as JSON
//            return
//        }
    }

    def getRegisteredMobileLocation() {
//        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def mobileno
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                mobileno = dataresponse.mobileno
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorAttendanceService.getRegisteredMobileLocation(mobileno, org)
            if (!data.msg.equals("success")) {
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            HashMap map = new HashMap()
            map.put("locationArray", data?.locationArray)
            map.put("compairGeolocation", data?.compairGeolocation)

            hm.put("data", map)
            hm.put("msg", data.msg)
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
//        } catch (Exception e) {
//            HashMap hm = new HashMap()
//            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
//            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
//            render hm as JSON
//            return
//        }
    }

    def markMobileAppAttendance() {
//        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def username
            def imeino
            def mobileno
            def latitude
            def longitude
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                username = dataresponse.username
                imeino = dataresponse.primarymobileno
                latitude = dataresponse.latitude
                longitude = dataresponse.longitude
                mobileno = dataresponse.mobileno
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorAttendanceService.markMobileAppAttendance(username, org, imeino, latitude, longitude, request.getRemoteAddr())
            if (data.msg.equals("User Not Found") || data.msg.equals("User Not Registered On This Device") || data.msg.equals("Internal Server Error..") || data.msg.equals("User Location Does not Match with Registered Department Location, Please make sure your GPS locations is on..")) {
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }

            HashMap map = new HashMap()
            map.put("profile_photo", data?.profile_photo)
            map.put("name", data?.name)
            map.put("intime", data?.intime)
            map.put("outtime", data?.outtime)
            map.put("totaltime", data?.totaltime)

            hm.put("data", map)
            hm.put("msg", data.msg)
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
//        } catch (Exception e) {
//            HashMap hm = new HashMap()
//            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
//            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
//            render hm as JSON
//            return
//        }
    }

    def editregisterlocationmastername() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def masterid
            def location_name
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                masterid = dataresponse.locationmasterid
                location_name = dataresponse.location_name
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorAttendanceService.editregisterlocationmastername(masterid, org, location_name, login.username, request.getRemoteAddr())
            if (!data.msg.equals("Successfully updated location master name")) {
                hm.put("msg", data.msg)
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                render hm as JSON
                return
            }
            hm.put("msg", data.msg)
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch (Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", "INTERNAL SERVER ERROR..! Please try again.. ")
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            render hm as JSON
            return
        }
    }


}
