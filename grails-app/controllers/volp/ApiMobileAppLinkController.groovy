package volp

import grails.converters.JSON
import groovy.json.JsonSlurper
import org.springframework.http.HttpStatus

class ApiMobileAppLinkController {

    ApiInstructorLoginService apiInstructorLoginService
    ApiMobileAppLinkService apiMobileAppLinkService

    def mobileLinks() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            println(apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token")))
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def mobileAppLinkList = apiMobileAppLinkService.getMobileLinks(login.username, org)
            def biometricLinkList = apiMobileAppLinkService.getBiometricLinks(login.username, org)
            def ayMap = apiMobileAppLinkService.getAyList()
            def semMap = apiMobileAppLinkService.getSemList(org)

            hm.put("biometric", biometricLinkList)
            hm.put("aylist", ayMap)
            hm.put("semlist", semMap)

            hm.put("status", HttpStatus.OK.value().toString())
            hm.put("msg", "Success")
            hm.put("Mobile_app_link_list", mobileAppLinkList)
            render hm as JSON
            return
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }

    def getCurrentAySem() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }
            def roleTypeName
            try {
                String body1 = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body1)
                roleTypeName = dataresponse.roletypename
            } catch(Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            def map = apiMobileAppLinkService.getCurrentApplicationAcademicYear(roleTypeName.trim(), org)
            hm.put("data", map)
            hm.put("status", HttpStatus.OK.value().toString())
            hm.put("msg", "success")
            render hm as JSON
            return
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }
}
