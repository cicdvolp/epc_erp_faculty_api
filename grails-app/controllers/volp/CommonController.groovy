package volp

class CommonController {

    def index() { }
    //paste in controller action
    /*
   def programtypelist = ProgramType.findAllByOrganization(org).name

   [programtypelist:programtypelist]
    */
    //paste in div in gsp
    /*
    <div onload="updatePY1()" class="col-sm-3">
            <label for="pwd">Program Type :<span style="color:red; font-size: 16px">*</span></label>
            <g:select id="programtype"  name="programtype" class="form-control"  from="${programtypelist}"  onchange="updatePY();updatePY1()"  />
     </div>
     */

    // <span id="year"></span>
    //  <span id="program"></span>

    //paste in script
    /*
         window.onload = function() {
            updatePY();
        };
    function updatePY(){
        var d = document.getElementById("programtype").value;
        <g:remoteFunction controller="Common" action="getprogramtype" update="program" params="'programtype='+d"/>
    }
     function updatePY1(){
         var d = document.getElementById("programtype").value;
         var d1 = document.getElementById("programyash1").value;
         <g:remoteFunction controller="Common" action="getprogramyear" update="year" params="'programtype='+d+'&program='+d1"/>
    }
    */

    def getprogramtype()
    {
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
//        println("params " +params)
        def programtype=null
        def programlist=null
        if(params.programtype=="All")
        {
            programlist=Program.findAllByOrganization(instructor.organization).name
        }
        else {
            programtype=ProgramType.findByNameAndOrganization(params.programtype,instructor.organization)
            programlist=Program.findAllByOrganizationAndProgramtype(instructor.organization,programtype).name
        }
        render g.select(id:'programyash1',required:"true", name:"program_name", class:"form-control" , from:programlist  ,noSelection:['':'Please Select'], onChange:'updatePY1()' )
    }
    def getprogramyear()
    {
        println("getprogramyear123 :: " + params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def yearlist=null
        def program=null
        if(params.program=="") {
            yearlist=Year.findAllByOrganization(instructor.organization)
        }
//        def programtype=null
//        if(params.program=='All' || params.program=='0')
//        {
//            programtype = ProgramType.findByName(params.programtype)
//            println("programtype :: " + programtype)
////            yearlist=Year.findAllByOrganization(instructor.organization)
//            yearlist=ProgramTypeYear.findAllByProgramtypeAndOrganization(programtype,instructor.organization)?.year
//            println("yearlist 1 : " + yearlist)
//        }
        else {
            println("///////////////////////")
            program=Program.findByNameAndOrganization(params.program,instructor.organization)
            yearlist=ProgramTypeYear.findAllByProgramtypeAndOrganization(program?.programtype,instructor.organization)?.year
            println("yearlist 2 : " + yearlist)
        }
        render g.select(id:'yearyash1',required:"true", name:"year", class:"form-control" , from:yearlist ,  optionKey: 'id', optionValue: 'display_name',   )
    }
    def getprogramtypeid()
    {
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
//        println("params " +params)
        def programtype=null
        def programlist=null
        if(params.programtype=="All")
        {
            programlist=Program.findAllByOrganization(instructor.organization)
        }
        else {
            programtype=ProgramType.findById(params.programtype)
            programlist=Program.findAllByOrganizationAndProgramtype(instructor.organization,programtype)
        }
        render g.select(id:'programyash1',required:"true", name:"program_name", class:"form-control" , from:programlist  ,noSelection:['':'Please select'], onChange:'updatePY1()',optionKey: 'id', optionValue: 'name' )
    }
    def getprogramyearid()
    {
        println("getprogramyear :: " + params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def yearlist=null
        def program=null
        if(params.program=='')
        {
            yearlist=Year.findAllByOrganization(instructor.organization)
        }
        else {
            println("///////////////////////")
            program=Program.findById(params.program)
            yearlist=ProgramTypeYear.findAllByProgramtypeAndOrganization(program?.programtype,instructor.organization)?.year
        }
        render g.select(id:'yearyash1',required:"true", name:"year", class:"form-control" , from:yearlist , optionKey: 'id', optionValue: 'display_name',   )
    }
    /*
   def programtypelist = ProgramType.findAllByOrganization(org).name

   [programtypelist:programtypelist]
   <g:select id="programtype"  name="programtype" class="form-control"  from="${programtypelist}"  onchange="updatePY()"  noSelection="['All':'All']"/>
  // <span id="year"></span>
    //  <span id="program"></span>

    window.onload = function() {
  updatePY1()
            updatePY();

        };
  function updatePY(){
                     var d = document.getElementById("programtype").value;
                     <g:remoteFunction controller="Common" action="getprogramtypeall" update="program" params="'programtype='+d"/>
                }
                 function updatePY1(){
                     var d = document.getElementById("programtype").value;
                     var d1 = document.getElementById("programyash1");
                       if(d1!=null)
                        d1 = document.getElementById("programyash1").value;
                     <g:remoteFunction controller="Common" action="getprogramyearall" update="year" params="'programtype='+d+'&program='+d1"/>
                };


    */

    def getprogramtypeall()
    {
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
//        println("params " +params)
        def programtype=null
        def programlist=null
        if(params.programtype=="All")
        {

            programlist=Program.findAllByOrganization(instructor.organization).name

        }
        else {
            programtype=ProgramType.findByNameAndOrganization(params.programtype,instructor.organization)
            programlist=Program.findAllByOrganizationAndProgramtype(instructor.organization,programtype).name
        }

        render g.select(id:'programyash1', name:"program_name", class:"form-control" , from:programlist ,noSelection:['All':'All'] ,onChange:'updatePY1()', onload:'updatePY1()' )

    }
    def getprogramyearall()
    {
        println("yash")
        println("params " +params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def yearlist=null
        def program=null

        if(params.program=='All'||params.program=='null')
        {
            yearlist=Year.findAllByOrganization(instructor.organization)
        }
        else {
            //    println("///////////////////////")
            program=Program.findByNameAndOrganization(params.program,instructor.organization)
            yearlist=ProgramTypeYear.findAllByProgramtypeAndOrganization(program?.programtype,instructor.organization)?.year
        }

        render g.select(id:'yearyash1', name:"year", class:"form-control" , from:yearlist ,noSelection:['All':'All'],optionKey: 'id', optionValue: 'display_name'  )

    }

    def getleavetypebyorganization()
    {
        if (session.loginid == null) {
            ////println("Your are logged out! Please login again. Thank you.")
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            ////println("I am in establishment_leave_report")
            Login login = Login.findById(session.loginid)
            Organization org =Organization.findById(params.organization)
            def lt = LeaveType.findAllByOrganization(org)
            render g.select(id:'leavetype', name:"leavetype", optionKey: 'id', optionValue: 'type', class:"form-control" , from:lt  ,noSelection:['':'All'],  )
        }
    }
    def getdeptmentbyorganization()
    {
        if (session.loginid == null) {
            ////println("Your are logged out! Please login again. Thank you.")
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            ////println("I am in establishment_leave_report")
            Login login = Login.findById(session.loginid)
            Organization org =Organization.findById(params.organization)
            def dept = Department.findAllByOrganization(org)
            render g.select(id:'dept', name:"dept", optionKey: 'id', optionValue: 'name', class:"form-control" , from:dept  ,noSelection:['':'All'],  )
        }
    }
    def getemployeetypebyorganization()
    {
        if (session.loginid == null) {
            ////println("Your are logged out! Please login again. Thank you.")
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            ////println("I am in establishment_leave_report")
            Login login = Login.findById(session.loginid)
            Organization org =Organization.findById(params.organization)
            def et = EmployeeType.findAllByOrganization(org)
            render g.select(id:'employeetype', name:"employeetype", optionKey: 'id', optionValue: 'type', class:"form-control" , from:et  ,noSelection:['':'All'],  )
        }
    }
    def getfacultilistbyorganization()
    {
        if (session.loginid == null) {
            ////println("Your are logged out! Please login again. Thank you.")
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            ////println("I am in establishment_leave_report")
            Login login = Login.findById(session.loginid)
            Organization org =Organization.findById(params.organization)
            def facultilist = Instructor.createCriteria().list {
                eq("organization", org)
                and {
                    eq("iscurrentlyworking", true)
                }
                order("employee_code", "asc")
            }
            render g.select(id:'facultilist', name:"facultilist",  class:"select2" , optionKey: 'id', from:facultilist  ,noSelection:['':'All'],  )
        }
    }

}
