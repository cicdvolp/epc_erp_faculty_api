package volp

import com.google.api.client.json.Json
import grails.converters.JSON
import groovy.json.JsonSlurper
import org.springframework.http.HttpStatus

import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

class ApiInstructorLeaveManagementController {

    ApiInstructorLeaveManagementService apiInstructorLeaveManagementService
    ApiInstructorLoginService apiInstructorLoginService

    def getLeavesDetails() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def colorList = ["0xFF9CEA89", "0xFFEAE889", "0xFFEA9689", "0xFF89BDEA", "0xFF9489EA", "0xFFCB89EA", "0xFFEA89E5", "0xFFEA89B2", "0xFFFFFFFF", "0xFF91BEB8", "0xFF9CEA89", "0xFFEAE889", "0xFFEA9689", "0xFF89BDEA", "0xFF9489EA", "0xFFCB89EA", "0xFFEA89E5", "0xFFEA89B2", "0xFFFFFFFF", "0xFF91BEB8"]
            HashMap data = apiInstructorLeaveManagementService.getLeaveDetails(login.username)
            List employeeleavebalancesheet = []
            data.employeeleavebalancesheet.forEach({
                obj ->
                    HashMap leaveBalanceMap = new HashMap()
                    leaveBalanceMap.put("id",obj?.leavetype?.id)
                    leaveBalanceMap.put("color",obj?.color)
                    leaveBalanceMap.put("leave_type",obj?.leavetype?.display_name)
                    leaveBalanceMap.put("total",obj?.total)
                    leaveBalanceMap.put("in_process",obj?.inprocess)
                    leaveBalanceMap.put("availed",obj?.approved)//Approved
                    leaveBalanceMap.put("balance",obj?.vacancy)
                    employeeleavebalancesheet.add(leaveBalanceMap)
            })
            List leavetypelist = []
            data.leavetypelist.forEach({
                obj ->
                    HashMap typeMap = new HashMap()
                    typeMap.put("display_name",obj.display_name)
                    typeMap.put("type",obj.type)
                    leavetypelist.add(typeMap)
            })

            hm.put("status", HttpStatus.OK.value().toString())
            hm.put("msg", "Success")
            hm.put("leavetypelist", leavetypelist)
            hm.put("employeeleavebalancesheet", employeeleavebalancesheet)
            render hm as JSON
            return
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }

    def applyLeave() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }
            String backto
            String instid
            String leavetypename
            String academicYear
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)
                backto = dataresponse.backto
                instid = dataresponse.instid
                leavetypename = dataresponse.leavetypename
                academicYear = dataresponse.ayid
            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorLeaveManagementService.applyLeave(login.username, backto, instid, leavetypename, academicYear)

            HashMap balanceMap = new HashMap()
            balanceMap.put("leavetype", data?.leavetype?.type)
            balanceMap.put("color",data?.color)
            balanceMap.put("total", data?.total_allowed_leaves)
            balanceMap.put("availed", data?.lbaprroved)
            balanceMap.put("in_process", data?.lbinporcess)
            balanceMap.put("balance", data?.balance)

            HashMap instMap = new HashMap()
            instMap.put("code", data?.instructor?.employee_code)
            instMap.put("name", data?.instructor?.person?.fullname_as_per_previous_marksheet)
            instMap.put("id", data?.instructor?.id)

            def count = 0
            def appliedleaveslist = []
            data.elt.forEach() {
                obj ->
                    HashMap leaveMap = new HashMap()
                    leaveMap.put("id", obj?.id)
                    leaveMap.put("leavetype", obj?.leavetype?.type)
                    leaveMap.put("application_date", obj?.application_date)
                    leaveMap.put("from_date", obj?.fromdate)
                    leaveMap.put("to_date", obj?.todate)
                    leaveMap.put("no_of_days", obj?.numberofdays)
                    leaveMap.put("no_of_hours", obj?.numberofhours)
                    leaveMap.put("from_time", obj?.fromtime)
                    leaveMap.put("to_time", obj?.totime)
                    leaveMap.put("reason", obj?.reason)
                    leaveMap.put("is_load_adjustment_done", obj?.is_load_adjustment_done)
                    leaveMap.put("pending_from", data?.aprovingAuthority[count])
                    leaveMap.put("adjusted", data?.adjusted[count])
                    leaveMap.put("loadRequired", data?.loadRequired[count])

                    appliedleaveslist.add(leaveMap)
                    count = count +1
            }

            hm.put("instructor", instMap)
            hm.put("appliedleaves", appliedleaveslist)
            hm.put("employeeleavebalance", balanceMap)
            hm.put("status", HttpStatus.OK.value().toString())
            hm.put("msg", "Success")
            render hm as JSON
            return
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }

    def saveApplyLeave() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }
            String backto
            String instid
            String academicYear
            String fromDate
            String toDate
            String ltype
            String bleaves
            String fromtime
            String totime
            String reason
            String is_load_adjustment_done
            String contact_phone_number
            String alternate_arragement_person_name
            String alternate_arragement_person_contact_number
            String nature_of_work
            String place_of_work
            String halfLeave
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)

                backto = null //dataresponse.backto
                instid = dataresponse.instid  //(required)
                academicYear = dataresponse.academicYear  //(required)
                fromDate = dataresponse.fromdate// (required)
                toDate = dataresponse.todate //(required)
                ltype = dataresponse.ltype //(required)
                bleaves = dataresponse.bleaves  //(required)
                fromtime = dataresponse.fromtime  //(optional) time
                totime = dataresponse.totime  //(optional) time
                reason = dataresponse.reason //(optional)
                is_load_adjustment_done = dataresponse.is_load_adjustment_done //(required)
                contact_phone_number = dataresponse.contact_phone_number // (optional)
                alternate_arragement_person_name = dataresponse.alternate_arragement_person_name //(optional)
                alternate_arragement_person_contact_number = dataresponse.alternate_arragement_person_contact_number //(optional)
                nature_of_work = dataresponse.nature_of_work //(optional)
                place_of_work = dataresponse.place_of_work //(optional)
                halfLeave = dataresponse.halfLeave //(if half day CL)

            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            // SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            def fromdate = formatter.parse(fromDate)
            def todate = formatter.parse(toDate)
            // Date fromdate = params.date('fromdate', 'yyyy-MM-dd')
            // Date todate = params.date('todate', 'yyyy-MM-dd')
            if(fromdate && todate) {
                if (fromdate > todate) {
                    hm.put("status", HttpStatus.NOT_ACCEPTABLE.value().toString())
                    hm.put("msg","FromDate Should be less than or equal to ToDate !!")
                    render hm as Json
                    return
                }
            }

            HashMap data = apiInstructorLeaveManagementService.saveleaveWithLeaveExpiry(login.username, backto, instid, academicYear, fromdate, todate,
                    ltype, bleaves, fromtime, totime, reason, is_load_adjustment_done, contact_phone_number, alternate_arragement_person_name,
                    alternate_arragement_person_contact_number, nature_of_work, place_of_work, halfLeave, request.getRemoteAddr().toString())
            println("data :: "+ data)

            hm.put("data", data)
            if(!data.msg) {
                hm.put("msg", "Success")
            } else {
                hm.put("msg", data.msg)
            }
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }

    def leaveapplicaionhistory() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def employeeLeaveTransactionId
            try {
                println("1")
                String body = request.getReader().text
                println("body :: " + body)
                println("2")
                def jsonSluper = new JsonSlurper()
                println("3")
                def dataresponse = jsonSluper.parseText(body)
                println("4")
                employeeLeaveTransactionId = dataresponse.employeeLeaveTransactionId
                println("5")
            } catch (Exception e) {
                println("6")
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorLeaveManagementService.leaveapplicaionhistory(employeeLeaveTransactionId)

            HashMap leaveTransactionMap = new HashMap()
            leaveTransactionMap.put("id", data?.employeeLeaveTransaction?.id)
            leaveTransactionMap.put("nature_of_work", data?.employeeLeaveTransaction?.nature_of_work)
            leaveTransactionMap.put("contact_phone_number", data?.employeeLeaveTransaction?.contact_phone_number)
            leaveTransactionMap.put("totime", data?.employeeLeaveTransaction?.totime)
            leaveTransactionMap.put("is_applied_by_establishment", data?.employeeLeaveTransaction?.is_applied_by_establishment)
            leaveTransactionMap.put("fromtime", data?.employeeLeaveTransaction?.fromtime)
            leaveTransactionMap.put("reason", data?.employeeLeaveTransaction?.reason)
            leaveTransactionMap.put("vacation_slot_number", data?.employeeLeaveTransaction?.vacation_slot_number)
            leaveTransactionMap.put("alternate_arragement_person_contact_number", data?.employeeLeaveTransaction?.alternate_arragement_person_contact_number)
            leaveTransactionMap.put("numberofhours", data?.employeeLeaveTransaction?.numberofhours)
            leaveTransactionMap.put("is_load_adjustment_done", data?.employeeLeaveTransaction?.is_load_adjustment_done)
            leaveTransactionMap.put("numberofdays", data?.employeeLeaveTransaction?.numberofdays)
            leaveTransactionMap.put("certificate_file_name", data?.employeeLeaveTransaction?.certificate_file_name)
            leaveTransactionMap.put("fromdate", data?.employeeLeaveTransaction?.fromdate)
            leaveTransactionMap.put("alternate_arragement_person_name", data?.employeeLeaveTransaction?.alternate_arragement_person_name)
            leaveTransactionMap.put("application_date", data?.employeeLeaveTransaction?.application_date)
            leaveTransactionMap.put("todate", data?.employeeLeaveTransaction?.todate)
            leaveTransactionMap.put("leavetype", data?.employeeLeaveTransaction?.leavetype?.type)
            leaveTransactionMap.put("color", data?.employeeLeaveTransaction?.leavetype?.leave_type_color)
            leaveTransactionMap.put("isfirsthalf", data?.employeeLeaveTransaction?.isfirsthalf)
            leaveTransactionMap.put("place_of_work", data?.employeeLeaveTransaction?.place_of_work)
            leaveTransactionMap.put("certificate_file_path", data?.employeeLeaveTransaction?.certificate_file_path)
            leaveTransactionMap.put("leaveapprovalstatusid", data?.employeeLeaveTransaction?.leaveapprovalstatus?.id)

            HashMap instMap = new HashMap()
            instMap.put("code", data?.employeeLeaveTransaction?.instructor?.employee_code)
            instMap.put("name", data?.employeeLeaveTransaction?.instructor?.person?.fullname_as_per_previous_marksheet)
            instMap.put("id", data?.employeeLeaveTransaction?.instructor?.id)

            hm.put("instructor", instMap)
            hm.put("employeeleavetransaction", leaveTransactionMap)
            hm.put("loadadjust", data?.loadadjust)

            def authorityList = []
            data.employeeLeaveTransactionApprovalList.forEach() {
                obj ->
                    HashMap authorityMap = new HashMap()
                    authorityMap.put("id", obj?.id)
                    authorityMap.put("authority_level", obj?.leaveapprovalhierarchy?.level)
                    authorityMap.put("authority", obj?.leaveapprovalhierarchy?.leaveapporvalauthority?.display_name)
                    authorityMap.put("emp_name", obj?.approvedby?.person?.firstName + " " + obj?.approvedby?.person?.lastName)
                    authorityMap.put("emp_code", obj?.approvedby?.employee_code)
                    authorityMap.put("date", obj?.application_push_date)
                    authorityMap.put("status", obj?.leaveapprovalstatus?.displayName)
                    authorityMap.put("remark", obj?.remark)
                    authorityList.add(authorityMap)
            }

            hm.put("authoritylist",authorityList)
            hm.put("msg","Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }

    def leaveloadadjustment() {
        try {
            HashMap hm = new HashMap()
            def loginid = request.getHeader("EPC-loginid")
            Login login = Login.findById(loginid)
            if (!login) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out! Please login again.")
                render hm as JSON
                return
            }
            def orgid = request.getHeader("EPC-orgid")
            Organization org = Organization.findById(orgid)
            if (!org) {
                hm.put("status", HttpStatus.NOT_FOUND.value().toString())
                hm.put("msg", "Organization not found.")
                render hm as JSON
                return
            }
            if (!apiInstructorLoginService.checkAuthentication(login.username, request.getHeader("EPC-token"))) {
                hm.put("status", HttpStatus.UNAUTHORIZED.value().toString())
                hm.put("msg", "You are logged out. Please re-login..!")
                render hm as JSON
                return
            }

            def employeeLeaveTransactionId
            try {
                String body = request.getReader().text
                def jsonSluper = new JsonSlurper()
                def dataresponse = jsonSluper.parseText(body)

                employeeLeaveTransactionId = dataresponse.employeeLeaveTransactionId

            } catch (Exception e) {
                hm.put("status", HttpStatus.BAD_REQUEST.value().toString())
                hm.put("msg", "Bad Request! Required parameters are missing..")
                render hm as JSON
                return
            }

            HashMap data = apiInstructorLeaveManagementService.loadadjustment(login.username, employeeLeaveTransactionId)

            HashMap leaveTransactionMap = new HashMap()
            leaveTransactionMap.put("id", data?.employeeLeaveTransaction?.id)
            leaveTransactionMap.put("nature_of_work", data?.employeeLeaveTransaction?.nature_of_work)
            leaveTransactionMap.put("contact_phone_number", data?.employeeLeaveTransaction?.contact_phone_number)
            leaveTransactionMap.put("totime", data?.employeeLeaveTransaction?.totime)
            leaveTransactionMap.put("is_applied_by_establishment", data?.employeeLeaveTransaction?.is_applied_by_establishment)
            leaveTransactionMap.put("fromtime", data?.employeeLeaveTransaction?.fromtime)
            leaveTransactionMap.put("reason", data?.employeeLeaveTransaction?.reason)
            leaveTransactionMap.put("vacation_slot_number", data?.employeeLeaveTransaction?.vacation_slot_number)
            leaveTransactionMap.put("alternate_arragement_person_contact_number", data?.employeeLeaveTransaction?.alternate_arragement_person_contact_number)
            leaveTransactionMap.put("numberofhours", data?.employeeLeaveTransaction?.numberofhours)
            leaveTransactionMap.put("is_load_adjustment_done", data?.employeeLeaveTransaction?.is_load_adjustment_done)
            leaveTransactionMap.put("numberofdays", data?.employeeLeaveTransaction?.numberofdays)
            leaveTransactionMap.put("certificate_file_name", data?.employeeLeaveTransaction?.certificate_file_name)
            leaveTransactionMap.put("fromdate", data?.employeeLeaveTransaction?.fromdate)
            leaveTransactionMap.put("alternate_arragement_person_name", data?.employeeLeaveTransaction?.alternate_arragement_person_name)
            leaveTransactionMap.put("application_date", data?.employeeLeaveTransaction?.application_date)
            leaveTransactionMap.put("todate", data?.employeeLeaveTransaction?.todate)
            leaveTransactionMap.put("leavetype", data?.employeeLeaveTransaction?.leavetype?.type)
            leaveTransactionMap.put("color", data?.employeeLeaveTransaction?.leavetype?.leave_type_color)
            leaveTransactionMap.put("isfirsthalf", data?.employeeLeaveTransaction?.isfirsthalf)
            leaveTransactionMap.put("place_of_work", data?.employeeLeaveTransaction?.place_of_work)
            leaveTransactionMap.put("certificate_file_path", data?.employeeLeaveTransaction?.certificate_file_path)
            leaveTransactionMap.put("leaveapprovalstatusid", data?.employeeLeaveTransaction?.leaveapprovalstatus?.id)

            HashMap instMap = new HashMap()
            instMap.put("code", data?.employeeLeaveTransaction?.instructor?.employee_code)
            instMap.put("name", data?.employeeLeaveTransaction?.instructor?.person?.fullname_as_per_previous_marksheet)
            instMap.put("id", data?.employeeLeaveTransaction?.instructor?.id)

            def load = []
            if(data?.erpTimeTablelistArray != null) {
                data?.erpTimeTablelistArray.forEach() {
                    obj ->
    //                    println("obj :: " + obj.date)
    //                    println("obj :: " + obj.erpTimeTablelist)

                        obj.erpTimeTablelist.forEach() {
                            obj1 ->
                                println("obj1 :: " + obj1?.leaveloadadjustment)
                                println("slot id :: " + obj1?.load?.slot?.id)
                                println("slot start time :: " + obj1?.load?.slot?.start_time)
                                println("slot end time :: " + obj1?.load?.slot?.end_time)
                        }
                }
            }

            if(data?.customloadArray != null) {
                data?.customloadArray.forEach() {
                    obj1 ->
                        obj1?.leaveloadadjustment.forEach() {
                            obj ->
                                HashMap load_map = new HashMap()
                                load_map.put("leave_date", obj?.leave_date)
                                load_map.put("start_time", obj?.start_time)
                                load_map.put("end_time", obj?.end_time)
                                load_map.put("load_description", obj?.load_description)
                                load_map.put("remark", obj?.remark)
                                load_map.put("isloadadjusted", obj?.isloadadjusted)
                                load_map.put("isloadrejected", obj?.isloadrejected)
                                load_map.put("iscustomload", obj?.iscustomload)
                                load_map.put("isdeleted", obj?.isdeleted)
                                load_map.put("employeeleavetransactionid", obj?.employeeleavetransaction?.id)

                                HashMap inst_map = new HashMap()
                                inst_map.put("id", obj?.instructor?.id)
                                inst_map.put("code", obj?.instructor?.employee_code)
                                inst_map.put("name", obj?.instructor?.person?.firstName + " " + obj?.instructor?.person?.lastName)
                                load_map.put("instructor", inst_map)

                                HashMap rejectInst_map = new HashMap()
                                rejectInst_map.put("id", obj?.rejectedby?.id)
                                rejectInst_map.put("code", obj?.rejectedby?.employee_coed)
                                rejectInst_map.put("name", obj?.rejectedby?.person?.firstName + " " + obj?.rejectedby?.person?.lastName)
                                load_map.put("rejectedby", rejectInst_map?.rejectedby?.id)
                                load.add(load_map)
                        }
                }
            }

            HashMap inst_map = new HashMap()
            if(data?.instlist != null) {
                data.instlist.forEach() {
                    obj ->
                        inst_map.put(obj?.id, obj?.employee_code + " : " + obj?.person?.firstName + " " + obj?.person?.lastName)
                }
            }

            hm.put("instructor", instMap)
            hm.put("employeeleavetransaction", leaveTransactionMap)
            hm.put("leaveapproved", data?.leaveapproved)
            hm.put("timetableload", data?.timetableload)
            hm.put("customloadad", data?.customloadad)
            hm.put("inst_list", inst_map)
            hm.put("load", load)

//            hm.put("data", data)

            hm.put("msg","Success")
            hm.put("status", HttpStatus.OK.value().toString())
            render hm as JSON
            return
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value().toString())
            hm.put("msg", "Internal Server Error")
            render hm as JSON
            return
        }
    }
}
