package volp

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.javanet.NetHttpTransport
import grails.validation.ValidationException
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import javax.activation.DataHandler
import javax.activation.DataSource
import javax.activation.FileDataSource
import javax.mail.BodyPart
import javax.mail.Message
import javax.mail.Multipart
import javax.mail.PasswordAuthentication
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart

import static org.springframework.http.HttpStatus.*
import com.google.gson.*
import grails.converters.JSON
import java.text.SimpleDateFormat
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.TimeUnit

import java.io.InputStream

import java.nio.file.Files
import java.nio.file.StandardCopyOption

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.sun.org.apache.xml.internal.security.utils.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.MessageDigest;

import com.paytm.pg.merchant.*;
import java.util.TreeMap;
import org.json.simple.JSONObject
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

class LoginController
{

    LoginService loginService
    PaymentGateWayService PaymentGateWayService
    def RegistrationService
    InformationService InformationService
    DTEAdmissionGRNOgenrationService DTEAdmissionGRNOgenrationService
    SendMailService sendMailService



    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

 /*   def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond loginService.list(params), model: [loginCount: loginService.count()]
    }

    def show(Long id) {
        respond loginService.get(id)
    }

    def create() {
        respond new Login(params)
    }

    def save(Login login) {
        if (login == null) {
            notFound()
            return
        }

        try {
            loginService.save(login)
        } catch (ValidationException e) {
            respond login.errors, view: 'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'login.label', default: 'Login'), login.id])
                redirect login
            }
            '*' { respond login, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond loginService.get(id)
    }

    def update(Login login) {
        if (login == null) {
            notFound()
            return
        }

        try {
            loginService.save(login)
        } catch (ValidationException e) {
            respond login.errors, view: 'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'login.label', default: 'Login'), login.id])
                redirect login
            }
            '*' { respond login, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        loginService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'login.label', default: 'Login'), id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'login.label', default: 'Login'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    } */

    def test1() {

        ////println("test1")

    }

    def login() {
        ////println("in looooogin")
        render"Please visit <a href='https://www.volp.in'>New Version</a>"
        return
        session.redirect = ""
        session.redirect_msg = ""
        session.uid = ""
        ApplicationType at = ApplicationType.findByApplication_type("VOLP")
        def usertype = UserType.findAllByApplication_typeAndTypeNotEqualAndTypeNotEqual(at, "Organization", "Administrator")
        ////println("login" + usertype.type)
        def fbkey = 2080629828838897
        session.apptype="VOLP"
        [usertype: usertype, fbkey: fbkey]
    }

    def setusertype() {
        ////println("in setusertype params:" + params)
        UserType ut = UserType.findById(params.utid)
        session.usertypo = ut
        String s = ""
        [s: s]
    }
    //google
    def processlogin() { //google
        ////println("in processlogin session:" + session)
        ////println("in processlogin params:" + params)
        GooglePojo data
        String uid
        System.out.println("entering doGet");
        try {
            // get code
            String code = params.code//request.getParameter("code");
            // format parameters to post
            // String urlParameters = "code="+ code + "&client_id=1044538822480-rp49uql6hntc6vk2eessltv99gecpfqp.apps.googleusercontent.com" + "&client_secret=ejzi69HPA9l0wzsxUFRtJVXS"+ "&redirect_uri=http://localhost:8080/login/processlogin"+ "&grant_type=authorization_code";
            String urlParameters = "code=" + code + "&client_id=1044538822480-rp49uql6hntc6vk2eessltv99gecpfqp.apps.googleusercontent.com" + "&client_secret=ejzi69HPA9l0wzsxUFRtJVXS" + "&redirect_uri=http://www.volp.in/login/processlogin" + "&grant_type=authorization_code";

            //post parameters
            URL url = new URL("https://accounts.google.com/o/oauth2/token");
            //URL url = new URL("https://www.googleapis.com/oauth2/v2/userinfo");
            URLConnection urlConn = url.openConnection();
            urlConn.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(
                    urlConn.getOutputStream());
            writer.write(urlParameters);
            writer.flush();

            //get output in outputString
            String line, outputString = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    urlConn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                outputString += line;
            }
            System.out.println(outputString);
            //get Access Token
            JsonObject json = (JsonObject) new JsonParser().parse(outputString);
            String access_token = json.get("access_token").getAsString();
            ////println(access_token);

            //get User Info
            url = new URL(
                    "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + access_token);
            urlConn = url.openConnection();
            outputString = "";
            reader = new BufferedReader(new InputStreamReader(
                    urlConn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                outputString += line;
            }
            ////println("OP:" + outputString);

            // Convert JSON response into Pojo class
            data = new Gson().fromJson(outputString, GooglePojo.class);
            ////println("Date:" + data);
            writer.close();
            reader.close();

        } catch (MalformedURLException e) {
            System.out.println(e);
        } catch (ProtocolException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
        ////println("Mail:" + data.email)
        ////println("F.Name:" + data.given_name)
        ////println("Last.Name:" + data.family_name)
        ////println("Gender:" + data.gender)
        Login user = Login.findByUsername(data.email)
        ////println("USer:" + user)
        if (user == null) {
            String fbid = data.email
            String fname = data.given_name
            String lname = data.family_name
            String type = session.usertypo.type
            String ip = request.getRemoteAddr()
            int f = RegistrationService.registeruserapi(fbid, "", fname, lname, type, ip, "Google")
            ////println("Reg:" + f)
            ////println("Type:" + session.usertypo.type)
            ////println("AT:" + session.usertypo.id)
            UserType ut = UserType.findById(session.usertypo.id)

            session.uid = data.email
            if (session.usertypo.type == "Instructor" && ut.application_type.application_type == "VOLP") {
                session.redirect = "Instructor"
                session.redirect_msg = ""
                //return
            } else if (session.usertypo.type == "Learner" && ut.application_type.application_type == "VOLP") {
                session.redirect = "Learner"
                session.redirect_msg = ""
                //return
            }
            redirect(action: "redirect")
            return
        } else {
            session.uid = user.username
            if (user.isloginblocked) {
                session.redirect = "login"
                session.redirect_msg = "User Blocked!!!."
                // return
            }
            if (user.usertype.type == "Instructor" && user.usertype.application_type.application_type == "VOLP") {
                session.redirect = "Instructor"
                session.redirect_msg = ""
                // return
            } else if (user.usertype.type == "Learner" && user.usertype.application_type.application_type == "VOLP") {
                session.redirect = "Learner"
                session.redirect_msg = ""
                // return
            }
            redirect(action: "redirect")
            return
            //return
        }
        [data: data]
    }
    //No API
    def processsignin() { //No API

        ////println("processsignin params:" + params)
        ////println("processreg seesion:" + session)

        Login user = Login.findByUsernameAndPassword(params.email, params.pwd)
        ////println("USer:" + user)
        if (user == null) {
            flash.message = params.email + " Invalid User name or Password!!!"
            redirect(action: "login")
            return
        } else {
            if (user.isloginblocked) {
                flash.message = params.email + " blocked!!!"
                redirect(action: "login")
                return
            }
//            if (!user.isVerified) {
//                flash.message = params.email + " not verified!!!"
//                redirect(action: "login")
//                return
//            }
            session.uid = user.username
            def ut = user.usertype
            for (UserType u : ut) {
                if (u.type == "Instructor" && u.application_type.application_type == "VOLP")
                    redirect(controller: "instructor", action: "instructor")
                else if (u.type == "Learner" && u.application_type.application_type == "VOLP")
                    redirect(controller: "learner", action: "learner")
                else if (u.type == "Organization" && u.application_type.application_type == "VOLP")
                    redirect(controller: "organization", action: "organization")
            }
            return
        }
    }

    def registration() {

    }
    //not used
    def processreg() { //google
        ////println("processreg params:" + params)
        ////println("processreg seesion:" + session)
        GooglePojo data
        String uid
        System.out.println("entering doGet");
        try {
            // get code
            String code = params.code//request.getParameter("code");
            // format parameters to post
            // String urlParameters = "code="+ code + "&client_id=1044538822480-rp49uql6hntc6vk2eessltv99gecpfqp.apps.googleusercontent.com" + "&client_secret=ejzi69HPA9l0wzsxUFRtJVXS"+ "&redirect_uri=http://localhost:8080/login/processreg"+ "&grant_type=authorization_code";
            String urlParameters = "code=" + code + "&client_id=1044538822480-rp49uql6hntc6vk2eessltv99gecpfqp.apps.googleusercontent.com" + "&client_secret=ejzi69HPA9l0wzsxUFRtJVXS" + "&redirect_uri=http://www.volp.in/login/processlogin" + "&grant_type=authorization_code";

            //post parameters
            URL url = new URL("https://accounts.google.com/o/oauth2/token");
            //URL url = new URL("https://www.googleapis.com/oauth2/v2/userinfo");
            URLConnection urlConn = url.openConnection();
            urlConn.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(
                    urlConn.getOutputStream());
            writer.write(urlParameters);
            writer.flush();

            //get output in outputString
            String line, outputString = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    urlConn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                outputString += line;
            }
            System.out.println(outputString);
            //get Access Token
            JsonObject json = (JsonObject) new JsonParser().parse(outputString);
            String access_token = json.get("access_token").getAsString();
            ////println(access_token);

            //get User Info
            url = new URL(
                    "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + access_token);
            urlConn = url.openConnection();
            outputString = "";
            reader = new BufferedReader(new InputStreamReader(
                    urlConn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                outputString += line;
            }
            ////println("OP:" + outputString);

            // Convert JSON response into Pojo class
            data = new Gson().fromJson(outputString, GooglePojo.class);
            ////println("Date:" + data);
            writer.close();
            reader.close();

        } catch (MalformedURLException e) {
            System.out.println(e);
        } catch (ProtocolException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
        ////println("Mail:" + data.email)
        ////println("F.Name:" + data.given_name)
        ////println("Last.Name:" + data.family_name)
        ////println("Gender:" + data.gender)

    }

    def processsignlingdin() { // linkedin
        ////println("in processsignlingdin params:" + params)
        ////println("session:" + session)
        Login user = Login.findByUsername(params.id)
        ////println("USer:" + user)
        if (user == null) {
            String fbid = params.id
            String fname = params.fname
            String lname = params.lname
            String type = session.usertypo.type
            String ip = request.getRemoteAddr()
            int f = RegistrationService.registeruserapi(fbid, "", fname, lname, type, ip, "LinkedIn")
            ////println("Reg:" + f)
            ////println("Type:" + session.usertypo.type)
            ////println("AT:" + session.usertypo.id)
            UserType ut = UserType.findById(session.usertypo.id)

            session.uid = params.id
            if (session.usertypo.type == "Instructor" && ut.application_type.application_type == "VOLP") {
                session.redirect = "Instructor"
                session.redirect_msg = ""
                return
            } else if (session.usertypo.type == "Learner" && ut.application_type.application_type == "VOLP") {
                session.redirect = "Learner"
                session.redirect_msg = ""
                return
            }

        } else {
            session.uid = user.username
            if (user.isloginblocked) {
                session.redirect = "login"
                session.redirect_msg = "User Blocked!!!."
                return
            }
            if (user.usertype.type == "Instructor" && user.usertype.application_type.application_type == "VOLP") {
                session.redirect = "Instructor"
                session.redirect_msg = ""
                return
            } else if (user.usertype.type == "Learner" && user.usertype.application_type.application_type == "VOLP") {
                session.redirect = "Learner"
                session.redirect_msg = ""
                return
            }
            return
        }
        /*if(user==null) {
            session.redirect = "login"
            session.redirect_msg ="Registered!!!. Login Again."
            return
        }
        else{
            session.uid=user.username
            if(user.isloginblocked){
                session.redirect = "login"
                session.redirect_msg ="User Blocked!!!."
                return
            }
            if (user.usertype.type=="Instructor" && user.usertype.application_type.application_type=="VOLP")
            {
                session.redirect = "Instructor"
                session.redirect_msg =""
                return
            }
            else if(user.usertype.type=="Learner"  && user.usertype.application_type.application_type=="VOLP")
            {
                session.redirect = "Learner"
                session.redirect_msg =""
                return
            }
            return
        }*/
    }

    def lind() {

    }
    //FB
    def processfblogin() {
        ////println("in processfblogin:" + params)

        ////println("session:" + session)
        Login user = Login.findByUsername(params.id)
        ////println("USer:" + user)
        if (user == null) {
            String fbid = params.id
            String fname = params.fname
            String lname = params.lname
            String type = session.usertypo.type
            String ip = request.getRemoteAddr()
            int f = RegistrationService.registeruserapi(fbid, "", fname, lname, type, ip, "Facebook")
            ////println("Reg:" + f)
            ////println("Type:" + session.usertypo.type)
            ////println("AT:" + session.usertypo.id)
            UserType ut = UserType.findById(session.usertypo.id)

            session.uid = params.id
            if (session.usertypo.type == "Instructor" && ut.application_type.application_type == "VOLP") {
                session.redirect = "Instructor"
                session.redirect_msg = ""
                return
            } else if (session.usertypo.type == "Learner" && ut.application_type.application_type == "VOLP") {
                session.redirect = "Learner"
                session.redirect_msg = ""
                return
            }

        } else {
            session.uid = user.username
            if (user.isloginblocked) {
                session.redirect = "login"
                session.redirect_msg = "User Blocked!!!."
                return
            }
            if (user.usertype.type == "Instructor" && user.usertype.application_type.application_type == "VOLP") {
                session.redirect = "Instructor"
                session.redirect_msg = ""
                return
            } else if (user.usertype.type == "Learner" && user.usertype.application_type.application_type == "VOLP") {
                session.redirect = "Learner"
                session.redirect_msg = ""
                return
            }
            return
        }
    }

    //redirect User
    def redirect() {
        ////println("in redirect params:" + params)
        ////println("in redirect session:" + session)
        if (session.redirect == "login") {
            flash.message = session.redirect_msg
            redirect(action: "login")
        }
        if (session.redirect == "Instructor") {
            flash.message = session.redirect_msg
            redirect(controller: "instructor", action: "instructor")
        }
        if (session.redirect == "Learner") {
            flash.message = session.redirect_msg
            redirect(controller: "learner", action: "learner")
        }
    }

    def test2() {
        session.type = "Instructor"
        session.type = "Learner"
        ////println("test2...")
    }
    // code by PHR

    def forgotPassword() {

    }

    def logout() {
        ////println("Loged out Successfully")
        session.uid = ""
        session.invalidate()
        redirect(controller: "login", action: "volpNewHome")
    }
    // end of PHR code
    def home1() {
//        ArrayList<Course> crsList = Course.list()
//        crsList.sort{it.rating}
        ////println("path:" + request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        ////println("result:" + results.size())
        ArrayList topCrs = new ArrayList<Course>()
        int top20 = 0;
        for (Course c1 : results) {
            if (top20 == top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        ////println("topCrs:" + topCrs)
        top20 = 0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for (CourseCategory cc : subCat) {

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for (Course c2 : allCrsCat) {
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if (top20 >= topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20 = 0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for (int j = topCat.size() - 2; j > 0;) {
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if (topCat[j] > topCat[j - 2]) {
                    int t = topCat[j - 2]
                    CourseCategory tc = topCat[j - 1]
                    topCat[j - 2] = topCat[j]
                    topCat[j - 1] = topCat[j + 1]
                    topCat[j] = t
                    topCat[j + 1] = tc
                }
                j = j - 2
            }
            // ////println("After::topCat:"+topCat)

        }
        ////println("topCat:" + topCat)
        top20 = 0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for (int i = 1; i < topCat.size(); i += 2) {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
             topSubCat.add(tm.getValue())
         }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        ////println("topSubCat:" + topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for (CourseCategory cc : topSubCat) {
            ////println("CC:" + cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if (cnt.size == 0)
                continue
            if (showTopCat == 4) {
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory {
                    eq('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1 = catCrs[0]
        cat1 = catCrs[1]
        def catname2 = catCrs[2]
        cat2 = catCrs[3]
        def catname3 = catCrs[4]
        cat3 = catCrs[5]
        def catname4 = catCrs[6]
        cat4 = catCrs[7]
        ////println("cat:" + cat1 + "\n" + cat2 + "\n" + cat3 + "\n" + cat4)
        ////println("catCrs:" + catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for (CourseCategory mc : mainCat) {
            ArrayList<CourseCategory> mm = new ArrayList()
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            ArrayList<CourseCategory> ssm = new ArrayList()
            boolean f = false
            ////println("CC:" + mc.name)
            for (CourseCategory cco : sm) {
                def crs = Course.findAllByCoursecategory(cco)
                if (crs.size() > 0) {
                    f = true
                    //break
                    ssm.add(cco)
                }
            }
            def crs = Course.findAllByCoursecategory(mc)
            ////println("crs:" + crs.course_name)
            if (crs.size() > 0) {
                f = true
                //break
            }
            if (f) {
                mm.add(mc)
            } else
                continue
            mm.add(ssm)
            mainmenu.add(mm)
            ////println("CC added:" + mc.name)

        }

        ////println("Categories MM SM:" + mainmenu.name)
        // session.catCrs = catCrs
        [crs: topCrs, cat1: cat1, cat2: cat2, cat3: cat3, cat4: cat4, cats1: cat1.size(), cats2: cat2.size(), cats3: cat3.size(), cats4: cat4.size(), catname1: catname1, catname2: catname2, catname3: catname3, catname4: catname4, menus: mainmenu]
    }

    def crsSlider() {
        ////println("in crsSlider:" + params)
        String[] ids = params.crsids.split(",")
        ////println("IDS:" + ids)
        ArrayList crs = new ArrayList()
        for (int i = 0; i < ids.size(); i++) {
            if (ids[i] == "")
                continue
            Course c = Course.findById(ids[i])
            crs.add(c)
        }
        [crs: crs]
    }

    def catSlider1() {
        ////println("in catSlider1:" + params)
        CourseCategory cc = CourseCategory.findById(params.cc)
        // ////println("cc crsSlider1:"+cc)
        String[] ids = params.crsids.split(",")
        ////println("IDS:" + ids)
        ArrayList crs = new ArrayList()
        for (int i = 0; i < ids.size(); i++) {
            if (ids[i] == "")
                continue
            Course c = Course.findById(ids[i])
            crs.add(c)
        }
        //int csize=Integer.parseInt(params.csize)
        // ////println("crs: crsSlider1"+crs)
        [crs: crs, cc: cc]
    }

    def catSlider4() {
        ////println("in catSlider4:" + params)
        CourseCategory cc = CourseCategory.findById(params.cc)
        String[] ids = params.crsids.split(",")
        ////println("IDS:" + ids)
        ArrayList crs = new ArrayList()
        for (int i = 0; i < ids.size(); i++) {
            if (ids[i] == "")
                continue
            Course c = Course.findById(ids[i])
            crs.add(c)
        }
        [crs: crs, cc: cc]
    }

    def catSlider2() {
        ////println("in crsSlider2:" + params)
        CourseCategory cc = CourseCategory.findById(params.cc)
        String[] ids = params.crsids.split(",")
        ////println("IDS:" + ids)
        ArrayList crs = new ArrayList()
        for (int i = 0; i < ids.size(); i++) {
            if (ids[i] == "")
                continue
            Course c = Course.findById(ids[i])
            crs.add(c)
        }
        [crs: crs, cc: cc]
    }

    def catSlider3() {
        ////println("in crsSlider3:" + params)
        CourseCategory cc = CourseCategory.findById(params.cc)
        String[] ids = params.crsids.split(",")
        ////println("IDS:" + ids)
        ArrayList crs = new ArrayList()
        for (int i = 0; i < ids.size(); i++) {
            if (ids[i] == "")
                continue
            Course c = Course.findById(ids[i])
            crs.add(c)
        }
        [crs: crs, cc: cc]
    }

    def course() {

        ////println("in course:" + params)
        Course course = Course.findById(params.cid)
        def coffrlst = CourseOffering.findAllByCourse(course)
        ////println("coffrlst:" + coffrlst)
        ArrayList coffrlnrlst = new ArrayList()

        for (CourseOffering co : coffrlst) {
            ArrayList learnercnt = new ArrayList()
            learnercnt.add(co)
            learnercnt.add(CourseOfferingLearner.findAllByCourseoffering(co).size())
            coffrlnrlst.add(learnercnt)
            //learnerlst.add(CourseOfferingLearner.findAllByCourseoffering(co))
        }
        ////println("Learner:" + coffrlnrlst)


        ////println("path:"+request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        //  ////println("result:"+results.size())
        ArrayList topCrs = new ArrayList<Course>()

        int top20 = 0;
        for(Course c1: results){
            if(top20==top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        int topthree = 0;
        ArrayList topthreeCrs = new ArrayList()
        for(Course c1: results){
            if(topthree==3)
                break;
            ArrayList data = new ArrayList()
            data.add(c1.id)
            data.add(c1.course_name)
            data.add(c1.coursecategory.name)
            data.add(c1.courseowner.person.firstName+" "+c1.courseowner.person.lastName)
            data.add(c1.rating)
            topthreeCrs.add(data)
            topthree++
        }
        ////println("topthreeCrs:"+topthreeCrs)
        ////println("topCrs:"+topCrs)
        top20=0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for(CourseCategory cc: subCat){

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for(Course c2:allCrsCat){
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if(top20>=topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20=0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for(int j=topCat.size()-2;j>0;){
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if(topCat[j]>topCat[j-2]){
                    int t = topCat[j-2]
                    CourseCategory tc = topCat[j-1]
                    topCat[j-2] = topCat[j]
                    topCat[j-1] = topCat[j+1]
                    topCat[j] = t
                    topCat[j+1] = tc
                }
                j=j-2
            }
            // ////println("After::topCat:"+topCat)

        }
        //    ////println("topCat:"+topCat)
        top20=0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for(int i=1;i<topCat.size();i+=2)
        {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
             topSubCat.add(tm.getValue())
         }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        //     ////println("topSubCat:"+topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for(CourseCategory cc: topSubCat){
            //   ////println("CC:"+cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if(cnt.size==0)
                continue
            if(showTopCat==4){
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory{
                    eq ('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1=catCrs[0]
        cat1=catCrs[1]
        def catname2=catCrs[2]
        cat2=catCrs[3]
        def catname3=catCrs[4]
        cat3=catCrs[5]
        def catname4=catCrs[6]
        cat4=catCrs[7]
        //    ////println("cat:"+cat1+"\n"+cat2+"\n"+cat3+"\n"+cat4)
        //    ////println("catCrs:"+catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for(CourseCategory mc:mainCat){
            ArrayList<CourseCategory> mm = new ArrayList()
            mm.add(mc)
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            mm.add(sm)
            mainmenu.add(mm)
        }

        [coffrlnrlst: coffrlnrlst, c: course,topthreeCrs:topthreeCrs,crs:topCrs,cat1:cat1,cat2:cat2,cat3:cat3,cat4:cat4,cats1:cat1.size(),cats2:cat2.size(),cats3:cat3.size(),cats4:cat4.size(),catname1:catname1,catname2:catname2,catname3:catname3,catname4:catname4,menus:mainmenu]
    }

    def catcrsSlider() {
        /*    ////println("in catcrsSlider params:"+params)
            ////println("in catcrsSlider session:"+session)

            [catCrs: session.catCrs]
            */
    }

    def changepassword() {
        ////println("I am in changepassword"+session)

    }

    def erpchangepassword() {
        ////println("I am in changepassword")
    }

    def savechangepassword() {
        ////println("I am in savechangepassword::" + params)
        ////println("session::"+session)
        String newpassword = params.newpassword
        String confirmpassword = params.confirmpassword
        Login login = null
        if(session.apptype=="VOLP")
        {
            login = Login.findByUsername(session.uid)
        }
        else if(session.apptype=="ERP")
        {
            login = Login.findById(session.loginid)
        }
        ////println("login::" + login)
        if (newpassword.equals(confirmpassword)) {
            login.password = newpassword
            login.save(failOnError: true, flush: true)
            flash.message = "Password Changed Successfully....."

        } else {
            flash.message = "New And Confirm Passwords are not matching..."
        }
        if(session.apptype=="ERP")
            redirect(controller: "Login", action: "erplogin")
        else if(session.apptype=="VOLP")
            redirect(controller: "Login", action: "login")
        return
    }
    // Home page Menu
    def home2() {

//        ArrayList<Course> crsList = Course.list()
//        crsList.sort{it.rating}
        ////println("path:" + request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        ////println("result:" + results.size())
        ArrayList topCrs = new ArrayList<Course>()
        int top20 = 0;
        for (Course c1 : results) {
            if (top20 == top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        ////println("topCrs:" + topCrs)
        top20 = 0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for (CourseCategory cc : subCat) {

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for (Course c2 : allCrsCat) {
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if (top20 >= topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20 = 0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for (int j = topCat.size() - 2; j > 0;) {
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if (topCat[j] > topCat[j - 2]) {
                    int t = topCat[j - 2]
                    CourseCategory tc = topCat[j - 1]
                    topCat[j - 2] = topCat[j]
                    topCat[j - 1] = topCat[j + 1]
                    topCat[j] = t
                    topCat[j + 1] = tc
                }
                j = j - 2
            }
            // ////println("After::topCat:"+topCat)

        }
        ////println("topCat:" + topCat)
        top20 = 0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for (int i = 1; i < topCat.size(); i += 2) {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
             topSubCat.add(tm.getValue())
         }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        ////println("topSubCat:" + topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for (CourseCategory cc : topSubCat) {
            ////println("CC:" + cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if (cnt.size == 0)
                continue
            if (showTopCat == 4) {
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory {
                    eq('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1 = catCrs[0]
        cat1 = catCrs[1]
        def catname2 = catCrs[2]
        cat2 = catCrs[3]
        def catname3 = catCrs[4]
        cat3 = catCrs[5]
        def catname4 = catCrs[6]
        cat4 = catCrs[7]
        ////println("cat:" + cat1 + "\n" + cat2 + "\n" + cat3 + "\n" + cat4)
        ////println("catCrs:" + catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for (CourseCategory mc : mainCat) {
            ArrayList<CourseCategory> mm = new ArrayList()
            mm.add(mc)
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            mm.add(sm)
            mainmenu.add(mm)
        }

        ////println("Categories MM SM:" + mainmenu.name)
        // session.catCrs = catCrs
        [crs: topCrs, cat1: cat1, cat2: cat2, cat3: cat3, cat4: cat4, cats1: cat1.size(), cats2: cat2.size(), cats3: cat3.size(), cats4: cat4.size(), catname1: catname1, catname2: catname2, catname3: catname3, catname4: catname4, menus: mainmenu]

    }

    def downloads() {

    }

    def forum() {

    }

    def faq() {

    }

    def partener() {

//        ArrayList<Course> crsList = Course.list()
//        crsList.sort{it.rating}
        ////println("path:" + request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        ////println("result:" + results.size())
        ArrayList topCrs = new ArrayList<Course>()
        int top20 = 0;
        for (Course c1 : results) {
            if (top20 == top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        ////println("topCrs:" + topCrs)
        top20 = 0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for (CourseCategory cc : subCat) {

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for (Course c2 : allCrsCat) {
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if (top20 >= topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20 = 0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for (int j = topCat.size() - 2; j > 0;) {
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if (topCat[j] > topCat[j - 2]) {
                    int t = topCat[j - 2]
                    CourseCategory tc = topCat[j - 1]
                    topCat[j - 2] = topCat[j]
                    topCat[j - 1] = topCat[j + 1]
                    topCat[j] = t
                    topCat[j + 1] = tc
                }
                j = j - 2
            }
            // ////println("After::topCat:"+topCat)

        }
        ////println("topCat:" + topCat)
        top20 = 0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for (int i = 1; i < topCat.size(); i += 2) {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
             topSubCat.add(tm.getValue())
         }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        ////println("topSubCat:" + topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for (CourseCategory cc : topSubCat) {
            ////println("CC:" + cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if (cnt.size == 0)
                continue
            if (showTopCat == 4) {
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory {
                    eq('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1 = catCrs[0]
        cat1 = catCrs[1]
        def catname2 = catCrs[2]
        cat2 = catCrs[3]
        def catname3 = catCrs[4]
        cat3 = catCrs[5]
        def catname4 = catCrs[6]
        cat4 = catCrs[7]
        ////println("cat:" + cat1 + "\n" + cat2 + "\n" + cat3 + "\n" + cat4)
        ////println("catCrs:" + catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for (CourseCategory mc : mainCat) {
            ArrayList<CourseCategory> mm = new ArrayList()
            mm.add(mc)
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            mm.add(sm)
            mainmenu.add(mm)
        }

        ////println("Categories MM SM:" + mainmenu.name)
        // session.catCrs = catCrs
        [crs: topCrs, cat1: cat1, cat2: cat2, cat3: cat3, cat4: cat4, cats1: cat1.size(), cats2: cat2.size(), cats3: cat3.size(), cats4: cat4.size(), catname1: catname1, catname2: catname2, catname3: catname3, catname4: catname4, menus: mainmenu]

    }

    def pricing() {

//        ArrayList<Course> crsList = Course.list()
//        crsList.sort{it.rating}
        ////println("path:" + request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        ////println("result:" + results.size())
        ArrayList topCrs = new ArrayList<Course>()
        int top20 = 0;
        for (Course c1 : results) {
            if (top20 == top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        ////println("topCrs:" + topCrs)
        top20 = 0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for (CourseCategory cc : subCat) {

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for (Course c2 : allCrsCat) {
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if (top20 >= topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20 = 0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for (int j = topCat.size() - 2; j > 0;) {
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if (topCat[j] > topCat[j - 2]) {
                    int t = topCat[j - 2]
                    CourseCategory tc = topCat[j - 1]
                    topCat[j - 2] = topCat[j]
                    topCat[j - 1] = topCat[j + 1]
                    topCat[j] = t
                    topCat[j + 1] = tc
                }
                j = j - 2
            }
            // ////println("After::topCat:"+topCat)

        }
        ////println("topCat:" + topCat)
        top20 = 0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for (int i = 1; i < topCat.size(); i += 2) {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
             topSubCat.add(tm.getValue())
         }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        ////println("topSubCat:" + topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for (CourseCategory cc : topSubCat) {
            ////println("CC:" + cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if (cnt.size == 0)
                continue
            if (showTopCat == 4) {
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory {
                    eq('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1 = catCrs[0]
        cat1 = catCrs[1]
        def catname2 = catCrs[2]
        cat2 = catCrs[3]
        def catname3 = catCrs[4]
        cat3 = catCrs[5]
        def catname4 = catCrs[6]
        cat4 = catCrs[7]
        ////println("cat:" + cat1 + "\n" + cat2 + "\n" + cat3 + "\n" + cat4)
        ////println("catCrs:" + catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for (CourseCategory mc : mainCat) {
            ArrayList<CourseCategory> mm = new ArrayList()
            mm.add(mc)
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            mm.add(sm)
            mainmenu.add(mm)
        }

        ////println("Categories MM SM:" + mainmenu.name)
        // session.catCrs = catCrs
        [crs: topCrs, cat1: cat1, cat2: cat2, cat3: cat3, cat4: cat4, cats1: cat1.size(), cats2: cat2.size(), cats3: cat3.size(), cats4: cat4.size(), catname1: catname1, catname2: catname2, catname3: catname3, catname4: catname4, menus: mainmenu]

    }

    def contactus() {

//        ArrayList<Course> crsList = Course.list()
//        crsList.sort{it.rating}
        ////println("path:" + request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        ////println("result:" + results.size())
        ArrayList topCrs = new ArrayList<Course>()
        int top20 = 0;
        for (Course c1 : results) {
            if (top20 == top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        ////println("topCrs:" + topCrs)
        top20 = 0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for (CourseCategory cc : subCat) {

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for (Course c2 : allCrsCat) {
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if (top20 >= topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20 = 0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for (int j = topCat.size() - 2; j > 0;) {
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if (topCat[j] > topCat[j - 2]) {
                    int t = topCat[j - 2]
                    CourseCategory tc = topCat[j - 1]
                    topCat[j - 2] = topCat[j]
                    topCat[j - 1] = topCat[j + 1]
                    topCat[j] = t
                    topCat[j + 1] = tc
                }
                j = j - 2
            }
            // ////println("After::topCat:"+topCat)

        }
        ////println("topCat:" + topCat)
        top20 = 0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for (int i = 1; i < topCat.size(); i += 2) {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
             topSubCat.add(tm.getValue())
         }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        ////println("topSubCat:" + topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for (CourseCategory cc : topSubCat) {
            ////println("CC:" + cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if (cnt.size == 0)
                continue
            if (showTopCat == 4) {
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory {
                    eq('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1 = catCrs[0]
        cat1 = catCrs[1]
        def catname2 = catCrs[2]
        cat2 = catCrs[3]
        def catname3 = catCrs[4]
        cat3 = catCrs[5]
        def catname4 = catCrs[6]
        cat4 = catCrs[7]
        ////println("cat:" + cat1 + "\n" + cat2 + "\n" + cat3 + "\n" + cat4)
        ////println("catCrs:" + catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for (CourseCategory mc : mainCat) {
            ArrayList<CourseCategory> mm = new ArrayList()
            mm.add(mc)
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            mm.add(sm)
            mainmenu.add(mm)
        }

        ////println("Categories MM SM:" + mainmenu.name)
        // session.catCrs = catCrs
        [crs: topCrs, cat1: cat1, cat2: cat2, cat3: cat3, cat4: cat4, cats1: cat1.size(), cats2: cat2.size(), cats3: cat3.size(), cats4: cat4.size(), catname1: catname1, catname2: catname2, catname3: catname3, catname4: catname4, menus: mainmenu]
    }

    def reviews() {

    }

    def aboutvolp() {

//        ArrayList<Course> crsList = Course.list()
//        crsList.sort{it.rating}
        ////println("path:" + request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        ////println("result:" + results.size())
        ArrayList topCrs = new ArrayList<Course>()
        int top20 = 0;
        for (Course c1 : results) {
            if (top20 == top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        ////println("topCrs:" + topCrs)
        top20 = 0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for (CourseCategory cc : subCat) {

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for (Course c2 : allCrsCat) {
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if (top20 >= topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20 = 0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for (int j = topCat.size() - 2; j > 0;) {
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if (topCat[j] > topCat[j - 2]) {
                    int t = topCat[j - 2]
                    CourseCategory tc = topCat[j - 1]
                    topCat[j - 2] = topCat[j]
                    topCat[j - 1] = topCat[j + 1]
                    topCat[j] = t
                    topCat[j + 1] = tc
                }
                j = j - 2
            }
            // ////println("After::topCat:"+topCat)

        }
        ////println("topCat:" + topCat)
        top20 = 0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for (int i = 1; i < topCat.size(); i += 2) {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
                 topSubCat.add(tm.getValue())
             }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        ////println("topSubCat:" + topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for (CourseCategory cc : topSubCat) {
            ////println("CC:" + cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if (cnt.size == 0)
                continue
            if (showTopCat == 4) {
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory {
                    eq('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1 = catCrs[0]
        cat1 = catCrs[1]
        def catname2 = catCrs[2]
        cat2 = catCrs[3]
        def catname3 = catCrs[4]
        cat3 = catCrs[5]
        def catname4 = catCrs[6]
        cat4 = catCrs[7]
        ////println("cat:" + cat1 + "\n" + cat2 + "\n" + cat3 + "\n" + cat4)
        ////println("catCrs:" + catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for (CourseCategory mc : mainCat) {
            ArrayList<CourseCategory> mm = new ArrayList()
            mm.add(mc)
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            mm.add(sm)
            mainmenu.add(mm)
        }

        ////println("Categories MM SM:" + mainmenu.name)
        // session.catCrs = catCrs
        [crs: topCrs, cat1: cat1, cat2: cat2, cat3: cat3, cat4: cat4, cats1: cat1.size(), cats2: cat2.size(), cats3: cat3.size(), cats4: cat4.size(), catname1: catname1, catname2: catname2, catname3: catname3, catname4: catname4, menus: mainmenu]
    }

    def homeDash(){
//        ArrayList<Course> crsList = Course.list()
//        crsList.sort{it.rating}
        Learner ins =Learner.findByUid(session.uid)
        session.firstName=ins.person.firstName
        //        ArrayList<Course> crsList = Course.list()
//        crsList.sort{it.rating}
        ////println("path:"+request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        ////println("result:"+results.size())
        ArrayList topCrs = new ArrayList<Course>()
        int top20 = 0;
        for(Course c1: results){
            if(top20==top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        ////println("topCrs:"+topCrs)
        top20=0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for(CourseCategory cc: subCat){

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for(Course c2:allCrsCat){
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if(top20>=topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20=0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for(int j=topCat.size()-2;j>0;){
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if(topCat[j]>topCat[j-2]){
                    int t = topCat[j-2]
                    CourseCategory tc = topCat[j-1]
                    topCat[j-2] = topCat[j]
                    topCat[j-1] = topCat[j+1]
                    topCat[j] = t
                    topCat[j+1] = tc
                }
                j=j-2
            }
            // ////println("After::topCat:"+topCat)

        }
        ////println("topCat:"+topCat)
        top20=0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for(int i=1;i<topCat.size();i+=2)
        {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
             topSubCat.add(tm.getValue())
         }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        ////println("topSubCat:"+topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for(CourseCategory cc: topSubCat){
            ////println("CC:"+cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if(cnt.size==0)
                continue
            if(showTopCat==4){
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory{
                    eq ('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1=catCrs[0]
        cat1=catCrs[1]
        def catname2=catCrs[2]
        cat2=catCrs[3]
        def catname3=catCrs[4]
        cat3=catCrs[5]
        def catname4=catCrs[6]
        cat4=catCrs[7]
        ////println("cat:"+cat1+"\n"+cat2+"\n"+cat3+"\n"+cat4)
        ////println("catCrs:"+catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for(CourseCategory mc:mainCat){
            ArrayList<CourseCategory> mm = new ArrayList()
            mm.add(mc)
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            mm.add(sm)
            mainmenu.add(mm)
        }

        ////println("Categories MM SM:"+mainmenu.name)
        // session.catCrs = catCrs
        [crs:topCrs,cat1:cat1,cat2:cat2,cat3:cat3,cat4:cat4,cats1:cat1.size(),cats2:cat2.size(),cats3:cat3.size(),cats4:cat4.size(),catname1:catname1,catname2:catname2,catname3:catname3,catname4:catname4,menus:mainmenu]
         }
    def homeDashInstructor(){
//        ArrayList<Course> crsList = Course.list()
//        crsList.sort{it.rating}
        Instructor ins =Instructor.findByUid(session.uid)
        ////println("Instructor:"+ins.person)
        if(ins.person.firstName==null)
            session.firstName=ins.person.firstName
        else
            session.firstName=""
        ////println("path:"+request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        ////println("result:"+results.size())
        ArrayList topCrs = new ArrayList<Course>()
        int top20 = 0;
        for(Course c1: results){
            if(top20==top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        ////println("topCrs:"+topCrs)
        top20=0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for(CourseCategory cc: subCat){

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for(Course c2:allCrsCat){
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if(top20>=topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20=0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for(int j=topCat.size()-2;j>0;){
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if(topCat[j]>topCat[j-2]){
                    int t = topCat[j-2]
                    CourseCategory tc = topCat[j-1]
                    topCat[j-2] = topCat[j]
                    topCat[j-1] = topCat[j+1]
                    topCat[j] = t
                    topCat[j+1] = tc
                }
                j=j-2
            }
            // ////println("After::topCat:"+topCat)

        }
        ////println("topCat:"+topCat)
        top20=0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for(int i=1;i<topCat.size();i+=2)
        {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
             topSubCat.add(tm.getValue())
         }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        ////println("topSubCat:"+topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for(CourseCategory cc: topSubCat){
            ////println("CC:"+cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if(cnt.size==0)
                continue
            if(showTopCat==4){
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory{
                    eq ('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1=catCrs[0]
        cat1=catCrs[1]
        def catname2=catCrs[2]
        cat2=catCrs[3]
        def catname3=catCrs[4]
        cat3=catCrs[5]
        def catname4=catCrs[6]
        cat4=catCrs[7]
        ////println("cat:"+cat1+"\n"+cat2+"\n"+cat3+"\n"+cat4)
        ////println("catCrs:"+catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for(CourseCategory mc:mainCat){
            ArrayList<CourseCategory> mm = new ArrayList()
            mm.add(mc)
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            mm.add(sm)
            mainmenu.add(mm)
        }

        ////println("Categories MM SM:"+mainmenu.name)
        // session.catCrs = catCrs
        [crs:topCrs,cat1:cat1,cat2:cat2,cat3:cat3,cat4:cat4,cats1:cat1.size(),cats2:cat2.size(),cats3:cat3.size(),cats4:cat4.size(),catname1:catname1,catname2:catname2,catname3:catname3,catname4:catname4,menus:mainmenu]
    }
    def coursenotfound()
    {
        ////println("path:"+request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        ////println("result:"+results.size())
        ArrayList topCrs = new ArrayList<Course>()
        int top20 = 0;
        for(Course c1: results){
            if(top20==top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        ////println("topCrs:"+topCrs)
        top20=0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for(CourseCategory cc: subCat){

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for(Course c2:allCrsCat){
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if(top20>=topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20=0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for(int j=topCat.size()-2;j>0;){
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if(topCat[j]>topCat[j-2]){
                    int t = topCat[j-2]
                    CourseCategory tc = topCat[j-1]
                    topCat[j-2] = topCat[j]
                    topCat[j-1] = topCat[j+1]
                    topCat[j] = t
                    topCat[j+1] = tc
                }
                j=j-2
            }
            // ////println("After::topCat:"+topCat)

        }
        ////println("topCat:"+topCat)
        top20=0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for(int i=1;i<topCat.size();i+=2)
        {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
             topSubCat.add(tm.getValue())
         }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        ////println("topSubCat:"+topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for(CourseCategory cc: topSubCat){
            ////println("CC:"+cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if(cnt.size==0)
                continue
            if(showTopCat==4){
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory{
                    eq ('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1=catCrs[0]
        cat1=catCrs[1]
        def catname2=catCrs[2]
        cat2=catCrs[3]
        def catname3=catCrs[4]
        cat3=catCrs[5]
        def catname4=catCrs[6]
        cat4=catCrs[7]
        ////println("cat:"+cat1+"\n"+cat2+"\n"+cat3+"\n"+cat4)
        ////println("catCrs:"+catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for(CourseCategory mc:mainCat){
            ArrayList<CourseCategory> mm = new ArrayList()
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            ArrayList<CourseCategory> ssm = new ArrayList()
            boolean f = false
            ////println("CC:"+mc.name)
            for(CourseCategory cco : sm){
                def crs = Course.findAllByCoursecategory(cco)
                if(crs.size()>0){
                    f=true
                    //break
                    ssm.add(cco)
                }
            }
            def crs = Course.findAllByCoursecategory(mc)
            ////println("crs:"+crs.course_name)
            if(crs.size()>0){
                f=true
                //break
            }
            if(f) {
                mm.add(mc)
            }
            else
                continue
            mm.add(ssm)
            mainmenu.add(mm)
            ////println("CC added:"+mc.name)

        }

        ////println("Categories MM SM:"+mainmenu.name)
        // session.catCrs = catCrs
        [crs:topCrs,cat1:cat1,cat2:cat2,cat3:cat3,cat4:cat4,cats1:cat1.size(),cats2:cat2.size(),cats3:cat3.size(),cats4:cat4.size(),catname1:catname1,catname2:catname2,catname3:catname3,catname4:catname4,menus:mainmenu]
    }
    
    def erplogin() {
        println("I am in erp login - " + params)
        // SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        // sendMailService.sendmail("volp.vu@gmail.com","volp@108","deepak.pawar@vit.edu","Your Leave Application dated:" + formatter.format(new java.util.Date()) + " is Sanctioned..","Your Leave Application Dated:" + new java.util.Date() + " is Sanctioned..\n\n Details are as below:" + "\n\n Leave Type:" + "xyz" + "\n\n From Date:" + new java.util.Date() + " \n To Date:" + new java.util.Date() +"\n\nNumber of Days:"+"3"+ " \n\n Thanks","")
        // test()
        def loginlogo = Loginlogo.list()[0]
        def image
        def text_color = "#F39C12"
        def button_color = "#F39C12"
        if(loginlogo) {
            image = loginlogo?.logo_path + loginlogo?.logo_name
            text_color = loginlogo?.text_color
            button_color = loginlogo?.button_color
        }

        if (params.logout) {
            session.loginid = null
            println("session Invalidated...")
            session.invalidate()
            redirect(controller: "Login", action: "erplogin")
        } else if (params.username) {
             [emp_code: params.username, loginlogo:image, text_color : text_color, button_color : button_color]
        } else {
            if(params.error){
                def error = flash.loginerror
                flash.loginerror = error
                [test : 'test', loginlogo:image, text_color : text_color, button_color : button_color]
            }else if (session.loginid == null) {
                session.invalidate()
                [loginlogo:image, text_color : text_color, button_color : button_color]
            } else {
                println("session.loginid is not null" + session.loginid)
                redirect(controller: "Login", action: "erphome")
            }
        }
    }

    def erplogin2()
    {
        println("I am in erp login - "+params)
       // SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
       // sendMailService.sendmail("volp.vu@gmail.com","volp@108","deepak.pawar@vit.edu","Your Leave Application dated:" + formatter.format(new java.util.Date()) + " is Sanctioned..","Your Leave Application Dated:" + new java.util.Date() + " is Sanctioned..\n\n Details are as below:" + "\n\n Leave Type:" + "xyz" + "\n\n From Date:" + new java.util.Date() + " \n To Date:" + new java.util.Date() +"\n\nNumber of Days:"+"3"+ " \n\n Thanks","")
       // test()

        if(params.logout){
            session.loginid = null
            //session.invalidate()
            redirect(controller: "Login", action: "erplogin")
        }else {
            if (session.loginid == null) {
                println("session.loginid is null")
                //session.invalidate()
            } else {
                println("session.loginid is not null" + session.loginid)
                redirect(controller: "Login", action: "erphome")
            }
        }
       //
    }
  /*  def test()
    {
        RoleType roletype=RoleType.findByType("Leave Management")
        ////println("roletype::"+roletype)
        //If Days Are Expired, then recommend/approve leave automatically
        LeaveApprovalStatus leaveapprovalstatusauto=LeaveApprovalStatus.findByStatus("inprocess")
        def employeeleavetransactionapprovalauto=EmployeeLeaveTransactionApproval.findAllByLeaveapprovalstatus(leaveapprovalstatusauto)
        for(EmployeeLeaveTransactionApproval elta:employeeleavetransactionapprovalauto)
        {
            ApplicationAcademicYear aay=ApplicationAcademicYear.findByIsActiveAndRoletypeAndOrganization(true,roletype,elta.organization)
            ////println("ApplicationAcademicYear::"+aay)
            java.util.Date currentdate=new java.util.Date()
            ////println("Current Date::"+currentdate)
            ////println("Application Push Date::"+elta.application_push_date)
            ////println("Approval period days:"+elta.leaveapprovalhierarchy.approval_period_days)
            //let us calculate diffrence between two dates
            long diff = currentdate.getTime() - elta.application_push_date.getTime()
            int diffdays=(int)TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
            ////println("Actual Difference Days:"+diffdays)
            if(diffdays>=elta.leaveapprovalhierarchy.approval_period_days)  //if diffrenece is larger, then do automatic approval/recomondation
            {
                ////println("Approval Days Exhausted..")
                if (elta.leaveapprovalhierarchy.leaveapporvalauthority.authority == "TRUST")
                {
                    ////println("I am in TRUST auto approval")
                    //Automatic Approval By TRUST
                    leaveapprovalstatusauto=LeaveApprovalStatus.findByStatus("approved")
                    elta.leaveapprovalstatus=leaveapprovalstatusauto
                    elta.save(failOnError:true,flush:true)
                    //If last aprroved approval then change final status to approved and decrement leave balance
                    if(elta.leaveapprovalhierarchy.is_last_approval==true && leaveapprovalstatusauto.status=="approved") {
                        elta.employeeleavetransaction.leaveapprovalstatus = leaveapprovalstatusauto
                        elta.save(failOnError: true, flush: true)
                        EmployeeLeaveBalanceSheet employeeleavebalancesheet = null
                        if (elta.employeeleavetransaction.leavetype.type == "CL") {
                            employeeleavebalancesheet = EmployeeLeaveBalanceSheet.findByInstructorAndOrganizationAndLeavetypeAndEmployeetypeAndAcademicyear(elta.employeeleavetransaction.instructor, elta.organization, elta.employeeleavetransaction.leavetype, elta.employeeleavetransaction.instructor.employeetype, aay.academicyear)
                            ////println("employeeleavebalancesheet:" + employeeleavebalancesheet)
                            employeeleavebalancesheet.balance = employeeleavebalancesheet.balance - elta.employeeleavetransaction.numberofdays
                            if (employeeleavebalancesheet.balance < 0) {
                                leaveapprovalstatusauto = LeaveApprovalStatus.findByStatus("rejected")
                                elta.leaveapprovalstatus = leaveapprovalstatusauto
                                elta.save(failOnError: true, flush: true)
                                employeeleavebalancesheet.balance = employeeleavebalancesheet.balance + elta.employeeleavetransaction.numberofdays
                            } else
                                employeeleavebalancesheet.total_availed_leaves = employeeleavebalancesheet.total_availed_leaves + elta.employeeleavetransaction.numberofdays
                            employeeleavebalancesheet.save(failOnError: true, flush: true)
                        } else if (elta.employeeleavetransaction.leavetype.type == "HD-CL") {
                            employeeleavebalancesheet = EmployeeLeaveBalanceSheet.findByInstructorAndOrganizationAndLeavetypeAndEmployeetypeAndAcademicyear(elta.employeeleavetransaction.instructor, elta.organization, "CL", elta.employeeleavetransaction.instructor.employeetype, aay.academicyear)
                            employeeleavebalancesheet.balance = employeeleavebalancesheet.balance - (elta.employeeleavetransaction.numberofdays / 2.0)
                            if (employeeleavebalancesheet.balance < 0) {
                                leaveapprovalstatusauto = LeaveApprovalStatus.findByStatus("rejected")
                                elta.leaveapprovalstatus = leaveapprovalstatusauto
                                elta.save(failOnError: true, flush: true)
                                employeeleavebalancesheet.balance = employeeleavebalancesheet.balance + (elta.employeeleavetransaction.numberofdays / 2.0)
                            } else
                                employeeleavebalancesheet.total_availed_leaves = employeeleavebalancesheet.total_availed_leaves + (elta.employeeleavetransaction.numberofdays / 2)
                            employeeleavebalancesheet.save(failOnError: true, flush: true)
                        } else if (elta.employeeleavetransaction.leavetype.type == "EL" || elta.employeeleavetransaction.leavetype.type == "HPL") {
                            employeeleavebalancesheet = EmployeeLeaveBalanceSheet.findByInstructorAndOrganizationAndLeavetypeAndEmployeetype(elta.employeeleavetransaction.instructor, elta.organization, elta.employeeleavetransaction.leavetype, elta.employeeleavetransaction.instructor.employeetype)
                            employeeleavebalancesheet.balance = employeeleavebalancesheet.balance - elta.employeeleavetransaction.numberofdays
                            if (employeeleavebalancesheet.balance < 0) {
                                leaveapprovalstatusauto = LeaveApprovalStatus.findByStatus("rejected")
                                elta.leaveapprovalstatus = leaveapprovalstatusauto
                                elta.save(failOnError: true, flush: true)
                                employeeleavebalancesheet.balance = employeeleavebalancesheet.balance + e.employeeleavetransaction.numberofdays
                            } else
                                employeeleavebalancesheet.total_availed_leaves = employeeleavebalancesheet.total_availed_leaves + e.employeeleavetransaction.numberofdays
                            employeeleavebalancesheet.save(failOnError: true, flush: true)
                        } else if (employeeleavebalancesheet.leavetype.type == "ML") {
                            employeeleavebalancesheet = EmployeeLeaveBalanceSheet.findByInstructorAndOrganizationAndLeavetypeAndEmployeetype(elta.employeeleavetransaction.instructor, elta.organization, "HPL", elta.employeeleavetransaction.instructor.employeetype)
                            employeeleavebalancesheet.balance = employeeleavebalancesheet.balance - (elta.employeeleavetransaction.numberofdays * employeeleavebalancesheet?.leavetype?.aditionalinfo1)
                            if (employeeleavebalancesheet.balance < 0) {
                                leaveapprovalstatusauto = LeaveApprovalStatus.findByStatus("rejected")
                                e.leaveapprovalstatus = leaveapprovalstatusauto
                                elta.save(failOnError: true, flush: true)
                                employeeleavebalancesheet.balance = employeeleavebalancesheet.balance + (elta.employeeleavetransaction.numberofdays * employeeleavebalancesheet?.leavetype?.aditionalinfo1)
                            } else
                                employeeleavebalancesheet.total_availed_leaves = employeeleavebalancesheet.total_availed_leaves + (elta.employeeleavetransaction.numberofdays * employeeleavebalancesheet?.leavetype?.aditionalinfo1)
                            employeeleavebalancesheet.save(failOnError: true, flush: true)
                        }
                        ////println("leave is approved by all levels, let us decrement balance,employeeleavebalancesheet::" + employeeleavebalancesheet)
                        //send mail to faculty
                        ////println("Sending Mail to Faculty...")
                        //As leave is sanctioned, send mail to faculty
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy")
                        if (elta.leaveapprovalstatus.status == "rejected")
                            sendMailService.sendmail(elta.organization.establishment_email, elta.organization.establishment_email_credentials, elta.employeeleavetransaction.instructor.uid, "Your Leave Application dated:" + formatter.format(elta.employeeleavetransaction.application_date) + " is NOT Sanctioned..", "Your Leave Application Dated:" + formatter.format(elta.employeeleavetransaction.application_date) + " is rejected.\n\nDetails are as below:" + "\n\n Leave Type:" + elta.employeeleavetransaction.leavetype.type + "\n\n From Date:" + formatter.format(elta.employeeleavetransaction.fromdate) + " \nTo Date:" + formatter.format(elta.employeeleavetransaction.todate) + "\n\nNumber of Days:" + elta.employeeleavetransaction.numberofdays + " \n\n Thanks", "")
                        else
                            sendMailService.sendmail(elta.organization.establishment_email, elta.organization.establishment_email_credentials, elta.employeeleavetransaction.instructor.uid, "Your Leave Application dated:" + formatter.format(elta.employeeleavetransaction.application_date) + " is Sanctioned..", "Your Leave Application Dated:" + formatter.format(elta.employeeleavetransaction.application_date) + " is Sanctioned.\n\nDetails are as below:" + "\n\n Leave Type:" + elta.employeeleavetransaction.leavetype.type + "\n\n From Date:" + formatter.format(elta.employeeleavetransaction.fromdate) + " \nTo Date:" + formatter.format(elta.employeeleavetransaction.todate) + "\n\nNumber of Days:" + elta.employeeleavetransaction.numberofdays + " \n\n Thanks", "")
                    }
                }
                else
                {
                    ////println("I am in other auto reco::"+elta.leaveapprovalhierarchy.leaveapporvalauthority.authority )
                    //Automatic Recommondation By HOD, Registrar and Director
                    leaveapprovalstatusauto=LeaveApprovalStatus.findByStatus("approved")
                    elta.leaveapprovalstatus=leaveapprovalstatusauto
                    elta.save(failOnError:true,flush:true)
                    //if it is not last recomondation, if this level has recomonded, then send it to next level for approval
                    if(elta.leaveapprovalhierarchy.is_last_approval==false && leaveapprovalstatusauto.status=="approved")
                    {
                        LeaveApprovalHierarchy leaveapprovalhierarchy=LeaveApprovalHierarchy.findByLevelAndLeavetypeAndOrganization(elta.leaveapprovalhierarchy.level+1,elta.employeeleavetransaction.leavetype,elta.employeeleavetransaction.instructor.organization)
                        if(elta.employeeleavetransaction.instructor.reportingorganization!=null && leaveapprovalhierarchy.leaveapporvalauthority.authority!="Registrar")
                        {
                            leaveapprovalhierarchy = LeaveApprovalHierarchy.findByLevelAndLeavetypeAndOrganization(elta.leaveapprovalhierarchy.level + 1, elta.employeeleavetransaction.leavetype, elta.employeeleavetransaction.instructor.reportingorganization)
                        }
                        EmployeeLeaveTransactionApproval employeeleavetransactionapproval=new EmployeeLeaveTransactionApproval()
                        employeeleavetransactionapproval.remark=""
                        employeeleavetransactionapproval.application_push_date=new java.util.Date()
                        employeeleavetransactionapproval.username="back-end-system"
                        employeeleavetransactionapproval.creation_date=new java.util.Date()
                        employeeleavetransactionapproval.updation_date=new java.util.Date()
                        employeeleavetransactionapproval.creation_ip_address="back-end-system"
                        employeeleavetransactionapproval.updation_ip_address="back-end-system"
                        employeeleavetransactionapproval.organization=elta.organization
                        employeeleavetransactionapproval.employeeleavetransaction=elta.employeeleavetransaction
                        LeaveApprovalStatus leaveapprovalstatus_inprocess=LeaveApprovalStatus.findByStatus("inprocess")
                        employeeleavetransactionapproval.leaveapprovalstatus=leaveapprovalstatus_inprocess
                        employeeleavetransactionapproval.leaveapprovalhierarchy=leaveapprovalhierarchy
                        employeeleavetransactionapproval.save(failOnError:true,flush:true)
                    }

                }
                //Store Automatic Approval History
                LeaveAutomaticApprovalHistory leaveautomaticapprovalhistory=LeaveAutomaticApprovalHistory.findByAcademicyearAndOrganizationAndEmployeeleavetransactionapproval(aay.academicyear,elta.organization,elta)
                if(leaveautomaticapprovalhistory==null)
                {
                    leaveautomaticapprovalhistory=new LeaveAutomaticApprovalHistory()
                    leaveautomaticapprovalhistory.academicyear=aay.academicyear
                    leaveautomaticapprovalhistory.organization=elta.organization
                    leaveautomaticapprovalhistory.employeeleavetransactionapproval=elta
                    leaveautomaticapprovalhistory.approvaldate=new java.util.Date()
                    leaveautomaticapprovalhistory.username="back-end-system"
                    leaveautomaticapprovalhistory.creation_date=new java.util.Date()
                    leaveautomaticapprovalhistory.updation_date=new java.util.Date()
                    leaveautomaticapprovalhistory.creation_ip_address="back-end-system"
                    leaveautomaticapprovalhistory.updation_ip_address="back-end-system"
                    leaveautomaticapprovalhistory.save(failOnError:true,flush:true)
                }
            }

        }

        // execute job
        HashMap<String,ArrayList> maillistoflist=new HashMap<String,ArrayList>()   // tomail EmployeeLeaveTransactionApproval
        String tomail=""
        LeaveApprovalStatus leaveapprovalstatus=LeaveApprovalStatus.findByStatus("inprocess")
        def employeeleavetransactionapproval=EmployeeLeaveTransactionApproval.findAllByLeaveapprovalstatus(leaveapprovalstatus)
        ////println("employeeleavetransactionapproval::"+employeeleavetransactionapproval)
        //initailisation of hashmap
        for(EmployeeLeaveTransactionApproval elta:employeeleavetransactionapproval)
        {
            if (elta.leaveapprovalhierarchy.leaveapporvalauthority.authority == "Registrar")
                tomail = elta.organization.official_registrar_email
            if (elta.leaveapprovalhierarchy.leaveapporvalauthority.authority == "Director")
                tomail = elta.organization.official_director_email
            if (elta.leaveapprovalhierarchy.leaveapporvalauthority.authority == "TRUST")
                tomail = elta.organization.official_bract_email
            if (elta.leaveapprovalhierarchy.leaveapporvalauthority.authority == "HOD")
            {
                if (elta.employeeleavetransaction.instructor.department != null)
                    tomail = elta.employeeleavetransaction.instructor.department.official_department_email
            }
            else if(elta.approvedby!=null)  //if it is direct reporting authority
            {
                tomail=elta.approvedby.uid
            }
            ArrayList myrow=new ArrayList()
            if(tomail!=null)
                maillistoflist.put(tomail,myrow)
        }
        //data filling
        for(EmployeeLeaveTransactionApproval elta:employeeleavetransactionapproval)
        {
            if (elta.leaveapprovalhierarchy.leaveapporvalauthority.authority == "Registrar")
                tomail = elta.organization.official_registrar_email
            if (elta.leaveapprovalhierarchy.leaveapporvalauthority.authority == "Director")
                tomail = elta.organization.official_director_email
            if (elta.leaveapprovalhierarchy.leaveapporvalauthority.authority == "TRUST")
                tomail = elta.organization.official_bract_email
            if (elta.leaveapprovalhierarchy.leaveapporvalauthority.authority == "HOD") {
                if (elta.employeeleavetransaction.instructor.program != null)
                    tomail = elta.employeeleavetransaction.instructor.program.department.official_department_email
                else
                    tomail = elta.employeeleavetransaction.instructor.department.official_department_email
            }
            else if(elta.approvedby!=null)  //if it is direct reporting authority
            {
                tomail=elta.approvedby.uid
            }
            if (tomail!=null)
                maillistoflist.get(tomail).add(elta)
        }
        //As leave is in-process, send mail to concern approving authority
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy")
        for (Map.Entry<String, ArrayList> entry : maillistoflist.entrySet())
        {
            String establishment_email=""
            String establishment_email_credentials=""
            String
            ////println("Send mail::" + entry.getKey() + "\n")
            def list = entry.getValue()
            String data = "Dear Sir,\nPlease Visit to VI ERP Software(http://www.vierp.in) and review leaves of following faculty(s), if you dont review, then leaves will be automatically approved by the system on behalf of you.\n\n\n"
            int sno = 1
            for (EmployeeLeaveTransactionApproval elt : list)
            {
                ApplicationAcademicYear aay=ApplicationAcademicYear.findByIsActiveAndRoletypeAndOrganization(true,roletype,elt.organization)
                ////println("ApplicationAcademicYear::"+aay)
                //Store Mail Sending History
                LeaveNotificationHistory leavenotificationhistory=LeaveNotificationHistory.findByAcademicyearAndOrganizationAndEmployeeleavetransactionapproval(aay.academicyear,elt.organization,elt)
                if(leavenotificationhistory==null)
                {
                    leavenotificationhistory=new LeaveNotificationHistory()
                    leavenotificationhistory.academicyear=aay.academicyear
                    leavenotificationhistory.organization=elt.organization
                    leavenotificationhistory.employeeleavetransactionapproval=elt
                    leavenotificationhistory.mailsentdate=new java.util.Date()
                    leavenotificationhistory.username="back-end-system"
                    leavenotificationhistory.creation_date=new java.util.Date()
                    leavenotificationhistory.updation_date=new java.util.Date()
                    leavenotificationhistory.creation_ip_address="back-end-system"
                    leavenotificationhistory.updation_ip_address="back-end-system"
                    leavenotificationhistory.save(failOnError:true,flush:true)
                    String temp = "" + sno + ":"
                    temp = elt.employeeleavetransaction.instructor.employee_code + " : " + elt.employeeleavetransaction.instructor.person.fullname_as_per_previous_marksheet + " :: " + elt.employeeleavetransaction.leavetype.display_name + " : Number of Days:" + elt.employeeleavetransaction.numberofdays + " :: From Date: " + formatter.format(elt.employeeleavetransaction.fromdate) + " -To Date: " + formatter.format(elt.employeeleavetransaction.todate)
                    temp = temp + "\n\n\n"
                    data = data + temp
                    sno++
                    establishment_email=elt.organization.establishment_email
                    establishment_email_credentials=elt.organization.establishment_email_credentials
                }
            }
            data = data + "\nThanks."
            ////println(data)
            if(!establishment_email.equals("") && sno>1)
                sendMailService.sendmail(establishment_email,establishment_email_credentials,entry.getKey(),"Review Faculty Leaves in VI ERP Software",data,"")
        }

    } */

    def processerplogin()
    {
        println("I am in processerplogin...123"+params)
        session.profilephoto = null
        session.isphotopresent = false
        ////println("I am in processerplogin..."+session)
        String username=params.username.replaceAll(" ", "")
        String password=params.password
        Organization organization = null
        ////println(username+"::"+password)
        def roletypelist=new ArrayList()
        int flagemailorinstitutecode=0      //email=0   and institutecode=1
        Login login=Login.findByUsernameAndPasswordAndIsloginblocked(username,password,false)
//        if(!login) {
//            login=Login.findByUsernameAndPasswordAndIsloginblocked(username,password,false)
//        }
        if(login==null) {
            Login blocked = Login.findByUsernameAndPasswordAndIsloginblocked(username,password,true)
            println("123")
            if(blocked)
                 flash.loginerror = "Your Login Is Blocked..!!!"
            else
                 flash.loginerror = "You Have Entered Invalid Username Or Password...!"

            redirect(controller: 'login', action: 'erplogin', params : [error : true])
            return
        }
        flash.loginerror = ""
        session.loginid = login.id
        //username
        ApplicationType at = ApplicationType.findByApplication_type("ERP")
        session.user = login.username
        def ut = login.usertype

        AWSFolderPath awsFolderPath = AWSFolderPath.findById('5')
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        AWSBucketService awsBucketService = new AWSBucketService()

        for(UserType u:ut) {
            if(u.type=="Employee") {
                Instructor inst=Instructor.findByUid(login.username)
                session.isstudent = false
                session.orgid = inst.organization.id
//                if(inst.organization.id == 10 || inst.organization.id == 11 ) {
//                    session.orgheadername = inst.organization.organization_name
//                } else {
//                    session.orgheadername = "Vishwakarma Group Of Institutes"
//                }
                session.orgheadername = inst?.organization?.organizationgroup?.name
                organization = inst.organization
                Facultyidcard facultyidcard = Facultyidcard.findByInstructorAndOrganization(inst, inst.organization)
                if(facultyidcard) {
                    def path = awsFolderPath.path + facultyidcard.icardPath + facultyidcard.icardPhotoFile
                    def profilephoto = awsBucketService.getPresignedUrl(awsBucket.bucketname,path, awsBucket.region)
                    println("profilephoto :: " + profilephoto)
                    boolean isphotopresent = true
                    if(facultyidcard.icardPhotoFile == null)
                        isphotopresent = false
                    if(facultyidcard.icardPath == null)
                        isphotopresent = false
                    session.profilephoto = profilephoto
                    session.isphotopresent = isphotopresent
                } else {

                }
                def name = inst.person.firstName + " " + inst.person.lastName
                session.userfullname = name
            }
            if(u.type=="Student") {
                println("1")
                Learner learner = Learner.findByUid(login.username)
                println("2")
                session.loginid = ""
                session.user = ""
                flash.loginerror = "Please login into " + learner.organization.learner_website
                println("3")
                redirect(controller: 'login', action: 'erplogin', params : [error : true])
                return
                /*Learner learner=Learner.findByUid(login.username)
                session.isstudent = true
                //session.learner=learner
                session.orgid = learner.organization.id
                organization = learner.organization
//                if(learner.organization.id == 10 || learner.organization.id == 11 ) {
//                    session.orgheadername = learner.organization.organization_name
//                } else {
//                    session.orgheadername = "Vishwakarma Group Of Institutes"
//                }
                session.orgheadername = learner?.organization?.organizationgroup?.name
                Studentidcard studentidcard = Studentidcard.findByLearnerAndOrganization(learner, learner.organization)
                if(studentidcard) {
                    def path = awsFolderPath.path + studentidcard.icardPath + studentidcard.icardPhotoFile
                    def profilephoto = awsBucketService.getPresignedUrl(awsBucket.bucketname,path, awsBucket.region)
                    boolean isphotopresent = true
                    if(studentidcard.icardPhotoFile == null)
                        isphotopresent = false
                    if(studentidcard.icardPath == null)
                        isphotopresent = false

                    session.profilephoto = profilephoto
                    session.isphotopresent = isphotopresent
                    //println("session.profilephoto :: " + session.profilephoto)
                } else {

                }
                ////println("session.learner::"+session.learner)
                def name = learner.person.firstName + " " + learner.person.lastName
                session.userfullname = name*/
            }
            if(u.type == "Vendor"){
                println"usertype == vendor found"
                InvVendor vendor = InvVendor.findByContact_person_email(login.username)
                session.isstudent = false
                session.orgid = vendor.organization.id
                session.orgheadername = vendor?.organization?.organizationgroup?.name
                organization = vendor.organization
                session.userfullname = login.username
            }
        }
        // def rt=RoleType.findAllByApplicationtypeAndOrganization(at, organization)   //all modules of ERP
        //let us check which modules are applicable, dont show modules which are not relevent to that user
        ////println("login.roles::"+login.roles)
        ArrayList rt=new ArrayList()
        TreeSet unique_roletype_id = new TreeSet()  //unique role types
        for(r in login.roles)
        {
            if(r.roletype.applicationtype==at)
                unique_roletype_id.add(r.roletype.id)
        }
        ////println("unique_roletype_id::"+unique_roletype_id)
        for(u in unique_roletype_id)
        {

            RoleType rtobj=RoleType.findById(u)
            rt.add(rtobj)
        }
        session.rt=rt
        //println("Role type(module) list:"+rt)


        def roles = login.roles

        ArrayList<RoleLink> rolelinkslist = new ArrayList<RoleLink>()
        def rolelinks = RoleLink.findAllByIsquicklinkAndRoleInListAndIsrolelinkactiveAndOrganization(true, roles, true, organization)
        for(RoleLink r : rolelinks)
        {
            boolean ispreset = false
            for(RoleLink rl : rolelinkslist)
            {
                if(r.id == rl.id )
                {
                    ispreset = true
                }
            }
                if(r != null && (!ispreset)){
                     rolelinkslist.add(r)
            }
        }
        def quicklinks = Quicklink.findAllByLoginAndIsactive(login, true)
        for(quicklink in quicklinks){
            if(quicklink.rolelinks.isrolelinkactive == true)
            {
                RoleLink  rl = quicklink.rolelinks
                rolelinkslist.add(rl)
            }
        }
        //println("rolelinkslist :: " + rolelinkslist)
        session.fixedrolelinkslist=rolelinkslist


        //------------------------------------------------------------------------------------------------------Search Role Link By Mayur(27-1-20)

        def rt12
        def ut1=login?.usertype

        ApplicationType at1=ApplicationType.findByApplication_type("ERP")

        def roles1=login?.roles

        def types1=roles1?.roletype?.id

        def apptype1=roles1?.roletype?.applicationtype
        //println("apptype:"+apptype)
        ArrayList rt11=new ArrayList()
        TreeSet unique_roletype_id1 = new TreeSet()  //unique role types
        for(r in roles1)
        {
            if(r?.roletype?.applicationtype?.id == at1?.id)
                unique_roletype_id1.add(r?.roletype?.id)
        }
        //println("unique_roletype_id1::"+unique_roletype_id1)

        for(u in unique_roletype_id1)
        {
            RoleType rtobj1=RoleType.findById(u)
            rt11.add(rtobj1)
        }
        rt11=rt12


        ArrayList<RoleLink> rolelinkslist1 = new ArrayList<RoleLink>()
        def rolelinks1 = RoleLink.findAllByRoleInListAndIsrolelinkactiveAndOrganization(roles,true,organization)
        for(RoleLink  r1 : rolelinks1)
        {
            boolean ispreset1 = false
            for(RoleLink  rl1 : rolelinkslist1)
            {
                if(r1?.id == rl1?.id )
                {
                    ispreset1 = true
                }
            }
            if(r1 != null && (!ispreset1)){
                rolelinkslist1.add(r1)
            }
        }

        //println("rolelinkslist1 :: " + rolelinkslist1?.link_name)
        //[rolelinkslist1:rolelinkslist1]
        session.dynamicrolelinks=rolelinkslist1
       // println("session :: " +   session.dynamicrolelinks)

//--------------------------------------------------------------------------------------------------------------------------------------------


        // new template side bar list - PPS - 14_03_2020
        def smarttemplatesidebarlist = []
        def smartrolelist = login.roles
        smartrolelist = smartrolelist.sort { it.sort_order }
        if(session.isstudent) {
            // sample [roletype, [role, [rolelinks]], roletype, [role, [rolelinks]]]
            def roletypelist2 = []
            for (role in smartrolelist) {
                RoleType roletype = role.roletype
                roletypelist2.add(roletype)
            }
            def smartroletypelist = Role.createCriteria().list() {
                projections {
                    distinct("roletype")
                }
                and {
                    'in'('roletype', roletypelist2)
                    'in'('organization', organization)
                }
            }.sort { it.sort_order }
            for (roletype in smartroletypelist) {
                def list = []
                list.add(roletype)
                def sublist = []
                for (role in smartrolelist) {
                    if (role.roletype.id == roletype.id) {
                        def subsublist = []
                        def smartrolelinks = RoleLink.createCriteria().list() {
                            'in'('organization', organization)
                            and {
                                'in'('role', role)
//                                'in'('roletype', roletype)
//                                'in'('usertype', login.usertype)
                                'in'('isrolelinkactive', true)
                            }
                        }.sort { it.sort_order }
                        sublist.add(role: role, smartrolelinks: smartrolelinks)
//                    sublist.add(smartrolelinks)
                    }
                    list.add(sublist)
                }
                smarttemplatesidebarlist.add(list)
            }
            session.smarttemplatesidebarlist = smarttemplatesidebarlist
            /*println("smarttemplatesidebarlist [0] :: " + smarttemplatesidebarlist[0])
            println("1 :: " + smarttemplatesidebarlist[0][0].type)
            println("2 :: " + smarttemplatesidebarlist[0][1])
            println("3 :: " + smarttemplatesidebarlist[0][1][0])
            println("4 :: " + smarttemplatesidebarlist[0][1][1])
            println("5 :: " + smarttemplatesidebarlist[1][1])*/
        } else {
            Organization orgn = Organization.findById(session.orgid)
            def newsmartroletypelist = RoleType.findAllByOrganization(orgn).sort { it.sort_order }
            //println("newsmartroletypelist :: " + newsmartroletypelist)
            def finalrolelinklist = []
            for (roletype in newsmartroletypelist) {
                //println("roletype  :: " + roletype)
                def list = []
                def sublist = []
                for (role in smartrolelist) {
                    //println("role :: " + role)
                    if (roletype.id == role.roletype.id) {
                        //println("role :: " + role)
                        def linkslist = RoleLink.createCriteria().list() {
                            'in'('organization', organization)
                            and {
                                'in'('role', role)
//                                'in'('roletype', roletype)
//                                'in'('usertype', login.usertype)
                                'in'('isrolelinkactive', true)
                            }
                        }.sort { it.sort_order }
                        //println("linkslist  : " + linkslist.size())
                        if (linkslist.size() > 0) {
                            sublist.add(role: role, rolelinks: linkslist)
                            //println("sublist ::  " + sublist)
                        }
                    }
                    //println("roletype  :: " + roletype)
                }
                //println("sublist :: " + sublist)
                if (sublist.size() > 0) {
                    list.add(roletype: roletype, link: sublist)
                    smarttemplatesidebarlist.add(list)
                }
                //println("list :: " + list)
            }
            session.smarttemplatesidebarlist = smarttemplatesidebarlist
            /*for (roletype in smarttemplatesidebarlist) {
                println("role type :: " + roletype.roletype.type)
                for (role in roletype.link) {
                    println("role :: " + role.role.role)
                    for (link in role.rolelinks) {
                        println("rolelink :: " + link.link_name)
                    }
                }
            }*/
        }
        // end new template side bar list - PPS - 14_03_2020

/*Organization orgn = Organization.findById(session.orgid)
        for(UserType u:ut) {
            //UserType userType1 = UserType.findByType("Student")
            if(u.type=="Employee") {
                for (roleType in rt) {
//                    def roles = Role.findAllByRoletypeAndOrganization(roleType, orgn)
                    def roles = Role.findAllByRoletypeAndUsertypeAndOrganization(roleType, u, orgn)
                    //println("roles ::  "+ roles)
                    for (r in roles) {
//                        def rolelinks = RoleLink.findAllByRoleAndOrganizationAndIsquicklinkAndIsrolelinkactive(r, login.organization, true, true)
                        def rolelinks = RoleLink.findAllByRoleAndOrganizationAndIsquicklinkAndIsrolelinkactive(r, login.organization, true, true)
                        //println("rolelinks :: " + rolelinks)
                        for (RoleLink  rl : rolelinks) {
                            rolelinkslist.add(rl)
                        }
                    }
                }
            }
            Organization orgn = Organization.findById(session.orgid)
            if(u.type=="Student"){
                for (roleType in rt) {
//                    def roles = Role.findAllByRoletypeAndOrganization(roleType, orgn)
                    def roles = Role.findAllByRoletypeAndUsertypeAndOrganization(roleType, u, orgn)
                    //println("roles ::  "+ roles)
                    for (r in roles) {
//                        def rolelinks = RoleLink.findAllByRoleAndOrganizationAndIsquicklinkAndIsrolelinkactive(r, login.organization, true, true)
                        def rolelinks = RoleLink.findAllByRoleAndOrganizationAndIsquicklinkAndIsrolelinkactive(r, login.organization, true, true)
                        //println("rolelinks :: " + rolelinks)
                        for (RoleLink  rl : rolelinks) {
                            rolelinkslist.add(rl)
                        }
                    }
                }
            }
        }
        */
        session.apptype="ERP"
        println("------")

        if(login.username == login.password){
            def learner = false
            def vendor = false
            for(e in login.usertype){
                //println"e--->"+e
                if(e.type == 'Learner' || e.type == 'Student')
                    learner = true
                else if(e.type == "Vendor"){
                    vendor =true
                }
            }
            if(vendor){
                println"redirecting to vendor dashboard"
                redirect(controller: "Login", action: "vendorDashboard")
                return
            }

            if(learner) {
                //redirect(controller:"Login", action: "erphome")
                if (session.isstudent) {
                    try {
                        if (learner.isemailverified || learner.ismobileverfied) {
                            redirect(controller: "Login", action: "verification")
                        } else {
                            redirect(controller: "Login", action: "erphome_student")
                        }
                    } catch(Exception e) {
                        redirect(controller: "Login", action: "erphome_student")
                    }
                } else {
                    redirect(controller: "Login", action: "erphome")
                }
            } else {
                if (login != null) {
                    //Registred..Send OTP for Password Reset
                    //Generate OTP
                    java.util.Random random = new java.util.Random()
                    String otp = ""
                    int min = 0, max = 0, n
                    for (int i = 1; i <= 6; i++) {
                        if (i == 1)
                            min = 1
                        else
                            min = 0
                        max = 9
                        n = random.nextInt(max) + min
                        otp = otp + n
                    }
                    Otp otpobj = Otp.findByEmail(login.username)
                    Date date = new java.util.Date()
                    if (otpobj == null) {
                        //insert otp
                        otpobj = new Otp()
                        otpobj.email = login.username
                        otpobj.otp = otp
                        otpobj.otpgenerationtime = date
                        otpobj.save(failOnError: true, flush: true)
                    } else {
                        //update otp
                        otpobj.otp = otp
                        otpobj.otpgenerationtime = date
                        otpobj.save(failOnError: true, flush: true)
                    }
                    println("Now sending mail of otp...." + otp)
                    sendMail {
                        to login.username
                        subject "Verify Your Email for VOLP/VIERP"
                        text "Your OTP for registration is " + otp + "\n" + "Thanks, VOLP/VIERP Team"
                    }
                    session.otpemail = login.username
                    flash.msg = "Your can not have your PRN No./Employee Code as a password... Please reset your password... Please check your registered email (" + login.username + ") for OTP"
                    redirect(controller: "login", action: "processemail")
                }
            }
        }
        else {
            if (session.isstudent) {
                Login login1 = Login.findById(session.loginid)
                Learner learner = Learner.findByUid(login1.username)
                println("learner :: " + learner.id)
                println("learner :: " + learner.isemailverified)
                println("learner :: " + learner.ismobileverfied)
                if (!learner.isemailverified || !learner.ismobileverfied) {
                    redirect(controller: "Login", action: "verification")
                } else {
                    redirect(controller: "Login", action: "erphome_student")
                }
                //redirect(controller: "login", action: "erphome_student")
            } else {
                redirect(controller: "login", action: "erphome")
            }
        }
    }

    def vendorDashboard(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }
        if(session.isstudent) {
            redirect(controller: "Login", action: "erphome_student")
        } else {
            Login log1 = Login.findByUsername(session.userfullname)
            //println"------->"+log1?.usertype?.type
                if(log1?.usertype?.type == "Vendor"){
                    println("I am in vendorDashboard::" + params)
                Organization organization = Organization.findById(session.orgid)
                ApplicationType at = ApplicationType.findByApplication_type("ERP")
                RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "Vendor", organization)
                [org: organization]
            }
            else{
                flash.message = "Please login again. Thank you."
                redirect(controller: "login", action: "erplogin")
            }
        }
    }

    def dynamicrolelinks()
    {
        //println("in dynamicrolelinks:"+params)
        [params:params]
    }

    def redirecttopage()
    {
        println("I am in redirecttopage::"+params)
        RoleLink  rolelinks=RoleLink.findById(params?.link)
        def controllername=rolelinks?.controller_name
        def actionname=rolelinks?.action_name
        println("controllername::"+controllername)
        println("actionname::"+actionname)
        redirect(controller: controllername, action: actionname )
    }

    def erphome()
    {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }
        if(session.isstudent) {
            redirect(controller: "Login", action: "erphome_student")
        } else {
            println("I am in erphome::" + params)
            Organization organization = Organization.findById(session.orgid)
            ApplicationType at = ApplicationType.findByApplication_type("ERP")
            RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "Registration", organization)
            ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true, organization)
            def ld = null
            def co = null
            if(aay != null)
            {
                 ld = LearnerDivision.findAllByAcademicyearAndSemesterAndOrganizationAndIsdeleted(aay.academicyear, aay.semester, organization, false)
                 co = ERPCourseOffering.findAllByAcademicyearAndSemesterAndOrganization(aay.academicyear, aay.semester, organization)
            }
            def inst = Instructor.findAllByIscurrentlyworkingAndOrganization(true, organization)
            def ldcount = 0
            def cocount = 0
            def instcount = 0
            if(ld) {
                ldcount = ld.size()
            }
            if(co) {
                cocount = co.size()
            }
            if(inst) {
                instcount = inst.size()
            }
            [org: organization, totalstudent: ldcount, coursecount: cocount, instcount: instcount]
        }
    }

    def erphome_student()
    {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        }
        println("I am in erphome_student::"+params)
    }
    def volpNewHome(){

//        ArrayList<Course> crsList = Course.list()
//        crsList.sort{it.rating}
        ////println("path:"+request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        //  ////println("result:"+results.size())
        ArrayList topCrs = new ArrayList<Course>()

        int top20 = 0;
        for(Course c1: results){
            if(top20==top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        int topthree = 0;
        ArrayList topthreeCrs = new ArrayList()
        for(Course c1: results){
            if(topthree==3)
                break;
            ArrayList data = new ArrayList()
            data.add(c1.id)
            data.add(c1.course_name)
            data.add(c1.coursecategory.name)
            data.add(c1.courseowner.person.firstName+" "+c1.courseowner.person.lastName)
            data.add(c1.rating)
            topthreeCrs.add(data)
            topthree++
        }
        ////println("topthreeCrs:"+topthreeCrs)
        ////println("topCrs:"+topCrs)
        top20=0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for(CourseCategory cc: subCat){

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for(Course c2:allCrsCat){
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if(top20>=topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20=0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for(int j=topCat.size()-2;j>0;){
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if(topCat[j]>topCat[j-2]){
                    int t = topCat[j-2]
                    CourseCategory tc = topCat[j-1]
                    topCat[j-2] = topCat[j]
                    topCat[j-1] = topCat[j+1]
                    topCat[j] = t
                    topCat[j+1] = tc
                }
                j=j-2
            }
            // ////println("After::topCat:"+topCat)

        }
        //    ////println("topCat:"+topCat)
        top20=0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for(int i=1;i<topCat.size();i+=2)
        {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
             topSubCat.add(tm.getValue())
         }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        //     ////println("topSubCat:"+topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for(CourseCategory cc: topSubCat){
            //   ////println("CC:"+cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if(cnt.size==0)
                continue
            if(showTopCat==4){
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory{
                    eq ('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1=catCrs[0]
        cat1=catCrs[1]
        def catname2=catCrs[2]
        cat2=catCrs[3]
        def catname3=catCrs[4]
        cat3=catCrs[5]
        def catname4=catCrs[6]
        cat4=catCrs[7]
        //    ////println("cat:"+cat1+"\n"+cat2+"\n"+cat3+"\n"+cat4)
        //    ////println("catCrs:"+catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for(CourseCategory mc:mainCat){
            ArrayList<CourseCategory> mm = new ArrayList()
            mm.add(mc)
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            mm.add(sm)
            mainmenu.add(mm)
        }

        //  ////println("Categories MM SM:"+mainmenu.name)
        // session.catCrs = catCrs
        [topthreeCrs:topthreeCrs,crs:topCrs,cat1:cat1,cat2:cat2,cat3:cat3,cat4:cat4,cats1:cat1.size(),cats2:cat2.size(),cats3:cat3.size(),cats4:cat4.size(),catname1:catname1,catname2:catname2,catname3:catname3,catname4:catname4,menus:mainmenu]


    }
    def viewAllTopCourses(){

//        ArrayList<Course> crsList = Course.list()
//        crsList.sort{it.rating}
        ////println("path:"+request.getContextPath())
        final int top = InitialData.findByName("top").number
        final int topcatcrs = InitialData.findByName("topcatcrs").number
        def c = Course.createCriteria()
        ArrayList<Course> results = c.list {
            order("rating", "desc")
        }
        //  ////println("result:"+results.size())
        ArrayList topCrs = new ArrayList<Course>()

        int top20 = 0;
        for(Course c1: results){
            if(top20==top)
                break;
            topCrs.add(c1.id)
            top20++
        }
        int topthree = 0;
        ArrayList topthreeCrs = new ArrayList()
        for(Course c1: results){
            if(topthree==top)
                break;
            ArrayList data = new ArrayList()
            data.add(c1.id)
            data.add(c1.course_name)
            data.add(c1.coursecategory.name)
            data.add(c1.courseowner.person.firstName+" "+c1.courseowner.person.lastName)
            data.add(c1.rating)
            topthreeCrs.add(data)
            topthree++
        }
        ////println("topthreeCrs:"+topthreeCrs)
        ////println("topCrs:"+topCrs)
        top20=0;
        ArrayList<CourseCategory> subCat = CourseCategory.findAllByCoursecategoryIsNotNull()
        //TreeMap<Double,CourseCategory> topCat = new TreeMap<Double,CourseCategory>()
        ArrayList topCat = new ArrayList()
        for(CourseCategory cc: subCat){

            def allCrsCat = Course.findAllByCoursecategory(cc)

            double x = 0.0
            for(Course c2:allCrsCat){
                //////println("b4--CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
                if(top20>=topcatcrs)
                    break;
                x += c2.rating
                top20++
                //     ////println("CC:"+cc.name+" T:"+top20+" final top:"+topcatcrs)
            }
            //   ////println("Rating:"+x)
            top20=0
            topCat.add(x)
            topCat.add(cc)
            //////println("B4::topCat:"+topCat)
            for(int j=topCat.size()-2;j>0;){
                //  ////println("j:"+j)
                // ////println("r:"+topCat[j-2])
                // ////println("cc:"+topCat[j-1])
                if(topCat[j]>topCat[j-2]){
                    int t = topCat[j-2]
                    CourseCategory tc = topCat[j-1]
                    topCat[j-2] = topCat[j]
                    topCat[j-1] = topCat[j+1]
                    topCat[j] = t
                    topCat[j+1] = tc
                }
                j=j-2
            }
            // ////println("After::topCat:"+topCat)

        }
        //    ////println("topCat:"+topCat)
        top20=0;



        ArrayList<CourseCategory> topSubCat = new ArrayList()
        for(int i=1;i<topCat.size();i+=2)
        {
            topSubCat.add(topCat[i])
        }

        /* for(Map.Entry<Double,CourseCategory> tm:topCat.entrySet()){
             topSubCat.add(tm.getValue())
         }*/
        //////println("topSubCat:"+topSubCat.name)
        //Collections.reverse(topSubCat)
        //     ////println("topSubCat:"+topSubCat.name)
        int showTopCat = 0
        ArrayList catCrs = new ArrayList()
        ArrayList cat1 = new ArrayList<Course>()
        ArrayList cat2 = new ArrayList<Course>()
        ArrayList cat3 = new ArrayList<Course>()
        ArrayList cat4 = new ArrayList<Course>()
        for(CourseCategory cc: topSubCat){
            //   ////println("CC:"+cc.name)
            def cnt = Course.findAllByCoursecategory(cc)
            if(cnt.size==0)
                continue
            if(showTopCat==4){
                break;
            }
            def cat = Course.createCriteria().list {
                coursecategory{
                    eq ('id', cc.id)
                }
                order('rating', 'desc')
                maxResults topcatcrs
            }
            catCrs.add(cc.id)
            catCrs.add(cat.id)
            showTopCat++
        }
        def catname1=catCrs[0]
        cat1=catCrs[1]
        def catname2=catCrs[2]
        cat2=catCrs[3]
        def catname3=catCrs[4]
        cat3=catCrs[5]
        def catname4=catCrs[6]
        cat4=catCrs[7]
        //    ////println("cat:"+cat1+"\n"+cat2+"\n"+cat3+"\n"+cat4)
        //    ////println("catCrs:"+catCrs)
        topCrs as JSON
        cat1 as JSON
        cat2 as JSON
        cat3 as JSON
        cat4 as JSON
        ArrayList<CourseCategory> mainCat = CourseCategory.findAllByCoursecategoryIsNull()
        ArrayList<CourseCategory> mainmenu = new ArrayList()
        for(CourseCategory mc:mainCat){
            ArrayList<CourseCategory> mm = new ArrayList()
            mm.add(mc)
            ArrayList<CourseCategory> sm = CourseCategory.findAllByCoursecategory(mc)
            mm.add(sm)
            mainmenu.add(mm)
        }

        //  ////println("Categories MM SM:"+mainmenu.name)
        // session.catCrs = catCrs
        [topthreeCrs:topthreeCrs,crs:topCrs,cat1:cat1,cat2:cat2,cat3:cat3,cat4:cat4,cats1:cat1.size(),cats2:cat2.size(),cats3:cat3.size(),cats4:cat4.size(),catname1:catname1,catname2:catname2,catname3:catname3,catname4:catname4,menus:mainmenu]


    }
   def erptemplateT2(){

   }
    def reseterppassword()
    {
        Login login = Login.findById(session.loginid)
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {

        }
        ////println("I am in reseterppassword...")
    }
    def storereseterppassword()
    {
        println("I am in storereseterppassword.."+params)
        Organization org = Organization.findById(session.orgid)

        if(params?.login){
            Login login=Login.findById(params?.login.replaceAll(" ", ""))
            if(login==null)
            {
                flash.error="User Not Found in system..."
                redirect(action: 'reseterppassword',controller:'Login')
                return
            }
            def password = InformationService.password_generation()
            login.password = password
            login.updation_date=new java.util.Date()
            login.updation_ip_address=request.getRemoteAddr()
            login.save(flush: true, failOnError: true)


            if(org?.establishment_email && org?.establishment_email_credentials) {
                def msg = "Dear Sir," +
                        "<br>ERP Password Reset Successfully for Username - " + login?.username  + "... New Password is "+ password  + "."

                sendMailService.sendmailwithcss(org?.establishment_email, org.establishment_email_credentials, login?.username, org?.organization_code + " Password Reset For ERP", msg, "")
            }

            def inst = Instructor.findByEmployee_codeAndOrganization(login?.username?.replaceAll(" ", ""), org)
            if(!inst)
                inst = Instructor.findByUidAndOrganization(login?.username?.replaceAll(" ", ""), org)

            def mobile
            if(inst)
                mobile = inst?.mobile_no

            def learner
            if(!inst)
                learner = Learner.findByRegistration_numberAndOrganization(login?.username?.replaceAll(" ", ""), org)
            if(!learner)
                learner = Learner.findByUidAndOrganization(login?.username?.replaceAll(" ", ""), org)

            if(learner)
                mobile = learner?.mobileno

            def user = inst?inst:learner

            if(!mobile)
                mobile = Contact.findByPerson(user?.person)?.mobile_no

            println("mobile "+mobile)

            if(mobile) {
                def msg = "Dear User ERP Password Reset Successfully for Username - " + login?.username  + "... New Password is "+ password  + "."
                InformationService.smsCommonService(msg, mobile, org, null)
            }

//            flash.message="Password Reset Successfully for Username - " + login?.username  + "... New Password is "+password
            flash.message="Password Reset Successfully... New Password is Sent On "+login?.username
            redirect(action: 'reseterppassword',controller:'Login')
        }
        else{
            flash.error="Please Enter PRN Number/Employee Code... "
            redirect(action: 'reseterppassword',controller:'Login')
            return
        }

    }

    def getinformation(){
        println("I am in getinformation.."+params)
        Organization org = Organization.findById(session.orgid)

        if(params?.grno) {
            Login login = Login.findById(session.loginid)
            def instructor = Instructor.findByEmployee_codeAndOrganization(params?.grno.replaceAll(" ", ""), org)
            if(!instructor)
                instructor = Instructor.findByUidAndOrganization(params?.grno.replaceAll(" ", ""), org)

            Login logina
            def learner
            if(!instructor) {
                learner = Learner.findByRegistration_numberAndOrganization(params?.grno.replaceAll(" ", ""), org)
                if(!learner)
                    learner = Learner.findByUidAndOrganization(params?.grno.replaceAll(" ", ""), org)

                if(learner)
                    logina = Login.findByUsername(learner?.uid)
            }else{
                logina = Login.findByUsername(instructor?.uid)
            }
            if (logina){
                Contact contact
                if(learner)
                 contact=Contact.findByPerson(learner.person)
                else
                 contact=Contact.findByPerson(instructor.person)
                [instructor: instructor,learner:learner, logina:logina,contact:contact]
            }
            else{
                flash.error="Employee Code/PRN No./Email Not Found ...  "
                redirect(action: 'getinformation',controller:'Login')
                return
            }
        }
    }


    def resetemail()
    {
        Login login = Login.findById(session.loginid)
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {

        }
        ////println("I am in resetemail.."+params)
    }

    def resetemailsubmit()
    {
        println("I am in resetemailsubmit.."+params)

        Login login=Login.findById(params?.login.replaceAll(" ", ""))
        println(" save login :"+login)
        Login loginusername = Login.findByUsername(params.email)
        println("save loginusername :"+loginusername)
        Organization org = Organization.findById(session.orgid)
        Learner learner = Learner.findById(params?.learner.replaceAll(" ", ""))
        println("save learner :"+learner)
        Instructor instructor  = Instructor.findById(params?.instructor.replaceAll(" ", ""))

        def grno = instructor?.employee_code ? instructor?.employee_code : learner?.registration_number
        if(loginusername == null)
        {
            if(login != null) {
                login.username = params.email
                login.updation_date = new java.util.Date()
                login.updation_ip_address = request.getRemoteAddr()
                login.save(flush: true, failOnError: true)

                println("I am in resetemailsubmit.. login user name reset to : " + params.email)
            }
        }
        else
        {
            println( params.email + " This email id already exist for employee code : " + grno)

            flash.message = params.email + " This email id already exist for employee code : " + grno
            redirect(action: 'changeemail',controller:'Login')
            return
        }

        //UPDATE `learner` SET `uid` = TRIM(email) WHERE `learner`.`registration_number` = TRIM(grnumber);
        if(learner != null) {
            learner.uid = params.email
            learner.updation_date = new java.util.Date()
            learner.updation_ip_address = request.getRemoteAddr()
            learner.save(flush: true, failOnError: true)

            println("I am in resetemailsubmit.. Learner uid reset to :" + params.email)
        }
        // UPDATE `person` SET `email` = TRIM(email) WHERE person.grno LIKE  TRIM(grnumber) ;

        if(instructor != null) {
            instructor.uid = params.email
            instructor.updation_date = new java.util.Date()
            instructor.updation_ip_address = request.getRemoteAddr()
            instructor.save(flush: true, failOnError: true)

            println("I am in resetemailsubmit.. Learner uid reset to :" + params.email)
        }


        Person person = Person.findById(params?.person.replaceAll(" ", ""))
        if(person != null) {
            person.email = params.email
            person.updation_date = new java.util.Date()
            person.updation_ip_address = request.getRemoteAddr()
            person.save(flush: true, failOnError: true)
            println("I am in resetemailsubmit.. Person email reset to :" + params.email)
        }

        flash.message="Email Reset Successfully..."
        redirect(action: 'changeemail',controller:'login')
    }
    def resetemailsubmitbackup()
    {
        ////println("I am in resetemailsubmit.."+params)

        Organization org = Organization.findById(session.orgid)

        Learner learner = Learner.findByRegistration_numberAndOrganization(params.grnumber, org)

        Login login = Login.findByUsername(learner?.uid)
        if(login != null) {
            login.username = params.email
            login.updation_date = new java.util.Date()
            login.updation_ip_address = request.getRemoteAddr()
            login.save(flush: true, failOnError: true)

            ////println("I am in resetemailsubmit.. login user name reset to : " + params.email)
        }

        //UPDATE `learner` SET `uid` = TRIM(email) WHERE `learner`.`registration_number` = TRIM(grnumber);
        if(learner != null) {
            learner.uid = params.email
            learner.updation_date = new java.util.Date()
            learner.updation_ip_address = request.getRemoteAddr()
            learner.save(flush: true, failOnError: true)

            ////println("I am in resetemailsubmit.. Learner uid reset to :" + params.email)
        }
        // UPDATE `person` SET `email` = TRIM(email) WHERE person.grno LIKE  TRIM(grnumber) ;

        Person person = Person.findByGrno(learner?.registration_number)
        if(person != null) {
            person.email = params.email
            person.updation_date = new java.util.Date()
            person.updation_ip_address = request.getRemoteAddr()
            person.save(flush: true, failOnError: true)
            ////println("I am in resetemailsubmit.. Person email reset to :" + params.email)
        }

        flash.message="Email Reset Successfully...Try loging to VOLP...if you are not able to login email your 'PRNnumber' and 'Official Email id' to nilesh.patel@vit.edu"
        redirect(action: 'resetemail',controller:'Login')
    }



    def forgetPassword(){
        println("in forgetPassword")
        //LoginImages logimg = LoginImages.findByName("Reset")
        //String logimglnk = null
        //def path = DomainPath.list().path
        //if(logimg!=null) {
        //logimglnk = path[0] + logimg.imgpath + logimg.imgname
        // logimglnk = logimglnk.replace("\\", "/")
        //}
        // [logimglnk:logimglnk]
    }

    def processemail(){
        println("in processemail"+params)
        [params:params]
    }

    def verifyemail(){
        println("in verifyemail:"+params)
        String email=params.email.trim()
        Login l=Login.findByUsername(email)
        if(!l) {
            flash.error = "Invalid email. Please contact ERP coordinator and update your email."
            redirect(controller:"login", action:"forgetPassword")
            return
        }
        def org

        Instructor instructor = Instructor.findByUid(l?.username)
        if(!instructor){
            Learner learner = Learner.findByUid(l?.username)
            if(learner)
                org = learner?.organization
        }else
            org = instructor?.organization

        session.otpemail = email
        if(l!=null)
        {
            //Registred..Send OTP for Password Reset
            //Generate OTP
            java.util.Random random=new java.util.Random()
            String otp=""
            int min=0,max=0,n
            for(int i=1;i<=6;i++)
            {
                if(i==1)
                    min=1
                else
                    min=0
                max=9
                n=random.nextInt(max)+min
                otp=otp+n
            }
            Otp otpobj=Otp.findByEmail(email)
            Date date=new java.util.Date()
            if(otpobj==null)
            {
                //insert otp
                otpobj=new Otp()
                otpobj.email=email
                otpobj.otp=otp
                otpobj.otpgenerationtime=date
                otpobj.save(failOnError:true,flush:true)
            }
            else
            {
                //update otp
                otpobj.otp=otp
                otpobj.otpgenerationtime=date
                otpobj.save(failOnError:true,flush:true)
            }
            println("1Now sending mail of otp...."+otp)
            try {

                /*final String username = "support@volp.in"
                final String password = "Admin@volp5"*/

                String username
                String password
                if(org?.establishment_email && org.establishment_email_credentials){
                    username = org?.establishment_email
                    password = org.establishment_email_credentials
                }
                String sendto = email
                String subject = "Verify Your Email for VOLP"
                String body = "Your OTP for registration is "+otp+"\n"+"Thanks, VOLP Team"

                Properties props = new Properties();
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "true");
                props.put("mail.smtp.host", "smtp.gmail.com");
                props.put("mail.smtp.port", "587");

                javax.mail.Session session = javax.mail.Session.getInstance(props,
                        new javax.mail.Authenticator() {
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication(username, password);
                            }
                        });

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(username));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(sendto));
                message.setSubject(subject);
                message.setText(body);

                Transport.send(message);
                System.out.println("Mail Sent Successfully.....");

                /*sendMail {
                    to email
                    subject "Verify Your Email for VOLP"
                    text "Your OTP for registration is "+otp+"\n"+"Thanks, VOLP Team"
                }*/
                //session.otpemail=email
                println("5Now sending mail of otp...."+otp)
                redirect(controller:"login", action:"processemail")

                return
            } catch (Exception e){
                println("Error :: " + e)
                render "<script>\n" +
                        "     alert(\"Email Sending Failed.\")\n" +
                        "history.back();\n"+
                        "  </script>"
                return
            }
        }
        else
        {
            render "<script>\n" +
                    "     alert(\"Please register or check your username.\")\n" +
                    "history.back();\n"+
                    "  </script>"
            return
        }
    }

    def verifyotp(){
        println("in verifyotp:"+params)
//        println("in verifyotp:"+session)
        String email=session.otpemail
        String otp=params.otp
        println("Email:"+email+" and otp:"+otp)
        Otp otpobj=Otp.findByEmailAndOtp(email,otp)
        if(otpobj!=null)
        {
            Date otpgenerationtime=otpobj.otpgenerationtime
            Date currenttime=new java.util.Date()
            println("OTP match:otpgenerationtime:"+otpgenerationtime+","+"currenttime:"+currenttime)
            long diff = currenttime.getTime() - otpgenerationtime.getTime() ;
            long diffInMinutes = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(diff);
            if(diffInMinutes<=30)
            {
                redirect action:"passwordreset"
                return
            }
            else
            {
                render "<script>\n" +
                        "     alert(\"OTP is expired..please generate OTP again..\")\n" +
                        "history.back();\n"+
                        "  </script>"
                return
            }
        }
        else
        {
            render "<script>\n" +
                    "     alert(\"OTP do not match, Please Enter OTP again...\")\n" +
                    "history.back();\n"+
                    "  </script>"
            return
        }
    }
    def passwordreset()
    {
        println("I am in password reset...")
        String email=session.otpemail
        [email:email]
    }

    def savepasswordreset()
    {
        println("I am in savepasswordreset...:"+params)
        String sessionemail = session.otpemail.trim()
        String email = params.email.trim()
        if(sessionemail == email) {
            String password = params.pwd
            println(email + " " + password)
            Login l = Login.findByUsername(email)
            if (l != null) {
                l.username = email
                l.updation_date = new java.util.Date()
                l.updation_ip_address = request.getRemoteAddr();
                l.password = password
                l.save(failOnError: true, flush: true)
                flash.message = "Password reset successfully. Please login again."
                redirect(action: "erplogin", controller: "Login")
            } else {
                println("You need to register first....")
            }
        } else {
            /*render "<script>\n" +
                    "     alert(\". Already Feedback type Added...\")\n" +
                    "history.back();\n" +
                    "  </script>"
            return*/
            flash.error = "Password not reset. Email ID INVALID. Contact to erp-coordinatoer..."
            redirect(action: "erplogin", controller: "Login")
        }
    }

    def addquicklinks() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            println("addquicklinks :: " + params)
            Login login = Login.findById(session.loginid)
            Organization org
            Instructor instructor=Instructor.findByUid(login.username)
            if(!instructor) {
                Learner learner = Learner.findByUid(login.username)
                if(learner) {
                    org = learner.organization
                } else {
                    org = Organization.findById(session.orgid)
                }
            } else {
                org = instructor.organization
            }
            ArrayList loginquicklinklist = new ArrayList()

            ArrayList rolelinkslist = new ArrayList()
            ArrayList quicklinkslist = new ArrayList()

            rolelinkslist = RoleLink.createCriteria().list() {
                'in'('role', login.roles)
                and {
//                    'in'('usertype', login.usertype)
                    'in'('isrolelinkactive', true)
                    'in'('organization', org)
                }
            }

            for(rolelink in rolelinkslist){
                def quicklink = Quicklink.createCriteria().list() {
                    'in'('rolelinks', rolelink)
                    and {
                        'in'('login', login)
                    }
                }
//                println("quicklink :: " + quicklink)
                loginquicklinklist.add(rolelink:rolelink, quicklink:quicklink)
            }

            //println("loginquicklinklist :: " + loginquicklinklist)

            [loginquicklinklist: loginquicklinklist]
            /*quicklinkslist = Quicklink.createCriteria().list() {
                'in'('rolelinks', rolelinkslist)
                and {
                    'in'('login', login)
                }
            }
            //println("quicklinkslist :: " + quicklinkslist)*/


            //def roles = login.roles
            //ArrayList rolelist = new ArrayList()
            /*for (Role r : roles) {
                rolelist.add(r)
            }*/
            //rolelist.sort { it.roletype.type }
            //println("rolelist:" + rolelist)
            //session.rolelist = rolelist.id
            //println " session.rolelist " + session.rolelist
            //[rolelinkslist: rolelinkslist, isquicklinklist:isquicklinklist, isnoquicklinklist:isnoquicklinklist, quicklinkslist:quicklinkslist]
        }
    }

    def activequicklink() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            println("activequicklink :: " + params)
            Login login = Login.findById(session.loginid)

            Quicklink quicklinks
            RoleLink  roleLinks = RoleLink.findById(params.rolelinkid)

            if(roleLinks) {
                quicklinks = Quicklink.findByLoginAndRolelinks(login, roleLinks)
                if(quicklinks){
                    if(quicklinks.isactive){
                        quicklinks.isactive = false
                        quicklinks.updation_username = login.username
                        quicklinks.updation_date = new java.util.Date()
                        quicklinks.updation_ip_address = request.getRemoteAddr()
                        quicklinks.save(flush: true, failOnError: true)
                    } else {
                        quicklinks.isactive = true
                        quicklinks.updation_username = login.username
                        quicklinks.updation_date = new java.util.Date()
                        quicklinks.updation_ip_address = request.getRemoteAddr()
                        quicklinks.save(flush: true, failOnError: true)
                    }
                } else {
                    quicklinks = new Quicklink()
                    quicklinks.isactive = true
                    quicklinks.login = login
                    quicklinks.rolelinks = roleLinks
                    quicklinks.creation_username = login.username
                    quicklinks.creation_date = new java.util.Date()
                    quicklinks.creation_ip_address = request.getRemoteAddr()
                    quicklinks.updation_username = login.username
                    quicklinks.updation_date = new java.util.Date()
                    quicklinks.updation_ip_address = request.getRemoteAddr()
                    quicklinks.save(flush: true, failOnError: true)
                }
            }

            def roles = login.roles
            def ut = login.usertype
            ArrayList<RoleLink> rolelinkslist = new ArrayList<RoleLink>()
            def rolelinks = RoleLink.findAllByIsquicklinkAndRoleInListAndIsrolelinkactiveAndOrganization(true, roles, true, login.organization)
            for(RoleLink  r : rolelinks)
            {
                boolean ispreset = false
                for(RoleLink  rl : rolelinkslist)
                {
                    if(r.id == rl.id )
                    {
                        ispreset = true
                    }
                }
                if(r != null && (!ispreset)){
                    rolelinkslist.add(r)
                }
            }
            def ql = Quicklink.findAllByLoginAndIsactive(login, true)
            //println(q1.rolelinks)
            for(quicklink in ql){
                //println(quicklink + " isrolelinkactive "+quicklink.rolelinks.isrolelinkactive)

                if(quicklink.rolelinks.isrolelinkactive == true)
                {
                    RoleLink  rl = quicklink.rolelinks
                    rolelinkslist.add(rl)
                }
            }
            //println("rolelinkslist :: " + rolelinkslist)
            session.fixedrolelinkslist=rolelinkslist

            redirect(controller: "Login", action: "addquicklinks")
        }
    }




//------------------------------by RJ ma'am-------------------------
    def changeemail(){

        flash.error = ""

        [i:0]
    }
    def changeemail1(){}
    def getemail()
    {
        Login login_inst = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login_inst.username)
        instructor=Instructor.findByUidAndOrganization(login_inst.username, instructor.organization)
        Organization org = instructor.organization

        def learner1
        Person person = null

        def instructor1 = Instructor.findByEmployee_codeAndOrganization(params?.grnumber.replaceAll(" ", ""), org)
        if(!instructor1)
            instructor1 = Instructor.findByUidAndOrganization(params?.grnumber.replaceAll(" ", ""), org)

        def login1

        if(!instructor1) {

            learner1 = Learner.findByRegistration_numberAndOrganization(params?.grnumber.replaceAll(" ", ""), org)
            println("learner Registration Number :" + learner1)

            if (!learner1)
                learner1 = Learner.findByUidAndOrganization(params?.grnumber.replaceAll(" ", ""), org)
                println("Learner Uid :" + learner1)

            if(learner1)
                login1 = Login.findByUsername(learner1?.uid)
                println("login :"+login1)
                person = learner1?.person

        }
        else{
            login1 = Login.findByUsername(instructor1?.uid)
            person = instructor1?.person
        }


        flash.error = ""
        def ename = ""
        def emailid = ""
        if(person == null)
        {
            flash.error = params.grnumber + " does not exists in the system."
        }
        else
        {
            ename = person.fullname_as_per_previous_marksheet
            emailid = login1?.username
        }

        [emailid:emailid,person:person , ename:ename,login1:login1,learner1:learner1,instructor1:instructor1,person:person]
    }

    def savebulkchangemailid() {
        println("savebulkchangemailid :: " + params)

        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization

        def ipaddress = request.getRemoteAddr()
        def failedlist = []
        def successlist = []
        if(params.Save == "Upload") {
            def file = request.getFile("importfile")
            if (!file.empty) {
                def sheetheader = []
                def values = []
                def workbook = new XSSFWorkbook(file.getInputStream())
                def sheet = workbook.getSheetAt(0)
                for (cell in sheet.getRow(0).cellIterator()) {
                    sheetheader << cell.stringCellValue
                }
                def headerFlag = true
                for (row in sheet.rowIterator()) {
                    if (headerFlag) {
                        headerFlag = false
                        continue
                    }
                    def value = ''
                    def map = [:]
                    for (cell in row.cellIterator()) {
                        switch (cell.cellType) {
                            case 1:
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 0:
                                cell.setCellType(cell.CELL_TYPE_STRING)
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 2:
                                cell.setCellType(cell.CELL_TYPE_STRING)
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            default:
                                value = ''
                        }
                    }
                    if (values.size() > 0) {
                        if (values[0].keySet().size() == map.size())
                            values.add(map)
                    } else {
                        values.add(map)
                    }
                }
                def colnamelist = values[0].keySet()
                for (int i = 0; i < values.size(); i++) {
                    def prnno = values[i].get('PRN No')
                    def email = values[i].get('New Email Id')
                    def row = [email : email,
                               prnno :prnno
                    ]
                    //println("row :: " + row)
                    Learner learner = Learner.findByRegistration_numberAndOrganization(prnno.trim(), org)
                    if(learner) {
                        def old_uid = learner.uid
                        Learner uid_learner = Learner.findByUidAndOrganization(email, org)
                        if(uid_learner) {
                            failedlist.add(row:row, msg : "Duplicate Email Id : " + email)
                            continue
                        } else {
                            Login login1 = Login.findByGrno_empid(prnno.trim())
                            if(login1) {
                                login.username = email
                                login.updation_date = new Date()
                                login.updation_ip_address = ipaddress
                                try {
                                    login.save(flush: true, failOnError: true)
                                } catch (Exception e) {
                                    failedlist.add(row:row, msg:"email id updation failed for login")
                                    continue
                                }
                            } else {
                                failedlist.add(row:row, msg:"Login Details Not Found for Email : " + email + ", Please provide correct email ID...")
                                continue
                            }
                            learner.uid = email
                            learner.username = login.username
                            learner.updation_date = new Date()
                            learner.updation_ip_address = ipaddress
                            try {
                                learner.save(flush: true, failOnError: true)
                            } catch (Exception e) {
                                login.username = old_uid
                                login.save(flush: true, failOnError: true)
                                failedlist.add(row:row, msg:"Email Id updation failed for learner")
                                continue
                            }
                            Person person = learner.person
                            person.email = email
                            person.username = login.username
                            person.updation_date = new Date()
                            person.updation_ip_address = ipaddress
                            try {
                                person.save(flush: true, failOnError: true)
                            } catch(Exception e) {
                                login.username = old_uid
                                login.save(flush: true, failOnError: true)
                                learner.uid = email
                                learner.save(flush: true, failOnError: true)
                                failedlist.add(row:row, msg: "Email Id failed in person Information..")
                                continue
                            }
                        }
                        successlist.add(row:row)
                    } else {
                        Instructor instructor1 = Instructor.findByEmployee_codeAndOrganization(prnno.trim(), org)
                        if(instructor1) {
                            def old_uid = instructor1.uid
                            Instructor uid_inst = Instructor.findByUidAndOrganization(email, org)
                            if(uid_inst) {
                                failedlist.add(row:row, msg : "Duplicate Email Id : " + email)
                                continue
                            } else {
                                Login login1 = Login.findByGrno_empid(prnno.trim())
                                if(login1) {
                                    login.username = email
                                    login.updation_date = new Date()
                                    login.updation_ip_address = ipaddress
                                    try {
                                        login.save(flush: true, failOnError: true)
                                    } catch (Exception e) {
                                        failedlist.add(row:row, msg:"email id updation failed for login")
                                        continue
                                    }
                                } else {
                                    failedlist.add(row:row, msg:"Login Details Not Found for Email : " + email + ", Please provide correct email ID...")
                                    continue
                                }
                                instructor1.uid = email
                                instructor1.username = login.username
                                instructor1.updation_date = new Date()
                                instructor1.updation_ip_address = ipaddress
                                try {
                                    instructor1.save(flush: true, failOnError: true)
                                } catch (Exception e) {
                                    login.username = old_uid
                                    login.save(flush: true, failOnError: true)
                                    failedlist.add(row:row, msg:"Email Id updation failed for instructor")
                                    continue
                                }
                                Person person = instructor1.person
                                person.email = email
                                person.username = login.username
                                person.updation_date = new Date()
                                person.updation_ip_address = ipaddress
                                try {
                                    person.save(flush: true, failOnError: true)
                                } catch(Exception e) {
                                    login.username = old_uid
                                    login.save(flush: true, failOnError: true)
                                    instructor1.uid = email
                                    instructor1.save(flush: true, failOnError: true)
                                    failedlist.add(row:row, msg: "Email Id failed in person Information..")
                                    continue
                                }
                            }
                            successlist.add(row:row)
                        } else {
                            failedlist.add(row:row, msg:"The given Email id details not available in the system.")
                            continue
                        }
                    }
                }
            } else {
                println("file is empty..")
                flash.errormessage = "file is empty.."
            }
        } else {
            println("save != upload")
        }
//        render  "<br/><br/><br/><br/>" + failedlist +
//                "<br/><br/><br/><br/>" + successlist
        [failedlist:failedlist, successlist:successlist]
    }

    def getusername() {
        println("getusername :: " + params)
        def org = Organization.findById(session.orgid);


        Instructor inst = Instructor.findByEmployee_codeAndOrganization(params.grno, org)

        Login login = Login.findByUsername(inst?.uid)
        println("login  :: " + login )
        //def email = login.username

        Contact contact = Contact.findByPerson(inst.person)
        println("contact  ::  " + contact )

        contact.email = login.username
        contact.save(failOnError: true, flush: true)

        [email : login.username]
    }

    def inactiveformerror() {
        println("inactiveformerror :: " + params)
    }

    def test() {
        println("test :: " + params)
    }

    def testreadawsfile() {
        println("testreadawsfile :: " + params)
        def baselink = "https://vierp.s3.ap-south-1.amazonaws.com/"
        def awsimagelink = "cloud/studentprofile/icard/photo/111581photo.jpg"
        def awsimagefilename ="111581photo.jpg"
        def completeurl = "https://vierp.s3.ap-south-1.amazonaws.com/cloud/studentprofile/icard/photo/111581photo.jpg"
        def url2complete = "https://vierp.s3.ap-south-1.amazonaws.com/cloud/Result/VIT__Result_2018-19_1/11810067_4.pdf"
        def url2path = "cloud/Result/VIT__Result_2018-19_1/11810067_4.pdf"
//
//        AWSBucket awsBucket = AWSBucket.findByContent("documents")

//
//        response.setContentType("image/jpg; charset=UTF-8")
//        response.setHeader("Content-Disposition", "Attachment;Filename=\""+awsimagefilename+"\"")
//        InputStream object = awsBucketService.downloadContentFromBucket(awsBucket.bucketname, awsBucket.region, awsimagelink)
//
//        File tmp = File.createTempFile("s3test", "");
//        //Files.copy(object, tmp.toPath(), StandardCopyOption.REPLACE_EXISTING);
//        def fileInputStream = new FileInputStream(tmp)
//        def outputStream = response.getOutputStream()
//        byte[] buffer = new byte[4096];
//        int len;
//        while ((len = fileInputStream.read(buffer)) > 0) {
//            outputStream.write(buffer, 0, len);
//        }
//
//        //return tmp
//
//        render "<img src='" + outputStream + "' />"
//
//        outputStream.flush()
//        outputStream.close()
//        fileInputStream.close()
        //[tmp:fileInputStream]

        AWSBucketService awsBucketService = new AWSBucketService()
        AWSBucket aws = AWSBucket.findByContent("documents")
        String a = awsBucketService.getPresignedUrl(aws.bucketname,awsimagelink, aws.region)
        println("a :: " + a)
        String b = awsBucketService.getPresignedUrl(aws.bucketname,url2path, aws.region)
        println("b :: " + b)

        [temp:a, temp2:b]
    }

    def verification() {
        println("verification :: " + params)
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            Login login = Login.findById(session.loginid)
            Learner learner = Learner.findByUid(login.username)
            def flag = 0
            if(learner.ismobileverfied) {
                //redirect(controller: "Login", action: "emailverification")
                //return
                flag = flag + 1
            }
            if(learner.isemailverified) {
                flag = flag + 1
            }
            if(flag ==2) {
                redirect(controller: "Login", action: "erphome_student")
                return
            } else if(learner.ismobileverfied) {
                redirect(controller: "Login", action: "emailverification")
                return
            }
            [learner : learner]
        }
    }

    def emailverification() {
        println("emailverification :: " + params)
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            Login login = Login.findById(session.loginid)
            Learner learner = Learner.findByUid(login.username)
            def flag = 0
            if(learner.ismobileverfied) {
                flag = flag + 1
            }
            if(learner.isemailverified) {
                flag = flag + 1
            }
            if(flag ==2) {
                redirect(controller: "Login", action: "erphome_student")
                return
            }
            [learner : learner]
        }
    }

    def mblotpverifyform() {
        println("mblotpverifyform :: " + params)
        Login login = Login.findById(session.loginid)
        Learner learner = Learner.findByUid(login.username)

        def mobileno = params.mbl.trim()
        learner.mobileno = mobileno
        learner.save(failOnError: true, flush: true)

        // OTP creation
        java.util.Random random = new java.util.Random()
        String otp = ""
        int min = 0, max = 0, n
        for (int i = 1; i <= 6; i++) {
            if (i == 1)
                min = 1
            else
                min = 0
            max = 9
            n = random.nextInt(max) + min
            otp = otp + n
        }

        // Send SMS
        InformationService infomationService = new InformationService()
        def phone =  mobileno //"9762340668"  //params.mobileno.trim()
        infomationService.sendSmslcl("Dear User %n Your OTP for Mobile Verification is "+otp+"\n"+" Thanks, EduPlusCampus Team ",phone)


        Otp otpobj = Otp.findByEmail(mobileno)
        if(otpobj) {
            otpobj.otp = otp
            otpobj.otpgenerationtime = new Date()
            otpobj.save(failOnError: true, flush: true)
        } else {
            otpobj = new Otp()
            otpobj.email = mobileno
            otpobj.otp = otp
            otpobj.otpgenerationtime = new Date()
            otpobj.save(failOnError: true, flush: true)
        }

        [learner : learner, mobile:mobileno]
    }

    def emailotpverifyform() {
        println("emailotpverifyform :: " + params)
        Login login = Login.findById(session.loginid)
        Learner learner = Learner.findByUid(login.username)

        def email = params.email

        // OTP creation
        java.util.Random random = new java.util.Random()
        String otp = ""
        int min = 0, max = 0, n
        for (int i = 1; i <= 6; i++) {
            if (i == 1)
                min = 1
            else
                min = 0
            max = 9
            n = random.nextInt(max) + min
            otp = otp + n
        }

        // send Email
        try {
            /*final String username = "support@volp.in"
            final String password = "Admin@volp5"*/
            String username
            String password
//            if(learner.organization.id == 5){
//                username = "est.erp@viit.ac.in"
//                password = "viit@0607"
//            } else if(learner.organization.id == 4){
//                username = "establishment@vit.edu"
//                password = "vit1234\$"
//            }

            if(learner.organization?.establishment_email && learner.organization.establishment_email_credentials){
                username = learner.organization?.establishment_email
                password = learner.organization.establishment_email_credentials
            }

            String sendto = email
            String subject = "Verify Your Email ID"
            String body = "Your OTP for Verification is "+otp+"\n"+"Thanks, EduPlusCampus Team"

            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            javax.mail.Session session = javax.mail.Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(sendto));
            message.setSubject(subject);
            message.setText(body);

            Transport.send(message);
            System.out.println("Mail Sent Successfully.....");

            /*sendMail {
                to email
                subject "Verify Your Email for VOLP"
                text "Your OTP for registration is "+otp+"\n"+"Thanks, VOLP Team"
            }*/
            //session.otpemail=email
            println("5Now sending mail of otp...."+otp)
        } catch (Exception e){
            render "<script>\n" +
                    "     alert(\"Email Sending Failed.\")\n" +
                    "history.back();\n"+
                    "  </script>"
            return
        }

        Otp otpobj = Otp.findByEmail(email)
        if(otpobj) {
            otpobj.otp = otp
            otpobj.otpgenerationtime = new Date()
            otpobj.save(failOnError: true, flush: true)
        } else {
            otpobj = new Otp()
            otpobj.email = email
            otpobj.otp = otp
            otpobj.otpgenerationtime = new Date()
            otpobj.save(failOnError: true, flush: true)
        }
        [learner : learner, email :  email ]
    }

    def verificationotp() {
        println("verificationotp :: " + params )
        Login login = Login.findById(session.loginid)
        Learner learner = Learner.findByUid(login.username)
        def type = params.type
        if(params.email) {
            def email = learner.uid
            Otp otpobj = Otp.findByEmail(params.email)
            if(params.otp == otpobj.otp) {
                learner.isemailverified = true
                learner.save(failOnError: true, flush: true)
                flash.messgae = "Email Verification is Done..."
                //redirect(controller: "Login", action: "erphome_student")
                //return
            }
        }
        if(params.mobile) {
            def mbl = learner.mobileno
            Otp otpobj = Otp.findByEmail(params.mobile)
            if(params.otp == otpobj.otp) {
                learner.ismobileverfied = true
                learner.save(failOnError: true, flush: true)

                flash.messgae = "Mobile Verification is Done..."
                //redirect(controller: "Login", action: "emailverification")
                //return
            }
        }

        def flag = 0
        if(learner.ismobileverfied) {
            flag = flag + 1
        }
        if(learner.isemailverified) {
            flag = flag + 1
        } else {
            redirect(controller:"login", action:"emailotpverifyform")
            return
        }
        if(flag == 2) {
            //redirect(controller: "Login", action: "erphome_student")
            render "<script>" +
                    "location.reload(true);" +
                    "</script>"
            return
        }
        [learner:learner]
    }

    def learnerredirect(){
        Organization org = Organization.findById(session.orgid)
        [form_name : session.currentrolelink?.link_displayname, org:org]
    }



    //Email Code

    def testemailcode() {
        println("testemailcode :: " + params)
        Login login_inst=Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        instructor=Instructor.findByUidAndOrganization(login_inst.username, instructor.organization)
        Organization org = instructor.organization

        if(org?.isgsuitapplicable) {
            AdminSDKDirectoryQuickstart service = new AdminSDKDirectoryQuickstart()
            def result = service.creategmailaccount('komal', 'shinde', 'kshinde13'+org?.gsuit_org_unit, '123438576', org)
            println("result  :: " + result)

            if (result) {
                def groupresult = service.creategroup("BTech-FY-2020-21-E", 'btech-fy-2020-21-e'+org?.gsuit_org_unit, 'kshinde13'+org?.gsuit_org_unit, org)
                println("groupresult : " + groupresult)
                if (groupresult) {
                    def member = service.addGoogleGroupMember('kshinde13'+org?.gsuit_org_unit, 'btech-fy-2020-21-e'+org?.gsuit_org_unit, 'BTech-FY-2020-21-E', 'MEMBER', org)
                    println("member :" + member)
                }
            }
            render "OK"
        }else
            render "GSUIT Not Applicable"

    }


    def testlearnerdivision(){
        Login login_inst=Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        instructor=Instructor.findByUidAndOrganization(login_inst.username, instructor.orgnization)
        Organization org = instructor.organization

        DTEAdmissionGRNOgenrationService div = new DTEAdmissionGRNOgenrationService()
        def username = 'test'
        def ip = '1.1.1.1'
        def year = Year.findById(1)
        def ay = AcademicYear.findById(20)
        def sem = Semester.findById(1)

        def programtype = ProgramType.findByName('BTech')
        def program = Program.findAllByProgramtype(programtype)
        def learnerdivision = LearnerDivision.findAllByOrganization(org)
        def learnerlist = Learner.createCriteria().list(){
            'in'('organization', org)
            and{
                'in'('program', program)
                'in'('current_year', year)
                not{
                    'in'('id', learnerdivision?.learner?.id)
                }
            }
        }

        def i = 0
        for(learner in learnerlist) {
            def result = div.offerlearnerdivision(learner, learner?.program, ay, sem, year, username, ip)
            i++
            if(i > 200)
                break;
        }

        render "OK"
    }




    /// Bill Desk Payment Gateway

    def testbilldeskpayment(){
        Login login_inst=Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        instructor=Instructor.findByUidAndOrganization(login_inst.username, instructor.organization)
        Organization org = instructor.organization
        [org : org]
    }

    //Checksum  Test Account
    def getChecksumTest(){
        println("params : :"+params)
        def str = params.token;
        def checksum = PaymentGateWayService.hashValue(str, 'G3eAmyVkAzKp8jFq0fqPEqxF4agynvtJ');
        checksum = checksum.toUpperCase()
        println("checksum pay 1: "+checksum)
        render checksum
    }

    //Checksum Production Account
    def checksumproduction()
    {
        def checksum ='HZ1o7dCRgY3o'
        def str = params.token;
        String strHash  = PaymentGateWayService.checkSumSHA256(str, checksum);
        println("strHash : "+strHash)
        render strHash

    }

    def payment_response(){
        println("payment_response"+params)

        def msg = params.msg.toString()

        println("msg : "+msg)
        msg = msg.split("\\|")
        println("msg : "+msg)

        println("msg[14] :"+msg[14])

        if(msg[14] == '0300')
        {
            println("success")
        }else{
            println("failed")

        }


        [params:params]
    }


    def gsuit_account_create(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            flash.error = ""

            [i:0]
        }
    }
    def gsuit_account_password_reset(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            flash.error = ""

            [i:0]
        }
    }
    def getinfo()
    {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            Login login_inst = Login.findById(session.loginid)
            Instructor logininstructor = Instructor.findByUid(login_inst.username)

            Instructor instructor = Instructor.findByUidAndOrganization( login_inst.username, logininstructor.organization )
            Organization org = instructor.organization

            def learner1
            Person person = null

            def instructor1 = Instructor.findByEmployee_codeAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )
            if (!instructor1)
                instructor1 = Instructor.findByUidAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )

            def login1
            if (!instructor1) {
                learner1 = Learner.findByRegistration_numberAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )
                if (!learner1)
                    learner1 = Learner.findByUidAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )
                if (learner1)
                    login1 = Login.findByUsername( learner1?.uid )
                person = learner1?.person
            } else {
                login1 = Login.findByUsername( instructor1?.uid )
                person = instructor1?.person
            }
            Contact contact
            if (person == null) {
                flash.error = params.grnumber + " does not exists in the system."
            } else {
                 contact =Contact.findByPerson(person)
                println("contact "+contact)
            }
            [grno:params?.grnumber,name:person?.fullname_as_per_previous_marksheet,offemail:login1?.username,pemail:contact?.email,contact:contact?.mobile_no]
        }
    }

    def reset_email_password()
    {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            Login login_inst = Login.findById( session.loginid )
            Instructor logininstructor = Instructor.findByUid( login_inst.username )
            Instructor instructor = Instructor.findByUidAndOrganization( login_inst.username, logininstructor.organization )
            Organization org = instructor.organization

            def learner1
            Person person = null

            def instructor1 = Instructor.findByEmployee_codeAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )
            if (!instructor1)
                instructor1 = Instructor.findByUidAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )

            def login1
            if (!instructor1) {
                learner1 = Learner.findByRegistration_numberAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )
                if (!learner1)
                    learner1 = Learner.findByUidAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )
                if (learner1)
                    login1 = Login.findByUsername( learner1?.uid )
                person = learner1?.person
            } else {
                login1 = Login.findByUsername( instructor1?.uid )
                person = instructor1?.person
            }

            flash.error = ""
            if (person == null) {
                flash.error = params.grnumber + " does not exists in the system."
            } else {
                Contact contact =Contact.findByPerson(person)
                println("contact "+contact)
                if(contact==null)
                {
                    flash.error = "Contact details not Found"
                }
                 else if(contact.email==null || contact.email==''){
                    flash.error = "Contact email not valid"
                }
                else if(contact.mobile_no==null || contact.mobile_no==''){
                    flash.error = "Contact mobile not valid"
                }
                else {
                    if (org?.isgsuitapplicable) {
                        if(login1) {
                            AdminSDKDirectoryQuickstart service = new AdminSDKDirectoryQuickstart()
                            def password = InformationService.password_generation()
                        def result = service.resetemailpassword( login1?.username,password,org )
                            if (result == 2) {
                                flash.message = login1?.username+" Account not found "
                            } else if (result == 1) {
                                def msg="Your password has been reset by "+logininstructor?.person?.fullname_as_per_previous_marksheet+" Your new password is <br>"+password
//                                def msg1="Your Official Email "+login1.username+" Password Reset "+"With Password "+password
                                def msg1="Dear User password has been reset by "+logininstructor?.person?.fullname_as_per_previous_marksheet+" Your new password is "+password
                                InformationService.smsCommonService(msg1, contact?.mobile_no, org, null)
                                sendMailService.sendmailwithcss(org.establishment_email, org.establishment_email_credentials, contact?.email, "reset Email Password", msg, "")
                                flash.message = "Your Official email '"+login1?.username+"' password reset "
                            } else {
                                flash.error = "Error While Reseting Gmail password : " + login1?.username
                            }
                        }else{
                            flash.error = "login Error While Reseting Gmail Account : " + login1?.username
                        }
                    }else{
                        flash.error = "Gsuit Is Not Applicable To Current Oraganization !!!"
                    }
                }
            }
            [org: org]
        }

    }

    def create_email_gsuit(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            Login login_inst = Login.findById( session.loginid )
            Instructor instructor = Instructor.findByUid( login_inst.username )
            instructor = Instructor.findByUidAndOrganization( login_inst.username, instructor.organization )
            Organization org = instructor.organization

            def learner1
            Person person = null

            def inst = false
            def instructor1 = Instructor.findByEmployee_codeAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )
            if (!instructor1) {
                instructor1 = Instructor.findByUidAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )
                if(instructor1)
                    inst = true
            }else {
                inst = true
            }

            def login1
            if (!instructor1) {
                learner1 = Learner.findByRegistration_numberAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )
                if (!learner1)
                    learner1 = Learner.findByUidAndOrganization( params?.grnumber.replaceAll( " ", "" ), org )
                if (learner1)
                    login1 = Login.findByUsername( learner1?.uid )
                person = learner1?.person
            } else {
                login1 = Login.findByUsername( instructor1?.uid )
                person = instructor1?.person
            }


            flash.error = ""
            if (person == null) {
                flash.error = params.grnumber + " does not exists in the system."
            } else {
                if (org?.isgsuitapplicable) {
                    if(login1) {

                        def email = login1?.username?.split('@')
                        def gsuitemail = login1?.username
                        def updateusername = false
                        println("email[1] : "+email[1])
                        if("@"+email[1] != org?.gsuit_org_unit){
                            if(inst == true)
                                gsuitemail = DTEAdmissionGRNOgenrationService.facultygmail(org, person?.firstName, person?.lastName, )
                            else
                                gsuitemail = DTEAdmissionGRNOgenrationService.emailgeration(org, person?.firstName, person?.lastName, null, person?.grno)
                            updateusername = true
                        }

                        if(gsuitemail) {
                            if(updateusername) {
                                login1.username = gsuitemail
                                login1.updation_date = new java.util.Date()
                                login1.updation_ip_address = request.getRemoteAddr()
                                login1.save( flush: true, failOnError: true )

                                if (inst == true) {
                                    instructor1.uid = gsuitemail
                                    instructor1.updation_date = new java.util.Date()
                                    instructor1.updation_ip_address = request.getRemoteAddr()
                                    instructor1.save( flush: true, failOnError: true )
                                } else {
                                    learner1.uid = gsuitemail
                                    learner1.updation_date = new java.util.Date()
                                    learner1.updation_ip_address = request.getRemoteAddr()
                                    learner1.save( flush: true, failOnError: true )
                                }

                                person.email = gsuitemail
                                person.updation_date = new java.util.Date()
                                person.updation_ip_address = request.getRemoteAddr()
                                person.save( flush: true, failOnError: true )
                            }

                            AdminSDKDirectoryQuickstart service = new AdminSDKDirectoryQuickstart()
                            def password = login1?.password +"@123"
                            def result = service.creategmailaccount( person?.firstName, person?.lastName, gsuitemail, password, org )
                            println("result  :"+result)
                            if (result == 2) {
                                flash.message = "Gmail With '" + login1?.username + "' Already Present...!"
                            } else if (result == 1) {
                                flash.message = "Gmail With '" + login1?.username + "' Created Successfully With Password as ERP_passwor@123"
                            } else {
                                flash.error = "Error While Creating Gmail Account : " + login1?.username
                            }
                        }else{
                            flash.error = "Error While Creating Gmail Account : " + login1?.username
                        }
                    }else{
                        flash.error = "Error While Creating Gmail Account : " + login1?.username
                    }
                }else{
                    flash.error = "Gsuit Is Not Applicable To Current Oraganization !!!"
                }
            }

            [org: org]
        }
    }


    def paytem_payment_gateway_demo(){
        JSONObject paytmParams = new JSONObject();
        def order_no = 'Komal_9875090'
        JSONObject body = new JSONObject();
        body.put("requestType", "Payment");
        body.put("mid", "Pimpri54854078391381");
        body.put("websiteName", "WEBSTAGING");
        body.put("orderId", order_no);
        body.put("callbackUrl", "http://localhost:8080/login/paytem_payment_gateway_demo_responce");

        JSONObject txnAmount = new JSONObject();
        txnAmount.put("value", "1.00");
        txnAmount.put("currency", "INR");

        JSONObject userInfo = new JSONObject();
        userInfo.put("custId", "CUST_001");
        body.put("txnAmount", txnAmount);
        body.put("userInfo", userInfo);

        String checksum = PaytmChecksum.generateSignature(body.toString(), "3seJivyC3fu6A#Zx");

        JSONObject head = new JSONObject();
        head.put("signature", checksum);

        paytmParams.put("body", body);
        paytmParams.put("head", head);

        String post_data = paytmParams.toString();

        URL url = new URL("https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid=Pimpri54854078391381&orderId="+order_no);

        def txnToken = ""
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
            requestWriter.writeBytes(post_data);
            requestWriter.close();
            String responseData = "";
            InputStream is = connection.getInputStream();
            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
            if ((responseData = responseReader.readLine()) != null) {
                println("Response: " + responseData);
                def data = JSON.parse(responseData)
                if(data?.body?.resultInfo?.resultCode == '0000')
                    txnToken = data?.body?.txnToken
            }
            responseReader.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        [txnToken : txnToken, order_no:order_no]
    }

    def paytem_payment_gateway_demo_responce(){
        println("params :  "+params)
    }

    def paytemchecksum(def order_id){
        /* initialize an hash */
        TreeMap<String, String> params = new TreeMap<String, String>();
        params.put("MID", "Pimpri54854078391381");
        params.put("ORDERID", order_id);
        /**
         * Generate checksum by parameters we have
         * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
         */
        String paytmChecksum = PaytmChecksum.generateSignature(params, "3seJivyC3fu6A#Zx");
        println("generateSignature Returns: " + paytmChecksum);

//        validatepaytemchecksum(order_id, paytmChecksum)


        /* initialize JSON String */
        String body = "{\"mid\":\"Pimpri54854078391381\",\"orderId\":\""+order_id+"\"}";

        /**
         * Generate checksum by parameters we have in body
         * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
         */
        paytmChecksum = PaytmChecksum.generateSignature(body, "3seJivyC3fu6A#Zx");
        println("generateSignature Returns: " + paytmChecksum);

        return  paytmChecksum
//        validatepaytemchecksum(order_id, paytmChecksum)
    }

    def validatepaytemchecksum(def order_id, def paytmChecksum){

        /* initialize an hash */
        TreeMap<String, String> params = new TreeMap<String, String>();
        params.put("MID", "Pimpri54854078391381");
        params.put("ORDERID", order_id);
        /**
         * Generate checksum by parameters we have
         * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
         */
        boolean verifySignature = PaytmChecksum.verifySignature(params, "3seJivyC3fu6A#Zx", paytmChecksum);
        println("verifySignature Returns: " + verifySignature);




        /* initialize JSON String */
        String body = "{\"mid\":\"Pimpri54854078391381\",\"orderId\":\""+order_id+"\"}";

        /**
         * Generate checksum by parameters we have in body
         * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
         */
        verifySignature = PaytmChecksum.verifySignature(body, "3seJivyC3fu6A#Zx", paytmChecksum);
        println("verifySignature Returns: " + verifySignature);
    }
}
