package volp
import grails.converters.JSON
import groovy.json.JsonSlurper
import org.apache.http.HttpResponse
import org.apache.http.HttpStatus
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.ietf.jgss.GSSException
import org.json.JSONArray
import org.json.JSONObject

import javax.mail.Message
import javax.mail.PasswordAuthentication
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage
import java.security.SecureRandom
class AppLoginController
{
    def PasswordEncryptionService
    SendMailService SendMailService
    //http://192.168.1.123:8080/appLogin/login?username=10921&password=123

    def getlogo(){
        println("I am in Get Logo ::")
        HashMap hm = new HashMap()
        def loginlogo = Loginlogo.list()[0]
        def image
        def text_color = "#F39C12"
        def button_color = "#F39C12"
        if(loginlogo) {
            image = loginlogo?.logo_path + loginlogo?.logo_name
            text_color = loginlogo?.text_color
            button_color = loginlogo?.button_color
        }
        hm.put("msg","200")
        hm.put("logo", image)
        //println("LOgo >>"+ image)
        hm.put("button_color", button_color)
        hm.put("text_color", text_color)
        render hm as JSON
    }

    def login() {
        println(params)
        String username=params.username.replaceAll(" ", "")
        String password=params.password
        Login login=Login.findByUsernameAndPassword(username, password)
        HashMap hm = new HashMap()
        if(login==null) {
            hm.put("status","fail")
            hm.put("msg","Invalid Username and Password")
        } else {
            if(login.isloginblocked==true){
                hm.put("status","fail")
                hm.put("msg","Sorry your account is BLOCKED!!!")
            } else {
                //This is successful login
                hm.put("status","pass")
                hm.put("loginid",login.id)
                Random rand = new Random();
                int x = rand.nextInt(100);
                String token = PasswordEncryptionService.encrypwd(x.toString())
                login.access_token = token
                login.save(failOnError:true,flush:true)
                def learner = Learner.findByUid(login.username)
                def inst = Instructor.findByUid(login.username)
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSFolderPath awsFolderPath = AWSFolderPath.findById(5)
                AWSBucketService awsBucketService = new AWSBucketService()
                if(learner!=null) {
                    hm.put("name",learner?.person?.fullname_as_per_previous_marksheet)
                    hm.put("organizatioon_id",learner?.organization?.id)
                    if(learner?.person?.firstName != null || learner?.person?.lastName != null)
                        hm.put("flname",learner?.person?.firstName+" "+learner?.person?.lastName)
                    else
                        hm.put("flname","NA")
                    Studentidcard studentidcard = Studentidcard.findByLearnerAndOrganization(learner, learner.organization)
                    if(studentidcard) {
                        if(studentidcard.icardPath && studentidcard.icardPhotoFile) {
                            def path = awsFolderPath.path + studentidcard.icardPath + studentidcard.icardPhotoFile
                            def url = awsBucketService.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
                            hm.put("profilephoto", url)
                        } else {
                            hm.put("profilephoto", "NA")
                        }
                    } else {
                        //if(facultyidcard.icardPath && facultyidcard.icardPhotoFile) {
                        hm.put("profilephoto", "NA")
                        //}
                    }
                } else if(inst!=null) {
                    //only for instructor
                    RoleType roleType = RoleType.findByTypeAndOrganization("Academics",inst.organization)
                    ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndOrganizationAndIsActive(roleType,inst.organization,true)
                    HashMap ay_map = new HashMap()
                    ay_map.put("cay",aay?.academicyear?.ay)
                    ay_map.put("cay_id",aay?.academicyear?.id)
                    hm.put("c_ay",ay_map)

                    HashMap sem_map = new HashMap()
                    sem_map.put("csem",aay?.semester?.sem)
                    sem_map.put("csem_id",aay?.semester?.id)
                    hm.put("c_sem",sem_map)

                    HashMap dept_map = new HashMap()
                    dept_map.put("department_id",inst?.department?.id)
                    dept_map.put("department",inst?.department?.name)
                    dept_map.put("department_abbrivation",inst?.department?.abbrivation)
                    hm.put("department",dept_map)

                    hm.put("aylist",AcademicYear.list().sort{it.ay}.reverse().ay)
                    hm.put("semlist",Semester.findAllByOrganization(inst.organization).sem)

                    // hm.put("instructor",inst.toJson())

                    HashMap inst_map = new HashMap()
                    inst_map.put("code",inst?.employee_code)
                    inst_map.put("email",inst?.department?.name)
                    inst_map.put("name",inst?.person?.fullname_as_per_previous_marksheet)
                    if(inst?.person?.firstName != null || inst?.person?.lastName != null)
                        inst_map.put("flname",inst?.person?.firstName+" "+inst?.person?.lastName)
                    else
                        inst_map.put("flname","NA")

                    HashMap orgMap = new HashMap()
                    orgMap.put("organization_id",inst.organization?.id)
                    orgMap.put("organization_name",inst.organization?.organization_name)
                    orgMap.put("organization_display_name",inst.organization?.display_name)
                    orgMap.put("organization_organization_detailed_name",inst.organization?.organization_detailed_name)
                    hm.put("organization", orgMap)

                    Facultyidcard facultyidcard = Facultyidcard.findByInstructorAndOrganization(inst, inst.organization)
                    if(facultyidcard) {
                        if(facultyidcard.icardPath && facultyidcard.icardPhotoFile) {
                            def path = awsFolderPath.path + facultyidcard.icardPath + facultyidcard.icardPhotoFile
                            def url = awsBucketService.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
                            inst_map.put("profilephoto", url)
                        } else {
                            inst_map.put("profilephoto", "https://www.kindpng.com/picc/m/252-2524695_dummy-profile-image-jpg-hd-png-download.png")
                        }
                    } else {
                        //if(facultyidcard.icardPath && facultyidcard.icardPhotoFile) {
                        inst_map.put("profilephoto", "https://www.kindpng.com/picc/m/252-2524695_dummy-profile-image-jpg-hd-png-download.png")
                        //}
                    }
                    hm.put("instructor",inst_map)
                } else{
                    hm.put("name","NA")
                    hm.put("flname","NA")
                    hm.put("profilephoto","NA")
                }
                hm.put("access_token",login.access_token)
            }
        }
        println("hm :: " + hm)
        render hm as JSON
        return
    }

    def loginvue() {
        String body =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def username =  dataresponse["uid"]
        def password =  dataresponse["pass"]
        username = username.replaceAll(" ", "")

        println("username : "+username)
        Login login=Login.findByUsernameAndPassword(username,password)

        HashMap hm = new HashMap()
        if(login==null) {
            hm.put("status","fail")
            hm.put("msg","Please check username/password")
        } else {
            if(login.isloginblocked==true) {
                hm.put("status","fail")
                hm.put("msg","Sorry user blocked!!!")
            } else {
                hm.put('msg', "200")
                hm.put("status","pass")
                hm.put('uid', login.username)
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                //hm.put("awspath", awsBucket.aws_path)

                hm.put("loginid",login.id)
                Random rand = new Random();
                int x = rand.nextInt(100);
                String access_token = PasswordEncryptionService.encrypwd(x.toString())
                login.access_token = access_token
                // --------------------------- Token Creation Code ---------------------------
                final SecureRandom secureRandom = new SecureRandom(); //threadsafe
                final Base64.Encoder base64Encoder = Base64.getUrlEncoder(); //threadsafe
                byte[] randomBytes = new byte[128];
                secureRandom.nextBytes(randomBytes);
                String token =  base64Encoder.encodeToString(randomBytes);
                println("token :: " + token)
                login.token = token
                login.token_creation_date = new Date()
                login.token_expiry_date = new Date() + 7
                // ---------------------------

                login.save(failOnError:true,flush:true)
                def learner = Learner.findByUid(login.username)
                def inst = Instructor.findByUid(login.username)
                AWSFolderPath awsFolderPath = AWSFolderPath.findById(5)
                AWSBucketService awsBucketService = new AWSBucketService()
                if(learner != null) {
                    hm.put("orgid", learner?.organization?.id)
                    hm.put("name", learner?.person?.fullname_as_per_previous_marksheet)
                    if (learner?.person?.firstName != null || learner?.person?.lastName != null) {
                        hm.put("flname", learner?.person?.firstName + " " + learner?.person?.lastName)
                    } else {
                        hm.put("flname", "NA")
                    }
                    Studentidcard studentidcard = Studentidcard.findByLearnerAndOrganization(learner, learner.organization)
                    if(studentidcard) {
                        if(studentidcard.icardPath && studentidcard.icardPhotoFile) {
                            def path = awsFolderPath.path + studentidcard.icardPath + studentidcard.icardPhotoFile
                            def url = awsBucketService.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
                            hm.put("profilephoto", url)
                        } else {
                            hm.put("profilephoto", "NA")
                        }
                    } else {
                        //if(facultyidcard.icardPath && facultyidcard.icardPhotoFile) {
                        hm.put("profilephoto", "NA")
                        //}
                    }
                    /*
                    // check TPO role assign to student
                    def roles_list = login.roles
                    RoleType tpo_role_type = RoleType.findByTypeAndIsactiveAndOrganization('TPO', true, learner.organization)
                    if(tpo_role_type) {
                        for (role in roles_list) {
                            if(role.roletype.id == tpo_role_type.id) {
                                // API Call in TPO project for login and generate token
                                JSONObject json = new JSONObject();
                                json.put("uid", login.username);
                                json.put("pass", login.password);
                                CloseableHttpClient httpClient = HttpClientBuilder.create().build();
                                String url = 'http://localhost:8081/login/process'
                                try {
                                    HttpPost request = new HttpPost(url);
                                    StringEntity params = new StringEntity(json.toString());
                                    request.addHeader("content-type", "application/json");
                                    request.setEntity(params);
                                    // httpClient.execute(request);
                                    HttpResponse response = httpClient.execute(request);
                                    println("response :: " + response)

                                    int statusCode = response.getStatusLine().getStatusCode();
                                    if (statusCode == 200) {
                                        println("statusCode : " + statusCode)
                                        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                                        String line;
                                        String line1;

                                        int i = 0
                                        while ((line = rd.readLine()) != null) {
                                            if (i == 0) {
                                                line1 = line
                                            } else {
                                                line1 += line
                                            }
                                            i++
                                        }
                                        println("line1" + line1)
                                        JSONObject jobj = new JSONObject(line1)
                                        println("jobj :: " + jobj)
                                        println("jobj.token :: " + jobj.token)

                                        login.token = jobj.token
                                        login.token_creation_date = new Date()
                                        login.token_expiry_date = new Date() + 7
                                        login.save(failOnError:true,flush:true)
                                        hm.put("token", jobj.token)

//                                        String courselist = jobj.getString("courselist")
//                                        JSONArray jsonArray = new JSONArray(courselist)
//                                        int howmanycourse = jsonArray.length()
//
//                                        for (int j = 0; j < howmanycourse; j++) {
//                                            HashMap hm = new HashMap()
//                                            String id = jsonArray.getJSONObject(j).getString("id")
//                                            String name = jsonArray.getJSONObject(j).getString("name")
//                                            hm.put("id", id)
//                                            hm.put("name", name)
//                                            volpcourselist.add(hm)
//
//                                        }
//                                        def erpcourselist = ERPCourseOfferingBatchInstructor.findAllByInstructor(instructor)
//                                        ArrayList test = new ArrayList()
//                                        for (eachcourse in erpcourselist) {
//                                            if (!test.contains(eachcourse.erpcourseoffering.erpcourse.course_name + ":" + eachcourse.erpcourseoffering.academicyear.ay))
//
//                                            {
//                                                HashMap hm = new HashMap()
//                                                hm.put("name", eachcourse.erpcourseoffering.erpcourse.course_name + ":" + eachcourse.erpcourseoffering.academicyear.ay)
//                                                hm.put("id", eachcourse.erpcourseoffering.erpcourse.id)
//                                                hm.put("course_code", eachcourse.erpcourseoffering.erpcourse.course_code)
//                                                erpcourses.add(hm)
//                                                test.add(eachcourse.erpcourseoffering.erpcourse.course_name + ":" + eachcourse.erpcourseoffering.academicyear.ay)
//                                            }
//                                        }
//
//                                        //render msg
//                                        [ay: ay, sem: sem, aylist: aylist, semlist: semlist, erpcourselist: erpcourses, volpcourselist: volpcourselist]

                                    } else if (statusCode == 500) {
                                        println("else part 500")
//                                        flash.message("Syllabus Cannot be Synch To VOLP(Status Code 500 From VOLP Server)!")
//                                        [ay: ay, sem: sem, aylist: aylist, semlist: semlist, erpcourselist: erpcourses, volpcourselist: volpcourselist]
                                    } else {
                                        println("else part")
//                                        flash.message("server side Error!")
//                                        [ay: ay, sem: sem, aylist: aylist, semlist: semlist, erpcourselist: erpcourses, volpcourselist: volpcourselist]

                                        //return
                                    }
                                } catch (Exception ex) {
                                    // handle exception here
                                } finally {
                                    httpClient.close();
                                }
                            }
                        }
                    }
                    */
                } else if(inst!=null) {
                    hm.put("orgid", inst?.organization?.id)
                    hm.put("name",inst?.person?.fullname_as_per_previous_marksheet)
                    if(inst?.person?.firstName != null || inst?.person?.lastName != null)
                        hm.put("flname",inst?.person?.firstName+" "+inst?.person?.lastName)
                    else
                        hm.put("flname","NA")
                    //only for instructor
                    RoleType roleType = RoleType.findByTypeAndOrganization("Academics",inst.organization)
                    ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndOrganizationAndIsActive(roleType,inst.organization,true)
                    hm.put("cay",aay?.academicyear.ay)
                    hm.put("csem",aay?.semester.sem)
                    hm.put("aylist",AcademicYear.list().sort{it.ay}.reverse().ay)
                    hm.put("semlist",Semester.findAllByOrganization(inst.organization).sem)
                    hm.put("instructor",inst.toJson())
                    Facultyidcard facultyidcard = Facultyidcard.findByInstructorAndOrganization(inst, inst.organization)
                    if(facultyidcard) {
                        if(facultyidcard.icardPath && facultyidcard.icardPhotoFile) {
                            def path = awsFolderPath.path + facultyidcard.icardPath + facultyidcard.icardPhotoFile
                            def url = awsBucketService.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
                            hm.put("profilephoto", url)
                        } else {
                            hm.put("profilephoto", "NA")
                        }
                    } else {
                        //if(facultyidcard.icardPath && facultyidcard.icardPhotoFile) {
                        hm.put("profilephoto", "NA")
                        //}
                    }
                }
                else{
                    hm.put("name","NA")
                    hm.put("flname","NA")
                    hm.put("profilephoto","NA")
                }
                hm.put("access_token",login.access_token)
            }
        }
        render hm as JSON
        return
    }

    def getlinks() {
        def loginid = request.getHeader("EPS-loginid")
        def orgid = request.getHeader("EPS-orgid")
        def organization = Organization.findById(orgid)
        HashMap hm = new HashMap()
        ArrayList Role_links=new ArrayList()
        Login log = Login.findById(loginid)
        def roles = log.roles
        def ut=login.usertype
        def links = RoleLink.findAllByIsquicklinkAndRoleInListAndIsrolelinkactiveAndOrganization(true, roles, true, organization).sort{it.sort_order}
        hm.put("links",links.link_name)
        for(l in links)
        {
            HashMap temp=new HashMap()
            temp.put('id',l?.id)
            temp.put('link_name',l?.link_displayname)
            temp.put('link',l?.link_name)
            temp.put('icon','')
            Role_links.add(temp)
        }
        hm.put("Role_links",Role_links)
        render hm as JSON
        return
    }

    def getUserRoleLinks() {
        def loginid = request.getHeader("EPS-loginid")
        if(loginid) {
            def orgid = request.getHeader("EPS-orgid")
            def organization = Organization.findById(orgid)
            HashMap hm = new HashMap()
            Login log = Login.findById(loginid)
            def roles = log.roles
            def ut = log.usertype

            def links = RoleLink.findAllByIsquicklinkAndRoleInListAndIsrolelinkactiveAndOrganization(true, roles, true, organization).sort {
                it.sort_order
            }
            ArrayList Role_links = new ArrayList()
            for (l in links) {
                HashMap temp = new HashMap()
                temp.put('id', l?.id)
                temp.put('link_name', l?.link_displayname)
                temp.put('link', l?.link_name)
                temp.put('icon', '')
                Role_links.add(temp)
            }
            hm.put("quick_links", Role_links)

            def alllinks = RoleLink.findAllByRoleInListAndIsrolelinkactiveAndOrganization(roles, true, organization).sort {
                it.sort_order
            }
            ArrayList All_Role_links = new ArrayList()
            for (l in alllinks) {
                HashMap temp = new HashMap()
                temp.put('id', l?.id)
                temp.put('link_name', l?.link_displayname)
                temp.put('link', l?.link_name)
                temp.put('icon', '')
                All_Role_links.add(temp)
            }
            hm.put('msg', "200")
            hm.put("all_role_links", All_Role_links)
            render hm as JSON
            return
        }else{
            HashMap hm = new HashMap()
            hm.put("msg", 'Logged out')
            render hm as JSON
            return
        }
    }

    def getrolelinks() {
        println("getrolelinks :: " + params)
        try {
            def loginid = request.getHeader("EPS-loginid")
            //println("loginid :: " + loginid)
            def orgid = request.getHeader("EPS-orgid")
            Login login = Login.findById(loginid)
            Organization org = Organization.findById(orgid)

            String url = request.getHeader("router-path")
            HashMap hm = new HashMap()
            def smarttemplatesidebarlist = []
            def smartrolelist = login.roles
            smartrolelist = smartrolelist.sort { it.sort_order }
            def roletypelist2 = []
            for (role in smartrolelist) {
                RoleType roletype = role.roletype
                roletypelist2.add(roletype)
            }
            def smartroletypelist = Role.createCriteria().list() {
                projections {
                    distinct("roletype")
                }
                and {
                    'in'('roletype', roletypelist2)
                    'in'('organization', org)
                }
            }.sort { it.sort_order }
            def search_link_list = []
            for (roletype in smartroletypelist) {
                HashMap role_type_map = new HashMap()
                role_type_map.put("role_type_display_name", roletype.type_displayname)
                def sublist = []
                for (role in smartrolelist) {
                    HashMap role_map = new HashMap()
                    if (role.roletype.id == roletype.id) {
                        def subsublist = []
                        def smartrolelinks = RoleLink.createCriteria().list() {
                            'in'('organization', org)
                            and {
                                'in'('role', role)
//                                'in'('roletype', roletype)
//                                'in'('usertype', login.usertype)
                                'in'('isrolelinkactive', true)
                                isNotNull("vue_js_name")
                            }
                        }.sort { it.sort_order }
                        if(smartrolelinks.size() > 0) {
                            for (link in smartrolelinks) {
                                HashMap link_map = new HashMap()
                                link_map.put("link_displayname", link.link_displayname)
                                link_map.put("link_vue_js_name", link.vue_js_name)
                                search_link_list.add(link_map)
                                subsublist.add(link_map)
                            }
                        } else {
                            continue
                        }
                        role_map.put("role_displayname", role.role_displayname)
                        sublist.add(role: role_map, smartrolelinks: subsublist)
                    }
                    role_type_map.put("roles", sublist)
                }
                smarttemplatesidebarlist.add(role_type_map)
            }
            //println("smarttemplatesidebarlist :: " + smarttemplatesidebarlist)
            //println("search_link_list :: " + search_link_list)

            def link_list = []
            for(link in smarttemplatesidebarlist) {
                HashMap obj = new HashMap()
//                println("link 1:: " + link)
//                println("link 2:: " + link.roles)
//                println("link 3:: " + link.role_type_display_name)

                if(link.roles.size() > 0) {
                    obj.put("role_type",link.role_type_display_name)
                    obj.put("roles",link.roles)
                    link_list.add(obj)
                }
            }



//              for (roletype in smartroletypelist) {
//                HashMap role_type_map = new HashMap()
//                role_type_map.put("role_type_display_name", roletype.type_displayname)
//                //def list = []
//                //list.add(roletype)
//                def sublist = []
//                for (role in smartrolelist) {
//                    HashMap role_map = new HashMap()
//                    if (role.roletype.id == roletype.id) {
//                        def subsublist = []
//
//                        def smartrolelinks = RoleLink.createCriteria().list() {
//                            'in'('organization', org)
//                            and {
//                                'in'('role', role)
//                                'in'('isrolelinkactive', true)
//                            }
//                        }.sort { it.sort_order }
//
//                        for(link in smartrolelinks) {
//                            HashMap link_map = new HashMap()
//                            link_map.put("link_displayname",link.link_displayname)
//                            link_map.put("link_vue_js_name",link.link_displayname)
//                            search_link_list.add(link_map)
//                            subsublist.add(link_map)
//                        }
//                        role_map.put("role_displayname", role.role_displayname)
//                        sublist.add(role: role_map, smartrolelinks: subsublist)
////                    sublist.add(smartrolelinks)
//                    }
//                    role_type_map.put("roles", sublist)
//                    //list.add(sublist)
//                }
//                smarttemplatesidebarlist.add(role_type_map)
//            }
            //println("smarttemplatesidebarlist :: " + smarttemplatesidebarlist)

            hm.put("link_list",link_list)
            hm.put("search_link_list",search_link_list)
            hm.put("status","SUCCESS")

//            ApplicationType at=ApplicationType.findByApplication_type("ERP")
//            HashSet roletype = []
//            ArrayList app_modules = new ArrayList()
//            Login log = Login.findById(loginid)
//            def roles = log.roles
//            for (role in roles) {
//                if (role.roletype.applicationtype == at)
//                    roletype.add(role.roletype)
//            }
//            ArrayList list = new ArrayList()
//            for (rt in roletype) {
//                HashMap x = new HashMap()
//                x.put('name', rt?.type_displayname)
//                x.put('icon', "")
//                x.put('model', false)
//                list.add(x)
//                app_modules.add(rt.type_displayname)
//            }
//            hm.put("links", list)
//            hm.put("app_modules", app_modules)
            render hm as JSON
            return
        } catch(Exception e) {
            HashMap hm = new HashMap()
            hm.put("msg", 'Logged out')
            render hm as JSON
            return
        }
    }

    def checkusername() {
        println("checkusername :: " + params)
        String body =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def username =  dataresponse["uid"]
        username = username.replaceAll(" ", "")

        println("username :: "+username)

        HashMap hm
        Login login = Login.findByUsernameAndIsloginblocked(username, false)
        if(!login){
            def learner
            if(username?.contains('@'))
            {
                username = username?.split('@')
                def logincode = ""
                if(username?.size()>1)
                    logincode = "@"+username[1]
                username = username[0]
                def organization = Organization.findAllByLogincodeIlike(logincode)
                if(organization?.size() > 1){
                    hm = new HashMap()
                    hm.put("status","FAILED")
                    hm.put("msg","Please use username !")
                    render hm as JSON
                    return
                }else if(organization){
                    learner = Learner.findAllByRegistration_numberAndOrganization(username?.trim(), organization)
                    if(learner?.size() > 1){
                        hm = new HashMap()
                        hm.put("status","FAILED")
                        hm.put("msg","Please use username !")
                        render hm as JSON
                        return
                    }else if(learner){
                        login = Login.findByUsernameAndIsloginblocked(learner?.uid, false)
                    }
                }
            }else {
                learner = Learner.findAllByRegistration_number(username)
                if (learner?.size() > 1) {
                    hm = new HashMap()
                    hm.put("status", "FAILED")
                    hm.put("msg", "Please use prn@org_code or username !")
                    render hm as JSON
                    return
                } else if (learner) {
                    login = Login.findByUsernameAndIsloginblocked(learner?.uid, false)
                }
            }
        }


        if(login) {
            hm = new HashMap()

            //Learner learner = Learner.findByUidAndIsadmissioncancelled(login.username, false)
            Learner learner = Learner.findByUid(login.username)
            if(learner) {
                hm.put("user_name", learner.person.fullname_as_per_previous_marksheet)
                hm.put("grno", login.username)
                hm.put("user_type", 'Learner')
            } else {
                Instructor inst = Instructor.findByUidAndIscurrentlyworking(login.username, true)
                if(inst) {
                    def inst_name = inst.person.firstName + " " + inst.person.lastName
                    hm.put("user_name", inst_name)
                    hm.put("grno", login.username)
                    hm.put("user_type", 'Instructor')
//                    println("test")
//                    redirect(url: "http://localhost:8080/login/erplogin?username="+ username)
                    //redirect(controller:"login", action:"erplogin", params:[username:username])
//                    return
                } else {
                    hm = new HashMap()
                    hm.put("status","FAILED")
                    hm.put("msg","Incorrect Username...")
                    render hm as JSON
                    return
                }
            }
            hm.put("status","SUCCESS")
            render hm as JSON
            return
        } else {
            hm = new HashMap()
            hm.put("status","FAILED")
            hm.put("msg","Incorrect Username...")
            render hm as JSON
            return
        }
    }

    def verifyemail(){

        String body1 =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body1)
        //println "Dataresponce "+dataresponse
        HashMap hm = new HashMap()
        String email=dataresponse.email
        email = email.replaceAll(" ", "")
        println(email)
        Login login = Login.findByUsername(email)
        if(!login) {
            hm.put("msg","Please enter correct email id.")
            hm.put("status", "401")
            render hm as JSON
            return
        }

        //Generate OTP
        java.util.Random random=new java.util.Random()
        String otp=""
        int min=0,max=0,n
        for(int i=1;i<=6;i++)
        {
            if(i==1)
                min=1
            else
                min=0
            max=9
            n=random.nextInt(max)+min
            otp=otp+n
        }

        Learner learner = Learner.findByUid(email)
        println("learner :: " + learner)
        Instructor inst = Instructor.findByUid(email)
        println("inst  :: " + inst )

        if(learner == null && inst == null) {
            hm.put("msg","We not found your details, please contact with ERP coordinator.")
            hm.put("status", "401")
            render hm as JSON
            return
        } else {
            session.otpemail = email
            //Registred..Send OTP for Password Reset

            Otp otpobj=Otp.findByEmail(email)
            Date date=new java.util.Date()
            if(otpobj==null) {
                //insert otp
                otpobj=new Otp()
                otpobj.email=email
                otpobj.otp=otp
                otpobj.otpgenerationtime=date
                otpobj.save(failOnError:true,flush:true)
            } else {
                //update otp
                otpobj.otp=otp
                otpobj.otpgenerationtime=date
                otpobj.save(failOnError:true,flush:true)
            }
            println("1Now sending mail of otp...."+otp)
            try {
                String msg = "Your OTP is " + otp + "\n"+"Thanks, EduPlusCampus Team.."
                def establishment_email
                def establishment_email_credentials
                def username
                def organization_code

                if(learner) {
                    establishment_email = learner?.organization?.establishment_email
                    establishment_email_credentials = learner?.organization?.establishment_email_credentials
                    username = login?.username
                    organization_code = learner?.organization?.organization_code + "Forgot Password Otp"
                } else if(inst) {
                    establishment_email = inst?.organization?.establishment_email
                    establishment_email_credentials = inst?.organization?.establishment_email_credentials
                    username = login?.username
                    organization_code = inst?.organization?.organization_code + "Forgot Password Otp"
                }

                boolean value = SendMailService.sendmailwithcss(establishment_email, establishment_email_credentials, username, organization_code, msg, "", "")
                if(value) {
                    hm.put("msg", "Otp Send to your Official Email.")
                    hm.put("status", "200")
                    render hm as JSON
                    return
                } else {
                    hm.put("msg","Email send failed")
                    hm.put("status","500")
                    render hm as JSON
                    return
                }
            } catch (Exception e){
                println e
                hm.put("msg","Email send failed")
                hm.put("status","500")
                render hm as JSON
                return
            }
        }
    }

    def processemail() {
        println("in processemail"+params)
        [params:params]
    }

    def verifyotp() {
        String body1 =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body1)
        //println "Dataresponce "+dataresponse
       //println("in verifyotp:"+session)
       // String email=session.otpemail
        String email=dataresponse.email

        String otp=dataresponse.otp
        //println("Email:"+email+" and otp:"+otp)
        Otp otpobj=Otp.findByEmailAndOtp(email,otp)
        HashMap hm = new HashMap()
        if(otpobj!=null)
        {
            Date otpgenerationtime=otpobj.otpgenerationtime
            Date currenttime=new java.util.Date()
            //println("OTP match:otpgenerationtime:"+otpgenerationtime+","+"currenttime:"+currenttime)
            long diff = currenttime.getTime() - otpgenerationtime.getTime() ;
            long diffInMinutes = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(diff);
            if(diffInMinutes<=30)
            {
//                redirect action:"passwordreset"
                hm.put("msg","200")//otp verified
                render hm as JSON
                return
            }
            else
            {
                hm.put("msg","401")//otp expire
                render hm as JSON
                return
            }
        }
        else
        {
            hm.put("msg","404")//otp not match
            render hm as JSON
            return
        }
    }

    def savechangepassword() {
        println("savechangepassword")
        String body1 =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body1)
        //println "Dataresponce "+dataresponse
        String newpassword = dataresponse.newpassword
        String confirmpassword = dataresponse.confirmpassword
        Login login = null
        HashMap hm = new HashMap()
        try{
//        if(session.apptype=="VOLP")
//        {
//            login = Login.findByUsername(session.uid)
//        }
//        else if(session.apptype=="ERP")
//        {
//            login = Login.findById(session.loginid)
//        }
             login = Login.findByUsername(dataresponse.email.replaceAll(" ", ""))
            //println("login::" + login)
            if (newpassword.equals(confirmpassword)) {
                login.password = newpassword
                login.save(failOnError: true, flush: true)
    //            /flash.message = "Password Changed Successfully....."
                hm.put("msg","200")//password change successfully
                render hm as JSON
                    return
            } else {
                hm.put("msg","401")//password not match
                render hm as JSON
                return
            }
        } catch (Exception e) {
            println e
            hm.put("msg","Error")//error
            render hm as JSON
            return
        }
//        if(session.apptype=="ERP")
//            redirect(controller: "Login", action: "erplogin")
//        else if(session.apptype=="VOLP")
//            redirect(controller: "Login", action: "login")
        return
    }

    def passwordreset() {
        println("I am in password reset...")
        String email=session.otpemail
        [email:email]
    }

    def saveresetpassword() {

        def loginid = request.getHeader("EPS-loginid")
        Login login = Login.findById(loginid)
        String body=  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        //println "Dataresponce "+dataresponse
        String oldpassword = dataresponse.oldpassword
        String newpassword = dataresponse.newpassword
        String confirmpassword = dataresponse.confirmpassword
        //println("newpassword ::"+newpassword)
        //println("confirmpassword ::"+confirmpassword)

       // Login login = null
        HashMap hm = new HashMap()

        //println("login::" + login)
        if (newpassword.equals(confirmpassword)) {
            //println("newpassword"+newpassword)
            login.password = newpassword

            login.save(failOnError: true, flush: true)
            hm.put("msg","200")//password change successfully
            render hm as JSON
            return
        } else {
            hm.put("msg","400")//password not match
            render hm as JSON
            return
        }
    }

    def dashboardlearnerprofile() {
        println("I am in Learner Dashboard Profile")
        def loginid = request.getHeader("EPS-loginid")
        def orgid = request.getHeader("EPS-orgid")
        Organization org = Organization.findById(orgid)
        Login login = Login.findById(loginid)
        Learner learner = Learner.findByUid(login.username)
        AddressType permanent_addresstype = AddressType.findByType("Permanent")
        Address permanent_address = Address.findByPersonAndAddresstype(learner.person, permanent_addresstype)

        ApplicationType at = ApplicationType.findByApplication_type("ERP")
        RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "Admission", org)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true, org)
        AcademicYear currentAcademicYear = aay.academicyear

        LearnerDivision learnerDivision = LearnerDivision.findByLearnerAndAcademicyearAndOrganization(learner, currentAcademicYear, org)

        String photourl
        String path
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        AWSFolderPath afp = AWSFolderPath.findById(5)
        AWSBucketService awsBucketService1 = new AWSBucketService()
        Studentidcard idcard = Studentidcard.findByLearnerAndOrganization(learner, org)
        if(idcard) {
            if(idcard.icardPhotoFile != null && idcard.icardPath != null ) {
                String photo_path = idcard.icardPath.trim()
                println("photo_path ::"  + photo_path.trim() )
                if(photo_path.contains("cloud")) {
                    path = photo_path + idcard.icardPhotoFile
                } else {
                    path = afp.path + photo_path + idcard.icardPhotoFile
                }
                println("path :: " + path)
                photourl = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
            }
        }
        else {
            photourl = null
        }
        def contact = Contact.findByPerson(learner?.person)

        HashMap hm =new HashMap()
        hm.put("name",learner?.person?.fullname_as_per_previous_marksheet)
        if(learner?.person?.firstName != null || learner?.person?.lastName != null)
            hm.put("flname",learner?.person?.firstName+" "+learner?.person?.lastName)
        else
            hm.put("flname","NA")

        hm.put("Email",learner.uid)
        hm.put("learner",learner)
        hm.put("Mobileno",contact?.mobile_no)
        hm.put("Roll_no",learnerDivision?.rollno)
        hm.put("Sem",learnerDivision?.semester?.sem)
        hm.put("Division", learnerDivision?.divisionoffering?.division?.name)
        hm.put("Program", learner?.program?.name)
        hm.put("Ay",currentAcademicYear.ay)
        hm.put("Address",permanent_address?.address)
        hm.put("Regno",learner.registration_number)
        hm.put("photourl",photourl)
        hm.put("msg","success")
        hm.put("status","200")
        render hm as JSON
        return
    }

    def Byloginvue() {
        println "aaa"+params
        String body =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body)
        def username =  dataresponse["_3mcdef"]
        AesCryptUtil aesUtil=new AesCryptUtil("25E53969C4B3ECA173F559D620019209");
        String decResp = aesUtil.decrypt(username);
        StringTokenizer tokenizer = new StringTokenizer(decResp, "&");
        Hashtable hs=new Hashtable();
        String pair=null, pname=null, pvalue=null;
        while (tokenizer.hasMoreTokens()) {
            pair = (String)tokenizer.nextToken();
            if(pair!=null) {
                StringTokenizer strTok=new StringTokenizer(pair, "=");
                pname=""; pvalue="";
                if(strTok.hasMoreTokens()) {
                    pname=(String)strTok.nextToken();
                    if(strTok.hasMoreTokens())
                        pvalue=(String)strTok.nextToken();
                    hs.put(pname, pvalue);
                }
            }
        }
        println "hs>>"+hs
        Login login=Login.findById(hs.get("loginId"))
        HashMap hm = new HashMap()
        if(login==null) {
            hm.put("status","fail")
            hm.put("msg","Please check username/password")
        } else {
            if(login.isloginblocked==true) {
                hm.put("status","fail")
                hm.put("msg","Sorry user blocked!!!")
            } else {
                hm.put('msg', "200")
                hm.put("status","pass")
                hm.put('uid', login.username)
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                //hm.put("awspath", awsBucket.aws_path)

                hm.put("loginid",login.id)
                Random rand = new Random();
                int x = rand.nextInt(100);
                String access_token = PasswordEncryptionService.encrypwd(x.toString())
                login.access_token = access_token
                // --------------------------- Token Creation Code ---------------------------
                final SecureRandom secureRandom = new SecureRandom(); //threadsafe
                final Base64.Encoder base64Encoder = Base64.getUrlEncoder(); //threadsafe
                byte[] randomBytes = new byte[128];
                secureRandom.nextBytes(randomBytes);
                String token =  base64Encoder.encodeToString(randomBytes);
                println("token :: " + token)
                login.token = token
                login.token_creation_date = new Date()
                login.token_expiry_date = new Date() + 7
                // ---------------------------

                login.save(failOnError:true,flush:true)
                def learner = Learner.findByUid(login.username)
                def inst = Instructor.findByUid(login.username)
                AWSFolderPath awsFolderPath = AWSFolderPath.findById(5)
                AWSBucketService awsBucketService = new AWSBucketService()
                if(learner != null) {
                    hm.put("orgid", learner?.organization?.id)
                    hm.put("instweb", learner?.organization?.instructor_website)
                    hm.put("name", learner?.person?.fullname_as_per_previous_marksheet)
                    if (learner?.person?.firstName != null || learner?.person?.lastName != null) {
                        hm.put("flname", learner?.person?.firstName + " " + learner?.person?.lastName)
                    } else {
                        hm.put("flname", "NA")
                    }
                    Studentidcard studentidcard = Studentidcard.findByLearnerAndOrganization(learner, learner.organization)
                    if(studentidcard) {
                        if(studentidcard.icardPath && studentidcard.icardPhotoFile) {
                            def path = awsFolderPath.path + studentidcard.icardPath + studentidcard.icardPhotoFile
                            def url = awsBucketService.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
                            hm.put("profilephoto", url)
                        } else {
                            hm.put("profilephoto", "NA")
                        }
                    } else {
                        //if(facultyidcard.icardPath && facultyidcard.icardPhotoFile) {
                        hm.put("profilephoto", "NA")
                        //}
                    }
                } else if(inst!=null) {
                    hm.put("orgid", inst?.organization?.id)
                    hm.put("name",inst?.person?.fullname_as_per_previous_marksheet)
                    if(inst?.person?.firstName != null || inst?.person?.lastName != null)
                        hm.put("flname",inst?.person?.firstName+" "+inst?.person?.lastName)
                    else
                        hm.put("flname","NA")
                    RoleType roleType = RoleType.findByTypeAndOrganization("Academics",inst.organization)
                    ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndOrganizationAndIsActive(roleType,inst.organization,true)
                    hm.put("cay",aay?.academicyear.ay)
                    hm.put("csem",aay?.semester.sem)
                    hm.put("aylist",AcademicYear.list().sort{it.ay}.reverse().ay)
                    hm.put("semlist",Semester.findAllByOrganization(inst.organization).sem)
                    hm.put("instructor",inst.toJson())
                    Facultyidcard facultyidcard = Facultyidcard.findByInstructorAndOrganization(inst, inst.organization)
                    if(facultyidcard) {
                        if(facultyidcard.icardPath && facultyidcard.icardPhotoFile) {
                            def path = awsFolderPath.path + facultyidcard.icardPath + facultyidcard.icardPhotoFile
                            def url = awsBucketService.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
                            hm.put("profilephoto", url)
                        } else {
                            hm.put("profilephoto", "NA")
                        }
                    } else {
                        hm.put("profilephoto", "NA")
                    }
                }
                else{
                    hm.put("name","NA")
                    hm.put("flname","NA")
                    hm.put("profilephoto","NA")
                }
                hm.put("access_token",login.access_token)
            }
        }
        render hm as JSON
        return
    }

    def changepassword() {
        String body1 =  request.getReader().text
        def jsonSluper = new JsonSlurper()
        def dataresponse = jsonSluper.parseText(body1)
        String oldpassword = dataresponse.oldpassword
        String newpassword = dataresponse.newpassword
        String confirmpassword = dataresponse.confirmpassword
        String email = dataresponse.email.replaceAll(" ", "")
        Login login = null
        HashMap hm = new HashMap()
        try{
            login = Login.findByUsernameAndPassword(email, oldpassword)
            if(!login) {
                hm.put("msg","Username and Password is not correct")
                hm.put("status", "401")
                render hm as JSON
                return
            } else {
                if (newpassword.equals(confirmpassword)) {
                    login.password = newpassword
                    login.save(failOnError: true, flush: true)
                    hm.put("msg","Password changed successfully.")
                    hm.put("msg", "200")//password change successfully
                    render hm as JSON
                    return
                } else {
                    hm.put("msg","New Password and confirm Password not matched.")
                    hm.put("msg", "401")//password not match
                    render hm as JSON
                    return
                }
            }
        } catch (Exception e) {
            hm.put("msg","Password changes failed.")
            hm.put("msg","500")//error
            render hm as JSON
            return
        }
        return
    }
}
